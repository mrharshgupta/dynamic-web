import React from 'react';
import Routes from './routes/index';
import {Provider} from 'react-redux';
import configureStore from './redux/configureStore';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import firebase from 'firebase/app';
import { firebaseConfig } from './config.js';

firebase.initializeApp(firebaseConfig);
const App = () => {
  return (
    <>
    <Provider store={configureStore().store}>
    	<DndProvider backend={HTML5Backend}>
    		<Routes />
      	</DndProvider>
    </Provider>
    </>
  );
}
export default App;
