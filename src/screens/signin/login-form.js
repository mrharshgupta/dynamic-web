import React from "react";
import styles from "./styles.js";
import "../../css/style.css";
import "../../css/device.css";
import "../../css/editor.css";
import "../../css/main.css";
import "../../css/sanjana.css";
import { Link } from "react-router-dom";

const LoginForm = (props) => {
  return (
    <div className="login-box">
      <div className="welcome">
        <img src={require("../../constants/images/logo.png")} alt="logo" />
      </div>
      <div className="login-wrap">
        <div name="frmLogin" id="frmLogin">
          <ul id="wrap-login">
            <li>
              <input
                type="text"
                style={styles.loginId}
                id="storeName"
                className="text"
                value={props.storeName}
                placeholder="Enter Store Name"
                title="Store Name"
                onChange={(e) => props.setstoreName(e.target.value)}
              />
            </li>
            <li>
              <input
                type="password"
                id="userPass"
                style={styles.loginPassword}
                className="text"
                value={props.password}
                placeholder="Login Password"
                title="Login Password"
                onChange={(e) => props.setpassword(e.target.value)}
              />
            </li>
            <li>
              <button
                className="button"
                style={styles.loginButton}
                onClick={() => props.loginButton()}
              >
                Login
              </button>

              {props.error && (
                <span style={styles.wrongDetails}>{props.error}</span>
              )}
            </li>
            <li>
              <Link to="/signup" className="link" style={styles.signup}>
                Do not have an account? Signup here
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default LoginForm;
