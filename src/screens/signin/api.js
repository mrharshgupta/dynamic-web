import { apiUrls } from "../../api/helper";
import { consoleInDebugModeOnly2 } from "../../helpers/debugging";
import { loginRequest, loginSuccess, loginFailure } from "./actions.js";

export default function logIn(
  dispatch,
  storeName,
  password,
  successCallback,
  setshowLoader,
  setwrongDetails
) {
  loginRequest(dispatch);
  fetch(apiUrls.login, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      storeName: storeName,
      password: password,
    }),
  })
    .then((response) => {
      console.log(response, "response");
      return response.json();
    })
    .then(async (responseData) => {
      consoleInDebugModeOnly2("logInSuccess-responseData", responseData);
      loginSuccess(dispatch, responseData);
      if (responseData.login) {
        if (responseData.active === "1") {
          localStorage.setItem("token", responseData.token);
          localStorage.setItem("storeName", responseData.storeName);
          localStorage.setItem("userId", responseData.userId);
          localStorage.setItem("appliedTemplate", responseData.appliedTemplate);
          localStorage.setItem("userTemplateId", responseData.userTemplateId);
          dispatch({
            type: "SET_USERTEMPLATEID_AND_APPLIEDTEMPLATE",
            data: {
              userTemplateId: responseData.userTemplateId,
              appliedTemplate: responseData.appliedTemplate,
            },
          });
          successCallback(localStorage.getItem("token"));
        } else {
          setwrongDetails("Your account is not verified yet");
        }
      }
      if (responseData.login === false) {
        setwrongDetails("Wrong UserId or Password");
      }
      setshowLoader();
    })
    .catch((error) => {
      consoleInDebugModeOnly2("logInErrorWithoutApi", error.response);
      loginFailure(dispatch);
      setshowLoader()
    });
}
