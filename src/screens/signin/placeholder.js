import React from 'react';
import LinearProgress from '@material-ui/core/LinearProgress';

const Placeholder = ({loaderProgress}) => {
return(
<div style={{backgroundColor: 'rgba(0,0,0,0.3)', position: 'absolute', height: '100%', justifyContent: 'center', alignItems: 'center', width: '100%'}}>
<LinearProgress />
</div>
);
}
export default Placeholder;