import React, { useState } from "react";
import "../../css/style.css";
import "../../css/device.css";
import "../../css/editor.css";
import "../../css/main.css";
import "../../css/sanjana.css";
import { Redirect } from "react-router-dom";
import { useDispatch } from "react-redux";
import LoginForm from "./login-form.js";
import logIn from "./api.js";
import Placeholder from "./placeholder.js";
import Loader from "react-loader";

const Signin = ({ login }) => {
  const [storeName, setstoreName] = useState(undefined);
  const [token, settoken] = useState(
    localStorage.getItem("token") || undefined
  );
  const [password, setpassword] = useState(undefined);
  const [error, setError] = useState("");
  const [showLoader, setshowLoader] = useState(undefined);
  const [loaderProgress, setloaderProgress] = useState(30);
  const dispatch = useDispatch();

  if (token) {
    return <Redirect to={`/${localStorage.getItem("storeName")}/home`} />;
  }

  const loginButton = () => {
    if (storeName === undefined || password === undefined) {
      alert("Please enter Store Name and Password");
    } else {
      setshowLoader(true);
      logIn(
        dispatch,
        storeName,
        password,
        (abc) => settoken(abc),
        () => setshowLoader(false),
        (abc) => setError(abc)
      );
    }
  };
  return (
    <>
      {showLoader && <Placeholder loaderProgress={loaderProgress} />}
      <div className="login-container">
        <LoginForm
          storeName={storeName}
          password={password}
          setstoreName={setstoreName}
          setpassword={setpassword}
          loginButton={loginButton}
          setshowLoader={setshowLoader}
          error={error}
        />
      </div>
    </>
  );
};

export default Signin;
