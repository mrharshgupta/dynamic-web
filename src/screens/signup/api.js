import { apiUrls } from "../../api/helper";
import { consoleInDebugModeOnly2 } from "../../helpers/debugging";
import * as firebase from "firebase";
//import {loginRequest, loginSuccess, loginFailure} from './actions.js';
import { applyTemplate } from "../../components/main-area/design/templates/api";

export default function logIn(
  dispatch,
  email,
  password,
  storeName,
  successCallback,
  setshowLoader,
  setshowModal
) {
  fetch(apiUrls.signup, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      storeName: storeName,
      email: email,
      password: password,
    }),
  })
    .then((response) => response.json())
    .then((responseData) => {
      consoleInDebugModeOnly2("SignUpSuccess-responseData", responseData);
      if (responseData.signUp === false) {
        setshowLoader();
        alert("Store Name already in use");
      } else {
        //localStorage.setItem("token", responseData.token);
        localStorage.setItem("userId", responseData.userId);
        //localStorage.setItem("appliedTemplate", undefined);
        localStorage.setItem("storeName", storeName);
        //successCallback(localStorage.getItem("token"));

        const settings = firebase.database().ref("userSettings/settings");
        settings.on("value", (settingsSnapshot) => {
          var userSettings = settingsSnapshot.val();
          userSettings.storeSettings.general.contactInformation.storeName = storeName;
          userSettings.storeSettings.general.contactInformation.email = email;
          userSettings.storeSettings.general.seoData.homePageTitle = storeName;
          const Data = firebase
            .database()
            .ref("userSettings/data/" + responseData.userId)
            .set(userSettings);
        });
        applyTemplate(
          dispatch,
          "1",
          () => setshowLoader(),
          () => setshowModal()
        );
        // setshowLoader();
        // setshowModal();
      }
    })
    .catch((error) => {
      consoleInDebugModeOnly2("SignUpErrorWithoutApi", error);
      //loginFailure(dispatch);
    });
}
