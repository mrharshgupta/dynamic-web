import React, { useState } from "react";
import { Form, Button } from "react-bootstrap";
import { Redirect } from "react-router-dom";
import styles from "./styles";
import "../../css/style.css";
import "../../css/device.css";
import "../../css/editor.css";
import "../../css/main.css";
import "../../css/sanjana.css";
import signup from "./api";
import Loader from "react-loader";
import Placeholder from "../signin/placeholder.js";
import { useHistory } from "react-router-dom";
import SimpleModal from "../../components/modal/index";
import { useDispatch } from "react-redux";

const Signup = () => {
  let history = useHistory();
  const dispatch = useDispatch();
  const [token, settoken] = useState(
    localStorage.getItem("token") || undefined
  );
  const [phone, setphone] = useState(undefined);
  const [name, setname] = useState(undefined);
  const [storeName, setstoreName] = useState("");
  const [storeNameError, setStoreNameError] = useState("");
  const [email, setemail] = useState("");
  const [emailError, setEmailError] = useState("");
  const [gender, setgender] = useState(undefined);
  const [dob, setdob] = useState(undefined);
  const [password, setpassword] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const [repassword, setrepassword] = useState("");
  const [showLoader, setshowLoader] = useState(undefined);
  const [showModal, setshowModal] = useState(false);
  const [showModalContent, setshowModalContent] = useState(
    "We have sent a verification link to your email please use that link to verify your account and enjoy our services."
  );

  if (token) {
    return <Redirect to={`/${localStorage.getItem("storeName")}/home`} />;
  }

  const sign_up = () => {
    const emailValidation = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const passwordValidation = /^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{8,}$/;
    let isEmailValidated = false;
    let isStoreNameValidated = false;
    let isPasswordValidated = false;
    setshowLoader(true);
    if (storeName.length > 0) {
      isStoreNameValidated = true;
    } else {
      setStoreNameError("Please enter a valid storeName");
      setshowLoader(false);
    }

    if (emailValidation.test(email)) {
      isEmailValidated = true;
    } else {
      setEmailError("Please enter a valid email");
      setshowLoader(false);
    }
    if (passwordValidation.test(password)) {
      isPasswordValidated = true;
    } else {
      setPasswordError(
        "Password must contain atleast 8 characters, one lowercase letter, one uppercase letter and special character"
      );
      setshowLoader(false);
    }

    if (isEmailValidated && isPasswordValidated && isStoreNameValidated) {
      console.log("nanana");
      signup(
        dispatch,
        email,
        password,
        storeName,
        (abc) => settoken(abc),
        () => setshowLoader(false),
        () => setshowModal(true)
      );
    }
  };

  const enterPassword = (e) => {
    setpassword(e.target.value);
    setPasswordError("");
  };

  const enterEmail = (e) => {
    setemail(e.target.value);
    setEmailError("");
  };

  const enterStoreName = (e) => {
    setstoreName(e.target.value);
    setStoreNameError("");
  };
  return (
    <>
      {showLoader && <Placeholder />}

      {showModal && (
        <SimpleModal
          showModal={showModal}
          setshowModal={setshowModal}
          title="Message"
          message={showModalContent}
        />
      )}
      <div style={styles.container}>
        <div className="login-container">
          <div className="login-box">
            <div className="welcome">
              <img
                src={require("../../constants/images/logo.png")}
                alt="logo"
              />
            </div>
            <div className="login-wrap">
              <div name="frmLogin" id="frmLogin">
                <span onClick={() => history.goBack()}>
                  <i class="fas fa-arrow-circle-left fa-2x"></i>
                </span>
                {/**<Form.Group controlId="Fullname">
    <Form.Label>Name</Form.Label>
    <Form.Control type="text" placeholder="Enter Full Name" onChange={(e)=>setname(e.target.value)} value={name} />
  </Form.Group>**/}

                <Form.Group controlId="exampleForm.ControlInput1">
                  <Form.Label>Email address</Form.Label>
                  <Form.Control
                    style={styles.inputField}
                    type="email"
                    placeholder="name@example.com"
                    onChange={enterEmail}
                    value={email}
                  />
                  {emailError && (
                    <span style={styles.wrongDetails}>{emailError}</span>
                  )}
                </Form.Group>

                <Form.Group controlId="Storename">
                  <Form.Label>Store Name</Form.Label>
                  <Form.Control
                    type="text"
                    style={styles.inputField}
                    placeholder="Enter Store Name"
                    onChange={enterStoreName}
                    value={storeName}
                  />
                  {storeNameError && (
                    <span style={styles.wrongDetails}>{storeNameError}</span>
                  )}
                </Form.Group>

                {/**<Form.Group controlId="phone">
    <Form.Label>Phone Number</Form.Label>
    <Form.Control type="tel" placeholder="Enter Contact Number" onChange={(e)=>setphone(e.target.value)} value={phone} />
  </Form.Group>

  <Form.Group controlId="dob">
    <Form.Label>Date Of Birth</Form.Label>
    <Form.Control type="date" placeholder="Enter Date of Birth" onChange={(e)=>setdob(e.target.value)} value={dob} />
  </Form.Group>
  
  <Form.Group controlId="exampleForm.ControlSelect1">
    <Form.Label>Gender</Form.Label>
    <Form.Control as="select" onChange={(e)=>setgender(e.target.value)} value={gender}>
      <option value="NA">choose...</option>
      <option value="Male">Male</option>
      <option value="Female">Female</option>
      <option value="Not Specified" >Not Specified</option>
    </Form.Control>
  </Form.Group>**/}

                <Form.Group controlId="password">
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    type="password"
                    style={styles.inputField}
                    placeholder="Enter Password"
                    onChange={enterPassword}
                    value={password}
                  />
                  {passwordError && (
                    <span style={styles.wrongDetails}>{passwordError}</span>
                  )}
                </Form.Group>

                <Form.Group controlId="repassword">
                  <Form.Label>Enter Password Again</Form.Label>
                  <Form.Control
                    type="password"
                    style={styles.inputField}
                    placeholder="Enter Password Again"
                    onChange={(e) => setrepassword(e.target.value)}
                    value={repassword}
                  />
                </Form.Group>
                {password !== repassword && (
                  <p style={styles.wrongDetails}>Passwords do not match</p>
                )}
                <Button
                  disabled={
                    password !== repassword ||
                    password === undefined ||
                    password === ""
                  }
                  variant="primary"
                  type="submit"
                  onClick={() => sign_up()}
                >
                  Sign Up
                </Button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Signup;
