//import {useDispatch} from 'react-redux';
export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';



export const loginRequest = (dispatch) => {
	//const dispatch = useDispatch();
	
	dispatch({type: LOGIN_REQUEST, data: {login: false}});
};

export const loginSuccess = (dispatch, data) => {
	//const dispatch = useDispatch();
	dispatch({
		type: LOGIN_SUCCESS,
		data: {login: true, data},
	});
};

export const loginFailure = (dispatch) => {
	//const dispatch = useDispatch();
	dispatch({
		type: LOGIN_FAILURE,
		data: {login: false},
	});
};
