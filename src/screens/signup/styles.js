const styles = {
  container: {
    height: "100vh",
    overflowY: "scroll",
  },

  inputField: {
    paddingLeft: "10px",
  },
  wrongDetails: {
    color: "red",
    marginTop: "5px",
  },
};

export default styles;
