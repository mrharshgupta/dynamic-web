import {ServerProvider} from './contexts.js';
import App from '../App.js';

const Provider = () => {
const server = 'ksmart.info/mopwnaplus';
return (<div>
            <ServerProvider value={server}>
                <App />
			</ServerProvider>
		</div>
  );
}

export default Provider;