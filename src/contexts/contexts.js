import * as React from "react";

export const ServerContext = React.createContext("3.131.72.146");

export const ServerProvider = ServerContext.Provider;
