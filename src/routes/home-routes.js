import React, { useState } from "react";
import { Route } from "react-router-dom";
import { connect } from "react-redux";
import Header from "../components/header/header.js";
import Footer from "../components/footer/footer.js";
import WrapLeft from "../components/left-side-bar/index";

import WrapRight from "../components/main-area/home/index";
//orders
import OrderList from "../components/main-area/orders/orders";
import Returns from "../components/main-area/orders/returns";
//products
import Products from "../components/main-area/products/products/index";
import Categories from "../components/main-area/products/categories/index";
import Options from "../components/main-area/products/options/index";
import Attributes from "../components/main-area/products/attributes/index";
import Reviews from "../components/main-area/products/reviews/index";
import Brands from "../components/main-area/products/brands/index";
import Downloads from "../components/main-area/products/downloads/index";
//customers
import Customers from "../components/main-area/customers/customers/index";
import CustomersExport from "../components/main-area/customers/customers/cust-export";
import CustomersImport from "../components/main-area/customers/customers/cust-import";
import AddCustomer from "../components/main-area/customers/customers/add-customer";
import CustomerGroups from "../components/main-area/customers/customer-groups/index";
import UpdateCustomerGroups from "../components/main-area/customers/customer-groups/update-customer-groups";
//marketing
import Campaigns from "../components/main-area/marketing/campaigns/index.js";
import Affiliates from "../components/main-area/marketing/affiliates/index.js";
import AddAffiliates from "../components/main-area/marketing/affiliates/add-affiliates";
import Coupons from "../components/main-area/marketing/coupons/index.js";
import AddCoupons from "../components/main-area/marketing/coupons/add-coupon.js";
import GiftVouchers from "../components/main-area/marketing/gift-vouchers/index.js";
import AddGiftVoucher from "../components/main-area/marketing/gift-vouchers/add-gift-voucher";
import GiftVoucherTheme from "../components/main-area/marketing/gift-vouchers/gift-voucher-theme";
import Integrations from "../components/main-area/marketing/integrations/index.js";

//designs
import Blog from "../components/main-area/design/blog/index";
import AddPost from "../components/main-area/design/blog/add-post.js";
import MediaManager from "../components/main-area/design/media-manager/index";
import Templates from "../components/main-area/design/templates/index";

import ImportTemplate from "../components/main-area/design/templates/import-template";
import Webpages from "../components/main-area/design/webpages/index";
import AddWebPage from "../components/main-area/design/webpages/add-web-page";
import WebInfo from "../components/main-area/design/webpages/web-info";
import WebDelInfo from "../components/main-area/design/webpages/web-del-info";
import WebStore from "../components/main-area/design/webpages/web-store";
import WebTerms from "../components/main-area/design/webpages/web-terms";

//analytics
import Analytics from "../components/main-area/analytics/index";

import Settings from "../components/main-area/settings/index";

import { MainAreaLoader } from "../components/loaders/index";
import SubscriptionRoutes from "./subscription-routes";

const HomeRoutes = ({
  slidingLoader,
  showFooterForSubscription,
  loginData,
}) => {
  const [hideSideBar, sethideSideBar] = useState(false);
  return (
    <div>
      {slidingLoader && <MainAreaLoader />}
      <Header sethideSideBar={sethideSideBar} hideSideBar={hideSideBar} />

      <div className={hideSideBar ? "wrapper active" : "wrapper"}>
        <Route path="/:storeName/home" component={WrapLeft} />
        <Route exact path="/:storeName/home" component={WrapRight} />

        <Route
          exact
          path="/:storeName/home/design/media-manager"
          component={MediaManager}
        />
        <Route exact path="/:storeName/home/design/blog" component={Blog} />
        <Route
          exact
          path="/:storeName/home/design/blog/add-post/:edit/:blogKey"
          component={AddPost}
        />
        <Route
          exact
          path="/:storeName/home/design/templates"
          component={Templates}
        />
        <Route
          exact
          path="/:storeName/home/design/templates/import-template"
          component={ImportTemplate}
        />
        <Route
          exact
          path="/:storeName/home/design/webpages"
          component={Webpages}
        />
        <Route
          exact
          path="/:storeName/home/design/webpages/add-webpage/:edit/:webpageKey"
          component={AddWebPage}
        />
        <Route
          exact
          path="/:storeName/home/design/webpages/web-info"
          component={WebInfo}
        />
        <Route
          exact
          path="/:storeName/home/design/webpages/web-store"
          component={WebStore}
        />
        <Route
          exact
          path="/:storeName/home/design/webpages/web-del-info"
          component={WebDelInfo}
        />
        <Route
          exact
          path="/:storeName/home/design/webpages/web-terms"
          component={WebTerms}
        />

        <Route exact path="/home/order-list" component={OrderList} />
        <Route exact path="/home/returns" component={Returns} />

        <Route exact path="/home/products" component={Products} />
        <Route exact path="/home/categories" component={Categories} />
        <Route exact path="/home/options" component={Options} />
        <Route exact path="/home/attributes" component={Attributes} />
        <Route exact path="/home/reviews" component={Reviews} />
        <Route exact path="/home/brands" component={Brands} />
        <Route exact path="/home/downloads" component={Downloads} />

        <Route exact path="/home/customers" component={Customers} />
        <Route
          exact
          path="/home/customers/cust-exports"
          component={CustomersExport}
        />
        <Route
          exact
          path="/home/customers/cust-imports"
          component={CustomersImport}
        />
        <Route
          exact
          path="/home/customers/add-customer"
          component={AddCustomer}
        />
        <Route exact path="/home/customer-groups" component={CustomerGroups} />
        <Route
          exact
          path="/home/customers/update-customer-groups"
          component={UpdateCustomerGroups}
        />

        <Route exact path="/home/marketing/campaigns" component={Campaigns} />
        <Route
          exact
          path="/home/marketing/integrations"
          component={Integrations}
        />
        <Route
          exact
          path="/home/marketing/gift-vouchers"
          component={GiftVouchers}
        />
        <Route
          exact
          path="/home/marketing/gift-vouchers/gift-voucher-theme"
          component={GiftVoucherTheme}
        />
        <Route
          exact
          path="/home/marketing/gift-vouchers/add-gift-voucher"
          component={AddGiftVoucher}
        />
        <Route exact path="/home/marketing/coupons" component={Coupons} />
        <Route
          exact
          path="/home/marketing/coupons/add-coupons"
          component={AddCoupons}
        />
        <Route exact path="/home/marketing/affiliates" component={Affiliates} />
        <Route
          exact
          path="/home/marketing/affiliates/add-affiliates"
          component={AddAffiliates}
        />

        <Route exact path="/home/analytics" component={Analytics} />

        <Route exact path="/:storeName/home/settings" component={Settings} />

        <Route
          path="/:storeName/home/subscriptions"
          component={SubscriptionRoutes}
        />
      </div>
      {parseInt(loginData.selectedPlan) === 1 && showFooterForSubscription && (
        <Footer />
      )}
    </div>
  );
};

const mapStateToProps = ({
  slidingLoader,
  showFooterForSubscription,
  loginData,
}) => ({
  slidingLoader,
  showFooterForSubscription,
  loginData,
});
export default connect(mapStateToProps)(HomeRoutes);
