import React from "react";
import { Route } from "react-router-dom";
import Subscriptions from "../components/subscriptions/index";

const SubscriptionRoutes = ({ mainLoader, slidingLoader }) => {
  return (
    <>
      <Route path="/:storeName/home/subscriptions" component={Subscriptions} />
    </>
  );
};

export default SubscriptionRoutes;
