import React from 'react';
import { Route } from 'react-router-dom';
import Customers from '../components/main-area/customers/customers/index';
import CustomerGroups from '../components/main-area/customers/customer-groups/index';
import CustomersImport from '../components/main-area/customers/customers/cust-import';

const Routes = () => {
  return (
   <div>
   <Route exact path="/home/customers" component={Customers} />
   <Route exact path="/home/customers/cust-imports" component={CustomersImport} />
   <Route exact path="/home/customer-groups" component={CustomerGroups} />     
   </div>
  );
}

export default Routes;