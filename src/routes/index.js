import React, { useEffect } from "react";
import { Route, Switch, HashRouter, BrowserRouter } from "react-router-dom";
import Signin from "../screens/signin/index";
import Signup from "../screens/signup/index";
import HomeRoutes from "./home-routes.js";
import { connect, useDispatch } from "react-redux";
import Demo from "../components/main-area/design/demo/index";
//import Template from '../components/main-area/design/templates/templates/apply-template/index';
import CustomizeTemplate from "../components/main-area/design/customize-template/index";
import { MainAreaLoader } from "../components/loaders/index";
import SubscriptionRoutes from "./subscription-routes";
import {
  verifyAutoLogin,
  getNumberOfUserBlogs,
  getNumberOfUserPages,
} from "../components/main-area/home/api";

const Routes = ({ mainLoader, slidingLoader }) => {
  const dispatch = useDispatch();
  useEffect(() => {
    localStorage.getItem("token") && verifyAutoLogin(dispatch);
  }, []);
  return (
    <>
      {slidingLoader === true && <MainAreaLoader />}
      <BrowserRouter basename="/onitt/react-files">
        <Switch>
          <Route path="/:storeName/home" component={HomeRoutes} />
          {/* <Route
            path="/:storeName/subscriptions"
            component={SubscriptionRoutes}
          /> */}
          <Route exact path="/" component={Signin} />
          <Route exact path="/signup" component={Signup} />

          {/*Templates */}
          <Route
            exact
            path="/:storeFront/customize-template/:userTemplateId/:appliedTemplate/:applied"
            component={CustomizeTemplate}
          />
          <Route path="/preview/:name/:templateId" component={Demo} />
        </Switch>
      </BrowserRouter>
    </>
  );
};

const mapStateToProps = ({ mainLoader, slidingLoader }) => ({
  mainLoader,
  slidingLoader,
});
export default connect(mapStateToProps)(Routes);
