const mainUrl = "http://3.131.72.146/onitt/files/apis";

const apiUrls = {
  login: `${mainUrl}/login.php`,
  verifyAutoLogin: `${mainUrl}/verify-auto-login.php`,
  signup: `${mainUrl}/signup.php`,
  imageUpload: `${mainUrl}/image-upload.php`,
  getTemplates: `${mainUrl}/get-templates.php`,
  choosePlan: `${mainUrl}/choose-plan.php`,
  editTemplate: `${mainUrl}/edit-template.php`,
  applyTemplate: `${mainUrl}/apply-template.php`,
  deleteFile: `${mainUrl}/delete-file.php`,
  rename: `${mainUrl}/rename.php`,
  addDomain: `${mainUrl}/add-domain.php`,
  copyFiles: `${mainUrl}/copy-files.php`,
  getMediaManagerImages: `${mainUrl}/get-media-manager-images.php`,

};

export { apiUrls };
