import { createStore, applyMiddleware } from "redux";
import { persistStore, persistReducer } from "redux-persist";
import storage from 'redux-persist/lib/storage';
import logger from "redux-logger";
import rootReducer from "./reducers";




let store = createStore(rootReducer);

store = createStore(rootReducer, applyMiddleware(logger));


export default () => {
  return {
    store
  };
};
