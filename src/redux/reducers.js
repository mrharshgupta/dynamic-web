import {
  LOGIN_REQUEST,
  LOGIN_FAILURE,
  LOGIN_SUCCESS,
} from "../screens/signin/actions";

import {
  TEMPLATES_REQUEST,
  TEMPLATES_FAILURE,
  TEMPLATES_SUCCESS,
} from "../components/main-area/design/templates/actions.js";

import {
  GET_USER_BLOGS_REQUEST,
  GET_USER_BLOGS_FAILURE,
  GET_USER_BLOGS_SUCCESS,
} from "../components/main-area/design/blog/actions.js";

const dataState = {
  mainLoader: false,
  slidingLoader: false,
  showFooterForSubscription: true,
  loginData: {},
  templates: {},
  userBlogs: {},
  customizeTemplateEditBarHeading: ["Edit Template"],
  imageFieldForMediaManagerCustomizeTemplate: "",
  serverName: "machinebe.com",
  appliedTemplate: localStorage.getItem("appliedTemplate")
    ? localStorage.getItem("appliedTemplate")
    : undefined,
  userTemplateId: localStorage.getItem("userTemplateId"),
  storeName: localStorage.getItem("storeName"),
};

const MainReducer = (state = dataState, action) => {
  console.log(action);
  switch (action.type) {
    case "GET_USER_DATA":
      return {
        ...state,
        loginData: action.data,
      };
    case "HIDE_SHOW_FOOTER_FOR_SUBSCRIPTION":
      return {
        ...state,
        showFooterForSubscription: action.data,
      };
    case "SET_USERTEMPLATEID_AND_APPLIEDTEMPLATE":
      return {
        ...state,
        userTemplateId: action.data.userTemplateId,
        appliedTemplate: action.data.appliedTemplate,
        customizeTemplateEditBarHeading: ["Edit Template"],
      };
    case "SET_MAIN_LOADER":
      return {
        ...state,
        mainLoader: action.data,
      };
    case "SET_SLIDING_LOADER":
      return {
        ...state,
        slidingLoader: action.data,
      };
    case "LOGOUT":
      return {
        loginData: {},
      };
    case "CHANGE_USERTEMPLATEID_AND_APPLIEDTEMPLATE":
      return {
        ...state,
        appliedTemplate: action.data.appliedTemplate,
        userTemplateId: action.data.userTemplateId,
      };
    case "GO_TO_HOME_AFTER_TEMPLATE_COMPONENT_ADD":
      return {
        ...state,
        customizeTemplateEditBarHeading: ["Edit Template"],
      };

    case "SET_IMAGE_FIELD_NAME_FOR_MEDIA_MANAGER":
      return {
        ...state,
        imageFieldForMediaManagerCustomizeTemplate: action.payload,
      };

    case "CUSTOMIZE_TEMPLATE_EDIT_BAR_CROSS_BUTTON":
      return {
        ...state,
        customizeTemplateEditBarHeading: ["Edit Template"],
      };

    case "CUSTOMIZE_TEMPLATE_EDIT_BAR_BACK_BUTTON":
      return {
        ...state,
        customizeTemplateEditBarHeading: [
          ...state.customizeTemplateEditBarHeading.slice(
            0,
            state.customizeTemplateEditBarHeading.length - 1
          ),
        ],
      };

    case "CHANGE_CUSTOMIZE_TEMPLATE_EDIT_BAR_HEADING":
      return {
        ...state,
        customizeTemplateEditBarHeading: [
          ...state.customizeTemplateEditBarHeading,
          action.payload,
        ],
      };

    case LOGIN_REQUEST:
    case LOGIN_FAILURE:
    case LOGIN_SUCCESS:
      return {
        loginData: action.data,
      };

    case TEMPLATES_REQUEST:
    case TEMPLATES_FAILURE:
    case TEMPLATES_SUCCESS:
      return {
        ...state,
        templates: action.data,
      };

    case GET_USER_BLOGS_REQUEST:
    case GET_USER_BLOGS_FAILURE:
    case GET_USER_BLOGS_SUCCESS:
      return {
        ...state,
        userBlogs: action.data,
      };

    default:
      return dataState;
  }
};

export default MainReducer;
