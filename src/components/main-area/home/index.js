import React, { useState, useEffect } from "react";
import { Link, Redirect } from "react-router-dom";
import styles from "./styles.js";
import {
  verifyAutoLogin,
  getNumberOfUserBlogs,
  getNumberOfUserPages,
} from "./api.js";
import "../../../css/style.css";
import "../../../css/device.css";
import "../../../css/editor.css";
import "../../../css/main.css";
import "../../../css/sanjana.css";
import "../../../css/bootstrap.min.css";
import { useDispatch } from "react-redux";

const WrapRight = () => {
  const dispatch = useDispatch();
  const [token, settoken] = useState(
    localStorage.getItem("token") || undefined
  );
  const [noOfUserBlogs, setNoOfUserBlogs] = useState(undefined);
  const [noOfUserPages, setNoOfUserPages] = useState(undefined);
  //console.log(token);

  useEffect(() => {
    verifyAutoLogin(dispatch, () => settoken(undefined));
    getNumberOfUserBlogs((abc) => setNoOfUserBlogs(abc));
    getNumberOfUserPages((abc) => setNoOfUserPages(abc));
  }, []);

  if (!token) {
    return <Redirect to="/" />;
  }

  return (
    <div class="wrap-right">
      <div class="page-nav">
        <div class="page-nav-left">
          <h1 class="pg-ttl">Welcome to Onitt!</h1>
        </div>
      </div>
      <div class="wrap-data" style={styles.wrapData}>
        <ul class="counter-box row">
          <li class="col-12 col-sm-6 col-md" >
            <Link
              style={styles.blockLink}
              to={`/${localStorage.getItem("storeName")}/home/design/blog`}
            >
              <div class="users">
                <center><span style={styles.blockHeading}>Total Blogs</span>
                <br />

                <span style={styles.blockSubText}>
                  {noOfUserBlogs ? noOfUserBlogs.length : "0"}
                </span></center>
              </div>
            </Link>
          </li>

          <li class="col-12 col-sm-6 col-md">
            <Link
              style={styles.blockLink}
              to={`/${localStorage.getItem("storeName")}/home/design/webpages`}
            >
              <div class="uploaded">
              <center><span style={styles.blockHeading}>Total Pages</span>
                <br />
                <span style={styles.blockSubText}>
                  {noOfUserPages ? noOfUserPages.length : "0"}
                </span></center>
              </div>
            </Link>
          </li>
         
        </ul>
        <ul class="panel-block row">
          <li class="col-12 mb-4">
            <div class="media">
              <div class="media-img">
                <i class="fas fa-shopping-cart"></i>
              </div>
              <div class="media-body">
                <h4>Customize your store</h4>
                <span>
                  Customize the look of your online store by selecting a new
                  template
                </span>
              </div>
              <div class="media-footer">
                <Link
                  to={`/${localStorage.getItem(
                    "storeName"
                  )}/home/design/templates`}
                  class="button"
                  style={styles.link}
                >
                  Select a Template
                </Link>
              </div>
            </div>
          </li>
         
          <li class="col-12">
            <div class="media">
              <div class="media-img">
                <i class="fas fa-flag"></i>
              </div>
              <div class="media-body">
                <h4>Set up your domain</h4>
                <span>
                  Select a plan and get a free domain for your online store
                </span>
              </div>
              <div class="media-footer">
                <Link to={`/${localStorage.getItem("storeName")}/home/settings`} class="button" style={styles.link}>
                  Set up domain
                </Link>
              </div>
            </div>
          </li>
        </ul>
        {/* <div class="panel-block row">
          <div class="col-12">
            <div class="tutorials">
              <h4>Check out those useful tutorials to get you started!</h4>
              <ul class="row mb-0">
                <li class="col-12 col-md-4 text-center">
                  <Link to="#" class="button" style={styles.link}>
                    How to add a product to your online store
                  </Link>
                </li>
                <li class="col-12 col-md-4 text-center">
                  <Link to="#" class="button" style={styles.link}>
                    How to edit your online store settings
                  </Link>
                </li>
                <li class="col-12 col-md-4 text-center">
                  <Link to="#" class="button" style={styles.link}>
                    How to customize your store design
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </div> */}
        {/* <div class="row">
          <div class="col-12">
            <div class="panel-block video">
              <iframe
                title="YT Video"
                src="https://www.youtube.com/embed/xcJtL7QggTI"
                frameBorder="0"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen
              />
            </div>
          </div>
        </div> */}
      </div>
    </div>
  );
};

const mapStateToProps = ({
  loginData: { loading = true, data = [] } = {},
}) => ({
  loginData: data,
  loading,
});

export default WrapRight;
