const styles = {
  wrapData: {
    height: "100vh",
    overflowY: "scroll",
  },
  link: {
    textDecoration: "none",
    color: "white",
  },
  blockHeading: {
    fontWeight: "bold",
    fontSize: 20,
  },
  blockSubText: {
    fontWeight: "bold",
    fontSize: 40,
    textAlign: "center",
    width: "100%",
    alignSelf: "center",
  },
  blockLink: {
    textDecoration: "none",
    color: "black",
  },
};

export default styles;
