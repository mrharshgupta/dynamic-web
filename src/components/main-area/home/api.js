import { apiUrls } from "../../../api/helper.js";
import { consoleInDebugModeOnly2 } from "../../../helpers/debugging";
import * as firebase from "firebase";

export function verifyAutoLogin(dispatch, settoken = () => {}) {
  fetch(apiUrls.verifyAutoLogin, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      token: localStorage.getItem("token"),
    }),
  })
    .then((response) => {
      console.log(response, "response");
      return response.json();
    })
    .then((responseData) => {
      consoleInDebugModeOnly2("autologInSuccess-responseData", responseData);

      if (responseData.autoLogin == false) {
        localStorage.clear();
        settoken();
        alert(
          "This app is in development, some structures are changed so kindly Sign Up again."
        );
      } else {
        dispatch({ type: "GET_USER_DATA", data: responseData });
        localStorage.setItem("selectedPlan", responseData.selectedPlan);
      }
    })
    .catch((error) => {
      consoleInDebugModeOnly2("autologInErrorWithoutApi", error.response);
    });
}

export function getNumberOfUserBlogs(
  setNoOfUserBlogs,
  setshowLoader = () => {}
) {
  const userBlogData = firebase
    .database()
    .ref("user_blog_data/" + localStorage.getItem("userId"));
  userBlogData.on("value", (snapshot) => {
    setNoOfUserBlogs(snapshot.val() && Object.keys(snapshot.val()));
    setshowLoader();
  });
}

export function getNumberOfUserPages(
  setNoOfUserPages,
  setshowLoader = () => {}
) {
  const userBlogData = firebase
    .database()
    .ref("user_webpages_data/" + localStorage.getItem("userId"));
  userBlogData.on("value", (snapshot) => {
    setNoOfUserPages(snapshot.val() && Object.keys(snapshot.val()));
    setshowLoader();
  });
}
