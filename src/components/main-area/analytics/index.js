import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import styles from './styles.js';
import '../../../css/style.css';
import '../../../css/device.css';
import '../../../css/editor.css';
import '../../../css/main.css';
import '../../../css/sanjana.css';
import '../../../css/bootstrap.min.css';


const Analytics = () => {
return(

	

<div class="wrap-right">
   <div class="page-nav">
      <div class="page-nav-left">
         <h1 class="pg-ttl"> Analytics</h1>
      </div>
      <div class="page-nav-right">
         <div class="paging">
            <input type="text" name="showRec" id="showRec" value="20" class="show-rec" original-title="Show Records" />
            <div class="result">1 – 20 of 42</div>
            <a class="prev" href="#"><i class="fas fa-caret-left"></i></a>
            <a class="next" href="#"><i class="fas fa-caret-right"></i></a>
         </div>
      </div>
   </div>
   <div class="wrap-data" style={styles.wrapData}>
      <div class="product-update-div">
         <div class="product-info-wrap">
            <div class="col-12">
               <div class="row">
                  <div class="col-12 any-title">
                     <h2>Setting</h2>
                  </div>
                  <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                     <div class="anylitic-div">
                        <Link to="#" class="" style={styles.link}>
                           <h3>Orders</h3>
                           <i class="fas fa-chart-line icon"></i>
                        </Link>
                     </div>
                  </div>
                  <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                     <div class="anylitic-div">
                        <Link to="#" class="" style={styles.link}>
                           <h3>Tax</h3>
                           <i class="fas fa-chart-line icon"></i>
                        </Link>
                     </div>
                  </div>
                  <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                     <div class="anylitic-div">
                        <Link to="#" class="" style={styles.link}>
                           <h3>Shipping</h3>
                           <i class="fas fa-chart-line icon"></i>
                        </Link>
                     </div>
                  </div>
                  <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                     <div class="anylitic-div">
                        <Link to="#" class="" style={styles.link}>
                           <h3>Returns</h3>
                           <i class="fas fa-chart-line icon"></i>
                        </Link>
                     </div>
                  </div>
                  <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                     <div class="anylitic-div">
                        <Link to="#" class="" style={styles.link}>
                           <h3>Coupons</h3>
                           <i class="fas fa-chart-line icon"></i>
                        </Link>
                     </div>
                  </div>
               </div>
                <div class="row">
                  <div class="col-12 any-title">
                     <h2>Customers</h2>
                  </div>
                  <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                     <div class="anylitic-div">
                        <Link to="#" class="" style={styles.link}>
                           <h3>Customers Online</h3>
                           <i class="fas fa-chart-line icon"></i>
                        </Link>
                     </div>
                  </div>
                  <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                     <div class="anylitic-div">
                        <Link to="#" class="" style={styles.link}>
                           <h3>Orders</h3>
                           <i class="fas fa-chart-line icon"></i>
                        </Link>
                     </div>
                  </div>
                  <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                     <div class="anylitic-div">
                        <Link to="#" class="" style={styles.link}>
                           <h3>Reward Points</h3>
                           <i class="fas fa-chart-line icon"></i>
                        </Link>
                     </div>
                  </div>
                  <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                     <div class="anylitic-div">
                        <Link to="#" class="" style={styles.link}>
                           <h3>Credit</h3>
                           <i class="fas fa-chart-line icon"></i>
                        </Link>
                     </div>
                  </div>
                 
               </div>
                <div class="row">
                  <div class="col-12 any-title">
                     <h2>Products</h2>
                  </div>
                  <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                     <div class="anylitic-div">
                        <Link to="#" class="" style={styles.link}>
                           <h3>Viewed</h3>
                           <i class="fas fa-chart-line icon"></i>
                        </Link>
                     </div>
                  </div>
                  <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                     <div class="anylitic-div">
                        <Link to="#" class="" style={styles.link}>
                           <h3>Purchased</h3>
                           <i class="fas fa-chart-line icon"></i>
                        </Link>
                     </div>
                  </div>
                  <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                     <div class="anylitic-div">
                        <Link to="#" class="" style={styles.link}>
                           <h3>Top 10 Purchased</h3>
                           <i class="fas fa-chart-line icon"></i>
                        </Link>
                     </div>
                  </div>
                 
                 
               </div>
                 <div class="row">
                  <div class="col-12 any-title">
                     <h2>Analytics</h2>
                  </div>
                  <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                     <div class="anylitic-div">
                        <Link to="#" class="" style={styles.link}>
                           <h3>Analytics Stats</h3>
                           <i class="fas fa-chart-line icon"></i>
                        </Link>
                     </div>
                  </div>
                  <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                     <div class="anylitic-div">
                        <Link to="#" class="" style={styles.link}>
                           <h3>Visitors Details</h3>
                           <i class="fas fa-chart-line icon"></i>
                        </Link>
                     </div>
                  </div>
                 
                 
                 
               </div>
               <div class="row">
                  <div class="col-12 any-title">
                     <h2>Affiliates</h2>
                  </div>
                  <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                     <div class="anylitic-div">
                        <Link to="#" class="" style={styles.link}>
                           <h3>Commission</h3>
                           <i class="fas fa-chart-line icon"></i>
                        </Link>
                     </div>
                  </div>
                 
                 
                 
                 
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
	);
}
export default Analytics; 