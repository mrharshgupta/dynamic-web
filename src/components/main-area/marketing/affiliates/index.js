import React, {useState} from 'react';
import Popups from '../../../popups/index';
import {Link} from 'react-router-dom';
import styles from './styles';
import '../../../../css/style.css';
import '../../../../css/device.css';
import '../../../../css/editor.css';
import '../../../../css/main.css';
import '../../../../css/sanjana.css';
import '../../../../css/bootstrap.min.css';

const Affiliates = (props) => {

return(
<>
   <div class="wrap-right">
      <div class="page-nav">
         <div class="page-nav-left">
            <div class="page-action">
               <a href="javascript:void(0)" class="button">export</a>
               <a href="javascript:void(0)" class="button">Delete</a>
            </div>
            <h1 class="pg-ttl">Affiliates</h1>
         </div>
         <div class="page-nav-right">
            <div class="paging">
               <input type="text" name="showRec" id="showRec" value="20" class="show-rec" original-title="Show Records" />
               <div class="result">1 – 20 of 42</div>
               <a class="prev" href="#"><i class="fas fa-caret-left"></i></a>
               <a class="next" href="#"><i class="fas fa-caret-right"></i></a>
            </div>
            <Link to="/home/marketing/affiliates/add-affiliates" className="button" style={styles.link}>Add New Affiliate</Link>
         </div>
      </div>
      <div class="wrap-data" style={styles.wrapData}>
         <table class="tbl-list">
            <thead>
               <tr>
                  <th><i class="chk"><input type="checkbox" class="chkAll" title="Select All" /><em></em></i></th>
                  <th><a class="sort" href="#">Affiliate Name</a></th>
                  <th><a class="sort" href="#">Email</a></th>
                  <th><a class="asc" href="#">Balance</a></th>
                  <th><a class="sort" href="#">Status</a></th>
                  <th><a class="sort" href="#">Approved</a></th>
                  <th><a class="sort" href="#">Date Added</a></th>
               </tr>
            </thead>
            <tbody>
               <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td>Lorem Lipsum</td>
                  <td>Lorem Ipsum@gmail.com</td>
                  <td>3,499.00 INR</td>
                  <td>
                     <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch1" />
                        <label class="custom-control-label" for="customSwitch1">Disable</label>
                     </div>
                  </td>
                  <td>Yes</td>
                  <td>2017-10-23 10:42:35</td>
               </tr>
               <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td>Lorem Lipsum</td>
                  <td>Lorem Ipsum@gmail.com</td>
                  <td>3,499.00 INR</td>
                  <td>
                     <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch2" />
                        <label class="custom-control-label" for="customSwitch2">Disable</label>
                     </div>
                  </td>
                  <td>Yes</td>
                  <td>2017-10-23 10:42:35</td>
               </tr>
               <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td>Lorem Lipsum</td>
                  <td>Lorem Ipsum@gmail.com</td>
                  <td>3,499.00 INR</td>
                  <td>
                     <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch3" />
                        <label class="custom-control-label" for="customSwitch3">Disable</label>
                     </div>
                  </td>
                  <td>Yes</td>
                  <td>2017-10-23 10:42:35</td>
               </tr>
               <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td>Lorem Lipsum</td>
                  <td>Lorem Ipsum@gmail.com</td>
                  <td>3,499.00 INR</td>
                  <td>
                     <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch4" />
                        <label class="custom-control-label" for="customSwitch4">Disable</label>
                     </div>
                  </td>
                  <td>Yes</td>
                  <td>2017-10-23 10:42:35</td>
               </tr>
            </tbody>
         </table>
      </div>
   </div>

</>
);
}

export default Affiliates;