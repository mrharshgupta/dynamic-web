import React, {useState} from 'react';
import Popups from '../../../popups/index';
import styles from './styles';
import {Link} from 'react-router-dom';
import '../../../../css/style.css';
import '../../../../css/device.css';
import '../../../../css/editor.css';
import '../../../../css/main.css';
import '../../../../css/sanjana.css';
import '../../../../css/bootstrap.min.css';

const AddAffiliates = (props) => {


const [showGeneral, setshowGeneral] = useState(undefined);
const [showPaymentDetails, setshowPaymentDetails] = useState(undefined);

return(
<>
   <div class="wrap-right">
      <div class="page-nav">
         <div class="page-nav-left">
            <h1 class="pg-ttl">Add New Affiliate</h1>
         </div>
         <div class="page-nav-right">
            <button class="button">Save</button>
            <button class="button">Cancle</button>
         </div>
      </div>
      <div class="wrap-data" style={styles.wrapData}>
         <div class="product-update-div">
            <div class="product-info-wrap">
               <a onClick={()=>{(showGeneral === true)? (setshowGeneral(undefined)) : (setshowGeneral(true))}} href="javascript:void(0)" class="search-btn" aria-expanded="false">General</a>
               {(showGeneral === true) && (
               <div id="General-affiliate" class="search-form1">
                  <form action="" class="col-12">
                     <ul class="row form-div">
                        <li class="col-12 form-group">
                           <label for="">Status</label>
                           <div class="custom-control custom-switch">
                              <input type="checkbox" class="custom-control-input" id="customSwitch1" checked />
                              <label class="custom-control-label" for="customSwitch1">Enable</label>
                           </div>
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">First Name <span class="text-red">*</span></label>
                           <input type="text" class="form-control" value="" />
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Last Name <span class="text-red">*</span></label>
                           <input type="text" class="form-control" value="" />
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Email Name <span class="text-red">*</span></label>
                           <input type="Email" class="form-control" value="" />
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Telephone No <span class="text-red">*</span></label>
                           <input type="text" class="form-control" value="" />
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Fax <span class="text-red">*</span></label>
                           <input type="text" class="form-control" value="" />
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Company <span class="text-red">*</span></label>
                           <input type="text" class="form-control" value="" />
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Address1 <span class="text-red">*</span></label>
                           <input type="text" class="form-control" value="" />
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Address2 <span class="text-red">*</span></label>
                           <input type="text" class="form-control" value="" />
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">City<span class="text-red">*</span></label>
                           <input type="text" class="form-control" value="" />
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Postcode<span class="text-red">*</span></label>
                           <input type="text" class="form-control" value="" />
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Tracking Code:The tracking code that will be used to track referrals. <span class="text-red">*</span></label>
                           <input type="text" class="form-control" value="" />
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Password <span class="text-red">*</span></label>
                           <input type="password" class="form-control" value="" />
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Confirm <span class="text-red">*</span></label>
                           <input type="password" class="form-control" value="" />
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Country <span class="text-red">*</span> </label>
                           <select class="form-control" id="">
                              <option>Select</option>
                              <option>India</option>
                              <option>America</option>
                           </select>
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Region / State <span class="text-red">*</span> </label>
                           <select class="form-control" id="">
                              <option>Select</option>
                              <option>Maharastra</option>
                              <option>Gujarat</option>
                           </select>
                        </li>
                     </ul>
                  </form>
               </div>)}
               <a onClick={()=>{(showPaymentDetails === true)? (setshowPaymentDetails(undefined)) : (setshowPaymentDetails(true))}} href="javascript:void(0)"  className="search-btn" aria-expanded="false">Payment Details</a>
                 {(showPaymentDetails === true) && (
               <div id="payment-detail" class="search-form1">
                  <form action="" class="col-12">
                     <ul class="row form-div">
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Commission (%):Percentage the affiliate recieves on each order.<span class="text-red">*</span></label>
                           <input type="number" class="form-control" value="" />
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Tax ID <span class="text-red">*</span></label>
                           <input type="number" class="form-control" value="" />
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Payment Method<span class="text-red">*</span> </label>
                           <select class="form-control" id="">
                              <option>Select</option>
                              <option>Paypal</option>
                              <option>Bank transfer</option>
                           </select>
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Cheque Payee Name <span class="text-red">*</span></label>
                           <input type="Email" class="form-control" value="" />
                        </li>
                     </ul>
                  </form>
               </div>)}
               <div class="btn-wrapp">
                  <button class="button">Save</button>
                  <button class="button">Cancle</button>
               </div>
            </div>
         </div>
      </div>
   </div>

</>
);
}

export default AddAffiliates;