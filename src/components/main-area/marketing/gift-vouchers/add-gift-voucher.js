import React, {useState} from 'react';
import Popups from '../../../popups/index';
import styles from './styles';
import {Link} from 'react-router-dom';
import '../../../../css/style.css';
import '../../../../css/device.css';
import '../../../../css/editor.css';
import '../../../../css/main.css';
import '../../../../css/sanjana.css';
import '../../../../css/bootstrap.min.css';

const AddGiftVoucher = (props) => {

return(
<>
   <div class="wrap-right">
      <div class="page-nav">
         <div class="page-nav-left">
            <div class="page-action">
               <a href="javascript:void(0)" class="button">export</a>
               <a href="javascript:void(0)" class="button">Delete</a>
               <a href="market-voucher.php" class="button">Gift Voucher</a>
               <Link to="/home/marketing/gift-vouchers/gift-voucher-theme" className="button" style={styles.link}>Voucher Theme</Link>
            </div>
            <h1 class="pg-ttl">Gift Vouchers</h1>
         </div>
         <div class="page-nav-right">
            <div class="paging">
               <input type="text" name="showRec" id="showRec" value="20" class="show-rec" original-title="Show Records" />
               <div class="result">1 – 20 of 42</div>
               <a class="prev" href="#"><i class="fas fa-caret-left"></i></a>
               <a class="next" href="#"><i class="fas fa-caret-right"></i></a>
            </div>
            <Link to="/home/marketing/gift-vouchers/add-gift-voucher" className="button" style={styles.link}>Create New Voucher</Link>
         </div>
      </div>
      <div class="wrap-data" style={styles.wrapData}>
         <table class="tbl-list">
            <thead>
               <tr>
                  <th><i class="chk"><input type="checkbox" class="chkAll" title="Select All" /><em></em></i></th>
                  <th><a class="sort" href="#">Code</a></th>
                  <th><a class="sort" href="#">From</a></th>
                  <th><a class="asc" href="#">To</a></th>
                  <th><a class="sort" href="#">Amount</a></th>
                  <th><a class="sort" href="#">Status</a></th>
               </tr>
            </thead>
            <tbody>
               <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td>1234</td>
                  <td>loem lipsum</td>
                  <td>loem lipsum</td>
                  <td>20.00 INR</td>
                  <td>
                     <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch1" />
                        <label class="custom-control-label" for="customSwitch1">Disable</label>
                     </div>
                  </td>
               </tr>
               <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td>1234</td>
                  <td>loem lipsum</td>
                  <td>loem lipsum</td>
                  <td>20.00 INR</td>
                  <td>
                     <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch2" />
                        <label class="custom-control-label" for="customSwitch2">Disable</label>
                     </div>
                  </td>
               </tr>
            </tbody>
         </table>
      </div>
   </div>

</>
);
}

export default AddGiftVoucher;