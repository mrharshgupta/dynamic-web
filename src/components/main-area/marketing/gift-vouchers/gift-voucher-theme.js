import React, {useState} from 'react';
import Popups from '../../../popups/index';
import styles from './styles';
import {Link} from 'react-router-dom';
import '../../../../css/style.css';
import '../../../../css/device.css';
import '../../../../css/editor.css';
import '../../../../css/main.css';
import '../../../../css/sanjana.css';
import '../../../../css/bootstrap.min.css';

const GiftVoucherTheme = (props) => {

return(
<>
  <div class="wrap-right">
      <div class="page-nav">
         <div class="page-nav-left">
            <div class="page-action">
               <a href="javascript:void(0)" class="button">export</a>
               <a href="javascript:void(0)" class="button">Delete</a>
               <a href="market-voucher.php" class="button">Gift Voucher</a>
               <a href="market-vouchertheme.php" class="button">Voucher Theme</a>
            </div>
            <h1 class="pg-ttl">Gift Vouchers</h1>
         </div>
         <div class="page-nav-right">
            <div class="paging">
               <input type="text" name="showRec" id="showRec" value="20" class="show-rec" original-title="Show Records" />
               <div class="result">1 – 20 of 42</div>
               <a class="prev" href="#"><i class="fas fa-caret-left"></i></a>
               <a class="next" href="#"><i class="fas fa-caret-right"></i></a>
            </div>
            <a href="add-vouchertheme.php" class="button">Create New Voucher Theme</a>
         </div>
      </div>
      <div class="wrap-data">
         <table class="tbl-list">
            <thead>
               <tr>
                  <th><i class="chk"><input type="checkbox" class="chkAll" title="Select All" /><em></em></i></th>
                  <th><a class="sort" href="#">Voucher Theme Name</a></th>
                  
               </tr>
            </thead>
            <tbody>
               <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td><a href="#">Lorem lipsum</a></td>
                 
               </tr>
                <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td><a href="#">Lorem lipsum</a></td>
                 
               </tr>
              
            </tbody>
         </table>
      </div>
   </div>

</>
);
}

export default GiftVoucherTheme;