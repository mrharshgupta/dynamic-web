import React, {useState} from 'react';
import Popups from '../../../popups/index';
import {Link} from 'react-router-dom';
import styles from './styles';
import '../../../../css/style.css';
import '../../../../css/device.css';
import '../../../../css/editor.css';
import '../../../../css/main.css';
import '../../../../css/sanjana.css';
import '../../../../css/bootstrap.min.css';

const AddCoupons = (props) => {

const [showStatus, setshowStatus] = useState(undefined);
const [showGlobalSettings, setshowGlobalSettings] = useState(undefined);
const [showExcludedProducts, setshowExcludedProducts] = useState(undefined);

return(
<>
  <div class="wrap-right">
      <div class="page-nav">
         <div class="page-nav-left">
            <h1 class="pg-ttl">Insert Coupan</h1>
         </div>
         <div class="page-nav-right">
            <button class="button">Save</button>
            <button class="button">Cancle</button>
         </div>
      </div>
      <div class="wrap-data" style={styles.wrapData}>
         <div class="product-update-div">
            <div class="product-info-wrap">
               <a onClick={()=>{(showStatus === true)? (setshowStatus(undefined)) : (setshowStatus(true))}} href="javascript:void(0)" className="search-btn" aria-expanded="false">Status</a>
            {(showStatus === true) && (
               <div id="status" class="search-form1">
                  <form action="" class="col-12">
                     <ul class="row form-div">
                        <li class="col-12 form-group">
                           <label for="">Status</label>
                           <div class="custom-control custom-switch">
                              <input type="checkbox" class="custom-control-input" id="customSwitch1" checked />
                              <label class="custom-control-label" for="customSwitch1">Enable</label>
                           </div>
                        </li>
                     </ul>
                  </form>
               </div>)}

               <a onClick={()=>{(showGlobalSettings === true)? (setshowGlobalSettings(undefined)) : (setshowGlobalSettings(true))}} href="javascript:void(0)" className="search-btn" data-toggle="collapse" aria-expanded="false">Global Settings</a>
               {(showGlobalSettings === true) && (
               <div id="globe-setting" class="search-form1">
                  <form action="" class="col-12">
                     <ul class="row form-div">
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Coupon Name<span class="text-red">*</span></label>
                           <input type="text" class="form-control" value="" />
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Code <span class="text-red">*</span></label>
                           <input type="text" class="form-control" value="" />
                           <p class="note-txt">The code the customer enters to get the discount</p>
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Type<span class="text-red">*</span> </label>
                           <select class="form-control" id="">
                              <option>Select</option>
                              <option>Percentage</option>
                              <option>Fixed Amount</option>
                           </select>
                           <p class="note-txt">Percentage or Fixed Amount</p>
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Discount<span class="text-red">*</span></label>
                           <input type="text" class="form-control" value="" />
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Minimum to apply<span class="text-red">*</span></label>
                           <input type="text" class="form-control" value="" />
                           <p class="note-txt">Set the minimum cart value to be able to apply the coupon</p>
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Maximum limit<span class="text-red">*</span></label>
                           <input type="text" class="form-control" value="" />
                           <p class="note-txt">Set your maximum limit of cart value
                              the coupon will applied on the first X amount of the cart value
                           </p>
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Customer Login<span class="text-red">*</span> </label>
                           <select class="form-control" id="">
                              <option>Select</option>
                              <option>Yes</option>
                              <option>No</option>
                           </select>
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Shipping cost<span class="text-red">*</span> </label>
                           <select class="form-control" id="">
                              <option>Select</option>
                              <option>Apply Coupan On Shipping Cost</option>
                              <option>Fixed Amount</option>
                           </select>
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Date Start <span class="text-red">*</span></label>
                           <input type="date" class="form-control" value="" />
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Date End<span class="text-red">*</span></label>
                           <input type="date" class="form-control" value="" />
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Uses Per Coupon<span class="text-red">*</span></label>
                           <input type="text" class="form-control" value="" />
                           <p class="note-txt">The maximum number of times the coupon can be used by any customer. Leave blank for unlimited</p>
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Uses Per Customer<span class="text-red">*</span></label>
                           <input type="text" class="form-control" value="" />
                           <p class="note-txt">Uses Per Customer</p>
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Products<span class="text-red">*</span></label>
                           <input type="text" class="form-control" value="" />
                           <p class="note-txt">Set your maximum limit of cart value
                              the coupon will applied on the first X amount of the cart value
                           </p>
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Category<span class="text-red">*</span></label>
                           <select class="form-control" id="">
                              <option>Select</option>
                              <option>option 1</option>
                              <option>option 2</option>
                           </select>
                           <p class="note-txt">Choose all products under selected category.</p>
                        </li>
                        <li class="col-12 form-group">
                           <label for="">Brands<span class="text-red">*</span></label>
                           <input type="text" class="form-control" value="" />
                           <p class="note-txt">Choose specific brands the coupon will apply to. Select no brands to apply coupon to entire cart.</p>
                        </li>
                     </ul>
                  </form>
               </div>)}
               

                <a onClick={()=>{(showExcludedProducts === true)? (setshowExcludedProducts(undefined)) : (setshowExcludedProducts(true))}} href="javascript:void(0)" className="search-btn" data-toggle="collapse" aria-expanded="false">Excluded products</a>
               {(showExcludedProducts === true) && (
               <div id="exclude-pro" class="search-form1">
                  <form action="" class="col-12">
                     <ul class="row form-div">
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Excluded products<span class="text-red">*</span></label>
                             <select class="form-control" id="">
                              <option>Select</option>
                              <option>Option 1</option>
                              <option>Option 2</option>
                           </select>
                            <p class="note-txt">Choose specific brands the coupon will not apply to.</p>
                        </li>

                          <li class="col-12 col-md-6 form-group">
                           <label for="">Categories<span class="text-red">*</span></label>
                             <select class="form-control" id="">
                              <option>Select</option>
                              <option>Option 1</option>
                              <option>Option 2</option>
                           </select>
                            <p class="note-txt">Choose specific brands the coupon will not apply to.</p>
                        </li>
                         <li class="col-12 col-md-6 form-group">
                           <label for="">Brand<span class="text-red">*</span></label>
                             <select class="form-control" id="">
                              <option>Select</option>
                              <option>Option 1</option>
                              <option>Option 2</option>
                           </select>
                            <p class="note-txt">Choose specific brands the coupon will not apply to.</p>
                        </li>
                      
                     </ul>
                  </form>
               </div>)}
               <div class="btn-wrapp">
                  <button class="button">Save</button>
                  <button class="button">Cancle</button>
               </div>
            </div>
         </div>
      </div>
      </div>
   


</>
);
}

export default AddCoupons;