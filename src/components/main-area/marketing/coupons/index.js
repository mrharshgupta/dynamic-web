import React, {useState} from 'react';
import Popups from '../../../popups/index';
import styles from './styles';
import {Link} from 'react-router-dom';
import '../../../../css/style.css';
import '../../../../css/device.css';
import '../../../../css/editor.css';
import '../../../../css/main.css';
import '../../../../css/sanjana.css';
import '../../../../css/bootstrap.min.css';

const Coupons = (props) => {

return(
<>
<div class="wrap-right">
      <div class="page-nav">
         <div class="page-nav-left">
            <div class="page-action">
               <a href="javascript:void(0)" class="button">export</a>
               <a href="javascript:void(0)" class="button">Delete</a>
            </div>
            <h1 class="pg-ttl">Discount Coupons</h1>
         </div>
         <div class="page-nav-right">
            <div class="paging">
               <input type="text" name="showRec" id="showRec" value="20" class="show-rec" original-title="Show Records" />
               <div class="result">1 – 20 of 42</div>
               <a class="prev" href="#"><i class="fas fa-caret-left"></i></a>
               <a class="next" href="#"><i class="fas fa-caret-right"></i></a>
            </div>
            <Link to="/home/marketing/coupons/add-coupons" className="button" style={styles.link}>Create New Coupan</Link>
         </div>
      </div>
      <div class="wrap-data" style={styles.wrapData}>
         <table class="tbl-list">
            <thead>
               <tr>
                  <th><i class="chk"><input type="checkbox" class="chkAll" title="Select All" /><em></em></i></th>
                  <th><a class="sort" href="#">Coupan Name</a></th>
                  <th><a class="sort" href="#">Code</a></th>
                  <th><a class="asc" href="#">Discount</a></th>
                  <th><a class="sort" href="#">Date Start</a></th>
                  <th><a class="sort" href="#">Date End</a></th>
                  <th><a class="sort" href="#">Status</a></th>
               </tr>
            </thead>
            <tbody>
               <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td>Lorem Lipsum</td>
                  <td>1234</td>
                  <td>loem lipsum</td>
                  <td>2017-10-23 10:42:35</td>
                  <td>2017-10-23 10:42:35</td>
                  <td>
                     <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch1" />
                        <label class="custom-control-label" for="customSwitch1">Disable</label>
                     </div>
                  </td>
                  
               </tr>
                  <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td>Lorem Lipsum</td>
                  <td>1234</td>
                  <td>loem lipsum</td>
                  <td>2017-10-23 10:42:35</td>
                  <td>2017-10-23 10:42:35</td>
                  <td>
                     <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch2" />
                        <label class="custom-control-label" for="customSwitch2">Disable</label>
                     </div>
                  </td>
                  
               </tr>
                  <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td>Lorem Lipsum</td>
                  <td>1234</td>
                  <td>loem lipsum</td>
                  <td>2017-10-23 10:42:35</td>
                  <td>2017-10-23 10:42:35</td>
                  <td>
                     <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch3" />
                        <label class="custom-control-label" for="customSwitch3">Disable</label>
                     </div>
                  </td>
                  
               </tr>
                  <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td>Lorem Lipsum</td>
                  <td>1234</td>
                  <td>loem lipsum</td>
                  <td>2017-10-23 10:42:35</td>
                  <td>2017-10-23 10:42:35</td>
                  <td>
                     <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch4" />
                        <label class="custom-control-label" for="customSwitch4">Disable</label>
                     </div>
                  </td>
                  
               </tr>
              
            </tbody>
         </table>
      </div>
   </div>
</>
);
}

export default Coupons;