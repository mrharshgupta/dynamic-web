import React, {useState} from 'react';
import Popups from '../../../popups/index';
import styles from './styles';
import {Link} from 'react-router-dom';
import '../../../../css/style.css';
import '../../../../css/device.css';
import '../../../../css/editor.css';
import '../../../../css/main.css';
import '../../../../css/sanjana.css';
import '../../../../css/bootstrap.min.css';

const Campaigns = (props) => {

return(
<>
<div class="wrap-right">
      <div class="page-nav">
         <div class="page-nav-left">
            <h1 class="pg-ttl">Campaigns</h1>
         </div>
         <div class="page-nav-right">
            <Link to="#" style={styles.link} class="button"><i class="text-white fas fa-envelope"></i> Mail</Link>
         </div>
      </div>
      <div class="wrap-data" style={styles.wrapData}>
         <div class="product-update-div">
            <div class="product-info-wrap">
               <div class="alert alert-warning" role="alert">
                  Please change the Mail Server configuration to your mail configuration
               </div>
               <form action="">
                  <div class="form-group">
                     <label>To</label>
                     <select  class="form-control">
                        <option>All Customer</option>
                        <option>Customers</option>
                        <option>Products</option>
                     </select>
                  </div>
                  <div class="form-group">
                     <label for="">Subject<span class="text-red">*</span></label>
                     <input type="text" class="form-control" value="" title="Customer" />
                  </div>
                  <div class="form-group">
                     <label>Subject</label>
                     <div class="txt-edit">
                        <textarea id="txtEditor" class="texteditor"></textarea> 
                     </div>
                  </div>
                  <div class="form-group">
                     <label>Available Variables</label>
                     <ul class="avail-var">
                        <li><a href="#">First Name : <span>{"{First Name}"}</span></a></li>
                        <li><a href="#">Last Name : <span>{"{Last Name}"}</span></a></li>
                        <li><a href="#">Phone No : <span>{"{Phone No}"}</span></a></li>
                     </ul>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</>
);
}

export default Campaigns;