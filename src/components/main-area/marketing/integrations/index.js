import React, {useState} from 'react';
import Popups from '../../../popups/index';
import styles from './styles';
import '../../../../css/style.css';
import '../../../../css/device.css';
import '../../../../css/editor.css';
import '../../../../css/main.css';
import '../../../../css/sanjana.css';
import '../../../../css/bootstrap.min.css';

const Integrations = (props) => {

return(
<>
<div class="wrap-right">
      <div class="page-nav">
         <div class="page-nav-left">
            <h1 class="pg-ttl"> Integrations</h1>
         </div>
         <div class="page-nav-right">
             <a href="#" class="button" data-toggle="modal" data-target="#exampleModal">Video Guide</a>
            <button class="button">Save</button>
            <button class="button">Cancle</button>
         </div>
      </div>
      <div class="wrap-data" style={styles.wrapData}>
         <div class="product-update-div">
            <div class="product-info-wrap">
               <a href="#fb-pixel" class="search-btn" data-toggle="collapse" aria-expanded="false"> FACEBOOK PIXEL</a>
               <div id="fb-pixel" class="collapse search-form1">
                  <form action="" class="col-12 px-0">
                     <ul class="row form-div">
                        <li class="col-12 form-group">
                           <label for="">Status</label>
                           <div class="custom-control custom-switch">
                              <input type="checkbox" class="custom-control-input" id="customSwitch1" checked />
                              <label class="custom-control-label" for="customSwitch1">Enable</label>
                           </div>
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Pixel ID</label>
                            <input type="text" class="form-control" value="" />
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Tracked Actions</label>
                           <select class="form-control" id="">
                              <option>Select</option>
                              <option>Option 1</option>
                              <option>Option 2</option>
                           </select>
                        </li>
                     </ul>
                  </form>
               </div>
                    <a href="#ggogle-analytics" class="search-btn" data-toggle="collapse" aria-expanded="false">GOOGLE ANALYTICS E-COMMERCE</a>
               <div id="ggogle-analytics" class="collapse search-form1">
                  <form action="" class="col-12 px-0">
                     <ul class="row form-div">
                        <li class="col-12 form-group">
                           <label for="">Status</label>
                           <div class="custom-control custom-switch">
                              <input type="checkbox" class="custom-control-input" id="customSwitch1" checked />
                              <label class="custom-control-label" for="customSwitch1">Enable</label>
                           </div>
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Property or Tracking ID</label>
                            <input type="text" class="form-control" value="" />
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Tracked Actions</label>
                           <select class="form-control" id="">
                              <option>Select</option>
                              <option>Option 1</option>
                              <option>Option 2</option>
                           </select>
                        </li>
                     </ul>
                  </form>
               </div>

                   <a href="#google-adword" class="search-btn" data-toggle="collapse" aria-expanded="false"> GOOGLE ADWORDS</a>
               <div id="google-adword" class="collapse search-form1">
                  <form action="" class="col-12 px-0">
                     <ul class="row form-div">
                        <li class="col-12 form-group">
                           <label for="">Status</label>
                           <div class="custom-control custom-switch">
                              <input type="checkbox" class="custom-control-input" id="customSwitch1" checked />
                              <label class="custom-control-label" for="customSwitch1">Enable</label>
                           </div>
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Conversion ID</label>
                            <input type="text" class="form-control" value="" />
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Conversion Label</label>
                           <input type="text" class="form-control" value="" />
                        </li>
                     </ul>
                  </form>
               </div>
                    <a href="#criteo" class="search-btn" data-toggle="collapse" aria-expanded="false"> CRITEO</a>
               <div id="criteo" class="collapse search-form1">
                  <form action="" class="col-12 px-0">
                     <ul class="row form-div">
                        <li class="col-12 form-group">
                           <label for="">Status</label>
                           <div class="custom-control custom-switch">
                              <input type="checkbox" class="custom-control-input" id="customSwitch1" checked />
                              <label class="custom-control-label" for="customSwitch1">Enable</label>
                           </div>
                        </li>
                        <li class="col-12 form-group">
                           <label for="">Account ID</label>
                            <input type="text" class="form-control" value="" />
                        </li>
                       
                     </ul>
                  </form>
               </div>
                 <a href="#snap-pixel" class="search-btn" data-toggle="collapse" aria-expanded="false"> SNAPCHAT PIXEL</a>
               <div id="snap-pixel" class="collapse search-form1">
                  <form action="" class="col-12 px-0">
                     <ul class="row form-div">
                        <li class="col-12 form-group">
                           <label for="">Status</label>
                           <div class="custom-control custom-switch">
                              <input type="checkbox" class="custom-control-input" id="customSwitch1" checked />
                              <label class="custom-control-label" for="customSwitch1">Enable</label>
                           </div>
                        </li>
                        <li class="col-12 form-group">
                           <label for="">Pixel ID</label>
                            <input type="text" class="form-control" value="" />
                        </li>
                        <li class="col-12 form-group">
                           <label for="">Email</label>
                            <input type="email" class="form-control" value="" />
                        </li>
                        <li class="col-12 col-md-6 form-group">
                           <label for="">Tracked Actions</label>
                           <select class="form-control" id="">
                              <option>Select</option>
                              <option>Option 1</option>
                              <option>Option 2</option>
                           </select>
                        </li>
                       
                     </ul>
                  </form>
               </div>
 <a href="#slack" class="search-btn" data-toggle="collapse" aria-expanded="false">SLACK</a>
               <div id="slack" class="collapse search-form1">
                  <form action="" class="col-12 px-0">
                     <ul class="row form-div">
                        <li class="col-12 form-group">
                           <label for="">Status</label>
                           <div class="custom-control custom-switch">
                              <input type="checkbox" class="custom-control-input" id="customSwitch1" checked />
                              <label class="custom-control-label" for="customSwitch1">Enable</label>
                           </div>
                        </li>
                        <li class="col-12 form-group">
                           <label for="">Webhook URL</label>
                            <input type="text" class="form-control" value="" />
                        </li>
                       
                       
                     </ul>
                  </form>
               </div>

               
               <div class="btn-wrapp">
                  <button class="button">Save</button>
                  <button class="button">Cancle</button>
               </div>
            </div>
         </div>
      </div>
   </div>



<div class="modal fade video-modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">How to Integrate Facebook pixel with Your Online Store</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="video-div">
               <iframe width="100%" height="100%" src="https://www.youtube.com/embed/yAoLSRbwxL8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
         </div>
      </div>
   </div>
</div>   
</>
);
}

export default Integrations;