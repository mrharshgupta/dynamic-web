import React, {useState} from 'react';
import Popups from '../../../popups/index';
import '../../../../css/style.css';
import '../../../../css/device.css';
import '../../../../css/editor.css';
import '../../../../css/main.css';
import '../../../../css/sanjana.css';
import '../../../../css/bootstrap.min.css';


const Options = (props) => {

return(
<>
   <div class="wrap-right">
      <div class="page-nav">
         <div class="page-nav-left">
            <div class="page-action">
               <a href="javascript:void(0)" class="button search">Search</a>
               <a href="javascript:void(0)" class="button">export</a>
              
            </div>
            <h1 class="pg-ttl">Product Option</h1>
         </div>
         <div class="page-nav-right">
            <div class="paging">
               <input type="text" name="showRec" id="showRec" value="20" class="show-rec" original-title="Show Records" />
               <div class="result">1 – 20 of 42</div>
               <a class="prev" href="#"><i class="fas fa-caret-left"></i></a>
               <a class="next" href="#"><i class="fas fa-caret-right"></i></a>
            </div>
        
                <a href="javascript:void(0)" class="button">delete</a>
                 <a href="pro-optionupdate.php" class="button">Create New Option</a>
                
         </div>
      </div>
      <div class="wrap-data">
         <table class="tbl-list">
            <thead>
               <tr>
                  <th><i class="chk"><input type="checkbox" class="chkAll" title="Select All" /><em></em></i></th>
                  <th><a class="sort" href="#">Option</a></th>
                  <th><a class="sort" href="#">Action</a></th>
               </tr>
            </thead>
            <tbody>
               <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td>
                     <a href="pro-optionupdate.php">Color</a>
                  </td>
                  <td>
                     <ul class="action-icon">
                        <li><a href="pro-optionupdate.php"><i class="act-icon text-green fas fa-edit"></i></a></li>
                        <li><a href="#"><i class="act-icon text-green far fa-minus-square"></i></a></li>
                        <li><a href="#"><i class="act-icon text-red far fa-trash-alt"></i></a></li>
                     </ul>
                  </td>
               </tr>

                <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td>
                     <a href="pro-optionupdate.php">Size</a>
                  </td>
                  <td>
                     <ul class="action-icon">
                        <li><a href="pro-optionupdate.php"><i class="act-icon text-green fas fa-edit"></i></a></li>
                        <li><a href="#"><i class="act-icon text-green far fa-minus-square"></i></a></li>
                        <li><a href="#"><i class="act-icon text-red far fa-trash-alt"></i></a></li>
                     </ul>
                  </td>
               </tr>

                <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td>
                     <a href="pro-optionupdate.php">Storage Capacity</a>
                  </td>
                  <td>
                     <ul class="action-icon">
                        <li><a href="pro-optionupdate.php"><i class="act-icon text-green fas fa-edit"></i></a></li>
                        <li><a href="#"><i class="act-icon text-green far fa-minus-square"></i></a></li>
                        <li><a href="#"><i class="act-icon text-red far fa-trash-alt"></i></a></li>
                     </ul>
                  </td>
               </tr>


               


              
            </tbody>
         </table>
      </div>
   </div>

</>
);
}

export default Options;