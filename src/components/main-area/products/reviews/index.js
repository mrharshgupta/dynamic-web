import React, {useState} from 'react';
import Popups from '../../../popups/index';
import '../../../../css/style.css';
import '../../../../css/device.css';
import '../../../../css/editor.css';
import '../../../../css/main.css';
import '../../../../css/sanjana.css';
import '../../../../css/bootstrap.min.css';


const Reviews = (props) => {

return(
<>
   <div class="wrap-right">
      <div class="page-nav">
         <div class="page-nav-left">
            <div class="page-action">
               <a href="javascript:void(0)" class="button">export</a>
               <a href="javascript:void(0)" class="button">archive</a>
            </div>
            <h1 class="pg-ttl">Product Reviews</h1>
         </div>
         <div class="page-nav-right">
            <div class="paging">
               <input type="text" name="showRec" id="showRec" value="20" class="show-rec" original-title="Show Records" />
               <div class="result">1 – 20 of 42</div>
               <a class="prev" href="#"><i class="fas fa-caret-left"></i></a>
               <a class="next" href="#"><i class="fas fa-caret-right"></i></a>
            </div>
         </div>
      </div>
      <div class="wrap-data">
         <table class="tbl-list">
            <thead>
               <tr>
                  <th><i class="chk"><input type="checkbox" class="chkAll" title="Select All" /><em></em></i></th>
                  <th><a class="sort" href="#">Product</a></th>
                  <th><a class="sort" href="#">Author</a></th>
                  <th><a class="sort" href="#">Rating</a></th>
                  <th><a class="sort" href="#">Date Added</a></th>
                  <th><a class="sort" href="#">Action</a></th>
               </tr>
            </thead>
            <tbody>
               <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td><a href="update-proreview.php">XXL Jack & Jones Fashion Joggers st</a></td>
                  <td>1</td>
                  <td>
                     <ul class="action-icon">
                        <li><a href="#"><i class="act-icon text-green fas fa-star"></i></a></li>
                        <li><a href="#"><i class="act-icon text-green fas fa-star"></i></a></li>
                        <li><a href="#"><i class="act-icon text-green fas fa-star"></i></a></li>
                        <li><a href="#"><i class="act-icon text-green far fa-star"></i></a></li>
                     </ul>
                  </td>
                  <td>2017-05-07 00:00:00</td>
                  <td>
                     <ul class="action-icon">
                        <li><a href="update-proreview.php"><i class="act-icon text-green fas fa-edit"></i></a></li>
                        <li><a href="#"><i class="act-icon text-red far fa-trash-alt"></i></a></li>
                     </ul>
                  </td>
               </tr>
               <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td><a href="update-proreview.php">XXL Jack & Jones Fashion Joggers st</a></td>
                  <td>1</td>
                  <td>
                     <ul class="action-icon">
                        <li><a href="#"><i class="act-icon text-green fas fa-star"></i></a></li>
                        <li><a href="#"><i class="act-icon text-green fas fa-star"></i></a></li>
                        <li><a href="#"><i class="act-icon text-green fas fa-star"></i></a></li>
                        <li><a href="#"><i class="act-icon text-green far fa-star"></i></a></li>
                     </ul>
                  </td>
                  <td>2017-05-07 00:00:00</td>
                  <td>
                     <ul class="action-icon">
                        <li><a href="update-proreview.php"><i class="act-icon text-green fas fa-edit"></i></a></li>
                        <li><a href="#"><i class="act-icon text-red far fa-trash-alt"></i></a></li>
                     </ul>
                  </td>
               </tr>
               <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td><a href="update-proreview.php">XXL Jack & Jones Fashion Joggers st</a></td>
                  <td>1</td>
                  <td>
                     <ul class="action-icon">
                        <li><a href="#"><i class="act-icon text-green fas fa-star"></i></a></li>
                        <li><a href="#"><i class="act-icon text-green fas fa-star"></i></a></li>
                        <li><a href="#"><i class="act-icon text-green fas fa-star"></i></a></li>
                        <li><a href="#"><i class="act-icon text-green far fa-star"></i></a></li>
                     </ul>
                  </td>
                  <td>2017-05-07 00:00:00</td>
                  <td>
                     <ul class="action-icon">
                        <li><a href="update-proreview.php"><i class="act-icon text-green fas fa-edit"></i></a></li>
                        <li><a href="#"><i class="act-icon text-red far fa-trash-alt"></i></a></li>
                     </ul>
                  </td>
               </tr>
               <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td><a href="update-proreview.php">XXL Jack & Jones Fashion Joggers st</a></td>
                  <td>1</td>
                  <td>
                     <ul class="action-icon">
                        <li><a href="#"><i class="act-icon text-green fas fa-star"></i></a></li>
                        <li><a href="#"><i class="act-icon text-green fas fa-star"></i></a></li>
                        <li><a href="#"><i class="act-icon text-green fas fa-star"></i></a></li>
                        <li><a href="#"><i class="act-icon text-green far fa-star"></i></a></li>
                     </ul>
                  </td>
                  <td>2017-05-07 00:00:00</td>
                  <td>
                     <ul class="action-icon">
                        <li><a href="update-proreview.php"><i class="act-icon text-green fas fa-edit"></i></a></li>
                        <li><a href="#"><i class="act-icon text-red far fa-trash-alt"></i></a></li>
                     </ul>
                  </td>
               </tr>
               <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td><a href="update-proreview.php">XXL Jack & Jones Fashion Joggers st</a></td>
                  <td>1</td>
                  <td>
                     <ul class="action-icon">
                        <li><a href="#"><i class="act-icon text-green fas fa-star"></i></a></li>
                        <li><a href="#"><i class="act-icon text-green fas fa-star"></i></a></li>
                        <li><a href="#"><i class="act-icon text-green fas fa-star"></i></a></li>
                        <li><a href="#"><i class="act-icon text-green far fa-star"></i></a></li>
                     </ul>
                  </td>
                  <td>2017-05-07 00:00:00</td>
                  <td>
                     <ul class="action-icon">
                        <li><a href="update-proreview.php"><i class="act-icon text-green fas fa-edit"></i></a></li>
                        <li><a href="#"><i class="act-icon text-red far fa-trash-alt"></i></a></li>
                     </ul>
                  </td>
               </tr>
               <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td><a href="update-proreview.php">XXL Jack & Jones Fashion Joggers st</a></td>
                  <td>1</td>
                  <td>
                     <ul class="action-icon">
                        <li><a href="#"><i class="act-icon text-green fas fa-star"></i></a></li>
                        <li><a href="#"><i class="act-icon text-green fas fa-star"></i></a></li>
                        <li><a href="#"><i class="act-icon text-green fas fa-star"></i></a></li>
                        <li><a href="#"><i class="act-icon text-green far fa-star"></i></a></li>
                     </ul>
                  </td>
                  <td>2017-05-07 00:00:00</td>
                  <td>
                     <ul class="action-icon">
                        <li><a href="update-proreview.php"><i class="act-icon text-green fas fa-edit"></i></a></li>
                        <li><a href="#"><i class="act-icon text-red far fa-trash-alt"></i></a></li>
                     </ul>
                  </td>
               </tr>
            </tbody>
         </table>
      </div>
   </div>

</>
);
}

export default Reviews;