import React, {useState} from 'react';
import Popups from '../../../popups/index';
import '../../../../css/style.css';
import '../../../../css/device.css';
import '../../../../css/editor.css';
import '../../../../css/main.css';
import '../../../../css/sanjana.css';
import '../../../../css/bootstrap.min.css';

const Products = (props) => {

return(
<>
<div class="wrapper">
   
   <div class="wrap-right">
      <div class="page-nav">
         <div class="page-nav-left">
            <div class="page-action">
               <a href="javascript:void(0)" class="button advsearch">Advanced search</a>
               <a href="export-product.php" class="button">export</a>
               <a href="import-product.php" class="button">import</a>
            </div>
            <h1 class="pg-ttl">Products</h1>
         </div>
         <div class="page-nav-right">
            <div class="paging">
               <input type="text" name="showRec" id="showRec" value="20" class="show-rec" original-title="Show Records" />
               <div class="result">1 – 20 of 42</div>
               <a class="prev" href="#"><i class="fas fa-caret-left"></i></a>
               <a class="next" href="#"><i class="fas fa-caret-right"></i></a>
            </div>
             <a href="pro-update.php" class="button">Add New Product</a>
             
            <a href="#" class="button" data-toggle="modal" data-target="#exampleModal">Video Guide</a>
         </div>
      </div>
      <div class="wrap-data">
         <table class="tbl-list">
            <thead>
               <tr>
                  <th><i class="chk"><input type="checkbox" class="chkAll" title="Select All" /><em></em></i></th>
                  <th width="10%"><a class="sort" href="#">Image</a></th>
                  <th width="10%"><a class="sort" href="#">Product Name</a></th>
                  <th width="15%"><a class="asc" href="#">Categories</a></th>
                  <th><a class="sort" href="#">Modal</a></th>
                  <th><a class="sort" href="#">Price</a></th>
                  <th><a class="sort" href="#">Quantity</a></th>
                  <th><a class="sort" href="#">Status</a></th>
                  <th><a class="sort" href="#">Date Added</a></th>
                  <th><a class="sort" href="#">Action</a></th>
               </tr>
            </thead>
            <tbody>
               <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td><img src="images/product1.jpg" alt="img" class="img-fluid pro-img" /></td>
                  <td>Samsung 32 - Inch HDD TV - UA32K400</td>
                  <td>
                     <ul class="cat-list">
                        <li>Home</li>
                        <li>tv</li>
                     </ul>
                  </td>
                  <td>Lorem Ipsum</td>
                  <td>3,499.00 INR</td>
                  <td>1</td>
                  <td>
                     <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch1" />
                        <label class="custom-control-label" for="customSwitch1">Enable</label>
                     </div>
                  </td>
                  <td>2017-10-23 10:42:35</td>
                  <td>
                     <ul class="action-icon">
                        <li><a href="#"><i class="act-icon text-blue fa fa-eye fa-lg"></i></a></li>
                        <li><a href="pro-update.php"><i class="act-icon text-green fas fa-edit"></i></a></li>
                        <li><a href="#"><i class="act-icon text-red far fa-trash-alt"></i></a></li>
                     </ul>
                  </td>
               </tr>
               <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td><img src="images/product1.jpg" alt="img" class="img-fluid pro-img" /></td>
                  <td>Samsung 32 - Inch HDD TV - UA32K400</td>
                  <td>
                     <ul class="cat-list">
                        <li>Home</li>
                        <li>tv</li>
                     </ul>
                  </td>
                  <td>Lorem Ipsum</td>
                  <td>3,499.00 INR</td>
                  <td>1</td>
                  <td>
                     <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch2" />
                        <label class="custom-control-label" for="customSwitch2">Enable</label>
                     </div>
                  </td>
                  <td>2017-10-23 10:42:35</td>
                  <td>
                     <ul class="action-icon">
                        <li><a href="#"><i class="act-icon text-blue fa fa-eye fa-lg"></i></a></li>
                        <li><a href="pro-update.php"><i class="act-icon text-green fas fa-edit"></i></a></li>
                        <li><a href="#"><i class="act-icon text-red far fa-trash-alt"></i></a></li>
                     </ul>
                  </td>
               </tr>
               <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td><img src="images/product1.jpg" alt="img" class="img-fluid pro-img" /></td>
                  <td>Samsung 32 - Inch HDD TV - UA32K400</td>
                  <td>
                     <ul class="cat-list">
                        <li>Home</li>
                        <li>tv</li>
                     </ul>
                  </td>
                  <td>Lorem Ipsum</td>
                  <td>3,499.00 INR</td>
                  <td>1</td>
                  <td>
                     <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch3" />
                        <label class="custom-control-label" for="customSwitch3">Enable</label>
                     </div>
                  </td>
                  <td>2017-10-23 10:42:35</td>
                  <td>
                     <ul class="action-icon">
                        <li><a href="#"><i class="act-icon text-blue fa fa-eye fa-lg"></i></a></li>
                        <li><a href="pro-update.php"><i class="act-icon text-green fas fa-edit"></i></a></li>
                        <li><a href="#"><i class="act-icon text-red far fa-trash-alt"></i></a></li>
                     </ul>
                  </td>
               </tr>
               <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td><img src="images/product1.jpg" alt="img" class="img-fluid pro-img" /></td>
                  <td>Samsung 32 - Inch HDD TV - UA32K400</td>
                  <td>
                     <ul class="cat-list">
                        <li>Home</li>
                        <li>tv</li>
                     </ul>
                  </td>
                  <td>Lorem Ipsum</td>
                  <td>3,499.00 INR</td>
                  <td>1</td>
                  <td>
                     <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch4" />
                        <label class="custom-control-label" for="customSwitch4">Enable</label>
                     </div>
                  </td>
                  <td>2017-10-23 10:42:35</td>
                  <td>
                     <ul class="action-icon">
                        <li><a href="#"><i class="act-icon text-blue fa fa-eye fa-lg"></i></a></li>
                        <li><a href="pro-update.php"><i class="act-icon text-green fas fa-edit"></i></a></li>
                        <li><a href="#"><i class="act-icon text-red far fa-trash-alt"></i></a></li>
                     </ul>
                  </td>
               </tr>
               <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td><img src="images/product1.jpg" alt="img" class="img-fluid pro-img" /></td>
                  <td>Samsung 32 - Inch HDD TV - UA32K400</td>
                  <td>
                     <ul class="cat-list">
                        <li>Home</li>
                        <li>tv</li>
                     </ul>
                  </td>
                  <td>Lorem Ipsum</td>
                  <td>3,499.00 INR</td>
                  <td>1</td>
                  <td>
                     <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch5" />
                        <label class="custom-control-label" for="customSwitch5">Enable</label>
                     </div>
                  </td>
                  <td>2017-10-23 10:42:35</td>
                  <td>
                     <ul class="action-icon">
                        <li><a href="#"><i class="act-icon text-blue fa fa-eye fa-lg"></i></a></li>
                        <li><a href="pro-update.php"><i class="act-icon text-green fas fa-edit"></i></a></li>
                        <li><a href="#"><i class="act-icon text-red far fa-trash-alt"></i></a></li>
                     </ul>
                  </td>
               </tr>
               <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td><img src="images/product1.jpg" alt="img" class="img-fluid pro-img" /></td>
                  <td>Samsung 32 - Inch HDD TV - UA32K400</td>
                  <td>
                     <ul class="cat-list">
                        <li>Home</li>
                        <li>tv</li>
                     </ul>
                  </td>
                  <td>Lorem Ipsum</td>
                  <td>3,499.00 INR</td>
                  <td>1</td>
                  <td>
                     <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch6" />
                        <label class="custom-control-label" for="customSwitch6">Enable</label>
                     </div>
                  </td>
                  <td>2017-10-23 10:42:35</td>
                  <td>
                     <ul class="action-icon">
                        <li><a href="#"><i class="act-icon text-blue fa fa-eye fa-lg"></i></a></li>
                        <li><a href="pro-update.php"><i class="act-icon text-green fas fa-edit"></i></a></li>
                        <li><a href="#"><i class="act-icon text-red far fa-trash-alt"></i></a></li>
                     </ul>
                  </td>
               </tr>
               <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td><img src="images/product1.jpg" alt="img" class="img-fluid pro-img" /></td>
                  <td>Samsung 32 - Inch HDD TV - UA32K400</td>
                  <td>
                     <ul class="cat-list">
                        <li>Home</li>
                        <li>tv</li>
                     </ul>
                  </td>
                  <td>Lorem Ipsum</td>
                  <td>3,499.00 INR</td>
                  <td>1</td>
                  <td>
                     <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch7" />
                        <label class="custom-control-label" for="customSwitch7">Enable</label>
                     </div>
                  </td>
                  <td>2017-10-23 10:42:35</td>
                  <td>
                     <ul class="action-icon">
                        <li><a href="#"><i class="act-icon text-blue fa fa-eye fa-lg"></i></a></li>
                        <li><a href="pro-update.php"><i class="act-icon text-green fas fa-edit"></i></a></li>
                        <li><a href="#"><i class="act-icon text-red far fa-trash-alt"></i></a></li>
                     </ul>
                  </td>
               </tr>
               <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td><img src="images/product1.jpg" alt="img" class="img-fluid pro-img" /></td>
                  <td>Samsung 32 - Inch HDD TV - UA32K400</td>
                  <td>
                     <ul class="cat-list">
                        <li>Home</li>
                        <li>tv</li>
                     </ul>
                  </td>
                  <td>Lorem Ipsum</td>
                  <td>3,499.00 INR</td>
                  <td>1</td>
                  <td>
                     <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch8" />
                        <label class="custom-control-label" for="customSwitch8">Enable</label>
                     </div>
                  </td>
                  <td>2017-10-23 10:42:35</td>
                  <td>
                     <ul class="action-icon">
                        <li><a href="#"><i class="act-icon text-blue fa fa-eye fa-lg"></i></a></li>
                        <li><a href="pro-update.php"><i class="act-icon text-green fas fa-edit"></i></a></li>
                        <li><a href="#"><i class="act-icon text-red far fa-trash-alt"></i></a></li>
                     </ul>
                  </td>
               </tr>
               <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td><img src="images/product1.jpg" alt="img" class="img-fluid pro-img" /></td>
                  <td>Samsung 32 - Inch HDD TV - UA32K400</td>
                  <td>
                     <ul class="cat-list">
                        <li>Home</li>
                        <li>tv</li>
                     </ul>
                  </td>
                  <td>Lorem Ipsum</td>
                  <td>3,499.00 INR</td>
                  <td>1</td>
                  <td>
                     <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch9" />
                        <label class="custom-control-label" for="customSwitch9">Enable</label>
                     </div>
                  </td>
                  <td>2017-10-23 10:42:35</td>
                  <td>
                     <ul class="action-icon">
                        <li><a href="#"><i class="act-icon text-blue fa fa-eye fa-lg"></i></a></li>
                        <li><a href="pro-update.php"><i class="act-icon text-green fas fa-edit"></i></a></li>
                        <li><a href="#"><i class="act-icon text-red far fa-trash-alt"></i></a></li>
                     </ul>
                  </td>
               </tr>
            </tbody>
         </table>
      </div>
   </div>
</div>



<div class="modal fade video-modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">How to Add a Product to Your Online Store</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="video-div">
               <iframe width="100%" height="100%" src="https://www.youtube.com/embed/yAoLSRbwxL8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
         </div>
      </div>
   </div>
</div>

</>
);
}

export default Products;