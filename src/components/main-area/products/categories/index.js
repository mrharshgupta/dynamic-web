import React, {useState} from 'react';
import Popups from '../../../popups/index';
import '../../../../css/style.css';
import '../../../../css/device.css';
import '../../../../css/editor.css';
import '../../../../css/main.css';
import '../../../../css/sanjana.css';
import '../../../../css/bootstrap.min.css';


const Categories = (props) => {

return(
	<div class="wrapper">
   
   <div class="wrap-right">
      <div class="page-nav">
         <div class="page-nav-left">
            <div class="page-action">
               <a href="javascript:void(0)" class="button search">Search</a>
               <a href="javascript:void(0)" class="button">export</a>
               <a href="javascript:void(0)" class="button">import</a>
            </div>
            <h1 class="pg-ttl">Product Categories</h1>
         </div>
         <div class="page-nav-right">
            <div class="paging">
               <input type="text" name="showRec" id="showRec" value="20" class="show-rec" original-title="Show Records" />
               <div class="result">1 – 20 of 42</div>
               <a class="prev" href="#"><i class="fas fa-caret-left"></i></a>
               <a class="next" href="#"><i class="fas fa-caret-right"></i></a>
            </div>
            <a href="javascript:void(0)" class="button">Create Return</a>
               
                <a href="pro-update.php" class="button">Create New Category</a>
                
         </div>
      </div>
      <div class="wrap-data">
         <table class="tbl-list">
            <thead>
               <tr>
                  <th><i class="chk"><input type="checkbox" class="chkAll" title="Select All" /><em></em></i></th>
                  <th><a class="sort" href="#">Image</a></th>
                  <th><a class="sort" href="#">Category Name</a></th>
                  <th><a class="sort" href="#">Status</a></th>
                  <th><a class="sort" href="#">Action</a></th>
               </tr>
            </thead>
            <tbody>
               <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td><img src="images/product1.jpg" alt="img" class="img-fluid pro-img" /></td>
                  <td><a href="pro-catupdate.php">Samsung 32 - Inch HDD TV - UA32K400</a></td>
                  <td>
                     <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch1" checked />
                        <label class="custom-control-label" for="customSwitch1">Enable</label>
                     </div>
                  </td>
                  <td>
                     <ul class="action-icon">
                        <li><a href="#"><i class="act-icon text-blue fa fa-eye fa-lg"></i></a></li>
                        <li><a href="pro-catupdate.php"><i class="act-icon text-green fas fa-edit"></i></a></li>
                        <li><a href="#"><i class="act-icon text-red far fa-trash-alt"></i></a></li>
                     </ul>
                  </td>
               </tr>

                <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td><img src="images/product1.jpg" alt="img" class="img-fluid pro-img" /></td>
                  <td><a href="pro-catupdate.php">Samsung 32 - Inch HDD TV - UA32K400</a></td>
                  <td>
                     <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch1" checked />
                        <label class="custom-control-label" for="customSwitch1">Enable</label>
                     </div>
                  </td>
                  <td>
                     <ul class="action-icon">
                        <li><a href="#"><i class="act-icon text-blue fa fa-eye fa-lg"></i></a></li>
                        <li><a href="pro-catupdate.php"><i class="act-icon text-green fas fa-edit"></i></a></li>
                        <li><a href="#"><i class="act-icon text-red far fa-trash-alt"></i></a></li>
                     </ul>
                  </td>
               </tr>

                <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td><img src="images/product1.jpg" alt="img" class="img-fluid pro-img" /></td>
                  <td><a href="pro-catupdate.php">Samsung 32 - Inch HDD TV - UA32K400</a></td>
                  <td>
                     <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch1" checked />
                        <label class="custom-control-label" for="customSwitch1">Enable</label>
                     </div>
                  </td>
                  <td>
                     <ul class="action-icon">
                        <li><a href="#"><i class="act-icon text-blue fa fa-eye fa-lg"></i></a></li>
                        <li><a href="pro-catupdate.php"><i class="act-icon text-green fas fa-edit"></i></a></li>
                        <li><a href="#"><i class="act-icon text-red far fa-trash-alt"></i></a></li>
                     </ul>
                  </td>
               </tr>

                <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td><img src="images/product1.jpg" alt="img" class="img-fluid pro-img" /></td>
                  <td><a href="pro-catupdate.php">Samsung 32 - Inch HDD TV - UA32K400</a></td>
                  <td>
                     <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch1" checked />
                        <label class="custom-control-label" for="customSwitch1">Enable</label>
                     </div>
                  </td>
                  <td>
                     <ul class="action-icon">
                        <li><a href="#"><i class="act-icon text-blue fa fa-eye fa-lg"></i></a></li>
                        <li><a href="pro-catupdate.php"><i class="act-icon text-green fas fa-edit"></i></a></li>
                        <li><a href="#"><i class="act-icon text-red far fa-trash-alt"></i></a></li>
                     </ul>
                  </td>
               </tr>

                <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td><img src="images/product1.jpg" alt="img" class="img-fluid pro-img" /></td>
                  <td><a href="pro-catupdate.php">Samsung 32 - Inch HDD TV - UA32K400</a></td>
                  <td>
                     <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch1" checked />
                        <label class="custom-control-label" for="customSwitch1">Enable</label>
                     </div>
                  </td>
                  <td>
                     <ul class="action-icon">
                        <li><a href="#"><i class="act-icon text-blue fa fa-eye fa-lg"></i></a></li>
                        <li><a href="pro-catupdate.php"><i class="act-icon text-green fas fa-edit"></i></a></li>
                        <li><a href="#"><i class="act-icon text-red far fa-trash-alt"></i></a></li>
                     </ul>
                  </td>
               </tr>


              
            </tbody>
         </table>
      </div>
   </div>
</div>



);
}

export default Categories;