import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import Popups from '../../popups/index';
import styles from './styles.js';
import '../../../css/style.css';
import '../../../css/device.css';
import '../../../css/editor.css';
import '../../../css/main.css';
import '../../../css/sanjana.css';
import '../../../css/bootstrap.min.css';

const OrderList = (props) => {
    const [showSearch, setshowSearch] = useState(false);
return(



<div class="wrapper">
    
    <div class="wrap-right">
        <div class="page-nav">
            <div class="page-nav-left">
                <div class="page-action">
                    <Link to="#" style={styles.link} className="button search" onClick={()=>setshowSearch(!showSearch)}>search</Link>
                    <Link to="#" style={styles.link} className="button">export</Link>
                    <Link to="#" style={styles.link} className="button">import</Link>
                    <Link to="#" style={styles.link} className="button">print order</Link>
                </div>
                <h1 class="pg-ttl">Orders</h1>
            </div>
            <div class="page-nav-right">
                <div class="paging">
                    <input type="text" name="showRec" id="showRec" value="20" class="show-rec" original-title="Show Records" />
                    <div class="result">1 – 20 of 42</div>
                    <a class="prev" href="#"><i class="fas fa-caret-left"></i></a>
                    <a class="next" href="#"><i class="fas fa-caret-right"></i></a>
                </div>
                
                <Link to="#" style={styles.link} className="button">Export</Link>
                    <Link to="#" style={styles.link} className="button">Delete</Link>
                    <Link to="#" style={styles.link} className="button">Archive</Link>
            </div>
        </div>
        <div class="wrap-data" style={styles.wrapData}>
            <table class="tbl-list">
                <thead>
                    <tr>
                        <th><i class="chk"><input type="checkbox" class="chkAll" title="Select All" /><em></em></i></th>
                        <th width="10%"><a class="sort" href="#">Order Id</a></th>
                        <th width="10%"><a class="sort" href="#">Customer</a></th>
                        <th width="15%"><a class="asc" href="#">Full Address</a></th>
                        <th><a class="sort" href="#">Phone Number</a></th>
                        <th><a class="sort" href="#">Shipping Method</a></th>
                        <th><a class="sort" href="#">Status</a></th>
                        <th><a class="sort" href="#">Status Comment</a></th>
                        <th><a class="sort" href="#">Total</a></th>
                        <th><a class="sort" href="#">Date Added</a></th>
                        <th><a class="sort" href="#">Date Modified</a></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                        <td>1</td>
                        <td>User One</td>
                        <td>Cecilia Chapman 711-2880 Nulla St. Mankato Mississippi 96522.</td>
                        <td>9876543210</td>
                        <td>Lorem Ipsum</td>
                        <td >Lorem Ipsum</td>
                        <td>Lorem Ipsum</td>
                        <td>987</td>
                        <td>20/05/2020</td>
                        <td>24/05/2020</td>
                    </tr>
                    <tr>
                        <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                        <td>1</td>
                        <td>User One</td>
                        <td>Cecilia Chapman 711-2880 Nulla St. Mankato Mississippi 96522.</td>
                        <td>9876543210</td>
                        <td>Lorem Ipsum</td>
                        <td >Lorem Ipsum</td>
                        <td>Lorem Ipsum</td>
                        <td>987</td>
                        <td>20/05/2020</td>
                        <td>24/05/2020</td>
                    </tr>
                    <tr>
                        <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                        <td>1</td>
                        <td>User One</td>
                        <td>Cecilia Chapman 711-2880 Nulla St. Mankato Mississippi 96522.</td>
                        <td>9876543210</td>
                        <td>Lorem Ipsum</td>
                        <td >Lorem Ipsum</td>
                        <td>Lorem Ipsum</td>
                        <td>987</td>
                        <td>20/05/2020</td>
                        <td>24/05/2020</td>
                    </tr>
                    <tr>
                        <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                        <td>1</td>
                        <td>User One</td>
                        <td>Cecilia Chapman 711-2880 Nulla St. Mankato Mississippi 96522.</td>
                        <td>9876543210</td>
                        <td>Lorem Ipsum</td>
                        <td >Lorem Ipsum</td>
                        <td>Lorem Ipsum</td>
                        <td>987</td>
                        <td>20/05/2020</td>
                        <td>24/05/2020</td>
                    </tr>
                    <tr>
                        <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                        <td>1</td>
                        <td>User One</td>
                        <td>Cecilia Chapman 711-2880 Nulla St. Mankato Mississippi 96522.</td>
                        <td>9876543210</td>
                        <td>Lorem Ipsum</td>
                        <td >Lorem Ipsum</td>
                        <td>Lorem Ipsum</td>
                        <td>987</td>
                        <td>20/05/2020</td>
                        <td>24/05/2020</td>
                    </tr>
                    <tr>
                        <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                        <td>1</td>
                        <td>User One</td>
                        <td>Cecilia Chapman 711-2880 Nulla St. Mankato Mississippi 96522.</td>
                        <td>9876543210</td>
                        <td>Lorem Ipsum</td>
                        <td >Lorem Ipsum</td>
                        <td>Lorem Ipsum</td>
                        <td>987</td>
                        <td>20/05/2020</td>
                        <td>24/05/2020</td>
                    </tr>
                    <tr>
                        <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                        <td>1</td>
                        <td>User One</td>
                        <td>Cecilia Chapman 711-2880 Nulla St. Mankato Mississippi 96522.</td>
                        <td>9876543210</td>
                        <td>Lorem Ipsum</td>
                        <td >Lorem Ipsum</td>
                        <td>Lorem Ipsum</td>
                        <td>987</td>
                        <td>20/05/2020</td>
                        <td>24/05/2020</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <Popups showSearch={showSearch} />
</div>


	);}
export default OrderList;