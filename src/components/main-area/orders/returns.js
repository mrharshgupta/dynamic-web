import React, {useState} from 'react';
import styles from './styles.js';
import {Link} from 'react-router-dom';
import Popups from '../../popups/index';
import '../../../css/style.css';
import '../../../css/device.css';
import '../../../css/editor.css';
import '../../../css/main.css';
import '../../../css/sanjana.css';
import '../../../css/bootstrap.min.css';

const Returns = (props) => {
const [showSearch, setshowSearch] = useState(false);
return(
<div class="wrap-right">
        <div class="page-nav">
            <div class="page-nav-left">
                <div class="page-action">
                    <Link to="#" style={styles.link} className="button search" onClick={()=>setshowSearch(!showSearch)}>search</Link>
                    
                    <Link to="#" style={styles.link} className="button">Export</Link>
                <Link to="#" style={styles.link} className="button">Import</Link>
                <Link to="#" style={styles.link} className="button">Print Order</Link>
                </div>
                <h1 class="pg-ttl">Returns</h1>
            </div>
            <div class="page-nav-right">
                <div class="paging">
                    <input type="text" name="showRec" id="showRec" value="20" class="show-rec" original-title="Show Records" />
                    <div class="result">1 – 20 of 42</div>
                    <a class="prev" href="#"><i class="fas fa-caret-left"></i></a>
                    <a class="next" href="#"><i class="fas fa-caret-right"></i></a>
                </div>
                <Link to="#" style={styles.link} className="button">Create Return</Link>
                <Link to="#" style={styles.link} className="button">delete</Link>
                <Link to="#" style={styles.link} className="button">archive</Link>
            </div>

        </div>
        <div class="wrap-data" style={styles.wrapData}>
            <table class="tbl-list">
                <thead>
                    <tr>
                        <th><i class="chk"><input type="checkbox" class="chkAll" title="Select All" /><em></em></i></th>
                        <th width="10%"><a class="sort" href="#">Return Id</a></th>
                        <th width="10%"><a class="sort" href="#">Order ID</a></th>
                        <th width="15%"><a class="asc" href="#">Customer</a></th>
                        <th><a class="sort" href="#">Status</a></th>
                        <th><a class="sort" href="#">Date Added</a></th>
                        <th><a class="sort" href="#">Date Modified</a></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                        <td>1</td>
                        <td>2</td>
                        <td>User 2</td>
                        <td>Returned</td>
                        <td>20/05/2020</td>
                        <td >25/05/2020</td>
                    </tr>
                    <tr>
                        <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                        <td>1</td>
                        <td>2</td>
                        <td>User 2</td>
                        <td>Returned</td>
                        <td>20/05/2020</td>
                        <td >25/05/2020</td>
                    </tr>
                    <tr>
                        <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                        <td>1</td>
                        <td>2</td>
                        <td>User 2</td>
                        <td>Returned</td>
                        <td>20/05/2020</td>
                        <td >25/05/2020</td>
                    </tr>
                    <tr>
                        <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                        <td>1</td>
                        <td>2</td>
                        <td>User 2</td>
                        <td>Returned</td>
                        <td>20/05/2020</td>
                        <td >25/05/2020</td>
                    </tr>
                    <tr>
                        <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                        <td>1</td>
                        <td>2</td>
                        <td>User 2</td>
                        <td>Returned</td>
                        <td>20/05/2020</td>
                        <td >25/05/2020</td>
                    </tr>
                    <tr>
                        <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                        <td>1</td>
                        <td>2</td>
                        <td>User 2</td>
                        <td>Returned</td>
                        <td>20/05/2020</td>
                        <td >25/05/2020</td>
                    </tr>
                    <tr>
                        <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                        <td>1</td>
                        <td>2</td>
                        <td>User 2</td>
                        <td>Returned</td>
                        <td>20/05/2020</td>
                        <td >25/05/2020</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <Popups showSearch={showSearch} />
</div>
);
}

export default Returns;