import React, { useState } from "react";
import styles from "./styles";
import imageToBase64 from "image-to-base64";
import { connect, useDispatch } from "react-redux";
import { addDomain } from "../../apis";
import { Link } from "react-router-dom";

const StoreGeneralSettings = ({
  settings = {},
  setsettings,
  imagesToBeUploaded,
  setimagesToBeUploaded,
  serverName,
  setshowModal,
  setshowModalContent,
  onClickSave,
  loginData,
}) => {
  const dispatch = useDispatch();
  const { storeSettings: { general } = {} } = settings;
  const [showDropdown, setshowDropdown] = useState(undefined);
  const [selectedLogo, setselectedLogo] = useState(undefined);
  const [selectedIcon, setselectedIcon] = useState(undefined);

  const fileSelect = (event, type) => {
    var file = event.target.files[0];
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      console.log(reader.result, "base64 sting of selected image");
      if (type === "logo") {
        setsettings(
          { ...settings },
          (settings.storeSettings.general.others.logo = reader.result)
        );
      }
      if (type === "icon") {
        setsettings(
          { ...settings },
          (settings.storeSettings.general.others.favicon = reader.result)
        );
      }
    };
    reader.onerror = function (error) {
      console.log("Error: ", error);
    };
  };
  console.log(settings, "general");
  // const styles = {
  //   container: {
  //     marginTop: "5px",
  //     marginBottom: "5px",
  //     marginRight: "10px",
  //     marginLeft: "10px",
  //     width: "90%",
  //   },

  //   image: { height: "30px", width: "60px" },
  // };
  const addDomainFunction = () => {
    dispatch({ type: "SET_SLIDING_LOADER", data: true });
    addDomain(
      dispatch,
      general.domain.name,
      () => setshowModal(true),
      (abc) => setshowModalContent(abc)
    );
    onClickSave();
  };
  return (
    <div class="col-12 col-lg-9">
      <div class="row">
        <div class="col-12">
          <div class="product-info-wrap">
            <a
              onClick={() =>
                setshowDropdown(
                  showDropdown === "contact-information"
                    ? undefined
                    : "contact-information"
                )
              }
              style={styles.leftBarLink}
              href="#sale-report"
              class="search-btn"
              data-toggle="collapse"
              aria-expanded="false"
            >
              Contact Information
            </a>
            {showDropdown === "contact-information" && (
              <div id="" class="collapse search-form1 show active">
                <form action="" class="col-12">
                  <ul class="row form-div">
                    <li class="col-12 form-group">
                      <label>
                        Store Name <span class="">*</span>
                      </label>
                      <input
                        type="text"
                        class="form-control"
                        placeholder="Store name"
                        value={general.contactInformation.storeName}
                        onChange={(e) => {
                          setsettings(
                            { ...settings },
                            (general.contactInformation.storeName =
                              e.target.value)
                          );
                        }}
                      />
                    </li>
                    <li class="col-12 form-group">
                      <label>
                        Store Description <span class="">*</span>
                        <span class="">*</span>
                      </label>
                      <textarea
                        class="form-control"
                        rows="3"
                        value={general.contactInformation.storeDescription}
                        onChange={(e) => {
                          setsettings(
                            { ...settings },
                            (general.contactInformation.storeDescription =
                              e.target.value)
                          );
                        }}
                      ></textarea>
                    </li>
                    <li class="col-12 form-group">
                      <label>
                        Mobile No<span class="">*</span>
                      </label>
                      <input
                        type="text"
                        class="form-control"
                        value={general.contactInformation.mobileNumber}
                        onChange={(e) => {
                          setsettings(
                            { ...settings },
                            (general.contactInformation.mobileNumber =
                              e.target.value)
                          );
                        }}
                      />
                    </li>
                    <li class="col-12 form-group">
                      <label>
                        Email<span class="">*</span>
                      </label>
                      <input
                        type="email"
                        class="form-control"
                        value={general.contactInformation.email}
                        onChange={(e) => {
                          setsettings(
                            { ...settings },
                            (general.contactInformation.email = e.target.value)
                          );
                        }}
                      />
                    </li>
                    <li class="col-12 form-group">
                      <label>
                        Phone<span class="">*</span>
                      </label>
                      <input
                        type="text"
                        class="form-control"
                        value={general.contactInformation.phone}
                        onChange={(e) => {
                          setsettings(
                            { ...settings },
                            (general.contactInformation.phone = e.target.value)
                          );
                        }}
                      />
                    </li>
                    <li class="col-12 form-group">
                      <label>
                        Enable Live Tracking <span class="">*</span>
                      </label>
                      <div class="custom-control custom-switch">
                        <input
                          type="checkbox"
                          class="custom-control-input"
                          id="customSwitch1"
                          onClick={() =>
                            general.contactInformation.liveTracking === true
                              ? setsettings(
                                  { ...settings },
                                  (general.contactInformation.liveTracking = false)
                                )
                              : setsettings(
                                  { ...settings },
                                  (general.contactInformation.liveTracking = true)
                                )
                          }
                          checked={
                            general.contactInformation.liveTracking === true
                              ? true
                              : false
                          }
                        />
                        <label class="custom-control-label" for="customSwitch1">
                          Enable
                        </label>
                      </div>
                      {/* <div class="custom-control custom-switch">
                                    <input onClick={()=>(addBlogData.status === 'active'? setaddBlogData({...addBlogData, status: 'inactive' }) : setaddBlogData({...addBlogData, status: 'active' }))} checked={addBlogData.status === 'active'? true : false} type="checkbox" class="custom-control-input" id="customSwitch1" />
                                    <label class="custom-control-label" for="customSwitch1">{addBlogData.status}</label>
                                 </div> */}
                    </li>
                  </ul>
                </form>
              </div>
            )}
          </div>
        </div>

        <div class="col-12">
          <div class="product-info-wrap">
            <a
              onClick={() =>
                setshowDropdown(
                  showDropdown === "seo-data" ? undefined : "seo-data"
                )
              }
              style={styles.leftBarLink}
              href="#seo-data"
              class="search-btn"
              data-toggle="collapse"
              aria-expanded="false"
            >
              SEO Data
            </a>
            {showDropdown === "seo-data" && (
              <div id="" class="collapse search-form1 show active">
                <form action="" class="col-12">
                  <ul class="row form-div">
                    <li class="col-12 form-group">
                      <label>
                        Home Page Title <span class="">*</span>
                      </label>
                      <input
                        type="text"
                        class="form-control"
                        value={general.seoData.homePageTitle}
                        onChange={(e) => {
                          setsettings(
                            { ...settings },
                            (general.seoData.homePageTitle = e.target.value)
                          );
                        }}
                      />
                    </li>
                    <li class="col-12 form-group">
                      <label>
                        Homepage Meta Description<span class="">*</span>
                        <span class="">*</span>
                      </label>
                      <textarea
                        class="form-control"
                        rows="3"
                        value={general.seoData.homePageMetaDescription}
                        onChange={(e) => {
                          setsettings(
                            { ...settings },
                            (general.seoData.homePageMetaDescription =
                              e.target.value)
                          );
                        }}
                      ></textarea>
                    </li>
                  </ul>
                </form>
              </div>
            )}
          </div>
        </div>

        <div class="col-12">
          <div class="product-info-wrap">
            <a
              onClick={() =>
                setshowDropdown(
                  showDropdown === "contact-us" ? undefined : "contact-us"
                )
              }
              style={styles.leftBarLink}
              href="#contact-data"
              class="search-btn"
              data-toggle="collapse"
              aria-expanded="false"
            >
              Contact Us Data
            </a>
            {showDropdown === "contact-us" && (
              <div id="" class="collapse search-form1 show active">
                <form action="" class="col-12">
                  <ul class="row form-div">
                    <li class="col-12 form-group">
                      <label>
                        Show Client Phone Number field<span class="">*</span>
                      </label>
                      <div class="custom-control custom-switch">
                        <input
                          type="checkbox"
                          class="custom-control-input"
                          id="customSwitch3"
                          onClick={() =>
                            general.contactUsData.showClientPhoneNumberField ===
                            true
                              ? setsettings(
                                  { ...settings },
                                  (general.contactUsData.showClientPhoneNumberField = false)
                                )
                              : setsettings(
                                  { ...settings },
                                  (general.contactUsData.showClientPhoneNumberField = true)
                                )
                          }
                          checked={
                            general.contactUsData.showClientPhoneNumberField ===
                            true
                              ? true
                              : false
                          }
                        />
                        <label class="custom-control-label" for="customSwitch3">
                          Enable
                        </label>
                      </div>
                      <p class="note-txt">
                        Allow clients to send their phone number with their
                        email message to the Admin through Contact Us page
                      </p>
                    </li>
                    <li class="col-12 form-group">
                      <label>
                        Client Phone Number<span class="">*</span>
                      </label>
                      <div class="custom-control custom-switch">
                        <input
                          type="checkbox"
                          class="custom-control-input"
                          id="customSwitch4"
                          onClick={() =>
                            general.contactUsData.clientPhoneNumber === true
                              ? setsettings(
                                  { ...settings },
                                  (general.contactUsData.clientPhoneNumber = false)
                                )
                              : setsettings(
                                  { ...settings },
                                  (general.contactUsData.clientPhoneNumber = true)
                                )
                          }
                          checked={
                            general.contactUsData.clientPhoneNumber === true
                              ? true
                              : false
                          }
                        />
                        <label class="custom-control-label" for="customSwitch4">
                          Enable
                        </label>
                      </div>
                      <p class="note-txt">
                        Make client phone number field is required to send with
                        their email message to the Admin or not
                      </p>
                    </li>
                  </ul>
                </form>
              </div>
            )}
          </div>
        </div>

        <div class="col-12">
          <div class="product-info-wrap">
            <a
              onClick={() =>
                setshowDropdown(
                  showDropdown === "maintenance-mode"
                    ? undefined
                    : "maintenance-mode"
                )
              }
              style={styles.leftBarLink}
              href="#maintenance"
              class="search-btn"
              data-toggle="collapse"
              aria-expanded="false"
            >
              Maintenance Mode
            </a>
            {showDropdown === "maintenance-mode" && (
              <div id="" class="collapse search-form1 show active">
                <form action="" class="col-12">
                  <ul class="row form-div">
                    <li class="col-12 form-group">
                      {/* <label>Show Client Phone Number field<span class="">*</span></label> */}
                      <div class="custom-control custom-switch">
                        <input
                          type="checkbox"
                          class="custom-control-input"
                          id="customSwitch5"
                          onClick={() =>
                            general.maintenanceMode.enable === true
                              ? setsettings(
                                  { ...settings },
                                  (general.maintenanceMode.enable = false)
                                )
                              : setsettings(
                                  { ...settings },
                                  (general.maintenanceMode.enable = true)
                                )
                          }
                          checked={
                            general.maintenanceMode.enable === true
                              ? true
                              : false
                          }
                        />
                        <label class="custom-control-label" for="customSwitch5">
                          Enable
                        </label>
                      </div>
                      <p class="note-txt">
                        Prevents customers from browsing your store. They will
                        instead see a maintenance message. If logged in as
                        admin, you will see the store as normal.
                      </p>
                    </li>
                  </ul>
                </form>
              </div>
            )}
          </div>
        </div>

        <div class="col-12">
          <div class="product-info-wrap">
            <a
              onClick={() =>
                setshowDropdown(
                  showDropdown === "store-logo" ? undefined : "store-logo"
                )
              }
              style={styles.leftBarLink}
              href="#store-logo"
              class="search-btn"
              data-toggle="collapse"
              aria-expanded="false"
            >
              Store Logo
            </a>
            {showDropdown === "store-logo" && (
              <div id="" class="collapse search-form1 active show">
                <form action="" class="col-12">
                  <ul class="row form-div">
                    <li class="col-12 col-md-6 form-group">
                      <label>
                        Store Logo<span class="">*</span>
                      </label>
                      <div class="img-option">
                        <ul class="cat-list">
                          {/* <li><a href="#" data-toggle="modal" data-target="#Browse-img">Browse</a></li> */}
                          <div
                            class="input-group mb-3"
                            style={styles.container}
                          >
                            <div class="custom-file">
                              <input
                                type="file"
                                class="custom-file-input"
                                id="inputGroupFile01"
                                onChange={(e) => fileSelect(e, "logo")}
                                accept=".png, .jpg, .jpeg"
                              />
                              <label
                                class="custom-file-label"
                                for="inputGroupFile01"
                              >
                                Choose file
                              </label>
                            </div>
                          </div>
                          {/* <li><a href="#">Clear</a></li> */}
                        </ul>
                        <div class="img-icon">
                          {/* <img src={selectedLogo} class="img-fluid" alt="img" /> */}
                          <img
                            src={
                              settings.storeSettings.general.others.logo.includes(
                                "."
                              )
                                ? `https://${serverName}/dq/files/media-manager/${localStorage.getItem(
                                    "userId"
                                  )}/submit/${
                                    settings.storeSettings.general.others.logo
                                  }`
                                : settings.storeSettings.general.others.logo
                            }
                            style={styles.image}
                          />
                        </div>
                      </div>
                    </li>
                    <li class="col-12 col-md-6 form-group">
                      <label>
                        Icon<span class="">*</span>
                      </label>
                      <div class="img-option">
                        <ul class="cat-list">
                          {/* <li><a href="#" data-toggle="modal" data-target="#Browse-img">Browse</a></li> */}
                          <div
                            class="input-group mb-3"
                            style={styles.container}
                          >
                            <div class="custom-file">
                              <input
                                type="file"
                                class="custom-file-input"
                                id="inputGroupFile01"
                                onChange={(e) => fileSelect(e, "icon")}
                                accept=".png, .jpg, .jpeg"
                              />
                              <label
                                class="custom-file-label"
                                for="inputGroupFile01"
                              >
                                Choose file
                              </label>
                            </div>
                          </div>
                          {/* <li><a href="#">Clear</a></li> */}
                        </ul>
                        <div class="img-icon">
                          {/* <img src={selectedIcon} class="img-fluid" alt="img" /> */}
                          <img
                            src={settings.storeSettings.general.others.favicon}
                            style={styles.image}
                          />
                        </div>
                      </div>
                      <p class="note-txt">
                        The icon should be a ico file that is 16px x 16px.
                      </p>
                    </li>
                  </ul>
                </form>
              </div>
            )}
          </div>
        </div>

        <div class="col-12">
          <div class="product-info-wrap">
            <a
              onClick={() =>
                setshowDropdown(
                  showDropdown === "connect-a-domain"
                    ? undefined
                    : "connect-a-domain"
                )
              }
              style={styles.leftBarLink}
              href="#seo-data"
              class="search-btn"
              data-toggle="collapse"
              aria-expanded="false"
            >
              Connect a domain
            </a>
            {showDropdown === "connect-a-domain" && (
              <div id="" class="collapse search-form1 show active">
                {parseInt(loginData?.selectedPlan) !== 1 ? (
                  <form action="" class="col-12">
                    <ul class="row form-div">
                      <li class="col-12 form-group">
                        <label>
                          Enter you domain (please add
                          ns1.md-in-90.webhostbox.net,
                          ns2.md-in-90.webhostbox.net nameservers in your domain
                          provider settings)
                        </label>
                        <input
                          type="text"
                          class="form-control"
                          placeholder="example.com"
                          value={general.domain.name}
                          onChange={(e) => {
                            setsettings(
                              { ...settings },
                              (general.domain.name = e.target.value)
                            );
                          }}
                        />
                      </li>
                      <li>
                        <button
                          style={styles.connectButton}
                          type="button"
                          class="btn btn-primary"
                          onClick={() => addDomainFunction()}
                        >
                          Connect
                        </button>
                      </li>
                    </ul>
                  </form>
                ) : (
                  <div>
                    {" "}
                    <Link
                      to={`/${localStorage.getItem(
                        "storeName"
                      )}/home/subscriptions`}
                      class="button"
                      style={styles.link}
                      data-toggle="modal"
                      data-target="#exampleModal"
                    >
                      <span>Choose a premium plan</span>
                    </Link>
                  </div>
                )}
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = ({ serverName, loginData }) => ({
  serverName: serverName,
  loginData,
});
export default connect(mapStateToProps)(StoreGeneralSettings);
