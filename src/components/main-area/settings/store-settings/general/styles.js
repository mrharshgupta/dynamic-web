const styles = {
  wrapData: {
    height: "80vh",
    overflowY: "scroll",
  },
  container: {
    marginTop: "5px",
    marginBottom: "5px",
    marginRight: "10px",
    marginLeft: "10px",
    width: "90%",
  },

  image: { height: "90px", width: "180px" },
  link: {
    textDecoration: "none",
    color: "white",
  },
  leftBarLink: {
    textDecoration: "none",
    color: "black",
    fontWeight: "bold",
    textDecoration: "none",
    color: "black",
  },
  connectButton: { marginLeft: 15 },
};

export default styles;
