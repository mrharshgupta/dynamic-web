import React, { useState } from "react";
import styles from "./styles";

const SettingsLeftBar = () => {
  const [showDropdown, setshowDropdown] = useState("store-settings");
  return (
    <div class="set-sidewrap">
      <div class="setting-sidebar">
        <a
          style={styles.leftBarLink}
          onClick={() =>
            setshowDropdown(
              showDropdown === "store-settings" ? undefined : "store-settings"
            )
          }
          href="#store-setting"
          class="search-btn active"
          data-toggle="collapse"
          aria-expanded="true"
        >
          Store Settings
        </a>
        {showDropdown == "store-settings" && (
          <div id="" class="collapse search-form1 show active">
            <ul class="setting-list">
              <li>
                <a style={styles.leftBarLink} href="javascript:void(0)">
                  General
                </a>
              </li>
              {/* <li><a style={styles.leftBarLink} href="javascript:void(0)">Shipping</a></li>
        <li><a style={styles.leftBarLink} href="javascript:void(0)">Payment</a></li>
        <li><a style={styles.leftBarLink} href="javascript:void(0)">Taxes</a></li>
        <li><a style={styles.leftBarLink} href="javascript:void(0)">Affiliate</a></li> */}
            </ul>
          </div>
        )}

        {/* <a
          style={styles.leftBarLink}
          onClick={() =>
            setshowDropdown(
              showDropdown === "language-and-region"
                ? undefined
                : "language-and-region"
            )
          }
          href="#lang"
          class="search-btn"
          data-toggle="collapse"
          aria-expanded="false"
        >
          Language & Region
        </a> */}
        {showDropdown === "language-and-region" && (
          <div id="" class="collapse search-form1 show active">
            <ul class="setting-list">
              <li>
                <a href="sett-langgeneral.php">General</a>
              </li>
              {/* <li><a href="sett-currency.php">Currency</a></li>
        <li><a href="sett-language.php">Language</a></li>
        <li><a href="sett-country.php">Countries & Cities</a></li>
            <li><a href="sett-geozone.php">Geo Zone</a></li>
        <li><a href="sett-units.php">Measurement Unit</a></li> */}
            </ul>
          </div>
        )}

        {/* <a style={styles.leftBarLink} onClick={()=>setshowDropdown(showDropdown === "administrations" ? undefined : "administrations" )} href="#administrators" class="search-btn" data-toggle="collapse" aria-expanded="false">Administrators</a>
    {showDropdown === "administrations" && <div id="" class="collapse search-form1 show active">
    <ul class="setting-list">
        <li><a href="sett-user.php">Users & Permissions</a></li>
        <li><a href="sett-acoption.php">Account Options</a></li>
    </ul>
    </div>}



    <a style={styles.leftBarLink} onClick={()=>setshowDropdown(showDropdown === "advanced-settings" ? undefined : "advanced-settings" )} href="#adv-setting" class="search-btn" data-toggle="collapse" aria-expanded="false">Advanced Setting</a>
    {showDropdown === "advanced-settings" && <div id="" class="collapse search-form1 active show">
    <ul class="setting-list">
        <li><a href="sett-product.php">products</a></li>
        <li><a href="sett-order.php">Orders</a></li>
        <li><a href="sett-stock.php">Stock</a></li>
        <li><a href="sett-returns.php">Returns</a></li>
        <li><a href="sett-taxr.php">Tax Option</a></li>
        <li><a href="sett-vouchers.php">Vouchers</a></li>
        <li><a href="sett-ordertotal.php">Order Totals</a></li>
        <li><a href="sett-workflow.php">Workflow Statuses</a></li>
        <li><a href="sett-customercode.php">Customer Code</a></li>
        <li><a href="sett-interface.php">Interface Customization</a></li>
        <li><a href="sett-mailserver.php">Mail Server</a></li>
        <li><a href="sett-integration.php">Integration</a></li>
        <li><a href="sett-audit.php">Audit Trail</a></li>
        <li><a href="sett-security.php">Security</a></li>
    </ul>
    </div>} */}
      </div>
    </div>
  );
};

export default SettingsLeftBar;
