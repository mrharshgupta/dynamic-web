import React, { useState, useEffect } from "react";
import "../../../css/style.css";
import "../../../css/device.css";
import "../../../css/editor.css";
import "../../../css/main.css";
import "../../../css/sanjana.css";
import "../../../css/bootstrap.min.css";
import SettingsLeftBar from "./left-side-bar";
import styles from "./styles";
import StoreGeneralSettings from "./store-settings/general/index";
import { getSettings, saveSettings, upLoadImages, renameStore } from "./apis";
import { Formik, Form } from "formik";
import { rename } from "fs-extra";
import { Spinner } from "react-bootstrap";
import SimpleModal from "../../modal/index";
import { MainAreaLoader } from "../../../components/loaders/index";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

const Settings = ({ serverName, dispatch }) => {
  const [showModal, setshowModal] = useState(false);
  const [showLoader, setshowLoader] = useState(true);
  const [saveButtonLoader, setsaveButtonLoader] = useState(false);
  const [settingToDisplay, setsettingToDisplay] = useState("store-general");
  const [imagesToBeUploaded, setimagesToBeUploaded] = useState({
    file: [],
    fileName: [],
  });

  const [oldSettings, setoldSettings] = useState({});
  const [settings, setsettings] = useState({});
  const [showModalContent, setshowModalContent] = useState("");
  // storeSettings: {
  //   general: {
  //     contactInformation: {
  //       storeName: localStorage.getItem("storeName"),
  //     },
  //   },
  // },

  useEffect(() => {
    dispatch({ type: "SET_SLIDING_LOADER", data: true });
    const getData = async () => {
      await getSettings(
        settings,
        (abc) => setsettings(abc),
        (abc) => setoldSettings(abc)
      );
    };
    getData();
  }, []);
  setTimeout(() => {
    dispatch({ type: "SET_SLIDING_LOADER", data: false });
  }, 2000);
  console.log(settings, "Store Settings");
  const onClickSave = async () => {
    //setshowLoader(true);
    dispatch({ type: "SET_SLIDING_LOADER", data: true });
    if (
      oldSettings.storeSettings.general.contactInformation.storeName !==
      settings.storeSettings.general.contactInformation.storeName
    ) {
      await renameStore(
        dispatch,
        settings.storeSettings.general.contactInformation.storeName,
        serverName,
        () => setshowModal(true),
        (abc) => setshowModalContent(abc),
        () => dispatch({ type: "SET_SLIDING_LOADER", data: false })
      );
    } else {
      dispatch({ type: "SET_SLIDING_LOADER", data: false });
    }
    saveSettings(settings);
  };

  const settingComponentProps = {
    settings: settings,
    setsettings: setsettings,
    imagesToBeUploaded: imagesToBeUploaded,
    setimagesToBeUploaded: setimagesToBeUploaded,
    setshowModal: setshowModal,
    setshowModalContent: setshowModalContent,
    onClickSave: onClickSave,
    oldSettings: oldSettings,
  };
  return (
    <>
      {showModal && (
        <SimpleModal
          showModal={showModal}
          setshowModal={setshowModal}
          title="Message"
          message={showModalContent}
        />
      )}

      <div class="wrap-right" style={styles.wrapData}>
        <>
          <div class="page-nav">
            <div class="page-nav-left">
              <div class="page-action">
                <a href="#" style={styles.link} class="button common-setting">
                  <i class="btn-icon fas fa-cog"></i>
                </a>
                <a
                  href="#"
                  class="button"
                  style={styles.link}
                  data-toggle="modal"
                  data-target="#exampleModal"
                >
                  <i class="btn-icon fab fa-youtube"></i>
                  <span>Video</span>
                </a>
              </div>
              <h1 class="pg-ttl">Store Settings</h1>
            </div>
            <div class="page-nav-right">
              <button class="button" onClick={() => onClickSave()}>
                {saveButtonLoader ? (
                  <Spinner
                    animation="border"
                    role="status"
                    variant="light"
                  ></Spinner>
                ) : (
                  <>
                    <i class="btn-icon far fa-file-alt"></i>
                    <span>Save</span>
                  </>
                )}
              </button>
              <Link
                to={`/${localStorage.getItem("storeName")}/home`}
                class="button"
                style={styles.link}
              >
                <i class="btn-icon fas fa-times"></i>
                <span>Cancel</span>
              </Link>
            </div>
          </div>
          <div class="wrap-data">
            <div class="setting-row col-12">
              <div class="row">
                <div class="col-12 col-lg-3">
                  <SettingsLeftBar
                    setsettingToDisplay={setsettingToDisplay}
                    settingToDisplay={setsettingToDisplay}
                  />
                </div>

                {settingToDisplay === "store-general" && (
                  <StoreGeneralSettings {...settingComponentProps} />
                )}
              </div>
            </div>
          </div>
        </>
      </div>
    </>
  );
};

const mapStateToProps = ({ serverName }) => ({
  serverName,
});
export default connect(mapStateToProps)(Settings);
