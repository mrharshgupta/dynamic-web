const styles = {
  wrapData: {
    height: "100vh",
    overflowY: "scroll",
  },
  loader: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: "80vh",
    width: "80%",
    position: "fixed",
    right: 0,
    zIndex: 9999,
    backgroundColor: "white",
  },
  link: {
    textDecoration: "none",
    color: "white",
  },
  leftBarLink: {
    textDecoration: "none",
    color: "black",
    fontWeight: "bold",
  },
};

export default styles;
