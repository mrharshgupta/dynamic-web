import * as firebase from "firebase";
import { apiUrls } from "../../../api/helper";
import { consoleInDebugModeOnly2 } from "../../../helpers/debugging";
import axios from "axios";
import _ from "lodash";

const getSettings = (stockSettings, setuserSettings, setuserOldSettings) => {
  const settings = firebase.database().ref("userSettings/settings");
  settings.on("value", (settingsSnapshot) => {
    const userSettings = firebase
      .database()
      .ref("userSettings/data/" + localStorage.getItem("userId"));
    userSettings.on("value", (userSettingsSnapshot) => {
      setuserSettings({
        ...settingsSnapshot.val(),
        ...userSettingsSnapshot.val(),
      });
      setuserOldSettings({
        ...settingsSnapshot.val(),
        ...userSettingsSnapshot.val(),
      });
    });
  });
};

const saveSettings = (settings) => {
  const Data = firebase
    .database()
    .ref("userSettings/data/" + localStorage.getItem("userId"))
    .set(settings);
};

const upLoadImages = async (imagesToBeUploaded) => {
  console.log(imagesToBeUploaded.file.length);
  console.log(imagesToBeUploaded);

  {
    _.times(imagesToBeUploaded.file.length, async (e) => {
      const fd = new FormData();
      imagesToBeUploaded &&
        fd.append(
          "image[]",
          imagesToBeUploaded.file[e],
          imagesToBeUploaded.file[e].name
        );
      fd.append("userId", localStorage.getItem("userId"));
      fd.append("imagesToBeUploaded", imagesToBeUploaded);
      fd.append("fileName[]", imagesToBeUploaded.fileName[e]);
      fd.append("saved", "true");
      try {
        const { data } = await axios.post(apiUrls.imageUpload, fd, {
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
          },
        });
        consoleInDebugModeOnly2("logInSuccess-responseData", data);
        const Data = firebase
          .database()
          .ref(
            "user_template_data/" +
              localStorage.getItem("userTemplateId") +
              "/saved/0/data/name/logoName"
          )
          .set(imagesToBeUploaded.file[e].name);
        const Data2 = firebase
          .database()
          .ref(
            "user_template_data/" +
              localStorage.getItem("userTemplateId") +
              "/submit/0/data/name/logoName"
          )
          .set(imagesToBeUploaded.file[e].name);
      } catch (error) {
        consoleInDebugModeOnly2("logInErrorWithoutApi", error.response);
      }
    });
  }
};

const renameStore = async (
  dispatch,
  newStoreName,
  serverName,
  setshowModal,
  setshowModalContent,
  setshowLoader
) => {
  console.log(newStoreName, "storeName");

  try {
    const { data } = await axios.post(
      apiUrls.rename,
      {
        newStoreName: newStoreName,
        oldStoreName: localStorage.getItem("storeName"),
        userId: localStorage.getItem("userId"),
      },
      {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      }
    );
    consoleInDebugModeOnly2("rename-store-Success-responseData", data);
    if (data.success) {
      await localStorage.setItem("storeName", newStoreName);

      setshowModalContent(
        `Your website will be live at ${
          localStorage.getItem("storeName") + ".machinebe.com"
        } within 15 minutes`
      );
      setshowModal();
      setshowLoader();
    } else {
      setshowLoader();
      setshowModalContent(`Store name already taken`);
      setshowModal();
    }
  } catch (error) {
    consoleInDebugModeOnly2("rename-store-Error", error);
  }
};

const addDomain = async (dispatch, domain, callBack, setshowModalContent) => {
  try {
    const { data } = await axios.post(
      apiUrls.addDomain,
      {
        domainName: domain,
        storeName: localStorage.getItem("storeName"),
      },
      {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      }
    );
    console.log(domain, "domain");
    dispatch({ type: "SET_SLIDING_LOADER", data: false });
    consoleInDebugModeOnly2("add-domain-Success-responseData", data);
    setshowModalContent(
      `Your website will be live at ${"www." + domain} within 2 days`
    );
    callBack();
  } catch (error) {
    consoleInDebugModeOnly2("add-domain-Error", error);
    dispatch({ type: "SET_SLIDING_LOADER", data: false });
  }
};

export { getSettings, saveSettings, upLoadImages, renameStore, addDomain };
