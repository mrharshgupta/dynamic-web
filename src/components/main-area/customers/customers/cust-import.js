import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import Popups from '../../../popups/index';
import {Animated} from "react-animated-css";
import '../../../../css/style.css';
import '../../../../css/device.css';
import '../../../../css/editor.css';
import '../../../../css/main.css';
import '../../../../css/sanjana.css';
import '../../../../css/bootstrap.min.css';

const CustomersImport = (props) => {

const [showDropDownCustomers, setshowDropDownCustomers] = useState(undefined);
const [showDropDownOrders, setshowDropDownOrders] = useState(undefined);

return(

<div class="wrap-right">
   <div class="page-nav">
      <div class="page-nav-left">
         <h1 class="pg-ttl">Import & Export </h1>
      </div>
   </div>
   <div class="wrap-data">
      <div class="product-update-div">
         <div class="col-12 px-0">
            <div class="row">
               <div class="col-12 col-md-6">
                  <a aria-expanded="false" onClick={()=>{(showDropDownCustomers === true)? (setshowDropDownCustomers(undefined)) : (setshowDropDownCustomers(true))}} href="#" class="search-btn bg-white">Customer</a>
                  {(showDropDownCustomers === true) && (
                  <div id="import-cust" class="search-form1">
                     <form action="" class="bg-white py-4 px-2">
                        <ul class="row form-div mb-0">
                           <li class="col-12 form-group">
                              <label class="d-block">Example File</label>
                              <input type="file" placeholder="Customer Name" />
                           </li>
                           <li class="col-12 form-group">
                              <label>Password</label>
                              <div class="custom-control custom-radio">
                                 <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input" />
                                 <label class="custom-control-label" for="customRadio1">Plain Password</label>
                              </div>
                              <div class="custom-control custom-radio">
                                 <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input" />
                                 <label class="custom-control-label" for="customRadio2">Encrypted Password</label>
                              </div>
                              <p class="note-txt"><b>Note:</b> Select Password Format Which used in xls sheet.</p>
                           </li>
                        </ul>
                        <div class="btn-wrapp text-center">
                           <button class="button">Button-import</button>
                        </div>
                     </form>
                  </div>)}
               </div>
               <div class="col-12 col-md-6">
                  <a onClick={()=>{(showDropDownOrders === true)? (setshowDropDownOrders(undefined)) : (setshowDropDownOrders(true))}} href="#" class="search-btn bg-white"  aria-expanded="false">Order</a>
                  {(showDropDownOrders === true) && (
                     <div id="import-order" class="search-form1">
                     <form action="" class="bg-white py-4 px-2">
                        <ul class="row form-div mb-0">
                           <li class="col-12 form-group">
                              <label class="d-block">Example File</label>
                              <input type="file" placeholder="Customer Name" />
                           </li>
                        </ul>
                        <div class="btn-wrapp text-center">
                           <button class="button">Button-import</button>
                        </div>
                     </form>
                  </div>)}
               </div>
            </div>
         </div>
      </div>
   </div>
</div>



);
}

export default CustomersImport;