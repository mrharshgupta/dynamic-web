import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import Popups from '../../../popups/index';
import styles from './styles.js';
import {Animated} from "react-animated-css";
import '../../../../css/style.css';
import '../../../../css/device.css';
import '../../../../css/editor.css';
import '../../../../css/main.css';
import '../../../../css/sanjana.css';
import '../../../../css/bootstrap.min.css';

const CustomersExport = (props) => {

const [showDropDownCustomers, setshowDropDownCustomers] = useState(undefined);
const [showDropDownOrders, setshowDropDownOrders] = useState(undefined);

return(
<>
<div class="wrap-right">
   <div class="page-nav">
      <div class="page-nav-left">
         <h1 class="pg-ttl">Import & Export </h1>
      </div>
   </div>
   <div class="wrap-data" style={styles.wrapData}>
      <div class="product-update-div">
         <div class="col-12 px-0">
            <div class="row">
               <div class="col-12 col-md-6">
                  <a onClick={()=>{(showDropDownCustomers === true)? (setshowDropDownCustomers(undefined)) : (setshowDropDownCustomers(true))}} href="javascript:void(0)" class="search-btn bg-white"  aria-expanded="false">Customer</a>
                  {(showDropDownCustomers === true) && (
                     <div id="export-cust" class="search-form1">
                     <form action="" class="bg-white py-4 px-2">
                        <ul class="row form-div">
                           <li class="col-12 col-md-6 form-group">
                              <label>Customer Name (English)<span class="text-red">*</span></label>
                              <input type="text" class="form-control" placeholder="Customer Name" />
                           </li>
                           <li class="col-12 col-md-6 form-group">
                              <label>E-Mail<span class="text-red">*</span> </label>
                              <input type="Email" class="form-control" placeholder="Email" />
                           </li>
                           <li class="col-12 col-md-6 form-group">
                              <label>Customer Group</label>
                              <select class="form-control">
                                 <option>Select</option>
                                 <option>General</option>
                              </select>
                           </li>
                           <li class="col-12 col-md-6 form-group">
                              <label>Status</label>
                              <select class="form-control">
                                 <option>Select</option>
                                 <option>Enable</option>
                                 <option>Disable</option>
                              </select>
                           </li>
                           <li class="col-12 col-md-6 form-group">
                              <label>Approved</label>
                              <select class="form-control">
                                 <option>Select</option>
                                 <option>Yes</option>
                                 <option>No</option>
                              </select>
                           </li>
                           <li class="col-12 col-md-6 form-group">
                              <label>IP<span class="text-red">*</span> </label>
                              <input type="Email" class="form-control" placeholder="Ip" />
                           </li>
                           <li class="col-12 form-group">
                              <label>Date Added<span class="text-red">*</span> </label>
                              <input type="Date" class="form-control" placeholder="Enter Date" />
                           </li>
                           <li class="col-12 form-group">
                              <label>Limit<span class="text-red">*</span> </label>
                              <div class="row mb-0">
                                 <div class="col-6">
                                    <div class="form-group"><input type="text" class="form-control" /></div>
                                 </div>
                                 <div class="col-6">
                                    <div class="form-group"><input type="text" class="form-control" /></div>
                                 </div>
                              </div>
                           </li>
                        </ul>
                        <div class="btn-wrapp text-center">
                           <button class="button">Export</button>
                        </div>
                     </form>
                  </div>
                  )}
               </div>
               <div class="col-12 col-md-6">
                  <a onClick={()=>{(showDropDownOrders === true)? (setshowDropDownOrders(undefined)) : (setshowDropDownOrders(true))}} href="#" class="search-btn bg-white"  aria-expanded="false">Order</a>
                  {(showDropDownOrders === true) && (
                     <div id="export-order" class=" search-form1">
                     <form action="" class="bg-white py-4 px-2">
                        <ul class="row form-div">
                           <li class="col-12  form-group">
                              <label>Order ID</label>
                              <div class="row mb-0">
                                 <div class="col-6"><input type="text" class="form-control" placeholder="To" /></div>
                                 <div class="col-6"><input type="text" class="form-control" placeholder="To" /></div>
                              </div>
                           </li>
                           <li class="col-12 col-md-6 form-group">
                              <label>Order Status</label>
                              <select class="form-control">
                                 <option>Select</option>
                                 <option>Yes</option>
                                 <option>No</option>
                              </select>
                           </li>
                           <li class="col-12 col-md-6 form-group">
                              <label>Total</label>
                              <input type="text" class="form-control" placeholder="Customer Name" />
                           </li>
                           <li class="col-12  form-group">
                              <label>Price</label>
                              <div class="row mb-0">
                                 <div class="col-6"><input type="number" class="form-control" value="0" /></div>
                                 <div class="col-6"><input type="number" class="form-control" value="0" /></div>
                              </div>
                           </li>
                           <li class="col-12  form-group ">
                              <label>Date Added</label>
                              <div class="row mb-0">
                                 <div class="col-6"><input type="Date" class="form-control" placeholder="To" /></div>
                                 <div class="col-6"><input type="Date" class="form-control" placeholder="From" /></div>
                              </div>
                           </li>
                           <li class="col-12  form-group">
                              <label>Date Modified</label>
                              <div class="row mb-0">
                                 <div class="col-6"><input type="Date" class="form-control" placeholder="To" /></div>
                                 <div class="col-6"><input type="Date" class="form-control" placeholder="From" /></div>
                              </div>
                           </li>
                        </ul>
                        <div class="btn-wrapp text-center">
                           <button class="button">Export</button>
                        </div>
                     </form>
                  </div>)}
               </div>
            </div>
         </div>
      </div>
   </div>
   </div>

</>

);
}

export default CustomersExport;