import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import Popups from '../../../popups/index';
import styles from './styles';
import '../../../../css/style.css';
import '../../../../css/device.css';
import '../../../../css/editor.css';
import '../../../../css/main.css';
import '../../../../css/sanjana.css';
import '../../../../css/bootstrap.min.css';

const AddCustomer = (props) => {

return(
<>
   <div class="wrap-right">
      <div class="page-nav">
         <div class="page-nav-left">
           
            <h1 class="pg-ttl">Add Customer</h1>
         </div>
         <div class="page-nav-right">
           
            
                <Link to="#" class="button" style={styles.link}>Cancle</Link>
                <Link to="#" class="button" style={styles.link}>Save</Link>
         </div>
      </div>
      <div class="wrap-data" style={styles.wrapData}>
         <div class="product-update-div">
         
            
               <form action="" class="col-12 bg-white py-3">
                  <ul class="row form-div">
                      <li class="col-12 col-md-6 form-group">
                        <label>First Name<span class="text-red">*</span></label>
                        <input type="text" class="form-control" />
                     </li>
                      <li class="col-12 col-md-6 form-group">
                        <label>Last Name<span class="text-red">*</span> </label>
                        <input type="text" class="form-control" />
                     </li>
                       <li class="col-12 col-md-6 form-group">
                        <label>Telephone No<span class="text-red">*</span></label>
                        <input type="text" class="form-control" />
                     </li>
                     <li class="col-12 col-md-6 form-group">
                        <label>Email<span class="text-red">*</span></label>
                        <input type="email" class="form-control" />
                     </li>
                     <li class="col-12 col-md-6 form-group">
                        <label>Password<span class="text-red">*</span></label>
                        <input type="password" class="form-control" />
                     </li>
                      <li class="col-12 col-md-6 form-group">
                        <label>Confirmed Password<span class="text-red">*</span></label>
                        <input type="password" class="form-control" />
                     </li>
                     <li class="col-12 col-md-6 form-group">
                        <label>Customer Group<span class="text-red">*</span></label>
                        <select class="form-control">
                           <option>Default</option>
                           <option>1</option>
                        </select>
                     </li>
                      <li class="col-12 col-md-6 form-group">
                        <label>Fax<span class="text-red">*</span></label>
                        <input type="text" class="form-control" />
                     </li>
                     <li class="col-12 col-md-6 form-group">
                        <label>Status</label>
                       <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch1" checked />
                        <label class="custom-control-label" for="customSwitch1">Enable</label>
                     </div>
                     </li>
                     <li class="col-12 col-md-6 form-group">
                        <label>Newsletter</label>
                       <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch1" checked />
                        <label class="custom-control-label" for="customSwitch1">Enable</label>
                     </div>
                     </li>
                   
                  </ul>
                   <div class="btn-wrapp">
                  <button class="button">Save</button>
                  <button class="button">Cancle</button>
               </div>
               </form>
            
           
      </div>
      </div>
   </div>
</>
);
}

export default AddCustomer;
