import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import Popups from '../../../popups/index';
import styles from './styles';
import '../../../../css/style.css';
import '../../../../css/device.css';
import '../../../../css/editor.css';
import '../../../../css/main.css';
import '../../../../css/sanjana.css';
import '../../../../css/bootstrap.min.css';

const Customers = (props) => {

return(<>
	<div class="wrapper">

   <div class="wrap-right">
      <div class="page-nav">
         <div class="page-nav-left">
            <div class="page-action">
              
               <Link to="/home/customers/cust-exports" class="button" style={styles.link}>export</Link>
               <Link to="/home/customers/cust-imports" class="button" style={styles.link}>import</Link>
               <Link to="#" class="button" style={styles.link}>delete</Link>
            </div>
            <h1 class="pg-ttl">Customer</h1>
         </div>
         <div class="page-nav-right">
            <div class="paging">
               <input type="text" name="showRec" id="showRec" value="20" class="show-rec" original-title="Show Records" />
               <div class="result">1 – 20 of 42</div>
               <a class="prev" href="#"><i class="fas fa-caret-left"></i></a>
               <a class="next" href="#"><i class="fas fa-caret-right"></i></a>
            </div>
            
                <Link to="/home/customers/add-customer" class="button" style={styles.link}>Add New Customer</Link>
                <Link to="#" class="button" style={styles.link}>Approve</Link>
         </div>
      </div>
      <div class="wrap-data" style={styles.wrapData}>
         <table class="tbl-list">
            <thead>
               <tr>
                  <th><i class="chk"><input type="checkbox" class="chkAll" title="Select All" /><em></em></i></th>
                  <th><a class="sort" href="#">Customer Name</a></th>
                  <th><a class="sort" href="#">Email</a></th>
                  <th><a class="sort" href="#">Phone No</a></th>
                  <th><a class="sort" href="#">Customer Group</a></th>
                   <th><a class="sort" href="#">Staus</a></th>
                    <th><a class="sort" href="#">Approved</a></th>
                     <th><a class="sort" href="#">Ip</a></th>
                      <th><a class="sort" href="#">Date Added</a></th>
               </tr>
            </thead>
            <tbody>
               <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td>lorem lipsum</td>
                  <td>lorem lipsum@gmail.com</td>
                  <td>123456789</td>
                  <td>lorem lipsum</td>
                   <td>
                     <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch1" checked />
                        <label class="custom-control-label" for="customSwitch1">Enable</label>
                     </div>
                  </td>
                  <td>lorem lipsum</td>
                   <td>lorem lipsum</td>
                    <td>lorem lipsum</td>
                 
               </tr>
               <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td>lorem lipsum</td>
                  <td>lorem lipsum@gmail.com</td>
                  <td>123456789</td>
                  <td>lorem lipsum</td>
                   <td>
                     <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch1" checked />
                        <label class="custom-control-label" for="customSwitch1">Enable</label>
                     </div>
                  </td>
                  <td>lorem lipsum</td>
                   <td>lorem lipsum</td>
                    <td>lorem lipsum</td>
                 
               </tr>
               <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td>lorem lipsum</td>
                  <td>lorem lipsum@gmail.com</td>
                  <td>123456789</td>
                  <td>lorem lipsum</td>
                   <td>
                     <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch1" checked />
                        <label class="custom-control-label" for="customSwitch1">Enable</label>
                     </div>
                  </td>
                  <td>lorem lipsum</td>
                   <td>lorem lipsum</td>
                    <td>lorem lipsum</td>
                 
               </tr>
               <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td>lorem lipsum</td>
                  <td>lorem lipsum@gmail.com</td>
                  <td>123456789</td>
                  <td>lorem lipsum</td>
                   <td>
                     <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch1" checked />
                        <label class="custom-control-label" for="customSwitch1">Enable</label>
                     </div>
                  </td>
                  <td>lorem lipsum</td>
                   <td>lorem lipsum</td>
                    <td>lorem lipsum</td>
                 
               </tr>
               <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td>lorem lipsum</td>
                  <td>lorem lipsum@gmail.com</td>
                  <td>123456789</td>
                  <td>lorem lipsum</td>
                   <td>
                     <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch1" checked />
                        <label class="custom-control-label" for="customSwitch1">Enable</label>
                     </div>
                  </td>
                  <td>lorem lipsum</td>
                   <td>lorem lipsum</td>
                    <td>lorem lipsum</td>
                 
               </tr>

              
            </tbody>
         </table>
      </div>
   </div>
</div>

</>
);
}

export default Customers;