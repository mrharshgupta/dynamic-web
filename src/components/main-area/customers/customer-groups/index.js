import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import Popups from '../../../popups/index';
import styles from './styles';
import '../../../../css/style.css';
import '../../../../css/device.css';
import '../../../../css/editor.css';
import '../../../../css/main.css';
import '../../../../css/sanjana.css';
import '../../../../css/bootstrap.min.css';

const CustomerGroups = (props) => {

return(<>

   
   <div class="wrap-right">
      <div class="page-nav">
         <div class="page-nav-left">
            
            <h1 class="pg-ttl">Customer Group</h1>
         </div>
         <div class="page-nav-right">
            <div class="paging">
               <input type="text" name="showRec" id="showRec" value="20" class="show-rec" original-title="Show Records" />
               <div class="result">1 – 20 of 42</div>
               <a class="prev" href="#"><i class="fas fa-caret-left"></i></a>
               <a class="next" href="#"><i class="fas fa-caret-right"></i></a>
            </div>
            
                <Link to="/home/customers/update-customer-groups" className="button" style={styles.link}>Create New Group</Link>
                
         </div>
      </div>
      <div class="wrap-data">
         <table class="tbl-list">
            <thead>
               <tr>
                  <th><i class="chk"><input type="checkbox" class="chkAll" title="Select All" /><em></em></i></th>
                  <th><a class="sort" href="update-custgrp.php">Customer Group Name</a></th>
                  <th><a class="sort" href="#">Action</a></th>
                  
               </tr>
            </thead>
            <tbody>
               <tr>
                  <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
                  <td><Link to="update-custgrp.php" >General</Link></td>
                  <td><ul class="action-icon">
                     
                        <li><a href="update-custgrp.php"><i class="act-icon text-green fas fa-edit"></i></a></li>
                        <li><a href="#"><i class="act-icon text-red far fa-trash-alt"></i></a></li>
                     </ul></td>
                
                 
               </tr>
               
              
            </tbody>
         </table>
      </div>
   </div>



</>
);
}

export default CustomerGroups;