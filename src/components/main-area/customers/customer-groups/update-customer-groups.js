import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import Popups from '../../../popups/index';
import styles from './styles';
import '../../../../css/style.css';
import '../../../../css/device.css';
import '../../../../css/editor.css';
import '../../../../css/main.css';
import '../../../../css/sanjana.css';
import '../../../../css/bootstrap.min.css';

const UpdateCustomerGroups = (props) => {

return(<>
   <div class="wrap-right">
      <div class="page-nav">
         <div class="page-nav-left">
           
            <h1 class="pg-ttl">Update Customer Group</h1>
         </div>
         <div class="page-nav-right">
           
            
                <a href="#" class="button">Cancle</a>
                <a href="javascript:void(0)" class="button">Save</a>
         </div>
      </div>
      <div class="wrap-data" style={styles.wrapData}>
         <div class="product-update-div">
         
            
               <form action="" class="col-12 bg-white py-3">
                  <ul class="row form-div">
                      <li class="col-12 form-group">
                        <label>Customer Group Name (English)<span class="text-red">*</span></label>
                        <input type="text" class="form-control" />
                     </li>
                      <li class="col-12 form-group">
                        <label>Description (English)<span class="text-red">*</span> </label>
                        <textarea class="form-control" rows="2"></textarea>
                     </li>
                       <li class="col-12 col-md-6 form-group">
                        <label>E-mail activation</label>
                       <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch1" />
                        <label class="custom-control-label" for="customSwitch1">Disable</label>
                     </div>
                     </li>
                     <li class="col-12 col-md-6 form-group">
                        <label>Approve New Customers</label>
                       <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch1" />
                        <label class="custom-control-label" for="customSwitch1">Disable</label>
                     </div>
                     <p class="note-txt">Customers must be approved by and administrator before they can login.</p>
                     </li>
                      <li class="col-12 col-md-6 form-group">
                        <label>Login after activation</label>
                       <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch1" />
                        <label class="custom-control-label" for="customSwitch1">Disable</label>
                     </div>
                     <p class="note-txt">Customer can`t login if he didn`t activate his account.</p>
                     </li>
                    
                   
                  </ul>
                   <div class="col-12 px-0">
                  <button class="button">Save</button>
                  <button class="button">Cancle</button>
               </div>
               </form>
            
           
      </div>
      </div>
   </div>
</>
);
}
export default UpdateCustomerGroups;