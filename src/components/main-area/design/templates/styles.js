const styles={

wrapData: {
height: '100vh', overflowY: 'scroll',
},

link: {
textDecoration: 'none',
color: 'white',
fontSize: 14
},

linkApplyTemplate: {
textDecoration: 'none',
color: 'white',
fontSize: 12
},

lookfortemplate:{
textDecoration: 'none',
color: 'grey',	
},

lookfortemplateoptions:{
color: 'black',
fontSize: 14,
},

templateDropDown: {top: '110%',
	right: "-47%",
	position: 'absolute',
	backgroundColor: 'white',
	justifyContent: 'center', 
	alignItems: 'center'
},

livePreview: {
	textDecoration: 'none',
	color: "grey"
}
}



export default styles;
