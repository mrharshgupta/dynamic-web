import React from "react";
import { Redirect } from "react-router-dom";

import { apiUrls } from "../../../../api/helper";
import {
  getTemplatesRequest,
  getTemplatesSuccess,
  getTemplatesFailure,
} from "./actions.js";
import { consoleInDebugModeOnly2 } from "../../../../helpers/debugging";
import * as firebase from "firebase";
import "firebase/database";

const getTemplates = (dispatch, setloading, setloaderProgress) => {
  setloaderProgress(50);
  getTemplatesRequest(dispatch);
  fetch(apiUrls.getTemplates, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      userId: localStorage.getItem("userId"),
    }),
  })
    .then((response) => {
      setloaderProgress(80);
      console.log(response, "get templates");
      return response.json();
    })
    .then((responseData) => {
      consoleInDebugModeOnly2("getTemplatesSuccess-responseData", responseData);
      getTemplatesSuccess(dispatch, responseData);
      setloading();
    })
    .catch((error) => {
      consoleInDebugModeOnly2("getTemplatesErrorWithoutApi", error);
      getTemplatesFailure(dispatch);
      setloading();
    });
  setloaderProgress(70);
};

const applyTemplate = async (
  dispatch,
  templateId,
  setshowLoader,
  setredirect
) => {
  fetch(apiUrls.applyTemplate, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      templateId: templateId,
      userId: localStorage.getItem("userId"),
      storeName: localStorage.getItem("storeName"),
    }),
  })
    .then((response) => {
      console.log(response, "Apply-Template");
      return response.json();
    })
    .then(async (responseData) => {
      consoleInDebugModeOnly2(
        "applyTemplateSuccess-responseData",
        responseData
      );
      if (responseData.applied === true) {
        await localStorage.setItem("appliedTemplate", templateId);
        await localStorage.setItem(
          "userTemplateId",
          responseData.userTemplateId
        );
        dispatch({
          type: "CHANGE_USERTEMPLATEID_AND_APPLIEDTEMPLATE",
          data: {
            userTemplateId: responseData.userTemplateId,
            appliedTemplate: templateId,
          },
        });
        dispatch({ type: "SET_SLIDING_LOADER", data: false });
        setshowLoader();
        setredirect();
      }
    })
    .catch((error) => {
      consoleInDebugModeOnly2("applyTemplateErrorWithoutApi", error);
      dispatch({ type: "SET_SLIDING_LOADER", data: false });
      setshowLoader();
    });
};

const editTemplate = (
  templateId,
  setshowLoader,
  setuserTemplateIdForTemplateEdit,
  setredirect
) => {
  fetch(apiUrls.editTemplate, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      templateId: templateId,
      userId: localStorage.getItem("userId"),
    }),
  })
    .then((response) => {
      console.log(response, "edit-Template");
      return response.json();
    })
    .then((responseData) => {
      consoleInDebugModeOnly2("editTemplateSuccess-responseData", responseData);
      if (responseData.msg === "success") {
        setredirect("Edit Template");
        setuserTemplateIdForTemplateEdit(responseData.userTemplateId);
        // localStorage.setItem(
        //   "userTemplateIdForEditingTemplate",
        //   responseData.userTemplateId
        // );
        setshowLoader();
      }
    })
    .catch((error) => {
      consoleInDebugModeOnly2("applyTemplateErrorWithoutApi", error);
      setshowLoader();
    });
};

export { applyTemplate, getTemplates, editTemplate };
