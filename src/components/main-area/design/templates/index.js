import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Link, Redirect } from "react-router-dom";
import styles from "./styles";
import { Animated } from "react-animated-css";
import TemplateComponent from "./template-component";
import { getTemplates } from "./api.js";
import { useDispatch } from "react-redux";
import "../../../../css/style.css";
import "../../../../css/device.css";
import "../../../../css/editor.css";
import "../../../../css/main.css";
import "../../../../css/sanjana.css";
import "../../../../css/bootstrap.min.css";
import { Placeholder, ApplyTemplatePlaceholder } from "./placeholder.js";
import Loader from "react-loader";

const Templates = ({ templates }) => {
  const dispatch = useDispatch();
  const [lookForTemplate, setlookForTemplate] = useState(true);
  const [showLoader, setshowLoader] = useState(true);
  const [loaderProgress, setloaderProgress] = useState(10);
  const [showApplyTemplateLoader, setshowApplyTemplateLoader] = useState(false);
  const [redirect, setredirect] = useState(undefined);
  const [templateIdForTemplateEdit, settemplateIdForTemplateEdit] = useState(
    undefined
  );
  const [
    userTemplateIdForTemplateEdit,
    setuserTemplateIdForTemplateEdit,
  ] = useState(undefined);
  const [category, setcategory] = useState([]);
  useEffect(() => {
    getTemplates(
      dispatch,
      () => setshowLoader(false),
      (abc) => setloaderProgress(abc)
    );
  }, []);

  console.log(templates, "category");
  var templatesToShow =
    category.length === 0
      ? templates
      : templates.filter((item) => {
          return category.includes(item.category);
        });
  console.log(localStorage.getItem("userId"));
  return (
    <>
      <div class="wrap-right">
        <div class="page-nav">
          <div class="page-nav-left">
            <div class="page-action">
              <Link to="#" style={styles.link} className="button">
                <i class="fas fa-undo mr-2"></i> Reset Current Theme to Default
              </Link>
              <Link
                to="/home/design/templates/import-template"
                style={styles.link}
                className="button"
              >
                <i class="fas fa-upload mr-2"></i> Import custom template
              </Link>
            </div>
            <h1 class="pg-ttl">Templates</h1>
          </div>
          <div class="page-nav-right">
            <Link to="#" style={styles.link} className="button">
              Your current theme: Manymore
            </Link>
          </div>
        </div>

        <div class="wrap-data template" style={styles.wrapData}>
          <div class="row">
            <div class="col-12 col-md-9">
              {showLoader && <Placeholder loaderProgress={loaderProgress} />}
              <div class="row">
                {showApplyTemplateLoader && <ApplyTemplatePlaceholder />}

                {!showApplyTemplateLoader &&
                  templatesToShow.map((value, index) => {
                    return (
                      <TemplateComponent
                        name={value.name}
                        image={value.image}
                        path={value.routePath}
                        templateId={value.templateId}
                        setshowApplyTemplateLoader={setshowApplyTemplateLoader}
                        showApplyTemplateLoader={showApplyTemplateLoader}
                        redirect={redirect}
                        setredirect={setredirect}
                        userTemplateIdForTemplateEdit={
                          userTemplateIdForTemplateEdit
                        }
                        setuserTemplateIdForTemplateEdit={
                          setuserTemplateIdForTemplateEdit
                        }
                        templateIdForTemplateEdit={templateIdForTemplateEdit}
                        settemplateIdForTemplateEdit={
                          settemplateIdForTemplateEdit
                        }
                      />
                    );
                  })}
              </div>
            </div>

            <div class="col-12 col-md-3">
              <ul class="template-filter sticky-top">
                <li class="active">
                  <a
                    href="javascript:void(0)"
                    style={styles.lookfortemplate}
                    class="down-arrow"
                    onClick={() => setlookForTemplate(!lookForTemplate)}
                  >
                    LOOK FOR A TEMPLATE
                  </a>
                  {lookForTemplate === true && (
                    <ul>
                      <li>
                        <div class="custom-control custom-checkbox">
                          <input
                            type="checkbox"
                            class="custom-control-input"
                            id="general"
                            checked={category.includes("General")}
                            onClick={() =>
                              setcategory(
                                category.includes("General")
                                  ? category.filter((item) => {
                                      return item !== "General";
                                    })
                                  : category.concat("General")
                              )
                            }
                          />
                          <label
                            class="custom-control-label"
                            for="general"
                            style={styles.lookfortemplateoptions}
                          >
                            General
                          </label>
                        </div>
                      </li>
                      <li>
                        <div class="custom-control custom-checkbox">
                          <input
                            type="checkbox"
                            class="custom-control-input"
                            id="fashion"
                            checked={category.includes("Fashion")}
                            onClick={() =>
                              setcategory(
                                category.includes("Fashion")
                                  ? category.filter((item) => {
                                      return item !== "Fashion";
                                    })
                                  : category.concat("Fashion")
                              )
                            }
                          />
                          <label
                            class="custom-control-label"
                            for="fashion"
                            style={styles.lookfortemplateoptions}
                          >
                            Fashion
                          </label>
                        </div>
                      </li>
                      {/* <li>
                        <div class="custom-control custom-checkbox">
                          <input
                            type="checkbox"
                            class="custom-control-input"
                            id="business"
                          />
                          <label
                            class="custom-control-label"
                            for="business"
                            style={styles.lookfortemplateoptions}
                          >
                            Business
                          </label>
                        </div>
                      </li> */}
                      {/* <li>
                        <div class="custom-control custom-checkbox">
                          <input
                            type="checkbox"
                            class="custom-control-input"
                            id="electronics"
                          />
                          <label
                            class="custom-control-label"
                            for="electronics"
                            style={styles.lookfortemplateoptions}
                          >
                            Electronics
                          </label>
                        </div>
                      </li> */}
                      <li>
                        <div class="custom-control custom-checkbox">
                          <input
                            type="checkbox"
                            class="custom-control-input"
                            id="sports"
                            checked={category.includes("Sports")}
                            onClick={() =>
                              setcategory(
                                category.includes("Sports")
                                  ? category.filter((item) => {
                                      return item !== "Sports";
                                    })
                                  : category.concat("Sports")
                              )
                            }
                          />
                          <label
                            class="custom-control-label"
                            for="sports"
                            style={styles.lookfortemplateoptions}
                          >
                            Sports
                          </label>
                        </div>
                      </li>
                      <li>
                        <div class="custom-control custom-checkbox">
                          <input
                            type="checkbox"
                            class="custom-control-input"
                            id="nature"
                            checked={category.includes("Nature")}
                            onClick={() =>
                              setcategory(
                                category.includes("Nature")
                                  ? category.filter((item) => {
                                      return item !== "Nature";
                                    })
                                  : category.concat("Nature")
                              )
                            }
                          />
                          <label
                            class="custom-control-label"
                            for="nature"
                            style={styles.lookfortemplateoptions}
                          >
                            Nature
                          </label>
                        </div>
                      </li>
                    </ul>
                  )}
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

const mapStateToProps = ({
  templates: { loading = true, data = [] } = {},
}) => ({
  templates: data,
  loading,
});

export default connect(mapStateToProps)(Templates);
