import React from "react";
import LinearProgress from "@material-ui/core/LinearProgress";
import CircularProgress from "@material-ui/core/CircularProgress";

export const Placeholder = ({ loaderProgress }) => {
  const style = {
    flexDirection: "column",
    position: "absolute",
    display: "flex",
    height: "60vh",
    justifyContent: "center",
    alignItems: "center",
    marginRight: "1000px",
    width: "100%",
    zIndex: 9999,
  };

  return (
    <div style={style}>
      <CircularProgress />
      <br />
      <span>Loading Templates...</span>
    </div>
  );
};
// flexDirection: 'column',
//              position: 'absolute',
//              display: 'flex',
//              //height: '60vh',
//              justifyContent: 'center',
//              backgroundColor: 'rgba(0,0,0,0.6)',
//              alignItems: 'center',
//              //marginRight: '1000px',
//              width: '100%',
//              height: '110%',
//              top: '0px',
//              zIndex: 99999

export const ApplyTemplatePlaceholder = () => {
  const styles = {
    flexDirection: "column",
    position: "absolute",
    display: "flex",
    height: "60vh",
    justifyContent: "center",
    alignItems: "center",
    marginRight: "1000px",
    width: "100%",
    zIndex: 9999,
  };
  return (
    <div style={styles}>
      <CircularProgress />
      <br />
      <span>Applying...</span>
    </div>
  );
};
