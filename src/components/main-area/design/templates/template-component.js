import React, { useState, useContext } from "react";
import { Dropdown } from "react-bootstrap";
import { Link, Redirect } from "react-router-dom";
import { useDispatch } from "react-redux";
import styles from "./styles";
import { applyTemplate, editTemplate } from "./api.js";
import "../../../../css/style.css";
import "../../../../css/device.css";
import "../../../../css/editor.css";
import "../../../../css/main.css";
import "../../../../css/sanjana.css";
import "../../../../css/bootstrap.min.css";
import "./style.css";

const TemplateComponent = ({
  name,
  image,
  path,
  templateId,
  showApplyTemplateLoader,
  setshowApplyTemplateLoader,
  redirect,
  setredirect,
  userTemplateIdForTemplateEdit,
  setuserTemplateIdForTemplateEdit,
  templateIdForTemplateEdit,
  settemplateIdForTemplateEdit,
}) => {
  const dispatch = useDispatch();
  if (redirect === "Edit Template") {
    return (
      <Redirect
        to={`/${localStorage.getItem(
          "storeName"
        )}/customize-template/${userTemplateIdForTemplateEdit}/${templateIdForTemplateEdit}/false`}
      />
    );
  }
  if (redirect === "Apply Template") {
    return (
      <Redirect
        to={`/${localStorage.getItem(
          "storeName"
        )}/customize-template/${localStorage.getItem(
          "userTemplateId"
        )}/${localStorage.getItem("appliedTemplate")}/true`}
      />
    );
  }
  const applyTemplateButton = async () => {
    //setshowApplyTemplateLoader(true);
    dispatch({ type: "SET_SLIDING_LOADER", data: true });
    await applyTemplate(
      dispatch,
      templateId,
      () => setshowApplyTemplateLoader(false),
      () => setredirect(true)
    );
  };

  const editTemplateButton = () => {
    setshowApplyTemplateLoader(true);
    settemplateIdForTemplateEdit(templateId);
    editTemplate(
      templateId,
      () => setshowApplyTemplateLoader(false),
      (abc) => setuserTemplateIdForTemplateEdit(abc),
      (abc) => setredirect(abc)
    );
  };

  return (
    <div class="col-12 col-md-4 mb-4">
      <div
        class="card text-center"
        style={
          templateId === localStorage.getItem("appliedTemplate")
            ? { backgroundColor: "rgb(174, 213, 129)" }
            : { backgroundColor: "white" }
        }
      >
        <div
          class="card-header"
          style={
            templateId === localStorage.getItem("appliedTemplate")
              ? { backgroundColor: "rgb(174, 213, 129)" }
              : { backgroundColor: "white" }
          }
        >
          {name}
        </div>
        <div class="card-body">
          <img src={`${image}`} alt="logo" />
        </div>
        <div
          class="card-footer text-muted"
          style={
            templateId === localStorage.getItem("appliedTemplate")
              ? { backgroundColor: "rgb(174, 213, 129)" }
              : { backgroundColor: "white" }
          }
        >
          <div class="btn-group">
            <button
              style={
                templateId === localStorage.getItem("appliedTemplate")
                  ? (styles.linkApplyTemplate,
                    { backgroundColor: "#e3e3e3", color: "grey", fontSize: 12 })
                  : styles.linkApplyTemplate
              }
              className="button mr-0"
              onClick={() => {
                applyTemplateButton();
              }}
              disabled={templateId === localStorage.getItem("appliedTemplate")}
            >
              <i className="fas fa-broom"></i>{" "}
              {templateId === localStorage.getItem("appliedTemplate")
                ? "Already Applied"
                : "Apply Template"}
            </button>

            <Dropdown>
              <Dropdown.Toggle
                id="dropdown-basic"
                className="button dropdown-toggle"
              ></Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item>
                  <Link
                    to={`/preview/${name}/${templateId}`}
                    target="_blank"
                    onClick={(event) => {
                      event.preventDefault();
                      window.open(`/preview/${name}/${templateId}`);
                    }}
                  >
                    <i class="fas fa-eye" style={{ marginRight: 10 }}></i>{" "}
                    Preview
                  </Link>
                </Dropdown.Item>

                <>
                  <Dropdown.Divider />{" "}
                  <Dropdown.Item>
                    <Link
                      onClick={() => {
                        templateId !==
                          localStorage.getItem("appliedTemplate") &&
                          editTemplateButton();
                      }}
                      to={
                        templateId ===
                          localStorage.getItem("appliedTemplate") &&
                        `/${localStorage.getItem(
                          "storeName"
                        )}/customize-template/${localStorage.getItem(
                          "userTemplateId"
                        )}/${localStorage.getItem("appliedTemplate")}/true`
                      }
                      style={styles.livePreview}
                    >
                      <i class="fas fa-edit" style={{ marginRight: 10 }}></i>{" "}
                      Edit{" "}
                    </Link>
                  </Dropdown.Item>
                </>
              </Dropdown.Menu>
            </Dropdown>
          </div>
        </div>
      </div>
    </div>
  );
};
export default TemplateComponent;
