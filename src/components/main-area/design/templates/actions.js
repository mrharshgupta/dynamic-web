export const TEMPLATES_REQUEST = 'TEMPLATES_REQUEST';
export const TEMPLATES_SUCCESS = 'TEMPLATES_SUCCESS';
export const TEMPLATES_FAILURE = 'TEMPLATES_FAILURE';

export const getTemplatesRequest = (dispatch) => {
	dispatch({type: TEMPLATES_REQUEST, data: {login: false}});
};

export const getTemplatesSuccess = (dispatch, data) => {
	dispatch({
		type: TEMPLATES_SUCCESS,
		data: {login: true, data},
	});
};

export const getTemplatesFailure = (dispatch) => {
	dispatch({
		type: TEMPLATES_FAILURE,
		data: {login: false},
	});
};
