import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import Popups from '../../../popups/index';
import styles from './styles';
import '../../../../css/style.css';
import '../../../../css/device.css';
import '../../../../css/editor.css';
import '../../../../css/main.css';
import '../../../../css/sanjana.css';
import '../../../../css/bootstrap.min.css';

const ImportTemplate = (props) => {

return(
<>
<div class="wrap-right" style={styles.wrapData}>
      <div class="page-nav">
         <div class="page-nav-left">
            <h1 class="pg-ttl"> Import template</h1>
         </div>
         <div class="page-nav-right">
          <button class="button">Cancle</button>
         </div>
      </div>
      <div class="wrap-data">
         <div class="product-update-div">
 <div class="subtitle">
   <h2>Setting</h2>
</div>
<div class="drop-filewrap text-center">
                     <div class="file-cont">
                        <form action="upload.php" method="POST">
                           <input type="file" multiple />
                           <i className="fa fa-folder fa-5x icon" ></i>
                           <p>Drag your files here or click in this area.</p>
                        </form>
                     </div>
                     <button type="submit" class="button mx-auto">Upload</button>
                  </div>
               <div class="col-12 px-0">
            <button class="button">Cancle</button>
         </div>
      </div>
   </div>
</div>
</>
);
}
export default ImportTemplate;
