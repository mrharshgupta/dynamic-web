import React from "react";
import SimpleInput from "../form-fields/simple-input";
import DoubleInput from "../form-fields/double-input.js";
import Link from "../form-fields/links.js";
import ColorPicker from "../form-fields/color-picker.js";
import _ from "lodash";

const ContactForm = ({
  userTemplateData,
  setuserTemplateData,
  componentIndex,
  formData,
  values,
  setactiveComponent,
  iFrameKey,
  setiframeKey,
  setshowLoader,
  setformData,
  currentPage,
}) => {
  setactiveComponent("contactForm" + componentIndex);
  const props = {
    iFrameKey: iFrameKey,
    setiframeKey: setiframeKey,
    setshowLoader: setshowLoader,
    values: values,
    formData: formData,
    userTemplateData: userTemplateData,
    setuserTemplateData: setuserTemplateData,
    componentIndex: componentIndex,
    currentPage: currentPage,
  };
  return (
    <div>
      <SimpleInput
        name={`${componentIndex}.data.title.text`}
        title="Title"
        placeHolder="Enter Title"
        {...props}
      />

      <SimpleInput
        name={`${componentIndex}.data.description.text`}
        title="Description"
        placeHolder="Enter Description"
        {...props}
      />

      <SimpleInput
        name={`${componentIndex}.data.email`}
        title="Email"
        placeHolder="Enter Email to recieve form responses"
        {...props}
      />

      <ColorPicker
        title="background Color"
        name={`${componentIndex}.data.styles.backgroundColor`}
        {...props}
      />
    </div>
  );
};
export default ContactForm;
