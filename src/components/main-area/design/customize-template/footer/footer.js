import React, { useState } from "react";
import SimpleInput from "../form-fields/simple-input.js";
import Link from "../form-fields/links.js";
import _ from "lodash";
import AddButton from "../form-fields/add-button.js";
import AdditionalPages from "./additional-pages.js";
import UsefulLinks from "./useful-links.js";
import Information from "./information.js";
import ColorPicker from "../form-fields/color-picker.js";
import SocialLinks from "./social-links.js";

const Footer = ({
  editFormNavigation,
  seteditFormNavigation,
  setactiveComponent,
  componentIndex,
  userTemplateData,
  setuserTemplateData,
  formData,
  setformData,
  values,
  setiframeKey,
  iFrameKey,
  setshowLoader,
  currentPage,
  allWebPages,
}) => {
  setactiveComponent("footer" + componentIndex);
  console.log(
    userTemplateData[currentPage][componentIndex].data.information,
    currentPage,
    componentIndex,
    "kcdvhdshkjcbhkcdvs"
  );
  const props = {
    iFrameKey: iFrameKey,
    setiframeKey: setiframeKey,
    setshowLoader: setshowLoader,
    values: values,
    formData: formData,
    userTemplateData: userTemplateData,
    setuserTemplateData: setuserTemplateData,
    editFormNavigation: editFormNavigation,
    seteditFormNavigation: seteditFormNavigation,
    componentIndex: componentIndex,
    currentPage: currentPage,
    allWebPages,
  };
  return (
    <div>
      {userTemplateData[currentPage][componentIndex].data.information && (
        <Information
          iFrameKey={iFrameKey}
          setiframeKey={setiframeKey}
          setshowLoader={setshowLoader}
          setformData={setformData}
          formData={formData}
          values={values}
          setuserTemplateData={setuserTemplateData}
          userTemplateData={userTemplateData}
          componentIndex={componentIndex}
          currentPage={currentPage}
          allWebPages={allWebPages}
        />
      )}

      {userTemplateData[currentPage][componentIndex].data.usefulLinks && (
        <UsefulLinks
          iFrameKey={iFrameKey}
          setiframeKey={setiframeKey}
          setshowLoader={setshowLoader}
          setformData={setformData}
          formData={formData}
          values={values}
          setuserTemplateData={setuserTemplateData}
          userTemplateData={userTemplateData}
          componentIndex={componentIndex}
          currentPage={currentPage}
          allWebPages={allWebPages}
        />
      )}

      {userTemplateData[currentPage][componentIndex].data.additionalPages && (
        <AdditionalPages
          iFrameKey={iFrameKey}
          setiframeKey={setiframeKey}
          setshowLoader={setshowLoader}
          setformData={setformData}
          formData={formData}
          values={values}
          setuserTemplateData={setuserTemplateData}
          userTemplateData={userTemplateData}
          componentIndex={componentIndex}
          currentPage={currentPage}
          allWebPages={allWebPages}
        />
      )}

      {userTemplateData[currentPage][componentIndex].data.social_links && (
        <SocialLinks
          iFrameKey={iFrameKey}
          setiframeKey={setiframeKey}
          setshowLoader={setshowLoader}
          setformData={setformData}
          formData={formData}
          values={values}
          editFormNavigation={editFormNavigation}
          seteditFormNavigation={seteditFormNavigation}
          setuserTemplateData={setuserTemplateData}
          userTemplateData={userTemplateData}
          componentIndex={componentIndex}
          currentPage={currentPage}
          allWebPages={allWebPages}
        />
      )}

      <SimpleInput
        name={`${componentIndex}.data.contactForm.email`}
        placeHolder="Enter Email to recieve form responses"
        title="Email"
        {...props}
      />

      <ColorPicker
        title="Background Color"
        name={`${componentIndex}.data.styles.backgroundColor`}
        setuserTemplateData={setuserTemplateData}
        userTemplateData={userTemplateData}
        values={values}
        formData={formData}
        iFrameKey={iFrameKey}
        setiframeKey={setiframeKey}
        setshowLoader={setshowLoader}
        currentPage={currentPage}
      />
      <hr />
    </div>
  );
};
export default Footer;
