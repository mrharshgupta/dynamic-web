import React, { useState } from "react";
import SimpleInput from "../form-fields/simple-input.js";
import ChooseSocialIcon from "../form-fields/choose-social-icon.js";
import _ from "lodash";
import initialValues from "../edit-bar/initialvalues.js";
import SwitchButton from "../form-fields/switch-button.js";
import RemoveButton from "../form-fields/remove-button.js";
import AddButton from "../form-fields/add-button.js";
import Link from "../form-fields/links.js";
import { Animated } from "react-animated-css";
import DropdownHeading from "../form-fields/dropdown-heading.js";

const Information = ({
  componentIndex,
  userTemplateData,
  setuserTemplateData,
  showField,
  setshowField,
  e,
  setiframeKey,
  iFrameKey,
  setshowLoader,
  formData,
  setformData,
  values,
  seteditFormNavigation,
  editFormNavigation,
  imagesToBeUploaded,
  setimagesToBeUploaded,
  currentPage,
}) => {
  const [showInformation, setshowInformation] = useState(false);
  const props = {
    iFrameKey: iFrameKey,
    setiframeKey: setiframeKey,
    setshowLoader: setshowLoader,
    values: values,
    formData: formData,
    setshowFields: setshowField,
    showFields: showField,
    setformData: setformData,
    userTemplateData: userTemplateData,
    setuserTemplateData: setuserTemplateData,
    currentPage: currentPage,
  };
  return (
    <div style={{ width: "100%" }}>
      <DropdownHeading
        heading={`Information`}
        action="T/F"
        setshowFields={setshowInformation}
        showFields={showInformation}
      />

      {showInformation &&
        userTemplateData[currentPage][componentIndex].data.information.name && (
          <SimpleInput
            title="Name:"
            name={`${componentIndex}.data.information.name`}
            placeHolder={`Enter name`}
            {...props}
          />
        )}
      {showInformation &&
        userTemplateData[currentPage][componentIndex].data.information
          .description && (
          <SimpleInput
            title="Description:"
            name={`${componentIndex}.data.information.description`}
            placeHolder={`Enter description`}
            {...props}
          />
        )}
    </div>
  );
};
export default Information;
