import React, { useState } from "react";
import SimpleInput from "../form-fields/simple-input.js";
import ChooseSocialIcon from "../form-fields/choose-social-icon.js";
import _ from "lodash";
import initialValues from "../edit-bar/initialvalues.js";
import SwitchButton from "../form-fields/switch-button.js";
import RemoveButton from "../form-fields/remove-button.js";
import AddButton from "../form-fields/add-button.js";
import Link from "../form-fields/links.js";
import { Animated } from "react-animated-css";
import DropdownHeading from "../form-fields/dropdown-heading.js";

const UsefulLinks = ({
  componentIndex,
  userTemplateData,
  setuserTemplateData,
  showField,
  setshowField,
  e,
  setiframeKey,
  iFrameKey,
  setshowLoader,
  formData,
  setformData,
  values,
  seteditFormNavigation,
  editFormNavigation,
  imagesToBeUploaded,
  setimagesToBeUploaded,
  currentPage,
  allWebPages,
}) => {
  const [showUsefulLinks, setshowUsefulLinks] = useState(false);
  const props = {
    iFrameKey: iFrameKey,
    setiframeKey: setiframeKey,
    setshowLoader: setshowLoader,
    values: values,
    formData: formData,
    setshowFields: setshowField,
    showFields: showField,
    setformData: setformData,
    userTemplateData: userTemplateData,
    setuserTemplateData: setuserTemplateData,
    currentPage: currentPage,
    allWebPages,
  };
  return (
    <div style={{ width: "100%" }}>
      <DropdownHeading
        heading={`Useful Links`}
        action="T/F"
        setshowFields={setshowUsefulLinks}
        showFields={showUsefulLinks}
      />
      {showUsefulLinks &&
        _.times(
          userTemplateData[currentPage][componentIndex].data.usefulLinks[0] !==
            "true" &&
            userTemplateData[currentPage][componentIndex].data.usefulLinks
              .length,
          (e) => (
            <>
              <SimpleInput
                name={`${componentIndex}.data.usefulLinks.${e}.name`}
                placeHolder={`Enter link ${e + 1} name`}
                {...props}
              />

              <Link
                name1={`${componentIndex}.data.usefulLinks.${e}.linkType`}
                name2={`${componentIndex}.data.usefulLinks.${e}.link`}
                {...props}
                targetBlankTitle="Open Link in New Tab"
                targetBlankName={`${componentIndex}.data.usefulLinks[${e}].targetBlank`}
                options={allWebPages}
                other={
                  values[componentIndex].data.usefulLinks[e] &&
                  values[componentIndex].data.usefulLinks[e].linkType
                }
              />

              <RemoveButton
                UTDpath={`${componentIndex}.data.usefulLinks`}
                {...props}
                e={e}
              />
            </>
          )
        )}
      {showUsefulLinks && (
        <>
          <hr />
          <AddButton
            pathToAdd={`${componentIndex}.data.usefulLinks`}
            pathFromAdd={`footer.data.usefulLinks`}
            values={values}
            title="Add Useful Link"
            iFrameKey={iFrameKey}
            setiframeKey={setiframeKey}
            setshowLoader={setshowLoader}
            setformData={setformData}
            formData={formData}
            userTemplateData={userTemplateData}
            setuserTemplateData={setuserTemplateData}
            currentPage={currentPage}
          />{" "}
        </>
      )}
    </div>
  );
};
export default UsefulLinks;
