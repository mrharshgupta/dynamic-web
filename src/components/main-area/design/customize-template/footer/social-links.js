import React, { useState } from "react";
import SimpleInput from "../form-fields/simple-input.js";
import ChooseSocialIcon from "../form-fields/choose-social-icon.js";
import _ from "lodash";
import initialValues from "../edit-bar/initialvalues.js";
import SwitchButton from "../form-fields/switch-button.js";
import SocialLinkComponent from "./social-link-component.js";
import AddButton from "../form-fields/add-button.js";
import DropdownHeading from "../form-fields/dropdown-heading.js";
import RemoveButton from "../form-fields/remove-button.js";

const SocialLinks = ({
  componentIndex,
  userTemplateData,
  setuserTemplateData,
  setiframeKey,
  setshowLoader,
  iFrameKey,
  formData,
  setformData,
  values,
  seteditFormNavigation,
  editFormNavigation,
  imagesToBeUploaded,
  setimagesToBeUploaded,
  currentPage,
}) => {
  const [showsocialLinks, setshowsocialLinks] = useState(false);

  return (
    <div>
      <DropdownHeading
        heading={`Social Links`}
        action="T/F"
        setshowFields={setshowsocialLinks}
        showFields={showsocialLinks}
      />
      {showsocialLinks &&
        _.times(
          userTemplateData[currentPage][componentIndex].data.social_links[0] !==
            "true" &&
            userTemplateData[currentPage][componentIndex].data.social_links &&
            userTemplateData[currentPage][componentIndex].data.social_links
              .length,
          (e) => (
            <div>
              <SimpleInput
                name={`${componentIndex}.data.social_links.${e}.link`}
                placeHolder={`Enter Link ${e + 1}`}
                values={values}
                formData={formData}
                iFrameKey={iFrameKey}
                setiframeKey={setiframeKey}
                setuserTemplateData={setuserTemplateData}
                userTemplateData={userTemplateData}
                setshowLoader={setshowLoader}
                currentPage={currentPage}
              />

              <SwitchButton
                title="Open link in New Tab"
                name={`${componentIndex}.data.social_links[${e}].targetBlank`}
                formData={formData}
                values={values}
                iFrameKey={iFrameKey}
                setiframeKey={setiframeKey}
                setuserTemplateData={setuserTemplateData}
                userTemplateData={userTemplateData}
                setshowLoader={setshowLoader}
                currentPage={currentPage}
              />
              <ChooseSocialIcon
                e={e}
                fileName={`social_link_${e}`}
                name={`${componentIndex}.data.social_links.${e}.logo`}
                values={values}
                setiframeKey={setiframeKey}
                iFrameKey={iFrameKey}
                setshowLoader={setshowLoader}
                formData={formData}
                other={
                  values[componentIndex].data.social_links[e] &&
                  values[componentIndex].data.social_links[e].logo
                }
                setimagesToBeUploaded={setimagesToBeUploaded}
                imagesToBeUploaded={imagesToBeUploaded}
                seteditFormNavigation={seteditFormNavigation}
                editFormNavigation={editFormNavigation}
                userTemplateData={userTemplateData}
                setuserTemplateData={setuserTemplateData}
                componentIndex={componentIndex}
                currentPage={currentPage}
              />

              <RemoveButton
                UTDpath={`${componentIndex}.data.social_links`}
                //setshowFields={setshowsocialLinks}
                values={values}
                //showFields={showsocialLinks}
                setiframeKey={setiframeKey}
                iFrameKey={iFrameKey}
                setshowLoader={setshowLoader}
                formData={formData}
                setformData={setformData}
                userTemplateData={userTemplateData}
                setuserTemplateData={setuserTemplateData}
                e={e}
                currentPage={currentPage}
              />
            </div>
          )
        )}
      {showsocialLinks && (
        <>
          {" "}
          <hr />
          <AddButton
            pathToAdd={`${componentIndex}.data.social_links`}
            pathFromAdd={`header.data.social_links`}
            values={values}
            title="Add Social Link"
            iFrameKey={iFrameKey}
            setiframeKey={setiframeKey}
            setshowLoader={setshowLoader}
            setformData={setformData}
            formData={formData}
            userTemplateData={userTemplateData}
            setuserTemplateData={setuserTemplateData}
            currentPage={currentPage}
          />{" "}
        </>
      )}
    </div>
  );
};
export default SocialLinks;
