import React, { useState } from "react";
import SimpleInput from "../form-fields/simple-input.js";
import ChooseSocialIcon from "../form-fields/choose-social-icon.js";
import _ from "lodash";
import initialValues from "../edit-bar/initialvalues.js";
import SwitchButton from "../form-fields/switch-button.js";
import RemoveButton from "../form-fields/remove-button.js";
import AddButton from "../form-fields/add-button.js";
import Link from "../form-fields/links.js";
import { Animated } from "react-animated-css";
import DropdownHeading from "../form-fields/dropdown-heading.js";

const AdditionalPages = ({
  componentIndex,
  userTemplateData,
  setuserTemplateData,
  showField,
  setshowField,
  e,
  setiframeKey,
  iFrameKey,
  setshowLoader,
  formData,
  setformData,
  values,
  seteditFormNavigation,
  editFormNavigation,
  imagesToBeUploaded,
  setimagesToBeUploaded,
  currentPage,
  allWebPages,
}) => {
  const [showAdditionalPages, setshowAdditionalPages] = useState(false);
  const props = {
    iFrameKey: iFrameKey,
    setiframeKey: setiframeKey,
    setshowLoader: setshowLoader,
    values: values,
    formData: formData,
    setshowFields: setshowField,
    showFields: showField,
    setformData: setformData,
    userTemplateData: userTemplateData,
    setuserTemplateData: setuserTemplateData,
    currentPage: currentPage,
    allWebPages,
  };
  return (
    <div style={{ width: "100%" }}>
      <DropdownHeading
        heading={`Additional Pages`}
        action="T/F"
        setshowFields={setshowAdditionalPages}
        showFields={showAdditionalPages}
      />
      {showAdditionalPages &&
        _.times(
          userTemplateData[currentPage][componentIndex].data
            .additionalPages[0] !== "true" &&
            userTemplateData[currentPage][componentIndex].data.additionalPages
              .length,
          (e) => (
            <>
              <SimpleInput
                name={`${componentIndex}.data.additionalPages.${e}.name`}
                placeHolder={`Enter page ${e + 1} name`}
                {...props}
              />

              <Link
                name1={`${componentIndex}.data.additionalPages.${e}.linkType`}
                name2={`${componentIndex}.data.additionalPages.${e}.link`}
                {...props}
                targetBlankTitle="Open Link in New Tab"
                targetBlankName={`${componentIndex}.data.additionalPages[${e}].targetBlank`}
                options={allWebPages}
                other={
                  values[componentIndex].data.additionalPages[e] &&
                  values[componentIndex].data.additionalPages[e].linkType
                }
              />

              <RemoveButton
                UTDpath={`${componentIndex}.data.additionalPages`}
                {...props}
                e={e}
              />
            </>
          )
        )}
      {showAdditionalPages && (
        <>
          <hr />
          <AddButton
            pathToAdd={`${componentIndex}.data.additionalPages`}
            pathFromAdd={`footer.data.additionalPages`}
            values={values}
            title="Add Additional Page Item"
            iFrameKey={iFrameKey}
            setiframeKey={setiframeKey}
            setshowLoader={setshowLoader}
            setformData={setformData}
            formData={formData}
            userTemplateData={userTemplateData}
            setuserTemplateData={setuserTemplateData}
            currentPage={currentPage}
          />{" "}
        </>
      )}
    </div>
  );
};
export default AdditionalPages;
