import React, { useState } from "react";
import UploadFiles from "../form-fields/upload-file.js";
import SimpleInput from "../form-fields/simple-input.js";
import Link from "../form-fields/links.js";
import UploadFile from "../form-fields/upload-file.js";
import _, { set } from "lodash";
import ColorPicker from "../form-fields/color-picker.js";
import { Animated } from "react-animated-css";
import UploadButton from "../form-fields/upload-button.js";
import AddButton from "../form-fields/add-button.js";
import RemoveButton from "../form-fields/remove-button.js";

const Partners = ({
  componentIndex,
  userTemplateData,
  setuserTemplateData,
  seteditFormNavigation,
  editFormNavigation,
  iFrameKey,
  setiframeKey,
  setshowLoader,
  setformData,
  formData,
  values,
  setactiveComponent,
  imagesToBeUploaded,
  setimagesToBeUploaded,
  currentPage,
}) => {
  setactiveComponent("partners" + componentIndex);
  return (
    <Animated
      animationIn="bounceInLeft"
      animationOut="fadeOut"
      isVisible={true}
    >
      <div>
        {_.times(
          userTemplateData[currentPage][componentIndex].data.partners[0] !==
            "true" &&
            userTemplateData[currentPage][componentIndex].data.partners.length,
          (e) => (
            <div>
              <UploadButton
                imageField={`${componentIndex}.data.partners.${e}.backgroundImage`}
                seteditFormNavigation={seteditFormNavigation}
                editFormNavigation={editFormNavigation}
                title={`Upload Image ${e + 1}`}
                userTemplateData={userTemplateData}
                setuserTemplateData={setuserTemplateData}
                values={values}
                formData={formData}
                iFrameKey={iFrameKey}
                setiframeKey={setiframeKey}
                setshowLoader={setshowLoader}
                currentPage={currentPage}
              />

              <RemoveButton
                UTDpath={`${componentIndex}.data.partners`}
                //setshowFields={setshowField}
                values={values}
                //showFields={showField}
                setiframeKey={setiframeKey}
                iFrameKey={iFrameKey}
                setshowLoader={setshowLoader}
                formData={formData}
                setformData={setformData}
                userTemplateData={userTemplateData}
                setuserTemplateData={setuserTemplateData}
                e={e}
                currentPage={currentPage}
              />
            </div>
          )
        )}
        <ColorPicker
          title="Background Color"
          name={`${componentIndex}.data.styles.backgroundColor`}
          setuserTemplateData={setuserTemplateData}
          userTemplateData={userTemplateData}
          values={values}
          formData={formData}
          iFrameKey={iFrameKey}
          setiframeKey={setiframeKey}
          setshowLoader={setshowLoader}
          currentPage={currentPage}
        />

        <AddButton
          pathToAdd={`${componentIndex}.data.partners`}
          pathFromAdd={`partners.data.partners`}
          title="Add Partner"
          componentIndex={componentIndex}
          iFrameKey={iFrameKey}
          setiframeKey={setiframeKey}
          setshowLoader={setshowLoader}
          setformData={setformData}
          formData={formData}
          userTemplateData={userTemplateData}
          setuserTemplateData={setuserTemplateData}
          values={values}
          currentPage={currentPage}
        />
      </div>
    </Animated>
  );
};
export default Partners;
