import React, { useState, useEffect } from "react";
import _ from "lodash";
import { connect } from "react-redux";
import { getMediaManagerImages, onClickSubmit } from "../api.js";
import UploadFile from "../form-fields/upload-file.js";
import Placeholder from "./placeholder.js";
import styles from "./styles.js";

const MediaManager = ({
  serverName,
  imageField,
  imageDetails,
  changeCustomizeTemplateEditBarHeading,
  name,
  editFormNavigation,
  seteditFormNavigation,
  userTemplateData,
  setuserTemplateData,
  setFieldValue,
  iFrameKey,
  setiframeKey,
  setshowLoader,
  setformData,
  formData,
  values,
  setactiveComponent,
  imagesToBeUploaded,
  setimagesToBeUploaded,
  userSettings,
  setuserSettings,
  currentPage,
}) => {
  const [mediaManagerImages, setmediaManagerImages] = useState(undefined);
  const [loader, setloader] = useState(true);
  const [apiStatus, setapiStatus] = useState(true);
  const [useEffectArray, setuseEffectArray] = useState(1);

  useEffect(() => {
    changeCustomizeTemplateEditBarHeading(name);
    getMediaManagerImages(
      (abc) => setmediaManagerImages(abc),
      () => setloader(false),
      (abc) => setapiStatus(abc)
    );
  }, [useEffectArray]);

  function sort_by_key(array, key) {
    return (
      array &&
      array.sort(function (b, a) {
        var x = a[key];
        var y = b[key];
        return x < y ? -1 : x > y ? 1 : 0;
      })
    );
  }
  let sortArray;
  sortArray = sort_by_key(mediaManagerImages, "date");
  console.log(mediaManagerImages, "images");
  let arrayOfImageAddressFields = imageDetails.fieldName.split(".");
  let a = imageDetails.imageType === "base64" ? userSettings : values;
  _.times(arrayOfImageAddressFields.length - 1, (e) => {
    a = a[arrayOfImageAddressFields[e]];
  });

  // const fileSelect = (event, type) => {
  //   var file = event.target.files[0];
  //   let reader = new FileReader();
  //   reader.readAsDataURL(file);
  //   reader.onload = () => {
  //     console.log(reader.result, "base64 sting of selected image");

  //       setuserSettings(
  //         { ...userSettings },
  //         (settings.storeSettings.general.others.logo = reader.result)
  //       );

  //   };
  //   reader.onerror = function (error) {
  //     console.log("Error: ", error);
  //   };
  // };

  return (
    <div>
      <UploadFile
        setiframeKey={setiframeKey}
        setuserTemplateData={setuserTemplateData}
        userTemplateData={userTemplateData}
        arrayOfImageAddressFields={arrayOfImageAddressFields}
        iFrameKey={iFrameKey}
        setshowLoader={setshowLoader}
        fileName="logo"
        setimagesToBeUploaded={setimagesToBeUploaded}
        imagesToBeUploaded={imagesToBeUploaded}
        emptyTextField="header.name.text"
        values={values}
        setFieldValue={setFieldValue}
        imageField="header.name.logoName"
        setshowLoader={setshowLoader}
        formData={formData}
        seteditFormNavigation={seteditFormNavigation}
        editFormNavigation={editFormNavigation}
        useEffectArray={useEffectArray}
        setuseEffectArray={setuseEffectArray}
        setloader={setloader}
        imageType={imageDetails.imageType}
        userSettings={userSettings}
        setuserSettings={setuserSettings}
        currentPage={currentPage}
      />
      <div>
        {loader && (
          <div style={styles.placeholder}>
            <Placeholder />
          </div>
        )}
        {mediaManagerImages && mediaManagerImages.length === 0 && (
          <h3>No uploaded image</h3>
        )}
        {mediaManagerImages &&
          mediaManagerImages.map((item, i) => {
            return (
              <>
                <span
                  style={{ margin: "10px" }}
                  onClick={() => {
                    if (imageDetails.imageType === "base64") {
                      setuserSettings(
                        { ...userSettings },
                        (a[
                          arrayOfImageAddressFields[
                            arrayOfImageAddressFields.length - 1
                          ]
                        ] = mediaManagerImages[i].name)
                      );
                      //onClickSubmit(values, userSettings);
                      onClickSubmit(
                        { ...userTemplateData, [currentPage]: values },
                        userSettings
                      );
                    } else {
                      setuserTemplateData(
                        { ...userTemplateData },
                        (a[
                          arrayOfImageAddressFields[
                            arrayOfImageAddressFields.length - 1
                          ]
                        ] = mediaManagerImages[i].name)
                      );
                      //onClickSubmit(values);
                      onClickSubmit({
                        ...userTemplateData,
                        [currentPage]: values,
                      });
                    }
                    setiframeKey(iFrameKey + 1);
                    setshowLoader(true);
                  }}
                >
                  {a &&
                  a[
                    arrayOfImageAddressFields[
                      arrayOfImageAddressFields.length - 1
                    ]
                  ] === mediaManagerImages[i].name ? (
                    <img
                      src={`http://${serverName}/dq/files/media-manager/${localStorage.getItem(
                        "userId"
                      )}/submit/${mediaManagerImages[i].name}`}
                      style={styles.selectedImage}
                    />
                  ) : (
                    <img
                      src={`http://${serverName}/dq/files/media-manager/${localStorage.getItem(
                        "userId"
                      )}/submit/${mediaManagerImages[i].name}`}
                      style={styles.image}
                    />
                  )}
                </span>
              </>
            );
          })}
      </div>
    </div>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    changeCustomizeTemplateEditBarHeading: (name) =>
      dispatch({
        type: "CHANGE_CUSTOMIZE_TEMPLATE_EDIT_BAR_HEADING",
        payload: name,
      }),
  };
};

const mapStateToProps = ({
  imageFieldForMediaManagerCustomizeTemplate,
  serverName,
}) => ({
  imageDetails: imageFieldForMediaManagerCustomizeTemplate,
  serverName: serverName,
});
export default connect(mapStateToProps, mapDispatchToProps)(MediaManager);
//export default MediaManager;
