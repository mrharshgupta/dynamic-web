import React from "react";
import { Button } from "react-bootstrap";
import _ from "lodash";
import { onClickSubmit } from "../api.js";

const AddButton = ({
  componentIndex,
  values,
  setformData,
  pathToAdd,
  pathFromAdd,
  userTemplateData,
  setuserTemplateData,
  title,
  formData,
  setshowLoader,
  iFrameKey,
  setiframeKey,
  currentPage,
}) => {
  let arrayOfPathToAddFields = pathToAdd.split(".");
  let b = values;
  _.times(arrayOfPathToAddFields.length, (i) => {
    b = b[arrayOfPathToAddFields[i]];
  });

  let arrayOfPathFromAddFields = pathFromAdd.split(".");
  let a = formData;
  _.times(arrayOfPathFromAddFields.length, (i) => {
    a = a[arrayOfPathFromAddFields[i]];
  });

  const styles = {
    container: { cursor: "pointer", marginBottom: "10px", marginLeft: "10px" },
  };

  return (
    <Button
      onClick={() => {
        setuserTemplateData(
          { ...userTemplateData },
          b[0] === "true" ? (b.splice(0, 1), b.push(a[0])) : b.push(a[0])
        );
        setshowLoader(true);
        setiframeKey(iFrameKey + 1);
        console.log(userTemplateData, "usdab");
        //onClickSubmit(values);
        onClickSubmit({ ...userTemplateData, [currentPage]: values });
      }}
      style={styles.container}
    >
      {title ? title : "Add"}
    </Button>
  );
};
export default AddButton;
