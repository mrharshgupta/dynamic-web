import React, { useState } from "react";
import { connect } from "react-redux";
import { Button } from "react-bootstrap";
import { onClickSubmit } from "../api.js";
import _ from "lodash";

const UploadButton = ({
  serverName,
  setImageField,
  imageField,
  values,
  seteditFormNavigation,
  editFormNavigation,
  title,
  userTemplateData,
  setuserTemplateData,
  formData,
  setiframeKey,
  iFrameKey,
  setshowLoader,
  imageType = "",
  userSettings,
  setuserSettings,
  currentPage,
}) => {
  const [showMediaManager, setshowMediaManager] = useState(false);
  let imageName;
  let res;
  let a;
  const styles = {
    imageAvailableContainer: {
      padding: "10px",
      flexDirection: "column",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      margin: "5px",
      border: "1px solid #d4d6d5",
      borderRadius: "3px",
    },
    image: { height: "50px", width: "50px", marginBottom: "10px" },
    noImageContainer: {
      flexDirection: "column",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      padding: "10px",
      margin: "5px",
      border: "1px solid #d4d6d5",
      borderRadius: "3px",
    },
  };
  if (imageType === "base64") {
    imageName = imageField;
    res = imageField.split(".");
    console.log(res, " res");
    imageName = userSettings;
    _.times(res.length, (e) => {
      imageName && (imageName = imageName[res[e]]);
    });
    a = userSettings;
    _.times(res.length - 1, (e) => {
      a = a[res[e]];
    });
  } else {
    res = imageField.split(".");
    imageName = values;
    _.times(res.length, (e) => {
      imageName && (imageName = imageName[res[e]]);
    });
    a = values;
    _.times(res.length - 1, (e) => {
      a = a[res[e]];
    });
  }
  console.log(imageName, "imageName");
  return (
    <>
      {imageName ? (
        <div style={styles.imageAvailableContainer}>
          {imageName && userSettings ? (
            <img
              src={
                userSettings.storeSettings.general.others.logo.includes(".")
                  ? `https://${serverName}/dq/files/media-manager/${localStorage.getItem(
                      "userId"
                    )}/submit/${userSettings.storeSettings.general.others.logo}`
                  : userSettings.storeSettings.general.others.logo
              }
              style={styles.image}
            />
          ) : (
            <img
              src={`https://${serverName}/dq/files/media-manager/${localStorage.getItem(
                "userId"
              )}/submit/${imageName}`}
              style={styles.image}
            />
          )}
          <div>
            <Button
              onClick={() => {
                //setImageField(imageField);
                setImageField(
                  imageField,
                  imageType === "base64" ? "base64" : ""
                );
                setshowMediaManager(true);
                seteditFormNavigation(
                  editFormNavigation.concat("mediaManager")
                );
              }}
              variant="primary"
            >
              Change Image
            </Button>
            <Button
              onClick={() => {
                if (imageType === "base64") {
                  setuserSettings(
                    { ...userSettings },
                    (a[res[res.length - 1]] = "")
                  );
                  onClickSubmit(
                    { ...userTemplateData, [currentPage]: values },
                    userSettings
                  );
                } else {
                  setuserTemplateData(
                    { ...userTemplateData },
                    (a[res[res.length - 1]] = "")
                  );
                  //onClickSubmit(values);
                  onClickSubmit(
                    { ...userTemplateData, [currentPage]: values },
                    userSettings
                  );
                }
                setiframeKey(iFrameKey + 1);
                setshowLoader(true);
                //onClickSubmit(values, userSettings);
              }}
              variant="primary"
            >
              Remove
            </Button>
          </div>
        </div>
      ) : (
        <div style={styles.noImageContainer}>
          <Button
            onClick={() => {
              setImageField(imageField, imageType === "base64" ? "base64" : "");

              setshowMediaManager(true);
              seteditFormNavigation(editFormNavigation.concat("mediaManager"));
            }}
            variant="primary"
          >
            {title}
          </Button>
        </div>
      )}
    </>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    setImageField: (fieldName, imageType) =>
      dispatch({
        type: "SET_IMAGE_FIELD_NAME_FOR_MEDIA_MANAGER",
        payload: { fieldName: fieldName, imageType: imageType },
      }),
  };
};
const mapStateToProps = ({ serverName }) => ({
  serverName: serverName,
});
export default connect(mapStateToProps, mapDispatchToProps)(UploadButton);
