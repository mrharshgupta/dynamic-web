import React, {useState} from 'react';
import { Field } from 'formik';
import { Editor } from '@tinymce/tinymce-react';

const HtmlEditorBasic = ({name, placeholder, values, title}) => {

const [editorValues, seteditorValues] = useState(values);
console.log(editorValues, "ev");
const Edit = () => {
const handleChange = (e) =>{
 values.header.name.text = e; 
}
return(
        <Editor
         initialValue={values.header.name.text}
         apiKey='51cwur6cytdvu3huo6pkx9zr6bjmkjdem4ljp1wenc4ih3ch'
         init={{
           height: 100,
           width: '100%',
           menubar: false,
           plugins: [
             'advlist autolink lists link image charmap print preview anchor',
             'searchreplace visualblocks code',
             'insertdatetime media table paste code'
           ],
           toolbar:
             'bold italic backcolor'
         }}
         value={editorValues}
         onEditorChange={(e)=>handleChange(e)}
       />      
)
}

return(
<div>
{title && <span>{title}</span>}
 <Field
  name={name}
  component={Edit}
/>
</div>
);
}
export default HtmlEditorBasic;