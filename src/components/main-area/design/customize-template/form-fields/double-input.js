import React from "react";
import { Field } from "formik";
import { onClickSubmit } from "../api.js";

const DoubleInput = ({
  setuserTemplateData,
  userTemplateData,
  name1,
  placeHolder1,
  name2,
  placeHolder2,
  title,
  formData,
  values,
  iFrameKey,
  setiframeKey,
  setshowLoader,
  currentPage,
}) => {
  const styles = {
    title: { marginLeft: "10px" },
    inputField1: {
      border: "1px solid lightgrey",
      backgroundColor: "white",
      paddingLeft: 5,
      borderRadius: 5,
      marginLeft: "10px",
      marginRight: "10px",
      marginTop: "10px",
      width: "90%",
    },
    inputField2: {
      border: "1px solid lightgrey",
      backgroundColor: "white",
      paddingLeft: 5,
      borderRadius: 5,
      marginLeft: "10px",
      marginRight: "10px",
      width: "90%",
    },
  };
  return (
    <div>
      {title && <span style={styles.title}>{title}</span>}
      <Field
        name={name1}
        placeHolder={placeHolder1}
        onBlur={() => {
          setiframeKey(iFrameKey + 1);
          setshowLoader(true);
          setuserTemplateData({
            ...userTemplateData,
            [currentPage]: values,
          });
          onClickSubmit({ ...userTemplateData, [currentPage]: values });
        }}
        style={styles.inputField1}
      />
      <Field
        name={name2}
        placeHolder={placeHolder2}
        onBlur={() => {
          setiframeKey(iFrameKey + 1);
          setuserTemplateData({
            ...userTemplateData,
            [currentPage]: values,
          });
          onClickSubmit({ ...userTemplateData, [currentPage]: values });
          setshowLoader(true);
        }}
        style={styles.inputField2}
      />
      <br />
    </div>
  );
};
export default DoubleInput;
