import React from "react";
import { Field } from "formik";
import UploadFile from "./upload-file.js";
import UploadButton from "./upload-button.js";
import { onClickSubmit } from "../api.js";

const ChooseSoicialIcon = ({
  componentIndex,
  userTemplateData,
  setuserTemplateData,
  e,
  seteditFormNavigation,
  editFormNavigation,
  setiframeKey,
  iFrameKey,
  setshowLoader,
  title,
  name,
  other,
  fileName,
  imagesToBeUploaded,
  setimagesToBeUploaded,
  values,
  formData,
  currentPage,
}) => {
  const styles = {
    container: { marginBottom: "10px" },
    title: { marginLeft: "10px", marginTop: "10px" },
    inputField: { width: "92%", marginLeft: "10px" },
  };

  return (
    <div style={styles.container}>
      {title && <span style={styles.title}>{title}</span>}
      <Field
        style={styles.inputField}
        name={name}
        as="select"
        onBlur={() => {
          setiframeKey(iFrameKey + 1);
          setshowLoader(true);
          //onClickSubmit(values);
          onClickSubmit({ ...userTemplateData, [currentPage]: values });
        }}
      >
        <option>Choose icon...</option>
        <option value="fa fa-facebook-f">Facebook</option>
        <option value="fa fa-instagram">Instagram</option>
        <option value="fa fa-whatsapp">Whatsapp</option>
        <option value="fa fa-linkedin">LinkedIn</option>
        <option value="fa fa-globe">Web</option>
        <option value="other">Other</option>
      </Field>

      {other === "other" && (
        <UploadButton
          imageField={`${componentIndex}.data.social_links.${e}.logoName`}
          seteditFormNavigation={seteditFormNavigation}
          editFormNavigation={editFormNavigation}
          title="Upload image"
          userTemplateData={userTemplateData}
          setuserTemplateData={setuserTemplateData}
          values={values}
          formData={formData}
          iFrameKey={iFrameKey}
          setiframeKey={setiframeKey}
          setshowLoader={setshowLoader}
          currentPage={currentPage}
        />
      )}
    </div>
  );
};
export default ChooseSoicialIcon;
