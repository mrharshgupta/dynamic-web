import React, {useState} from 'react';
import { Field } from 'formik';

const PlusMinus = ({setformData, formData, entry1, entry2, entry, title}) => {
console.log(formData, "entry");
return(
<div>
{entry1}
<span onClick={()=>{if(entry1<3){setformData({...formData}, formData.stats.description = 5)}}}>
	<i class="far fa-plus-square fa-2x"></i>
</span>
<span onClick={()=>{if(entry<=3 && entry>=1){setformData({...formData}, entry = entry - 1)}}}>
	<i class="far fa-minus-square fa-2x"></i>
</span>


</div>
);
}
export default PlusMinus;