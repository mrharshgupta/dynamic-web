import React, { useState } from "react";
import { consoleInDebugModeOnly2 } from "../../../../../helpers/debugging.js";
import axios from "axios";
import { apiUrls } from "../../../../../api/helper.js";
import { onClickSubmit } from "../api.js";
import _ from "lodash";

const UploadFile = ({
  userSettings,
  setuserSettings,
  imageType,
  setuserTemplateData,
  userTemplateData,
  arrayOfImageAddressFields,
  setloader,
  useEffectArray,
  setuseEffectArray,
  imageField,
  setFieldValue,
  fileName,
  values,
  name,
  title,
  imagesToBeUploaded,
  setimagesToBeUploaded,
  setiframeKey,
  iFrameKey,
  setshowLoader,
  formData,
  seteditFormNavigation,
  editFormNavigation,
  currentPage,
}) => {
  const [selectedFile, setselectedFile] = useState(undefined);
  const fileSelect = (event) => {
    setloader(true);
    let a;
    if (imageType === "base64") {
      a = userSettings;
      _.times(arrayOfImageAddressFields.length - 1, (e) => {
        a = a[arrayOfImageAddressFields[e]];
      });
      {
        event.target.files[0].name &&
          setuserSettings(
            { ...userSettings },
            (a[
              arrayOfImageAddressFields[arrayOfImageAddressFields.length - 1]
            ] = event.target.files[0].name)
          );
      }
    } else {
      a = values;
      _.times(arrayOfImageAddressFields.length - 1, (e) => {
        a = a[arrayOfImageAddressFields[e]];
      });
      {
        event.target.files[0].name &&
          setuserTemplateData(
            { ...userTemplateData },
            (a[
              arrayOfImageAddressFields[arrayOfImageAddressFields.length - 1]
            ] = event.target.files[0].name)
          );
      }
    }
    setshowLoader(true);
    if (imageType === "base64") {
      //onClickSubmit(values, userSettings);
      onClickSubmit(
        { ...userTemplateData, [currentPage]: values },
        userSettings
      );
    } else {
      //onClickSubmit(values);
      onClickSubmit({ ...userTemplateData, [currentPage]: values });
    }
    setimagesToBeUploaded(
      { ...imagesToBeUploaded },
      imagesToBeUploaded.fileName.push(event.target.files[0].name),
      imagesToBeUploaded.file.push(event.target.files[0])
    );
    setselectedFile(URL.createObjectURL(event.target.files[0]));

    const fd = new FormData();

    fd.append("image[]", event.target.files[0]);
    fd.append("fileName[]", event.target.files[0].name);

    fd.append("userId", localStorage.getItem("userId"));

    axios
      .post(apiUrls.imageUpload, fd, {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      })
      .then((response) => {
        console.log(response, "response");
        setuseEffectArray(useEffectArray + 1);
        return response.json();
      })
      .then((response) => {
        consoleInDebugModeOnly2("logInSuccess-responseData", response);
        setloader(false);
      })
      .catch((error) => {
        consoleInDebugModeOnly2("logInErrorWithoutApi", error.response);
        setloader(false);
      });

    setiframeKey(iFrameKey + 1);
  };

  const styles = {
    container: {
      marginTop: "5px",
      marginBottom: "5px",
      marginRight: "10px",
      marginLeft: "10px",
      width: "90%",
    },

    image: { height: "30px", width: "60px" },
  };

  return (
    <>
      <div class="input-group mb-3" style={styles.container}>
        <img src={selectedFile} class="input-group-text" style={styles.image} />
        <div class="custom-file">
          <input
            type="file"
            class="custom-file-input"
            id="inputGroupFile01"
            onChange={fileSelect}
            accept=".png, .jpg, .jpeg"
          />
          <label class="custom-file-label" for="inputGroupFile01">
            Choose file
          </label>
        </div>
      </div>
    </>
  );
};
export default UploadFile;
