import React from "react";
import { Button } from "react-bootstrap";
import _ from "lodash";
import { onClickSubmit } from "../api.js";

const RemoveButton = ({
  values,
  setshowFields,
  showFields,
  setformData,
  UTDpath,
  FDpath,
  userTemplateData,
  setuserTemplateData,
  e,
  name,
  formData,
  setshowLoader,
  iFrameKey,
  setiframeKey,
  currentPage,
}) => {
  let arrayOfUTDPathFields = UTDpath.split(".");
  let a = values;
  _.times(arrayOfUTDPathFields.length, (i) => {
    a = a[arrayOfUTDPathFields[i]];
  });

  // let arrayOfFDPathFields = FDpath.split(".");
  // let b = formData;
  // _.times(arrayOfFDPathFields.length - 1, (i) =>{ b = b[arrayOfFDPathFields[i]]; })
  const styles = {
    container: { cursor: "pointer", marginBottom: "10px", marginLeft: "10px" },
  };

  return (
    <span
      onClick={() => {
        setuserTemplateData(
          { ...userTemplateData },
          a.length > 1
            ? a.splice(e, 1)
            : (a.splice(e, 1), a.splice(e, 0, "true"))
        );
        //setformData({...formData}, b[arrayOfFDPathFields[arrayOfFDPathFields.length - 1 ]] = b[arrayOfFDPathFields[arrayOfFDPathFields.length - 1 ]] - 1)
        setshowLoader(true);
        setiframeKey(iFrameKey + 1);
        //onClickSubmit(values);
        onClickSubmit({ ...userTemplateData, [currentPage]: values });
        setshowFields && setshowFields(undefined);
      }}
      variant="danger"
      style={styles.container}
    >
      <i class="fas fa-minus-square fa-2x"></i>
    </span>
  );
};
export default RemoveButton;
