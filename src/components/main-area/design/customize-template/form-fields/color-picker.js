import React from "react";
import { Field } from "formik";
import { onClickSubmit } from "../api.js";

const ColorPicker = ({
  setuserTemplateData,
  userTemplateData,
  name,
  placeHolder,
  title,
  formData,
  values,
  setshowLoader,
  iFrameKey,
  setiframeKey,
  currentPage,
}) => {
  const styles = {
    container: {
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
      height: "50px",
      margin: "10px",
      border: "1px solid #d4d6d5",
      borderRadius: "3px",
    },

    title: { marginLeft: "10px" },

    inputField: { borderRadius: 5, width: "20%", marginRight: "10px" },
  };
  return (
    <div style={styles.container}>
      {title && <span style={styles.title}>{title}: </span>}
      <Field
        type="color"
        opacity
        colorFormat="rgba"
        name={name}
        placeHolder={placeHolder}
        onBlur={() => {
          setiframeKey(iFrameKey + 1);
          setshowLoader(true);
          setuserTemplateData({
            ...userTemplateData,
            [currentPage]: values,
          });
          onClickSubmit({ ...userTemplateData, [currentPage]: values });
        }}
        style={styles.inputField}
      />
    </div>
  );
};
export default ColorPicker;
