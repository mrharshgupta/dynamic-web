import React from "react";
import { Field } from "formik";
import { onClickSubmit } from "../api.js";

const SimpleInput = ({
  setuserTemplateData,
  userTemplateData,
  name,
  placeHolder,
  title,
  values,
  formData,
  setshowLoader,
  iFrameKey,
  setiframeKey,
  currentPage,
}) => {
  console.log(userTemplateData, "kahcjs");
  const styles = {
    inputField: {
      border: "1px solid lightgrey",
      backgroundColor: "white",
      paddingLeft: 5,
      borderRadius: 5,
      marginRight: "10px",
      marginLeft: "10px",
      width: "90%",
    },
    title: { marginLeft: "10px" },
  };
  return (
    <div>
      {title && <span style={styles.title}>{title}</span>}
      <Field
        name={name}
        placeHolder={placeHolder}
        onBlur={() => {
          setiframeKey(iFrameKey + 1);
          setshowLoader(true);
          setuserTemplateData({
            ...userTemplateData,
            [currentPage]: values,
          });
          onClickSubmit({ ...userTemplateData, [currentPage]: values });
        }}
        style={styles.inputField}
      />{" "}
      <br />
    </div>
  );
};
export default SimpleInput;
