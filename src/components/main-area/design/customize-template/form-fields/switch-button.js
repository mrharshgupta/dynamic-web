import React from "react";
import { Field } from "formik";
import { onClickSubmit } from "../api.js";

const SwitchButton = ({
  setuserTemplateData,
  name,
  title,
  formData,
  values,
  setshowLoader,
  iFrameKey,
  setiframeKey,
  currentPage,
  userTemplateData,
}) => {
  const styles = {
    title: { fontSize: 14, marginLeft: "10px" },
  };

  return (
    <div class="switch">
      <label>
        <span style={styles.title}>{title} :</span>
        <Field
          type="checkbox"
          name={name}
          onBlur={() => {
            setiframeKey(iFrameKey + 1);
            setshowLoader(true);
            setuserTemplateData({
              ...userTemplateData,
              [currentPage]: values,
            });
            onClickSubmit({ ...userTemplateData, [currentPage]: values });
          }}
        />
        <span class="lever"></span>
      </label>
    </div>
  );
};
export default SwitchButton;
