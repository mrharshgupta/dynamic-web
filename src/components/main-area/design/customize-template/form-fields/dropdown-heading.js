import React from 'react';

const DropdownHeading = ({action, heading, setshowFields, showFields, e}) => {

        const styles = {
            container: {padding: '10px', marginBottom: '10px', width: '100%', display: 'flex', justifyContent: 'space-between', backgroundColor: 'rgba(220, 220, 220, 0.7)', cursor: 'pointer'} 
        }
    return (
        <>
        <span style={styles.container} 
	  onClick={action === 'T/F' ? ()=>setshowFields(!showFields) : ()=>{setshowFields(showFields === e ? undefined : e)} }>
	  {heading}
	  <i class="fas fa-sort-down"></i>
	  </span>
        </>
    )
}

export default DropdownHeading;