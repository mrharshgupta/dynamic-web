import React, { useState } from "react";
import { Field } from "formik";
import SimpleInput from "../form-fields/simple-input.js";
import { onClickSubmit } from "../api.js";
import SwitchButton from "./switch-button.js";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import ListSubheader from "@material-ui/core/ListSubheader";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  inputField: {
    width: "92%",
    marginLeft: "10px",
    backgroundColor: "white",
    borderWidth: 1,
  },
}));

const Link = ({
  setuserTemplateData,
  userTemplateData,
  name1,
  name2,
  placeHolder,
  title,
  values,
  e,
  options,
  other,
  formData,
  iFrameKey,
  setiframeKey,
  setshowLoader,
  targetBlankName,
  targetBlankTitle,
  currentPage,
}) => {
  const styles = {
    title: { marginLeft: "10px", marginTop: "10px" },
    inputField: { width: "92%", marginLeft: "10px" },
  };
  const classes = useStyles();
  return (
    <div>
      {title && <span style={styles.title}>{title}</span>}
      <Field
        className={classes.inputField}
        //style={styles.inputField}
        name={name1}
        //as="select"
        as={Select}
        onBlur={() => {
          setiframeKey(iFrameKey + 1);
          setshowLoader(true);
          // setuserTemplateData(values);
          // onClickSubmit(values);
          setuserTemplateData({
            ...userTemplateData,
            [currentPage]: values,
          });
          onClickSubmit({ ...userTemplateData, [currentPage]: values });
        }}
      >
        {Object.keys(options).map((value) => {
          let itemToReturn;
          itemToReturn =
            options[value] === "-N:PHD" || options[value] === "-{SHRD" ? (
              <ListSubheader>{value}</ListSubheader>
            ) : (
              <MenuItem value={options[value]}>{value}</MenuItem>
            );
          return itemToReturn;
        })}
      </Field>

      {other === "other" && (
        <SimpleInput
          name={name2}
          placeHolder="Enter link here"
          values={values}
          formData={formData}
          iFrameKey={iFrameKey}
          setiframeKey={setiframeKey}
          setshowLoader={setshowLoader}
          userTemplateData={userTemplateData}
          setuserTemplateData={setuserTemplateData}
          currentPage={currentPage}
        />
      )}
      <SwitchButton
        title={targetBlankTitle}
        name={targetBlankName}
        formData={formData}
        values={values}
        iFrameKey={iFrameKey}
        setiframeKey={setiframeKey}
        userTemplateData={userTemplateData}
        setuserTemplateData={setuserTemplateData}
        setshowLoader={setshowLoader}
        currentPage={currentPage}
      />
    </div>
  );
};
export default Link;
