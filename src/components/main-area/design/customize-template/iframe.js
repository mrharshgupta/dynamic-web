import React, { useContext } from "react";
import styles from "./styles.js";
import { ServerContext } from "../../../../contexts/contexts.js";
import { SlidingLoader } from "../../../loaders/index";
import "./styles.css";

const IFrame = ({
  submit,
  setshowLoader,
  showLoader,
  iFrameKey,
  activeComponent,
  iFrameState,
  applied,
  appliedTemplate,
  userTemplateId,
  currentPage,
  defaultPages,
  setiframeKey2,
  iFrameKey2,
}) => {
  const server = useContext(ServerContext);
  var pageName;
  if (defaultPages.includes(currentPage)) {
    if (currentPage === "home") {
      pageName = "index";
    } else {
      pageName = currentPage;
    }
  } else {
    pageName = "webpage";
  }
console.log(`http://${server}/onitt/stores/${localStorage.getItem(
  "storeName"
)}/${pageName}?submit=true&activeComponent=${activeComponent}`, "dsjbcjkdscbjkcbkscbkdscj")
  return (
    <div style={styles.iFrameContainer}>
      {showLoader && <SlidingLoader />}
     

      <iframe
        src={
          applied === "true"
            ? `http://${server}/onitt/stores/${localStorage.getItem(
                "storeName"
              )}/${pageName}.php?submit=true&activeComponent=${activeComponent}`
            : `http://${server}/onitt/files/stock/${appliedTemplate}?userTemplateId=${userTemplateId}&userId=${localStorage.getItem(
                "userId"
              )}&submit=true&activeComponent=${activeComponent}`
        }
        title="Canary"
        className={!showLoader ? "iframeShow" : "iframeHide"}
        key={iFrameKey}
        style={Object.assign(
          iFrameState === "full"
            ? styles.iFrameFull
            : iFrameState === "phone"
            ? styles.iFramePhone
            : styles.iFrameTablet
        )}
        onLoad={() => {
          setshowLoader(false);
        }}
      />
    </div>
  );
};
export default IFrame;
