import React, { useState } from "react";
import SimpleInput from "../form-fields/simple-input.js";
import DoubleInput from "../form-fields/double-input.js";
import UploadFiles from "../form-fields/upload-file.js";
import Link from "../form-fields/links.js";
import PlusMinus from "../form-fields/plus-minus.js";
import ColorPicker from "../form-fields/color-picker.js";
import _ from "lodash";
import UploadButton from "../form-fields/upload-button.js";
import DropdownHeading from "../form-fields/dropdown-heading.js";
import RemoveButton from "../form-fields/remove-button.js";
import AddButton from "../form-fields/add-button.js";

const Statistics = ({
  componentIndex,
  userTemplateData,
  setuserTemplateData,
  seteditFormNavigation,
  editFormNavigation,
  iFrameKey,
  setiframeKey,
  setshowLoader,
  setformData,
  formData,
  values,
  setactiveComponent,
  imagesToBeUploaded,
  setimagesToBeUploaded,
  currentPage,
  allWebPages,
}) => {
  setactiveComponent("stats" + componentIndex);
  const [showDescriptions, setshowDescriptions] = useState(false);
  const [showStats, setshowStats] = useState(false);

  const props = {
    setiframeKey: setiframeKey,
    iFrameKey: iFrameKey,
    setactiveComponent: setactiveComponent,
    setformData: setformData,
    values: values,
    formData: formData,
    setshowLoader: setshowLoader,
    userTemplateData: userTemplateData,
    setuserTemplateData: setuserTemplateData,
    seteditFormNavigation: seteditFormNavigation,
    editFormNavigation: editFormNavigation,
    imagesToBeUploaded: imagesToBeUploaded,
    setimagesToBeUploaded: setimagesToBeUploaded,
    componentIndex: componentIndex,
    currentPage: currentPage,
    allWebPages,
  };
  return (
    <div>
      <SimpleInput
        name={`${componentIndex}.data.heading.text`}
        placeHolder="Enter Heading"
        title="Heading"
        {...props}
      />
      <SimpleInput
        name={`${componentIndex}.data.title.text`}
        placeHolder="Enter Title"
        title="Title"
        {...props}
      />

      <UploadButton
        imageField={`${componentIndex}.data.backgroundImage`}
        {...props}
        title="Upload background image"
      />

      <SimpleInput
        name={`${componentIndex}.data.button.text`}
        placeHolder="Enter button text"
        title="Button Text"
        {...props}
      />

      <Link
        title="Button Link"
        name1={`${componentIndex}.data.button.linkType`}
        name2={`${componentIndex}.data.button.link`}
        targetBlankTitle="Open Link in New Tab"
        targetBlankName={`${componentIndex}.data.button.targetBlank`}
        {...props}
        options={allWebPages}
        other={
          values[componentIndex].data.button &&
          values[componentIndex].data.button.linkType
        }
      />

      <ColorPicker
        title="Button Color"
        name={`${componentIndex}.data.button.style.backgroundColor`}
        {...props}
      />

      <DropdownHeading
        heading="Descriptions"
        action="T/F"
        setshowFields={setshowDescriptions}
        showFields={showDescriptions}
      />

      {showDescriptions &&
        _.times(
          userTemplateData[currentPage][componentIndex].data.description[0] !==
            "true" &&
            userTemplateData[currentPage][componentIndex].data.description
              .length,
          (e) => (
            <>
              <span style={{ marginLeft: "10px" }}>Description {e + 1}</span>
              <SimpleInput
                name={`${componentIndex}.data.description.${e}.text`}
                placeHolder={`Enter description ${e + 1}`}
                e={e}
                {...props}
              />

              <RemoveButton
                UTDpath={`${componentIndex}.data.description`}
                {...props}
                e={e}
              />

              <br />
            </>
          )
        )}
      {showDescriptions && (
        <>
          <hr />

          <AddButton
            pathToAdd={`${componentIndex}.data.description`}
            pathFromAdd={`stats.data.description`}
            title="Add Description"
            {...props}
          />
        </>
      )}

      <DropdownHeading
        heading="Stats"
        action="T/F"
        setshowFields={setshowStats}
        showFields={showStats}
      />

      {showStats &&
        _.times(
          userTemplateData[currentPage][componentIndex].data.stats[0] !==
            "true" &&
            userTemplateData[currentPage][componentIndex].data.stats.length,
          (e) => (
            <>
              <DoubleInput
                name1={`${componentIndex}.data.stats.${e}.statNumber`}
                placeHolder1={`Enter stat number`}
                name2={`${componentIndex}.data.stats.${e}.statName`}
                placeHolder2={`Enter stat name`}
                title={`Stat ${e + 1}`}
                {...props}
              />

              <RemoveButton
                UTDpath={`${componentIndex}.data.stats`}
                {...props}
                e={e}
              />
            </>
          )
        )}
      {showStats && (
        <>
          <hr />

          <AddButton
            pathToAdd={`${componentIndex}.data.stats`}
            pathFromAdd={`stats.data.stats`}
            title="Add Stat"
            {...props}
          />
        </>
      )}
    </div>
  );
};
export default Statistics;
