import React, { useState } from "react";
import UploadFile from "../form-fields/upload-file.js";
import SimpleInput from "../form-fields/simple-input.js";
import Link from "../form-fields/links.js";
import ColorPicker from "../form-fields/color-picker.js";
import _ from "lodash";
import UploadButton from "../form-fields/upload-button.js";
import styles from "./styles.js";
import DropdownHeading from "../form-fields/dropdown-heading.js";
import RemoveButton from "../form-fields/remove-button.js";

const CarouselFormComponent = ({
  componentIndex,
  showFields,
  setshowFields,
  userTemplateData,
  setuserTemplateData,
  iFrameKey,
  setiframeKey,
  setshowLoader,
  seteditFormNavigation,
  editFormNavigation,
  e,
  setformData,
  formData,
  values,
  setactiveComponent,
  imagesToBeUploaded,
  setimagesToBeUploaded,
  currentPage,
  allWebPages,
}) => {
  const props = {
    iFrameKey: iFrameKey,
    setiframeKey: setiframeKey,
    setshowLoader: setshowLoader,
    values: values,
    formData: formData,
    setuserTemplateData: setuserTemplateData,
    userTemplateData: userTemplateData,
    currentPage: currentPage,
    allWebPages: allWebPages,
  };
  return (
    <div style={styles.componentContainer}>
      <DropdownHeading
        heading={`Slide ${e + 1}`}
        action="setAsE"
        e={e}
        setshowFields={setshowFields}
        showFields={showFields}
        currentPage={currentPage}
        values={values}
        userTemplateData={userTemplateData}
        setuserTemplateData={setuserTemplateData}
      />
      {showFields === e && (
        <>
          <SimpleInput
            name={`${componentIndex}.data.slides.${e}.heading`}
            placeHolder={`Enter heading`}
            {...props}
          />
          <SimpleInput
            name={`${componentIndex}.data.slides.${e}.title`}
            placeHolder={`Enter title`}
            {...props}
          />

          <SimpleInput
            name={`${componentIndex}.data.slides.${e}.description`}
            placeHolder={`Enter description`}
            {...props}
          />
          <SimpleInput
            name={`${componentIndex}.data.slides.${e}.button.text`}
            placeHolder={`Enter button text`}
            {...props}
          />

          <ColorPicker
            title="Button Color"
            name={`${componentIndex}.data.slides[${e}].button.styles.backgroundColor`}
            {...props}
          />

          <Link
            title="Button Link"
            name1={`${componentIndex}.data.slides.${e}.button.linkType`}
            name2={`${componentIndex}.data.slides.${e}.button.link`}
            values={values}
            e={e + 1}
            options={allWebPages}
            other={
              values[componentIndex].data.slides[e] &&
              values[componentIndex].data.slides[e].button.linkType
            }
            iFrameKey={iFrameKey}
            setiframeKey={setiframeKey}
            setshowLoader={setshowLoader}
            formData={formData}
            setuserTemplateData={setuserTemplateData}
            userTemplateData={userTemplateData}
            targetBlankTitle="Open Link in New Tab"
            targetBlankName={`${componentIndex}.data.slides[${e}].button.targetBlank`}
            currentPage={currentPage}
          />

          <UploadButton
            imageField={`${componentIndex}.data.slides.${e}.backgroundImage`}
            seteditFormNavigation={seteditFormNavigation}
            editFormNavigation={editFormNavigation}
            title="Upload Image"
            userTemplateData={userTemplateData}
            setuserTemplateData={setuserTemplateData}
            values={values}
            formData={formData}
            iFrameKey={iFrameKey}
            setiframeKey={setiframeKey}
            setshowLoader={setshowLoader}
            currentPage={currentPage}
          />

          <RemoveButton
            UTDpath={`${componentIndex}.data.slides`} //FDpath={`${componentIndex}.data.slides`}
            setshowFields={setshowFields} //FDpath={`formData.slides.data.slides`}
            showFields={showFields}
            setiframeKey={setiframeKey}
            iFrameKey={iFrameKey}
            values={values}
            setshowLoader={setshowLoader}
            formData={formData}
            setformData={setformData}
            userTemplateData={userTemplateData}
            setuserTemplateData={setuserTemplateData}
            e={e}
            currentPage={currentPage}
          />
        </>
      )}
    </div>
  );
};
export default CarouselFormComponent;
