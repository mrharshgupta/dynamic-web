import React, { useState } from "react";
import UploadFiles from "../form-fields/upload-file.js";
import DoubleInput from "../form-fields/double-input.js";
import Link from "../form-fields/links.js";
import CarouselFormComponent from "./carousel-form-component.js";
import _ from "lodash";
import AddButton from "../form-fields/add-button.js";

const Carousel = ({
  componentIndex,
  userTemplateData,
  setuserTemplateData,
  iFrameKey,
  setiframeKey,
  setshowLoader,
  seteditFormNavigation,
  editFormNavigation,
  setformData,
  formData,
  values,
  setactiveComponent,
  imagesToBeUploaded,
  setimagesToBeUploaded,
  currentPage,
  allWebPages,
}) => {
  const [showFields, setshowFields] = useState(0);
  setactiveComponent("carousel" + componentIndex);

  return (
    <div>
      {_.times(
        userTemplateData[currentPage][componentIndex].data.slides[0] !==
          "true" &&
          userTemplateData[currentPage][componentIndex].data.slides.length,
        (e) => (
          <CarouselFormComponent
            e={e}
            componentIndex={componentIndex}
            showFields={showFields}
            setshowFields={setshowFields}
            imagesToBeUploaded={imagesToBeUploaded}
            setimagesToBeUploaded={setimagesToBeUploaded}
            setformData={setformData}
            formData={formData}
            values={values}
            setactiveComponent={setactiveComponent}
            seteditFormNavigation={seteditFormNavigation}
            editFormNavigation={editFormNavigation}
            iFrameKey={iFrameKey}
            setiframeKey={setiframeKey}
            setshowLoader={setshowLoader}
            userTemplateData={userTemplateData}
            setuserTemplateData={setuserTemplateData}
            currentPage={currentPage}
            allWebPages={allWebPages}
          />
        )
      )}
      <hr />
      <AddButton
        pathToAdd={`${componentIndex}.data.slides`}
        pathFromAdd={`slides.data.slides`}
        title="Add Slide"
        componentIndex={componentIndex}
        iFrameKey={iFrameKey}
        setiframeKey={setiframeKey}
        setshowLoader={setshowLoader}
        setformData={setformData}
        formData={formData}
        userTemplateData={userTemplateData}
        setuserTemplateData={setuserTemplateData}
        values={values}
        currentPage={currentPage}
      />
    </div>
  );
};
export default Carousel;
