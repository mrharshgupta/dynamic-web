import React from "react";
import SimpleInput from "../form-fields/simple-input";
import ColorPicker from "../form-fields/color-picker.js";

const Map = ({
  componentIndex,
  setuserTemplateData,
  userTemplateData,
  sectionNumber,
  formData,
  values,
  setactiveComponent,
  iFrameKey,
  setiframeKey,
  setshowLoader,
  currentPage,
  allWebPages,
}) => {
  setactiveComponent("map" + componentIndex);
  const props = {
    formData: formData,
    iFrameKey: iFrameKey,
    setiframeKey: setiframeKey,
    setshowLoader: setshowLoader,
    values: values,
    userTemplateData: userTemplateData,
    setuserTemplateData: setuserTemplateData,
    currentPage: currentPage,
    allWebPages: allWebPages,
  };

  return (
    <div>
      {userTemplateData[currentPage][componentIndex].data.heading.text !==
        undefined && (
        <SimpleInput
          name={`${componentIndex}.data.heading.text`}
          title="Title"
          placeHolder="Enter title"
          {...props}
        />
      )}
      {userTemplateData[currentPage][componentIndex].data.description.text !==
        undefined && (
        <SimpleInput
          name={`${componentIndex}.data.description.text`}
          title="Description"
          placeHolder="Enter description"
          {...props}
        />
      )}
      {userTemplateData[currentPage][componentIndex].data.address.text !==
        undefined && (
        <SimpleInput
          name={`${componentIndex}.data.address.text`}
          title="Address"
          placeHolder="Enter Address"
          {...props}
        />
      )}

      {userTemplateData[currentPage][componentIndex].data.styles
        .backgroundColor !== undefined && (
        <ColorPicker
          title="background Color"
          name={`${componentIndex}.data.styles.backgroundColor`}
          {...props}
        />
      )}
    </div>
  );
};
export default Map;
