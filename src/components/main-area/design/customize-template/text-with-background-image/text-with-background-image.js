import React, { useState } from "react";
import UploadFiles from "../form-fields/upload-file.js";
import UploadButton from "../form-fields/upload-button.js";
import DoubleInput from "../form-fields/double-input.js";
import SimpleInput from "../form-fields/simple-input.js";
import Link from "../form-fields/links.js";
import ColorPicker from "../form-fields/color-picker.js";
import _ from "lodash";
import styles from "./styles.js";

const TextWithBackgroundImage = ({
  componentIndex,
  userTemplateData,
  setuserTemplateData,
  seteditFormNavigation,
  editFormNavigation,
  iFrameKey,
  setiframeKey,
  setshowLoader,
  setformData,
  formData,
  values,
  setactiveComponent,
  imagesToBeUploaded,
  setimagesToBeUploaded,
  currentPage,
}) => {
  setactiveComponent("textWithBackgroundImage" + componentIndex);
  const props = {
    iFrameKey: iFrameKey,
    setiframeKey: setiframeKey,
    setshowLoader: setshowLoader,
    values: values,
    formData: formData,
    setuserTemplateData: setuserTemplateData,
    userTemplateData: userTemplateData,
    seteditFormNavigation: seteditFormNavigation,
    editFormNavigation: editFormNavigation,
    currentPage: currentPage,
  };
  return (
    <div>
      <SimpleInput
        name={`${componentIndex}.data.heading.text`}
        placeHolder="Enter Title"
        title="Title"
        {...props}
      />
      <SimpleInput
        name={`${componentIndex}.data.description.text`}
        placeHolder="Enter Description"
        title="Description"
        {...props}
      />

      <UploadButton
        imageField={`${componentIndex}.data.styles.background.image`}
        title={`Upload Image`}
        {...props}
      />
    </div>
  );
};
export default TextWithBackgroundImage;
