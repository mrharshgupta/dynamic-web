import React, { useState } from "react";
import { connect } from "react-redux";
import styles from "./style.js";

import { Animated } from "react-animated-css";
import { onClickSubmit } from "../api.js";

const EditBarCards = ({
  trash,
  componentIndex,
  changeCustomizeTemplateEditBarHeading,
  editBarHeading,
  seteditBarHeading,
  setshowLoader,
  formData,
  setformData,
  values,
  userTemplateData,
  setuserTemplateData,
  eye,
  handle,
  setiframeKey,
  iFrameKey,
  seteditFormNavigation,
  editFormNavigation,
  editFormNavigationValue,
  name,
  activeComponent,
  currentPage,
}) => {
  const [eyeIcon, seteyeIcon] = useState(0);

  const sectionShowHandler = () => {
    userTemplateData[currentPage][componentIndex].sectionSettings.show ===
    "false"
      ? setuserTemplateData(
          { ...userTemplateData },
          (userTemplateData[currentPage][componentIndex].sectionSettings.show =
            "true")
        )
      : setuserTemplateData(
          { ...userTemplateData },
          (userTemplateData[currentPage][componentIndex].sectionSettings.show =
            "false")
        );

    //onClickSubmit(values);
    onClickSubmit({ ...userTemplateData, [currentPage]: values });
    setshowLoader(true);
    setiframeKey(iFrameKey + 1);
    //seteyeIcon(eyeIcon+1);
  };
  const sectionDeleteHandler = () => {
    userTemplateData[currentPage].splice(componentIndex, 1);
    //onClickSubmit(values);
    onClickSubmit({ ...userTemplateData, [currentPage]: values });
    setshowLoader(true);
    setiframeKey(iFrameKey + 1);
  };
  return (
    <div
      className="mainDiv"
      style={
        editFormNavigationValue === activeComponent
          ? styles.activeBar
          : styles.nonActiveBar
      }
    >
      <div
        onClick={() => {
          changeCustomizeTemplateEditBarHeading(name);
          seteditFormNavigation(
            editFormNavigation.concat(editFormNavigationValue)
          );
        }}
        style={styles.nameDiv}
      >
        <h5 style={styles.name}>{name}</h5>
      </div>

      {eye && (
        <span
          key={iFrameKey}
          className="button"
          style={styles.eye}
          onClick={() => sectionShowHandler()}
        >
          {userTemplateData &&
          userTemplateData[currentPage][componentIndex] &&
          userTemplateData[currentPage][componentIndex].sectionSettings.show ===
            "false" ? (
            <i class="fas fa-eye-slash"></i>
          ) : (
            <i class="far fa-eye"></i>
          )}
        </span>
      )}

      {trash && (
        <div style={styles.eye} onClick={() => sectionDeleteHandler()}>
          <i class="act-icon text-red far fa-trash-alt"></i>
        </div>
      )}

      {handle && (
        <span {...handle} className="button" style={styles.dragHandle}>
          <i class="fas fa-grip-lines"></i>
        </span>
      )}
    </div>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    changeCustomizeTemplateEditBarHeading: (name) =>
      dispatch({
        type: "CHANGE_CUSTOMIZE_TEMPLATE_EDIT_BAR_HEADING",
        payload: name,
      }),
  };
};

export default connect(null, mapDispatchToProps)(EditBarCards);
