import _ from 'lodash';
export const getHeader =(userTemplateData, formData, menuStockNames, e)=> {
  return ({
    type: 'header',
    sectionSettings: {
      show: true,
    },
    // name: {
    //   text: ((userTemplateData) && userTemplateData[e] && userTemplateData[e].name ? (userTemplateData[e].name.text) : 'FINANCEBUSSINESS'),
    //   logoName: ((userTemplateData) && userTemplateData[e] && userTemplateData[e].name ? (userTemplateData.header.name.logoName) : ''),
    // },
    // phone: {
    //   text: ((userTemplateData) && userTemplateData[e] && userTemplateData[e].phone ? (userTemplateData.header.phone.text) : '090-080-0760'),
    // },
    // time: {
    //   text: ((userTemplateData) && userTemplateData[e] && userTemplateData[e].time ? (userTemplateData[e].time.text) : 'Mon-Fri 09:00-17:00'),
    // }, 
    // social_links: 
    //   _.times(formData && formData.orderOfSections[e] && formData.orderOfSections[e].social_links, (i) =>{ return ({
    //       link: ((userTemplateData && userTemplateData[e] && userTemplateData[e].social_links && userTemplateData[e].social_links[i])? (userTemplateData[e].social_links[i].link) : ''),
    //       targetBlank: ((userTemplateData && userTemplateData[e] && userTemplateData[e].social_links && userTemplateData[e].social_links[i] )? (userTemplateData.header.social_links[i].targetBlank) : false),
    //       logo: ((userTemplateData && userTemplateData[e] && userTemplateData[e].social_links && userTemplateData[e].social_links[i]) ? (userTemplateData[e].social_links[i].logo) : 'fas fa-circle'),
    //       logoName: ((userTemplateData && userTemplateData[e] && userTemplateData[e].social_links && userTemplateData[e].social_links[i]) ? (userTemplateData[e].social_links[i].logoName) : ''),
    //     })}
    //   ),
    // menus: 
    //   _.times(formData && formData.sections.header && formData.sections.header.menus, (e) =>{ return ({
    //       name: ((userTemplateData && userTemplateData.header && userTemplateData.header.menus && userTemplateData.header.menus[e] && userTemplateData.header.menus[e].name)? (userTemplateData.header.menus[e].name) : menuStockNames[e] ? menuStockNames[e] : '' ),
    //       link: ((userTemplateData && userTemplateData.header && userTemplateData.header.menus && userTemplateData.header.menus[e] && userTemplateData.header.menus[e].link)? (userTemplateData.header.menus[e].link) : ''),
    //       linkType: ((userTemplateData && userTemplateData.header && userTemplateData.header.menus && userTemplateData.header.menus[e] && userTemplateData.header.menus[e].linkType)? (userTemplateData.header.menus[e].linkType) : 'default'),
    //       targetBlank: ((userTemplateData && userTemplateData.header &&userTemplateData.header.menus && userTemplateData.header.menus[e] && userTemplateData.header.menus[e].targetBlank)? (userTemplateData.header.menus[e].targetBlank) : false),
    //     })},
    //   ),

  // styles: {
  //       backgroundColorTop: ((userTemplateData && userTemplateData[e] && userTemplateData[e].styles && userTemplateData[e].styles.backgroundColorTop)? (userTemplateData[e].styles.backgroundColorTop) : ''),
  //       backgroundColorBottom: ((userTemplateData && userTemplateData[e] && userTemplateData[e].styles && userTemplateData[e].styles.backgroundColorBottom)? (userTemplateData[e].styles.backgroundColorBottom) : 'transparent'),
  //     }
    
  })
}

export const getCarousel =(userTemplateData = {}, formData, e)=> {
    return ({
        type: "carousel",
        sectionSettings: {
            show: ('true' ),
        },
        // slides:
        //     _.times(formData && formData.orderOfSections[e] && formData.orderOfSections[e].slides, (i) =>{return ({
        //     title: ((userTemplateData && userTemplateData[e] && userTemplateData[e].slides && userTemplateData[e].slides[i] && userTemplateData[e].slides[i].title) ? (userTemplateData[e].slides[i].title) : 'Financial Analysis<br>&amp; Consulting'),
        //     heading: ((userTemplateData && userTemplateData[e] && userTemplateData[e].slides && userTemplateData[e].slides[i] && userTemplateData[e].slides[i].heading) ? (userTemplateData[e].slides[i].heading) : 'we are ready to help you'),
        //     description: ((userTemplateData && userTemplateData[e] && userTemplateData[e].slides && userTemplateData[e].slides[i] && userTemplateData[e].slides[i].description) ? (userTemplateData[e].slides[i].description) : 'This finance HTML template is 100% free of charge provided by TemplateMo for everyone. You can download, edit and use this layout for your business website.'),
        //     backgroundImage: ((userTemplateData && userTemplateData[e] && userTemplateData[e].slides && userTemplateData[e].slides[i]) && userTemplateData[e].slides[i].backgroundImage? userTemplateData[e].slides[i].backgroundImage : ''),
        //     button: {
        //         text: ((userTemplateData) && userTemplateData[e] && userTemplateData[e].slides && userTemplateData[e].slides[i] && userTemplateData[e].slides[i].button.text ? (userTemplateData[e].slides[i].button.text) : 'Contact Us'),
        //         linkType: ((userTemplateData) && userTemplateData[e] && userTemplateData[e].slides && userTemplateData[e].slides[i] && userTemplateData[e].slides[i].button.linkType ? (userTemplateData[e].slides[i].button.linkType) : 'default'),
        //         link: ((userTemplateData) && userTemplateData[e] && userTemplateData[e].slides && userTemplateData[e].slides[i] && userTemplateData[e].slides[i].button.link ? (userTemplateData[e].slides[i].button.link) : ''),
        //         targetBlank: ((userTemplateData) && userTemplateData[e] && userTemplateData[e].slides && userTemplateData[e].slides[i] && userTemplateData[e].slides[i].button.targetBlank ? (userTemplateData[e].slides[i].button.targetBlank) : false),
        //         styles: {
        //         backgroundColor: ((userTemplateData) && userTemplateData.carousel && userTemplateData[e].slides && userTemplateData[e].slides[i] && userTemplateData[e].slides[i].button && userTemplateData[e].slides[i].button.styles && userTemplateData[e].slides[i].button.styles.backgroundColor ? (userTemplateData[e].slides[i].button.styles.backgroundColor) : ''),   
        //         }
        //     }
        //         })}
        //     ),
    
    }  
)
}
 
export const getContactInfo =(userTemplateData = {}, formData, e)=> {
  const {
    title: {
      text: titleText = 'Request a call back right now ?',
    } = {}, 
    description: {
      text: descriptionText = 'Mauris ut dapibus velit cras interdum nisl ac urna tempor mollis.',
    }} = userTemplateData;
  
  return ({
      type: "contactInfo",
      sectionSettings: {
        show: ('true' ),
      },
        title: {
          text: ((userTemplateData && userTemplateData.title && userTemplateData.title.text) ? (userTemplateData.title.text) : 'Request a call back right now ?'),
        },
      description: {
        text: ((userTemplateData && userTemplateData.description && userTemplateData.description.text)? (userTemplateData.description.text) : 'Mauris ut dapibus velit cras interdum nisl ac urna tempor mollis.'),
      },
      styles: {
        backgroundColor: (userTemplateData && userTemplateData.styles && userTemplateData.styles.backgroundColor)? userTemplateData.styles.backgroundColor : '',
      },
      button: {
        text: ((userTemplateData && userTemplateData.button && userTemplateData.button.text)? (userTemplateData.button.text) : 'Contact Us'),
        linkType: ((userTemplateData &&  userTemplateData.button && userTemplateData.button.linkType)? (userTemplateData.button.linkType) : 'default'),
        link: ((userTemplateData && userTemplateData.button && userTemplateData.button.link)? (userTemplateData.button.link) : ''),
        targetBlank: ((userTemplateData)  && userTemplateData.button && userTemplateData.button.targetBlank ? (userTemplateData.button.targetBlank) : false),
      },
})}

// export const getServices =(userTemplateData = {}, formData)=> {
//     return ({
//         type: "services",
//         sectionSettings: {
//             show: (userTemplateData && userTemplateData.services && userTemplateData.services.sectionSettings && userTemplateData.services.sectionSettings.show ? userTemplateData.services.sectionSettings.show : 'true' ),
//         },
//         title: {
//             text: ((userTemplateData) && userTemplateData.services && userTemplateData.services.title && userTemplateData.services.title.text ? (userTemplateData.services.title.text) : 'Financial Services'),
//         },
//         description: {
//             text: ((userTemplateData) && userTemplateData.services && userTemplateData.services.description && userTemplateData.services.description.text ? (userTemplateData.services.description.text) : 'ALIQUAM ID URNA IMPERDIET LIBERO MOLLIS HENDRERIT'),
//         },
//         services: 
//             _.times(formData && formData.sections.services && formData.sections.services.services, (e) =>{ return ({
//             title: {
//                 text: ((userTemplateData) && userTemplateData.services && userTemplateData.services.services && userTemplateData.services.services[e] && userTemplateData.services.services[e].title && userTemplateData.services.services[e].title.text ? (userTemplateData.services.services[e].title.text) : 'Digital Currency'),
//             },
//             backgroundImage: ((userTemplateData) && userTemplateData.services && userTemplateData.services.services && userTemplateData.services.services[e] &&  userTemplateData.services.services[e].backgroundImage ? (userTemplateData.services.services[e].backgroundImage) : ''),
//             description: {
//                 text: ((userTemplateData) && userTemplateData.services && userTemplateData.services.services && userTemplateData.services.services[e] && userTemplateData.services.services[e].description && userTemplateData.services.services[e].description.text ? (userTemplateData.services.services[e].description.text) : 'Sed tincidunt dictum lobortis. Aenean tempus diam vel augue luctus dignissim. Nunc ornare leo tortor.'),
//             },
//             button: {
//                 text: ((userTemplateData) && userTemplateData.services && userTemplateData.services.services && userTemplateData.services.services[e] && userTemplateData.services.services[e].button && userTemplateData.services.services[e].button.text ? (userTemplateData.services.services[e].button.text) : 'Read More'),
//                 linkType: ((userTemplateData) && userTemplateData.services && userTemplateData.services.services && userTemplateData.services.services[e] && userTemplateData.services.services[e].button && userTemplateData.services.services[e].button.linkType ? (userTemplateData.services.services[e].button.linkType) : 'default'),
//                 link:  ((userTemplateData) && userTemplateData.services && userTemplateData.services.services && userTemplateData.services.services[e] && userTemplateData.services.services[e].button && userTemplateData.services.services[e].button.link ? (userTemplateData.services.services[e].button.link) : ''),
//                 targetBlank: ((userTemplateData) && userTemplateData.services && userTemplateData.services.services && userTemplateData.services.services[e] && userTemplateData.services.services[e].button && userTemplateData.services.services[e].button.targetBlank ? (userTemplateData.services.services[e].button.targetBlank) : false),
//                 styles: {
//                 backgroundColor: ((userTemplateData) && userTemplateData.services && userTemplateData.services.services && userTemplateData.services.services[e] && userTemplateData.services.services[e].button && userTemplateData.services.services[e].button.styles && userTemplateData.services.services[e].button.styles.backgroundColor? (userTemplateData.services.services[e].button.styles.backgroundColor) : ''),   
//                 }
//             }
//                 })}
//             ),
//             styles: {
//                 backgroundColor: ((userTemplateData && userTemplateData.services && userTemplateData.services.styles && userTemplateData.services.styles.backgroundColor)? (userTemplateData.services.styles.backgroundColor) : ''),   
//                 }
//     }  
// )}



// export const getHeader =(userTemplateData = {}, formData, menuStockNames, e)=> {
//   const { 
//     name: {
//       text: nameText = 'FINANCEBUSSINESS', logoName = ''
//   },
//   phone: {
//       text: phoneText = '090-080-0760' 
//   },
//   time: {
//       text: timeText = 'Mon-Fri 09:00-17:00'
//   },
//   social_links: socialLinks, 
//   menus,
//   styles: {
//     backgroundColorTop = '',
//     backgroundColorBottom = 'transparent',
//   }
// } = userTemplateData;
//   return ({
//   name: {
//     text: nameText,
//     logoName: logoName,
//   },
//   phone: {
//     text: phoneText,
//   },
//   time: {
//     text: timeText,
//   }, 
//   social_links: 
//     _.times(formData && formData[e].social_links, (i) =>{ return ({
//         link: ((socialLinks && socialLinks[i] && socialLinks[i].link)? (socialLinks[i].link) : ''),
//         targetBlank: ((socialLinks && socialLinks[i] && socialLinks[i].targetBlank)? (socialLinks[i].targetBlank) : false),
//         logo: ((socialLinks && socialLinks[i] && socialLinks[i].logo)? (socialLinks[i].logo) : 'fas fa-circle'),
//         logoName: ((socialLinks && socialLinks[i] && socialLinks[i].logoName)? (socialLinks[i].logoName) : ''),
//       })}
//     ),
//   menus: 
//     _.times(formData && formData[e].menus, (i) =>{ return ({
//         name: ((menus && menus[i] && menus[i].name)? (menus[i].name) : menuStockNames[i] ? menuStockNames[i] : '' ),
//         link: ((menus && menus[i] && menus[i].link)? (menus[i].link) : ''),
//         linkType: ((menus && menus[i] && menus[i].link)? (menus[i].menus[i].linkType) : 'default'),
//         targetBlank: ((menus && menus[i] && menus[i].link)? (menus[i].targetBlank) : false),
//       })},
//     ),

// styles: {
//       backgroundColorTop: backgroundColorTop,
//       backgroundColorBottom: backgroundColorBottom,
//     }
//   }  
// )}