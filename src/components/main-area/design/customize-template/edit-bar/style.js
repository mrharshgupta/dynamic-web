const styles = {
  activeBar: {
    borderRight: "4px solid rgb(0, 229, 234)",
    height: "55px",
    marginTop: "7px",
    marginBottom: "7px",
    //cursor: 'pointer',
    display: "flex",
    alignItems: "center",
    marginLeft: "3px",
    marginRight: "3px",
  },
  nonActiveBar: {
    height: "55px",
    marginTop: "7px",
    marginBottom: "7px",
    //cursor: 'pointer',
    display: "flex",
    alignItems: "center",
    marginLeft: "3px",
    marginRight: "3px",
  },
  nameDiv: { width: "90%", cursor: "pointer" },
  name: { marginLeft: "20px" },
  dragHandle: {
    marginRight: "15px",
  },

  eye: {
    marginRight: "10px",
    cursor: "pointer",
  },
};
export default styles;
