import _ from 'lodash';
import {getHeader, getServices, getCarousel, getContactInfo} from './component-values';

const initialValues = (userTemplateData, formData)=>{
const menuStockNames = ['Home', 'About Us', 'Our Services', 'Contact Us'];


const values = _.times(formData && formData.orderOfSections && formData.orderOfSections.length, (e)=>{
  if(!userTemplateData || !userTemplateData[e]){ 
    
    if(formData && formData.orderOfSections[e] && formData.orderOfSections[e].type === 'header'){
      return(getHeader(userTemplateData[e], formData, menuStockNames, e))
    }
    if(formData && formData.orderOfSections[e] && formData.orderOfSections[e].type === 'contactInfo'){
      return(getContactInfo(userTemplateData[e], formData, e))
    }
    if(formData && formData.orderOfSections[e] && formData.orderOfSections[e].type === 'carousel'){
      return(getCarousel(userTemplateData[e], formData, e))
    }
  } else {
    //console.log(userTemplateData, 'usdiiv')
    if(userTemplateData && userTemplateData[e] && userTemplateData[e].type === 'header'){
      return(getHeader(userTemplateData[e], _.times(formData && formData.orderOfSections.length, (i)=>{if(formData.orderOfSections[i].linkingId === e){return formData.orderOfSections[i]}}), menuStockNames, e))
    }
    if(userTemplateData && userTemplateData[e] && userTemplateData[e].type === 'contactInfo'){
      return(getContactInfo(userTemplateData[e], _.times(formData && formData.orderOfSections.length, (i)=>{if(formData.orderOfSections[i].linkingId === e){return formData.orderOfSections[i]}}), e))
    }
    if(userTemplateData && userTemplateData[e] && userTemplateData[e].type === 'carousel'){
      return(getCarousel(userTemplateData[e], _.times(formData && formData.orderOfSections.length, (i)=>{if(formData.orderOfSections[i].linkingId === e){return formData.orderOfSections[i]}}), e))
    }
  }
  
  
})        

return values;  
}

export default initialValues;

// const values = { 
//         header: {
//           name: {
//             text: ((userTemplateData) && userTemplateData.header && userTemplateData.header.name && userTemplateData.header.name.text ? (userTemplateData.header.name.text) : 'FINANCEBUSSINESS'),
//             logo: ((userTemplateData) && userTemplateData.header && userTemplateData.header.name && userTemplateData.header.name.logo ? (userTemplateData.header.name.logo) : false),
//             logoName: ((userTemplateData) && userTemplateData.header && userTemplateData.header.name && userTemplateData.header.name.logoName ? (userTemplateData.header.name.logoName) : ''),
//           },
//           phone: {
//             text: ((userTemplateData) && userTemplateData.header && userTemplateData.header.phone && userTemplateData.header.phone.text ? (userTemplateData.header.phone.text) : '090-080-0760'),
//           },
//           time: {
//             text: ((userTemplateData) && userTemplateData.header && userTemplateData.header.time && userTemplateData.header.time.text ? (userTemplateData.header.time.text) : 'Mon-Fri 09:00-17:00'),
//           }, 
//           social_links: 
//             _.times(formData && formData.sections.header && formData.sections.header.social_links, (e) =>{ return ({
//                 link: ((userTemplateData && userTemplateData.header && userTemplateData.header.social_links && userTemplateData.header.social_links[e] && userTemplateData.header.social_links[e].link)? (userTemplateData.header.social_links[e].link) : ''),
//                 targetBlank: ((userTemplateData && userTemplateData.header && userTemplateData.header.social_links && userTemplateData.header.social_links[e] && userTemplateData.header.social_links[e].targetBlank)? (userTemplateData.header.social_links[e].targetBlank) : false),
//                 logo: ((userTemplateData && userTemplateData.header && userTemplateData.header.social_links && userTemplateData.header.social_links[e] && userTemplateData.header.social_links[e].logo)? (userTemplateData.header.social_links[e].logo) : 'fas fa-circle'),
//                 logoName: ((userTemplateData && userTemplateData.header && userTemplateData.header.social_links && userTemplateData.header.social_links[e] && userTemplateData.header.social_links[e].logoName)? (userTemplateData.header.social_links[e].logoName) : ''),
//               })}
//             ),
//           menus: 
//             _.times(formData && formData.sections.header && formData.sections.header.menus, (e) =>{ return ({
//                 name: ((userTemplateData && userTemplateData.header && userTemplateData.header.menus && userTemplateData.header.menus[e] && userTemplateData.header.menus[e].name)? (userTemplateData.header.menus[e].name) : menuStockNames[e] ? menuStockNames[e] : '' ),
//                 link: ((userTemplateData && userTemplateData.header && userTemplateData.header.menus && userTemplateData.header.menus[e] && userTemplateData.header.menus[e].link)? (userTemplateData.header.menus[e].link) : ''),
//                 linkType: ((userTemplateData && userTemplateData.header && userTemplateData.header.menus && userTemplateData.header.menus[e] && userTemplateData.header.menus[e].linkType)? (userTemplateData.header.menus[e].linkType) : 'default'),
//                 targetBlank: ((userTemplateData && userTemplateData.header &&userTemplateData.header.menus && userTemplateData.header.menus[e] && userTemplateData.header.menus[e].targetBlank)? (userTemplateData.header.menus[e].targetBlank) : false),
//               })},
//             ),
 
//         styles: {
//               backgroundColorTop: ((userTemplateData && userTemplateData.header && userTemplateData.header.styles && userTemplateData.header.styles.backgroundColorTop)? (userTemplateData.header.styles.backgroundColorTop) : ''),
//               backgroundColorBottom: ((userTemplateData && userTemplateData.header && userTemplateData.header.styles && userTemplateData.header.styles.backgroundColorBottom)? (userTemplateData.header.styles.backgroundColorBottom) : 'transparent'),
//             }
//       },
//       carousel: {
//         sectionSettings: {
//             show: (userTemplateData && userTemplateData.carousel && userTemplateData.carousel.sectionSettings && userTemplateData.carousel.sectionSettings.show ? userTemplateData.carousel.sectionSettings.show : 'true' ),
//         },
//         slides:
//           _.times(formData && formData.sections.carousel && formData.sections.carousel.slides, (e) =>{return ({
//             title: ((userTemplateData && userTemplateData.carousel && userTemplateData.carousel.slides && userTemplateData.carousel.slides[e] && userTemplateData.carousel.slides[e].title) ? (userTemplateData.carousel.slides[e].title) : 'Financial Analysis<br>&amp; Consulting'),
//             heading: ((userTemplateData && userTemplateData.carousel && userTemplateData.carousel.slides && userTemplateData.carousel.slides[e] && userTemplateData.carousel.slides[e].heading) ? (userTemplateData.carousel.slides[e].heading) : 'we are ready to help you'),
//             description: ((userTemplateData && userTemplateData.carousel && userTemplateData.carousel.slides && userTemplateData.carousel.slides[e] && userTemplateData.carousel.slides[e].description) ? (userTemplateData.carousel.slides[e].description) : 'This finance HTML template is 100% free of charge provided by TemplateMo for everyone. You can download, edit and use this layout for your business website.'),
//             backgroundImage: ((userTemplateData && userTemplateData.carousel && userTemplateData.carousel.slides && userTemplateData.carousel.slides[e]) && userTemplateData.carousel.slides[e].backgroundImage? userTemplateData.carousel.slides[e].backgroundImage : ''),
//             button: {
//               text: ((userTemplateData) && userTemplateData.carousel && userTemplateData.carousel.slides && userTemplateData.carousel.slides[e] && userTemplateData.carousel.slides[e].button.text ? (userTemplateData.carousel.slides[e].button.text) : 'Contact Us'),
//               linkType: ((userTemplateData) && userTemplateData.carousel && userTemplateData.carousel.slides && userTemplateData.carousel.slides[e] && userTemplateData.carousel.slides[e].button.linkType ? (userTemplateData.carousel.slides[e].button.linkType) : 'default'),
//               link: ((userTemplateData) && userTemplateData.carousel && userTemplateData.carousel.slides && userTemplateData.carousel.slides[e] && userTemplateData.carousel.slides[e].button.link ? (userTemplateData.carousel.slides[e].button.link) : ''),
//               targetBlank: ((userTemplateData) && userTemplateData.carousel && userTemplateData.carousel.slides && userTemplateData.carousel.slides[e] && userTemplateData.carousel.slides[e].button.targetBlank ? (userTemplateData.carousel.slides[e].button.targetBlank) : false),
//               styles: {
//                 backgroundColor: ((userTemplateData) && userTemplateData.carousel && userTemplateData.carousel.slides && userTemplateData.carousel.slides[e] && userTemplateData.carousel.slides[e].button && userTemplateData.carousel.slides[e].button.styles && userTemplateData.carousel.slides[e].button.styles.backgroundColor ? (userTemplateData.carousel.slides[e].button.styles.backgroundColor) : ''),   
//               }
//             }
//               })}
//             ),  
//       },
//       contactInfo: {
//         //_.times(formData && formData.sections.contactInfo && formData.sections.contactInfo.numberOfSections, (e) =>{return {
//           sectionSettings: {
//             show: (userTemplateData && userTemplateData.contactInfo && userTemplateData.contactInfo.sectionSettings && userTemplateData.contactInfo.sectionSettings.show ? userTemplateData.contactInfo.sectionSettings.show : 'true' ),
//         },
//         title: {
//           text: ((userTemplateData && userTemplateData.contactInfo &&  userTemplateData.contactInfo.title && userTemplateData.contactInfo.title.text)? (userTemplateData.contactInfo.title.text) : 'Request a call back right now ?'),
//         },
//         description: {
//           text: ((userTemplateData && userTemplateData.contactInfo && userTemplateData.contactInfo.description && userTemplateData.contactInfo.description.text)? (userTemplateData.contactInfo.description.text) : 'Mauris ut dapibus velit cras interdum nisl ac urna tempor mollis.'),
//         },
//         styles: {
//           backgroundColor: ((userTemplateData && userTemplateData.contactInfo && userTemplateData.contactInfo.styles && userTemplateData.contactInfo.styles.backgroundColor)? (userTemplateData.contactInfo.styles.backgroundColor) : ''),
//         },
//         button: {
//           text: ((userTemplateData && userTemplateData.contactInfo && userTemplateData.contactInfo.button && userTemplateData.contactInfo.button.text)? (userTemplateData.contactInfo.button.text) : 'Contact Us'),
//           linkType: ((userTemplateData && userTemplateData.contactInfo && userTemplateData.contactInfo.button && userTemplateData.contactInfo.button.linkType)? (userTemplateData.contactInfo.button.linkType) : 'default'),
//           link: ((userTemplateData && userTemplateData.contactInfo && userTemplateData.contactInfo.button && userTemplateData.contactInfo.button.link)? (userTemplateData.contactInfo.button.link) : ''),
//           targetBlank: ((userTemplateData) && userTemplateData.contactInfo && userTemplateData.contactInfo.button && userTemplateData.contactInfo.button.targetBlank ? (userTemplateData.contactInfo.button.targetBlank) : false),
//         },
//       },
//       //}}),
//       services: {
//         sectionSettings: {
//           show: (userTemplateData && userTemplateData.services && userTemplateData.services.sectionSettings && userTemplateData.services.sectionSettings.show ? userTemplateData.services.sectionSettings.show : 'true' ),
//       },
//         title: {
//           text: ((userTemplateData) && userTemplateData.services && userTemplateData.services.title && userTemplateData.services.title.text ? (userTemplateData.services.title.text) : 'Financial Services'),
//         },
//         description: {
//           text: ((userTemplateData) && userTemplateData.services && userTemplateData.services.description && userTemplateData.services.description.text ? (userTemplateData.services.description.text) : 'ALIQUAM ID URNA IMPERDIET LIBERO MOLLIS HENDRERIT'),
//         },
//         services: 
//           _.times(formData && formData.sections.services && formData.sections.services.services, (e) =>{ return ({
//             title: {
//               text: ((userTemplateData) && userTemplateData.services && userTemplateData.services.services && userTemplateData.services.services[e] && userTemplateData.services.services[e].title && userTemplateData.services.services[e].title.text ? (userTemplateData.services.services[e].title.text) : 'Digital Currency'),
//             },
//             backgroundImage: ((userTemplateData) && userTemplateData.services && userTemplateData.services.services && userTemplateData.services.services[e] &&  userTemplateData.services.services[e].backgroundImage ? (userTemplateData.services.services[e].backgroundImage) : ''),
//             description: {
//               text: ((userTemplateData) && userTemplateData.services && userTemplateData.services.services && userTemplateData.services.services[e] && userTemplateData.services.services[e].description && userTemplateData.services.services[e].description.text ? (userTemplateData.services.services[e].description.text) : 'Sed tincidunt dictum lobortis. Aenean tempus diam vel augue luctus dignissim. Nunc ornare leo tortor.'),
//             },
//             button: {
//               text: ((userTemplateData) && userTemplateData.services && userTemplateData.services.services && userTemplateData.services.services[e] && userTemplateData.services.services[e].button && userTemplateData.services.services[e].button.text ? (userTemplateData.services.services[e].button.text) : 'Read More'),
//               linkType: ((userTemplateData) && userTemplateData.services && userTemplateData.services.services && userTemplateData.services.services[e] && userTemplateData.services.services[e].button && userTemplateData.services.services[e].button.linkType ? (userTemplateData.services.services[e].button.linkType) : 'default'),
//               link:  ((userTemplateData) && userTemplateData.services && userTemplateData.services.services && userTemplateData.services.services[e] && userTemplateData.services.services[e].button && userTemplateData.services.services[e].button.link ? (userTemplateData.services.services[e].button.link) : ''),
//               targetBlank: ((userTemplateData) && userTemplateData.services && userTemplateData.services.services && userTemplateData.services.services[e] && userTemplateData.services.services[e].button && userTemplateData.services.services[e].button.targetBlank ? (userTemplateData.services.services[e].button.targetBlank) : false),
//               styles: {
//                 backgroundColor: ((userTemplateData) && userTemplateData.services && userTemplateData.services.services && userTemplateData.services.services[e] && userTemplateData.services.services[e].button && userTemplateData.services.services[e].button.styles && userTemplateData.services.services[e].button.styles.backgroundColor? (userTemplateData.services.services[e].button.styles.backgroundColor) : ''),   
//               }
//             }
//               })}
//             ),
//           styles: {
//                 backgroundColor: ((userTemplateData && userTemplateData.services && userTemplateData.services.styles && userTemplateData.services.styles.backgroundColor)? (userTemplateData.services.styles.backgroundColor) : ''),   
//               }
//         },
//       stats: {
//         sectionSettings: {
//           show: (userTemplateData && userTemplateData.stats && userTemplateData.stats.sectionSettings && userTemplateData.stats.sectionSettings.show ? userTemplateData.stats.sectionSettings.show : 'true' ),
//       },
//         heading: {
//           text: ((userTemplateData) && userTemplateData.stats && userTemplateData.stats.heading && userTemplateData.stats.heading.text ? (userTemplateData.stats.heading.text) : 'LOREM IPSUM DOLOR SIT AMET'),
//         },
//         title: {
//           text: ((userTemplateData) && userTemplateData.stats && userTemplateData.stats.title &&  userTemplateData.stats.title.text? (userTemplateData.stats.title.text) : 'Our solutions for your business growth'),
//         },
//         backgroundImage: ((userTemplateData) && userTemplateData.stats && userTemplateData.stats.backgroundImage ? userTemplateData.stats.backgroundImage : ''),
//         description: 
//           _.times(formData && formData.sections.stats && formData.sections.stats.description, (e) =>{ return ({
//             text: ((userTemplateData) && userTemplateData.stats && userTemplateData.stats.description && userTemplateData.stats.description[e] && userTemplateData.stats.description[e].text ? (userTemplateData.stats.description[e].text) : 'Pellentesque ultrices at turpis in vestibulum. Aenean pretium elit nec congue elementum. Nulla luctus laoreet porta. Maecenas at nisi tempus, porta metus vitae, faucibus augue.'),
//           })}),
//         button :{
//           text: ((userTemplateData) && userTemplateData.stats && userTemplateData.stats.button && userTemplateData.stats.button.text ? (userTemplateData.stats.button.text) : 'Read More'),
//           linkType: ((userTemplateData) && userTemplateData.stats && userTemplateData.stats.button && userTemplateData.stats.button.linkType ? (userTemplateData.stats.button.linkType) : 'default'),
//           targetBlank: ((userTemplateData) && userTemplateData.stats && userTemplateData.stats.button  && userTemplateData.stats.button.targetBlank ? (userTemplateData.stats.button.targetBlank) : false),
//           link: ((userTemplateData) && userTemplateData.stats && userTemplateData.stats.button && userTemplateData.stats.button.link ? (userTemplateData.stats.button.link) : ''),
//           styles: {
//             backgroundColor: ((userTemplateData) && userTemplateData.stats && userTemplateData.stats.button && userTemplateData.stats.button.styles && userTemplateData.stats.button.styles.backgroundColor ? (userTemplateData.stats.button.styles.backgroundColor) : ''),   
//           }
//         },
//         stats: 
//           _.times(formData && formData.sections.stats && formData.sections.stats.stats, (e) =>{ return ({
//             statNumber: ((userTemplateData) && userTemplateData.stats && userTemplateData.stats.stats && userTemplateData.stats.stats[e] && userTemplateData.stats.stats[e].statNumber ? (userTemplateData.stats.stats[e].statNumber) : '625'),
//             statName: ((userTemplateData) && userTemplateData.stats && userTemplateData.stats.stats && userTemplateData.stats.stats[e] && userTemplateData.stats.stats[e].statName ? (userTemplateData.stats.stats[e].statName) : 'Projects'),
//           })}),
//       },
      
//       imageWithText: {
//       //  sectionPosition: formData && formData.orderOfSections && formData.orderOfSections.imageWithText && formData.orderOfSections.imageWithText,
//       sectionSettings: {
//         show: (userTemplateData && userTemplateData.imageWithText && userTemplateData.imageWithText.sectionSettings && userTemplateData.imageWithText.sectionSettings.show ? userTemplateData.imageWithText.sectionSettings.show : 'true' ),
//     },  
//       heading: {
//           text: ((userTemplateData) && userTemplateData.imageWithText && userTemplateData.imageWithText.heading && userTemplateData.imageWithText.heading.text ? (userTemplateData.imageWithText.heading.text) : 'WHO WE ARE'),
//         },
//         title: {
//           text: ((userTemplateData) && userTemplateData.imageWithText && userTemplateData.imageWithText.title && userTemplateData.imageWithText.title.text ? (userTemplateData.imageWithText.title.text) : 'Get to know about our company'),
//         },
//         description: {
//           text: ((userTemplateData) && userTemplateData.imageWithText && userTemplateData.imageWithText.description && userTemplateData.imageWithText.description.text ? (userTemplateData.imageWithText.description.text) : 'Get to know about our company'),
//         },
//         backgroundImage: ((userTemplateData) && userTemplateData.imageWithText && userTemplateData.imageWithText.backgroundImage ? userTemplateData.imageWithText.backgroundImage : ''),
//         // description: 
//         //   _.times(formData && formData.sections.imageWithText && formData.sections.imageWithText.description, (e) =>{ return ({
//         //     text: ((userTemplateData) && userTemplateData.imageWithText && userTemplateData.imageWithText.description && userTemplateData.imageWithText.description[e] && userTemplateData.imageWithText.description[e].text ? (userTemplateData.imageWithText.description[e].text) : 'Curabitur pulvinar sem a leo tempus facilisis. Sed non sagittis neque. Nulla conse quat tellus nibh, id molestie felis sagittis ut. Nam ullamcorper tempus ipsum in cursus'),
//         //   })}),
//         button :{
//           text: ((userTemplateData) && userTemplateData.imageWithText && userTemplateData.imageWithText.button && userTemplateData.imageWithText.button.text ? (userTemplateData.imageWithText.button.text) : 'Read More'),
//           linkType: ((userTemplateData) && userTemplateData.imageWithText && userTemplateData.imageWithText.button && userTemplateData.imageWithText.button.linkType ? (userTemplateData.imageWithText.button.linkType) : 'default'),
//           link: ((userTemplateData) && userTemplateData.imageWithText && userTemplateData.imageWithText.button && userTemplateData.imageWithText.button.link ? (userTemplateData.imageWithText.button.link) : ''),
//           targetBlank: ((userTemplateData) && userTemplateData.imageWithText && userTemplateData.imageWithText.button  && userTemplateData.imageWithText.button.targetBlank ? (userTemplateData.imageWithText.button.targetBlank) : false),
//           styles: {
//             backgroundColor: ((userTemplateData) && userTemplateData.imageWithText && userTemplateData.imageWithText.button && userTemplateData.imageWithText.button.styles && userTemplateData.imageWithText.button.styles.backgroundColor ? (userTemplateData.imageWithText.button.styles.backgroundColor) : ''),
//           },
//         },
//         styles: {
//           backgroundColor: ((userTemplateData) && userTemplateData.imageWithText && userTemplateData.imageWithText.styles && userTemplateData.imageWithText.styles.backgroundColor ? (userTemplateData.imageWithText.styles.backgroundColor) : ''),
//         },
//       },
      
//       testimonials: {
//         //sectionPosition: formData && formData.orderOfSections && formData.orderOfSections.testimonials && formData.orderOfSections.testimonials,
//         sectionSettings: {
//           show: (userTemplateData && userTemplateData.testimonials && userTemplateData.testimonials.sectionSettings && userTemplateData.testimonials.sectionSettings.show ? userTemplateData.testimonials.sectionSettings.show : 'true' ),
//       },
//         description: {
//           text: ((userTemplateData) && userTemplateData.testimonials && userTemplateData.testimonials.description && userTemplateData.testimonials.description.text ? (userTemplateData.testimonials.description.text) : 'What they say about us'),
//         },
//         title: {
//           text: ((userTemplateData) && userTemplateData.testimonials && userTemplateData.testimonials.title && userTemplateData.testimonials.title.text ? (userTemplateData.testimonials.title.text) : 'TESTIMONIALS FROM OUR GREATEST CLIENTS'),
//         },
//         styles: {
//           backgroundColor: ((userTemplateData) && userTemplateData.testimonials && userTemplateData.testimonials.styles && userTemplateData.testimonials.styles.backgroundColor ? (userTemplateData.testimonials.styles.backgroundColor) : ''),
//         },
//         testimonials: 
//           _.times(formData && formData.sections.testimonials && formData.sections.testimonials.testimonials, (e) =>{ return ({
//             name: ((userTemplateData) && userTemplateData.testimonials && userTemplateData.testimonials.testimonials && userTemplateData.testimonials.testimonials[e] && userTemplateData.testimonials.testimonials[e].name ? (userTemplateData.testimonials.testimonials[e].name) : 'Andrew Boom'),
//             position: ((userTemplateData) && userTemplateData.testimonials && userTemplateData.testimonials.testimonials && userTemplateData.testimonials.testimonials[e] && userTemplateData.testimonials.testimonials[e].position ? (userTemplateData.testimonials.testimonials[e].position) : 'Marketing Head'),
//             review: ((userTemplateData) && userTemplateData.testimonials && userTemplateData.testimonials.testimonials && userTemplateData.testimonials.testimonials[e] && userTemplateData.testimonials.testimonials[e].review ? (userTemplateData.testimonials.testimonials[e].review) : 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.'),
//             backgroundImage: ((userTemplateData) && userTemplateData.testimonials && userTemplateData.testimonials.testimonials && userTemplateData.testimonials.testimonials[e] &&  userTemplateData.testimonials.testimonials[e].backgroundImage ? (userTemplateData.testimonials.testimonials[e].backgroundImage) : ''),
//             styles: {
//               backgroundColor: ((userTemplateData) && userTemplateData.testimonials && userTemplateData.testimonials.testimonials && userTemplateData.testimonials.testimonials[e] && userTemplateData.testimonials.testimonials[e].styles && userTemplateData.testimonials.testimonials[e].styles.backgroundColor ? (userTemplateData.testimonials.testimonials[e].styles.backgroundColor) : ''),
//         },
//           })}),
//       },
//       contactForm: {
//         sectionSettings: {
//           show: (userTemplateData && userTemplateData.contactForm && userTemplateData.contactForm.sectionSettings && userTemplateData.contactForm.sectionSettings.show ? userTemplateData.contactForm.sectionSettings.show : 'true' ),
//       },
//         styles: {
//           backgroundColor: (userTemplateData && userTemplateData.contactForm && userTemplateData.contactForm.styles && userTemplateData.contactForm.styles.backgroundColor ? (userTemplateData.contactForm.styles.backgroundColor) : ''),
//         },
//         title: {
//           text: (userTemplateData && userTemplateData.contactForm && userTemplateData.contactForm.title && userTemplateData.contactForm.title.text ? (userTemplateData.contactForm.title.text) : 'Request a call back'),
//         },
//         description: {
//           text: (userTemplateData && userTemplateData.contactForm && userTemplateData.contactForm.description && userTemplateData.contactForm.description.text ? (userTemplateData.contactForm.description.text) : 'ETIAM SUSCIPIT ANTE A ODIO CONSEQUAT'),
//         },
//         email: (userTemplateData && userTemplateData.contactForm && userTemplateData.contactForm.email ? (userTemplateData.contactForm.email) : ''),
//       },
//       partners:{
//         sectionSettings: {
//           show: (userTemplateData && userTemplateData.partners && userTemplateData.partners.sectionSettings && userTemplateData.partners.sectionSettings.show ? userTemplateData.partners.sectionSettings.show : 'true' ),
//       },
//         partners:
//           _.times(formData && formData.sections.partners && formData.sections.partners.partners, (e) =>{ return ({
//             backgroundImage: (userTemplateData && userTemplateData.partners.partners && userTemplateData.partners.partners[e] && userTemplateData.partners.partners[e].backgroundImage ? userTemplateData.partners.partners[e].backgroundImage : ''),
//           })}),
//         styles: {
//           backgroundColor: (userTemplateData && userTemplateData.partners && userTemplateData.partners.styles && userTemplateData.partners.styles.backgroundColor ? (userTemplateData.partners.styles.backgroundColor) : ''),
//         },
//       }
//     }
     
// return values
// }

// export default initialValues;
