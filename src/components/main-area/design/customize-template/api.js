import { apiUrls } from "../../../../api/helper";
import { consoleInDebugModeOnly2 } from "../../../../helpers/debugging";
import * as firebase from "firebase";
import axios from "axios";
import _ from "lodash";

export function copyFiles(setiframeKey) {
  fetch(apiUrls.copyFiles, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      source: `../media-manager/${localStorage.getItem("userId")}/submit`,
      destination: `../media-manager/${localStorage.getItem("userId")}/saved`,
    }),
  })
    .then((response) => {
      console.log(response, "response");
      return response.json();
    })
    .then((responseData) => {
      consoleInDebugModeOnly2("logInSuccess-responseData", responseData);
      setiframeKey();
    })
    .catch((error) => {
      consoleInDebugModeOnly2("logInErrorWithoutApi", error.response);
    });
}

//set to delete
export function upLoadImages(imagesToBeUploaded, setiFrameKey) {
  console.log(imagesToBeUploaded.file.length);
  console.log(imagesToBeUploaded);

  {
    _.times(
      imagesToBeUploaded.file.length,
      (e) => {
        const fd = new FormData();
        imagesToBeUploaded &&
          fd.append(
            "image",
            imagesToBeUploaded.file[e],
            imagesToBeUploaded.file[e].name
          );
        fd.append("userId", localStorage.getItem("userId"));
        fd.append("imagesToBeUploaded", imagesToBeUploaded);
        fd.append("fileName", imagesToBeUploaded.fileName[e]);
        fd.append("saved", true);

        axios
          .post(apiUrls.imageUpload, fd, {
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
            },
          })
          .then((response) => {
            console.log(response, "response");
            return response.json();
          })
          .then((response) => {
            consoleInDebugModeOnly2("logInSuccess-responseData", response);
          })
          .catch((error) => {
            consoleInDebugModeOnly2("logInErrorWithoutApi", error.response);
          });
      },
      setiFrameKey()
    );
  }
}

export function onClickSubmit(templateData, userSettings) {
  let dataToSubmit = {
    templateData,
  };

  const Data = firebase
    .database()
    .ref(
      "user_template_data/" +
        localStorage.getItem("userTemplateIdForEditingTemplate") +
        "/submit"
    )
    .set(templateData);
  if (userSettings) {
    const settingsData = firebase
      .database()
      .ref("userSettings/data/" + localStorage.getItem("userId"))
      .set(userSettings);
  }
}

export function onClickSave(templateData, userTemplateId) {
  let dataToSubmit = {
    templateData,
  };
  const Data = firebase
    .database()
    .ref(
      "user_template_data/" +
        localStorage.getItem("userTemplateIdForEditingTemplate") +
        "/saved"
    )
    .set(templateData);
}

export function removeTemplateEntry(entryPath) {
  const userRef = firebase
    .database()
    .ref(
      "user_template_data/" +
        localStorage.getItem("userTemplateId") +
        "/" +
        entryPath
    )
    .remove();
}

export function getMediaManagerImages(
  setmediaManagerImages,
  setloader,
  setapiStatus
) {
  fetch(apiUrls.getMediaManagerImages, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      userId: localStorage.getItem("userId"),
    }),
  })
    .then((response) => {
      console.log(response, "response");
      return response.json();
    })
    .then((responseData) => {
      consoleInDebugModeOnly2("logInSuccess-responseData", responseData);
      setmediaManagerImages(responseData);
      setloader();
      setapiStatus(true);
    })
    .catch((error) => {
      consoleInDebugModeOnly2("logInErrorWithoutApi", error.response);
      setloader();
      setapiStatus(false);
    });
}
