import React from "react";
import SimpleInput from "../form-fields/simple-input";
import DoubleInput from "../form-fields/double-input.js";
import Link from "../form-fields/links.js";
import ColorPicker from "../form-fields/color-picker.js";

const ContactInfo = ({
  componentIndex,
  setuserTemplateData,
  userTemplateData,
  sectionNumber,
  formData,
  values,
  setactiveComponent,
  iFrameKey,
  setiframeKey,
  setshowLoader,
  currentPage,
  allWebPages,
}) => {
  setactiveComponent("contactInfo" + componentIndex);
  const props = {
    formData: formData,
    iFrameKey: iFrameKey,
    setiframeKey: setiframeKey,
    setshowLoader: setshowLoader,
    values: values,
    userTemplateData: userTemplateData,
    setuserTemplateData: setuserTemplateData,
    currentPage: currentPage,
    allWebPages: allWebPages,
  };

  return (
    <div>
      {userTemplateData[currentPage][componentIndex].data.title.text !==
        undefined && (
        <SimpleInput
          name={`${componentIndex}.data.title.text`}
          title="Title"
          placeHolder="Enter title"
          {...props}
        />
      )}
      {userTemplateData[currentPage][componentIndex].data.description.text !==
        undefined && (
        <SimpleInput
          name={`${componentIndex}.data.description.text`}
          title="Description"
          placeHolder="Enter description"
          {...props}
        />
      )}
      {userTemplateData[currentPage][componentIndex].data.button.text !==
        undefined && (
        <>
          <SimpleInput
            name={`${componentIndex}.data.button.text`}
            placeHolder="Enter button text"
            title="Button"
            {...props}
          />
          <Link
            name1={`${componentIndex}.data.button.linkType`}
            name2={`${componentIndex}.data.button.link`}
            targetBlankTitle="Open Link in New Tab"
            targetBlankName={`${componentIndex}.data.button.targetBlank`}
            {...props}
            options={allWebPages}
            other={
              values[componentIndex].data.button &&
              values[componentIndex].data.button.linkType
            }
          />
        </>
      )}

      {userTemplateData[currentPage][componentIndex].data.styles
        .backgroundColor !== undefined && (
        <ColorPicker
          title="background Color"
          name={`${componentIndex}.data.styles.backgroundColor`}
          {...props}
        />
      )}
    </div>
  );
};
export default ContactInfo;
