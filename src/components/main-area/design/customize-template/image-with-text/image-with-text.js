import React, { useState } from "react";
import UploadFiles from "../form-fields/upload-file.js";
import UploadButton from "../form-fields/upload-button.js";
import DoubleInput from "../form-fields/double-input.js";
import SimpleInput from "../form-fields/simple-input.js";
import Link from "../form-fields/links.js";
import ColorPicker from "../form-fields/color-picker.js";
import _ from "lodash";
import styles from "./styles.js";

const ImageWithText = ({
  componentIndex,
  userTemplateData,
  setuserTemplateData,
  seteditFormNavigation,
  editFormNavigation,
  iFrameKey,
  setiframeKey,
  setshowLoader,
  setformData,
  formData,
  values,
  setactiveComponent,
  imagesToBeUploaded,
  setimagesToBeUploaded,
  currentPage,
  allWebPages,
}) => {
  setactiveComponent("imageWithText" + componentIndex);
  const props = {
    iFrameKey: iFrameKey,
    setiframeKey: setiframeKey,
    setshowLoader: setshowLoader,
    values: values,
    formData: formData,
    setuserTemplateData: setuserTemplateData,
    userTemplateData: userTemplateData,
    seteditFormNavigation: seteditFormNavigation,
    editFormNavigation: editFormNavigation,
    currentPage: currentPage,
    allWebPages,
  };
  return (
    <div>
      <SimpleInput
        name={`${componentIndex}.data.heading.text`}
        placeHolder="Enter Heading"
        title="Heading"
        {...props}
      />
      <SimpleInput
        name={`${componentIndex}.data.title.text`}
        placeHolder="Enter Title"
        title="Title"
        {...props}
      />
      <SimpleInput
        name={`${componentIndex}.data.description.text`}
        placeHolder="Enter Description"
        title="Description"
        {...props}
      />

      <UploadButton
        imageField={`${componentIndex}.data.backgroundImage`}
        title={`Upload Image`}
        {...props}
      />

      <SimpleInput
        name={`${componentIndex}.data.button.text`}
        placeHolder="Enter button text"
        title="Button Text"
        {...props}
      />
      <Link
        title="Button Link"
        name1={`${componentIndex}.data.button.linkType`}
        name2={`${componentIndex}.data.button.link`}
        {...props}
        targetBlankTitle="Open Link in New Tab"
        targetBlankName={`${componentIndex}.data.button.targetBlank`}
        options={allWebPages}
        other={
          values[componentIndex].data.button &&
          values[componentIndex].data.button.linkType
        }
      />
      <ColorPicker
        title="Button Color"
        name={`${componentIndex}.data.button.styles.backgroundColor`}
        {...props}
      />

      <ColorPicker
        title="Background Color"
        name={`${componentIndex}.data.styles.backgroundColor`}
        {...props}
      />
    </div>
  );
};
export default ImageWithText;
