import React, { useState, useEffect, useContext } from "react";
import { Link } from "react-router-dom";
import { onClickSubmit } from "./api";
import IFrame from "./iframe.js";
import styles from "./styles";
import * as firebase from "firebase";
import EditForm from "./edit-bar.js";
import "./styles.css";
import { ServerContext } from "../../../../contexts/contexts.js";
import initialValues from "./edit-bar/initialvalues.js";
import {
  Spinner,
  DropdownButton,
  Dropdown,
  ButtonGroup,
} from "react-bootstrap";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import NativeSelect from "@material-ui/core/NativeSelect";
import InputBase from "@material-ui/core/InputBase";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import { useDispatch, connect } from "react-redux";
import _ from "lodash";
import { getComponentsAccordingToPlan } from "./utils/functions";
import { SlidingLoader } from "../../../loaders/index";

const BootstrapInput = withStyles((theme) => ({
  root: {
    marginRight: "50px",
    "label + &": {
      //marginTop: theme.spacing(3),
    },
  },
  input: {
    //minWidth: "100px",
    borderRadius: 4,

    position: "relative",

    //backgroundColor: theme.palette.background.paper,
    border: "1px solid #ced4da",
    fontSize: 16,
    //padding: "10px 26px 10px 12px",
    transition: theme.transitions.create(["border-color", "box-shadow"]),
    // Use the system font instead of the default Roboto font.

    "&:focus": {
      borderRadius: 4,
      borderColor: "#80bdff",
      boxShadow: "0 0 0 0.2rem rgba(0,123,255,.25)",
    },
  },
}))(InputBase);

const useStyles = makeStyles((theme) => ({
  margin: {
    //margin: theme.spacing(1),
  },
}));

const Template = ({ match, loginData: { selectedPlan } = {} }) => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const [showLoader, setshowLoader] = useState(true);
  const [editBarLoader, seteditBarLoader] = useState(true);
  const [iFrameState, setiFrameState] = useState("full");
  const [iFrameKey, setiframeKey] = useState(1);
  const [iFrameKey2, setiframeKey2] = useState(1);
  const [submit, setsubmit] = useState(false);
  const [activeComponent, setactiveComponent] = useState(undefined);
  const [userTemplateData, setuserTemplateData] = useState(undefined);
  const [userTemplateDataBackup, setuserTemplateDataBackup] = useState(
    undefined
  );
  const [currentPage, setcurrentPage] = useState("home");
  const [formData, setformData] = useState(undefined);
  const [userSettings, setuserSettings] = useState(undefined);
  const [userWebPagesData, setuserWebPagesData] = useState({});
  const [userWebPagesDataKeys, setuserWebPagesDataKeys] = useState([]);
  const [userBlogData, setuserBlogData] = useState({});
  const [userBlogDataKeys, setuserBlogDataKeys] = useState([]);
  const [editFormNavigation, seteditFormNavigation] = useState(["home"]);
  const [editBarHeading, seteditBarHeading] = useState(["Edit Template"]);

  const server = useContext(ServerContext);
  const userTemplateId = match.params.userTemplateId;
  localStorage.setItem("userTemplateIdForEditingTemplate", userTemplateId);
  const appliedTemplate = match.params.appliedTemplate;
  const applied = match.params.applied;
  var list = {};
  var defaultPages = [];
  var defaultPagesList = [];
  var userBlogs = [];

  useEffect(() => {
    //get user webpages
    const userWebPageData = firebase
      .database()
      .ref("user_webpages_data/" + localStorage.getItem("userId"));
    userWebPageData.on("value", (snapshot) => {
      setuserWebPagesData(snapshot.val() ? Object.values(snapshot.val()) : {});
      setuserWebPagesDataKeys(
        snapshot.val() ? Object.keys(snapshot.val()) : []
      );
    });

    //get user blogs
    const userBlogData = firebase
      .database()
      .ref("user_blog_data/" + localStorage.getItem("userId"));
    userBlogData.on("value", (snapshot) => {
      setuserBlogData(snapshot.val() ? Object.values(snapshot.val()) : {});
      setuserBlogDataKeys(snapshot.val() ? Object.keys(snapshot.val()) : []);
    });

    const userSettingsRef = firebase
      .database()
      .ref("userSettings/data/" + localStorage.getItem("userId"));

    userSettingsRef.on("value", (userSettingsSnapshot) => {
      setuserSettings(userSettingsSnapshot.val());
    });

    const stockTemplateDataRef = firebase
      .database()
      .ref("form_data/" + appliedTemplate);
    userTemplateId !== "undefined" &&
      appliedTemplate !== "undefined" &&
      stockTemplateDataRef.on("value", async (stockTemplateDataSnapshot) => {
        let stockTemplateData = await getComponentsAccordingToPlan(
          stockTemplateDataSnapshot.val().userData,
          "withPages"
        );
        let stockFormData = await getComponentsAccordingToPlan(
          stockTemplateDataSnapshot.val().defaultSectionsData,
          "onlyComponents"
        );
        console.log(stockTemplateData, "stockTemplateDataSnapshot");
        const savedData = firebase
          .database()
          .ref("user_template_data/" + userTemplateId + "/saved");
        savedData.on("value", (savedDataSnapshot) => {
          if (savedDataSnapshot.val() !== null) {
            //setuserTemplateData(savedDataSnapshot.val());
            setuserTemplateData({
              ...stockTemplateData,
              ...savedDataSnapshot.val(),
            });
            setuserTemplateDataBackup({
              ...stockTemplateData,
              ...savedDataSnapshot.val(),
            });
            console.log(
              stockFormData,
              "stockFormDatastockFormDatastockFormData"
            );
            setformData(stockFormData);
            onClickSubmit(savedDataSnapshot.val());
          } else if (savedDataSnapshot.val() === null) {
            setuserTemplateData(stockTemplateData);
            setuserTemplateDataBackup(stockTemplateData);
            setformData(stockFormData);
            onClickSubmit(stockTemplateData && stockTemplateData);
          }
        });
        seteditBarLoader(false);
      });
  }, []);

  if (appliedTemplate === "undefined") {
    return (
      <div style={styles.noTemplateContainer}>
        <h1>No Template Chosen</h1>
        <Link
          style={styles.link}
          className="button"
          to={`/${localStorage.getItem("storeName")}/home/design/templates`}
        >
          Go to Templates
        </Link>
        <hr />
      </div>
    );
    setshowLoader(false);
  }
  const getPages = () => {
    for (var i = 0; i < userWebPagesData.length; i++) {
      list[userWebPagesData[i]["name"]] = userWebPagesDataKeys[i];
    }

    defaultPages = Object.keys(userTemplateData);
    defaultPagesList = [];
    for (var i = 0; i < defaultPages.length; i++) {
      defaultPagesList[
        defaultPages[i].charAt(0).toUpperCase() + defaultPages[i].slice(1)
      ] = defaultPages[i];
    }

    list = {
      "Choose..": "default",
      ...defaultPagesList,
      "Custom Pages": "-{SHRD", //taken one alphabet right of PAGES
      ...list,
    };
  };

  const getBlogs = () => {
    var data = [];
    for (var i = 0; i < userBlogData.length; i++) {
      data[userBlogData[i]["title"]] = userBlogDataKeys[i];
    }

    list = {
      ...list,
      Blogs: "-N:PHD", //taken one alphabet right of BLOGS
      "All Blogs": "blog",
      ...data,
      Other: "other",
    };
  };
  if (userTemplateData) {
    getPages();
    getBlogs();
  }

  console.log(formData, " jksbckjsdbcjksdc");
  console.log(userBlogDataKeys, "listlist");
  return (
    <div>
      {/* {showLoader && (
        <SlidingLoader />
        // <div style={styles.loader}>
        //   <Spinner animation="grow" variant="primary" size="xl" />
        // </div>
      )} */}

      <IFrame
        submit={submit}
        setshowLoader={setshowLoader}
        iFrameKey={iFrameKey}
        setiframeKey2={setiframeKey2}
        iFrameKey2={iFrameKey2}
        activeComponent={activeComponent}
        iFrameState={iFrameState}
        applied={applied}
        appliedTemplate={appliedTemplate}
        userTemplateId={userTemplateId}
        currentPage={currentPage}
        defaultPages={defaultPages}
        showLoader={showLoader}
      />

      {userTemplateData && (
        <EditForm
          currentPage={currentPage}
          userTemplateData={userTemplateData}
          setuserTemplateData={setuserTemplateData}
          setshowLoader={setshowLoader}
          iFrameKey={iFrameKey}
          setiframeKey={setiframeKey}
          formData={formData}
          setformData={setformData}
          setsubmit={setsubmit}
          setactiveComponent={setactiveComponent}
          activeComponent={activeComponent}
          editBarLoader={editBarLoader}
          seteditBarLoader={seteditBarLoader}
          userTemplateId={userTemplateId}
          userSettings={userSettings}
          setuserSettings={setuserSettings}
          userWebPagesData={userWebPagesData}
          userWebPagesDataKeys={userWebPagesDataKeys}
          editFormNavigation={editFormNavigation}
          seteditFormNavigation={seteditFormNavigation}
          editBarHeading={editBarHeading}
          seteditBarHeading={seteditBarHeading}
          defaultPages={defaultPages}
          allWebPages={list}
          userTemplateDataBackup={userTemplateDataBackup}
        />
      )}

      <div className="trial-bottom" style={styles.footer}>
        <p>
          {userTemplateData && (
            <FormControl className={classes.margin}>
              <Select
                labelId="demo-customized-select-label"
                id="demo-customized-select"
                placeholder="Blog category"
                value={currentPage}
                onChange={(e) => {
                  setcurrentPage(e.target.value);
                  setshowLoader(true);
                  setiframeKey(iFrameKey + 1);
                  seteditFormNavigation(["home"]);
                  seteditBarHeading(["Edit Template"]);
                  dispatch({
                    type: "CUSTOMIZE_TEMPLATE_EDIT_BAR_CROSS_BUTTON",
                  });
                }}
                input={<BootstrapInput />}
              >
                {Object.keys(defaultPagesList).map((item, i) => (
                  <MenuItem value={`${list[item]}`}>
                    {item.slice(0, 1).toUpperCase() + item.slice(1)}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          )}

          <button
            style={styles.link}
            className="button"
            onClick={() => {
              setiframeKey(iFrameKey + 1);
              setshowLoader(true);
            }}
          >
            <i class="fas fa-sync-alt" style={styles.footerButtons}></i>Refresh
          </button>
          <a
            style={styles.link}
            href={
              applied === "true"
                ? `http://${server}/onitt/stores/${localStorage.getItem(
                    "storeName"
                  )}`
                : `http://${server}/onitt/files/stock/${appliedTemplate}?userTemplateId=${userTemplateId}&userId=${localStorage.getItem(
                    "userId"
                  )}`
            }
            className="button"
            target="_blank"
          >
            <i class="fas fa-eye" style={styles.footerButtons}></i>Live Preview
          </a>

          <span
            className="button"
            style={styles.gadgetsButton}
            onClick={() => setiFrameState("full")}
          >
            <i class="fas fa-laptop"></i>
          </span>
          <span
            className="button"
            style={styles.gadgetsButton}
            onClick={() => setiFrameState("phone")}
          >
            <i class="fas fa-mobile-alt"></i>
          </span>
          <span
            className="button"
            style={styles.gadgetsButton}
            onClick={() => setiFrameState("tablet")}
          >
            <i class="fas fa-tablet-alt"></i>
          </span>
        </p>
      </div>
    </div>
  );
};
const mapStateToProps = ({ loginData }) => ({
  loginData,
});
export default connect(mapStateToProps)(Template);
