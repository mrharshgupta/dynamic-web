import React, { useState, useEffect } from "react";
import _ from "lodash";
import AddTemplateComponentCard from "./add-template-component-card.js";

const AddTemplateComponent = ({
  values,
  setshowLoader,
  setiframeKey,
  iFrameKey,
  userTemplateData,
  setuserTemplateData,
  editFormNavigation,
  seteditFormNavigation,
  formData,
  setformData,
  currentPage,
}) => {
  console.log(userTemplateData, "usd");

  const props = {
    seteditFormNavigation: seteditFormNavigation,
    editFormNavigation: editFormNavigation,
    formData: formData,
    setformData: setformData,
    userTemplateData: userTemplateData,
    setuserTemplateData: setuserTemplateData,
    values: values,
    setshowLoader: setshowLoader,
    setiframeKey: setiframeKey,
    iFrameKey: iFrameKey,
    currentPage: currentPage,
  };
  return (
    <div>
      {formData && formData.contactInfo && (
        <AddTemplateComponentCard name="CTA" type="contactInfo" {...props} />
      )}

      {formData && formData.slides && (
        <AddTemplateComponentCard name="Slides" type="slides" {...props} />
      )}

      {formData && formData.services && (
        <AddTemplateComponentCard name="Services" type="services" {...props} />
      )}

      {formData && formData.stats && (
        <AddTemplateComponentCard name="Stats" type="stats" {...props} />
      )}

      {formData && formData.imageWithText && (
        <AddTemplateComponentCard
          name="Image With Text"
          type="imageWithText"
          {...props}
        />
      )}

      {formData && formData.testimonials && (
        <AddTemplateComponentCard
          name="Testimonials"
          type="testimonials"
          {...props}
        />
      )}

      {formData && formData.contactForm && (
        <AddTemplateComponentCard
          name="Contact Form"
          type="contactForm"
          {...props}
        />
      )}

      {formData && formData.partners && (
        <AddTemplateComponentCard name="Partners" type="partners" {...props} />
      )}

      {formData && formData.textWithBackgroundImage && (
        <AddTemplateComponentCard
          name="Text With Background Image"
          type="textWithBackgroundImage"
          {...props}
        />
      )}
      {formData && formData.map && (
        <AddTemplateComponentCard name="Map" type="map" {...props} />
      )}
    </div>
  );
};
export default AddTemplateComponent;
