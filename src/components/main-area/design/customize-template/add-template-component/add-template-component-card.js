import React, { useState, useEffect } from "react";
import _ from "lodash";
import { connect } from "react-redux";
import { onClickSubmit } from "../api.js";
import styles from "./card-styles.js";
import "./card-styles.css";

const AddTemplateComponentCard = ({
  setshowLoader,
  setiframeKey,
  iFrameKey,
  values,
  userTemplateData,
  setuserTemplateData,
  editFormNavigation,
  seteditFormNavigation,
  goToHome,
  type,
  name,
  formData,
  setformData,
  currentPage,
}) => {
  const addComponentCard = () => {
    let key = _.size(values);
    key = key - 1;

    values.splice([key], 0, formData[type]);
    //onClickSubmit(userTemplateData);
    onClickSubmit({ ...userTemplateData, [currentPage]: values });
    goToHome();
    seteditFormNavigation(["home"]);
    setshowLoader(true);
    setiframeKey(iFrameKey + 1);
  };

  return (
    <div
      className="mainDiv"
      style={styles.container}
      onClick={() => addComponentCard()}
    >
      <h5 style={styles.name}>{name}</h5>
    </div>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    goToHome: () =>
      dispatch({ type: "GO_TO_HOME_AFTER_TEMPLATE_COMPONENT_ADD" }),
  };
};

export default connect(null, mapDispatchToProps)(AddTemplateComponentCard);
