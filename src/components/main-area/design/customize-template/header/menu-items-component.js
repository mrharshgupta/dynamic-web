import React, { useState } from "react";
import SimpleInput from "../form-fields/simple-input.js";
import ChooseSocialIcon from "../form-fields/choose-social-icon.js";
import _ from "lodash";
import initialValues from "../edit-bar/initialvalues.js";
import SwitchButton from "../form-fields/switch-button.js";
import RemoveButton from "../form-fields/remove-button.js";
import Link from "../form-fields/links.js";
import { Animated } from "react-animated-css";
import DropdownHeading from "../form-fields/dropdown-heading.js";

const MenuItemsComponent = ({
  componentIndex,
  userTemplateData,
  setuserTemplateData,
  showField,
  setshowField,
  e,
  setiframeKey,
  iFrameKey,
  setshowLoader,
  formData,
  setformData,
  values,
  seteditFormNavigation,
  editFormNavigation,
  imagesToBeUploaded,
  setimagesToBeUploaded,
  userWebPagesData,
  userWebPagesDataKeys,
  currentPage,
  allWebPages,
}) => {
  const props = {
    iFrameKey: iFrameKey,
    setiframeKey: setiframeKey,
    setshowLoader: setshowLoader,
    values: values,
    formData: formData,
    e: e,
    setshowFields: setshowField,
    showFields: showField,
    setformData: setformData,
    userTemplateData: userTemplateData,
    setuserTemplateData: setuserTemplateData,
    currentPage: currentPage,
    allWebPages,
  };

  var list = {};
  for (var i = 0; i < userWebPagesData.length; i++) {
    list[userWebPagesData[i]["name"]] = userWebPagesDataKeys[i];
  }

  var defaultPages = Object.keys(userTemplateData);
  var defaultPagesList = [];
  for (var i = 0; i < defaultPages.length; i++) {
    console.log("dsvfjh");
    defaultPagesList[defaultPages[i]] = defaultPages[i];
  }
  console.log(defaultPages, "defaultPages");
  list = {
    "Choose..": "default",
    Blog: "blog",
    ...defaultPagesList,
    ...list,
    Other: "other",
  };
  console.log(list, "userWebPagesData");

  return (
    <div style={{ width: "100%" }}>
      <DropdownHeading
        heading={`Menu ${e + 1}`}
        action="setAsE"
        setshowFields={setshowField}
        showFields={showField}
        e={e}
        currentPage={currentPage}
      />

      {showField === e && (
        <Animated
          animationIn="bounceInLeft"
          animationOut="fadeOut"
          isVisible={true}
        >
          <div>
            <SimpleInput
              name={`${componentIndex}.data.menus.${e}.name`}
              placeHolder={`Enter menu ${e + 1} name`}
              {...props}
            />

            <Link
              name1={`${componentIndex}.data.menus.${e}.linkType`}
              name2={`${componentIndex}.data.menus.${e}.link`}
              {...props}
              targetBlankTitle="Open Link in New Tab"
              targetBlankName={`${componentIndex}.data.menus[${e}].targetBlank`}
              // options={{
              //   "Choose..": "default",
              //   Blog: "blog",
              //   Home: "index",
              //   About: "about",
              //   Services: "services",
              //   contact: "contact",
              //   Other: "other",
              // }}
              options={allWebPages}
              other={
                values[componentIndex].data.menus[e] &&
                values[componentIndex].data.menus[e].linkType
              }
            />
          </div>

          <RemoveButton UTDpath={`${componentIndex}.data.menus`} {...props} />
        </Animated>
      )}
    </div>
  );
};
export default MenuItemsComponent;
