import React, { useState } from "react";
import SimpleInput from "../form-fields/simple-input.js";
import ChooseSocialIcon from "../form-fields/choose-social-icon.js";
import _ from "lodash";
import initialValues from "../edit-bar/initialvalues.js";
import SwitchButton from "../form-fields/switch-button.js";
import SocialLinkComponent from "./social-link-component.js";
import AddButton from "../form-fields/add-button.js";

const SocialLinks = ({
  componentIndex,
  userTemplateData,
  setuserTemplateData,
  setiframeKey,
  setshowLoader,
  iFrameKey,
  formData,
  setformData,
  values,
  seteditFormNavigation,
  editFormNavigation,
  imagesToBeUploaded,
  setimagesToBeUploaded,
  currentPage,
}) => {
  const [showField, setshowField] = useState(0);

  return (
    <div>
      {_.times(
        userTemplateData[currentPage][componentIndex].data.social_links[0] !==
          "true" &&
          userTemplateData[currentPage][componentIndex].data.social_links &&
          userTemplateData[currentPage][componentIndex].data.social_links
            .length,
        (e) => (
          <div>
            {" "}
            {/** length-1 because one additional thing is there in database **/}
            <SocialLinkComponent
              e={e}
              imagesToBeUploaded={imagesToBeUploaded}
              setimagesToBeUploaded={setimagesToBeUploaded}
              setformData={setformData}
              formData={formData}
              values={values}
              componentIndex={componentIndex}
              iFrameKey={iFrameKey}
              setiframeKey={setiframeKey}
              setshowLoader={setshowLoader}
              seteditFormNavigation={seteditFormNavigation}
              editFormNavigation={editFormNavigation}
              userTemplateData={userTemplateData}
              setuserTemplateData={setuserTemplateData}
              showField={showField}
              setshowField={setshowField}
              currentPage={currentPage}
            />
          </div>
        )
      )}
      <hr />
      <AddButton
        pathToAdd={`${componentIndex}.data.social_links`}
        pathFromAdd={`header.data.social_links`}
        values={values}
        title="Add Social Link"
        iFrameKey={iFrameKey}
        setiframeKey={setiframeKey}
        setshowLoader={setshowLoader}
        setformData={setformData}
        formData={formData}
        userTemplateData={userTemplateData}
        setuserTemplateData={setuserTemplateData}
        currentPage={currentPage}
      />
    </div>
  );
};
export default SocialLinks;
