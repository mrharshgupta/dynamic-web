import React, { useState } from "react";
import SimpleInput from "../form-fields/simple-input.js";
import SocialLinks from "./social-links.js";
import EditBarCards from "../edit-bar/edit-bar.js";
import MenuItems from "./menu-items.js";
import HtmlEditorBasic from "../form-fields/html-editor-basic.js";
import ColorPicker from "../form-fields/color-picker.js";
import UploadFile from "../form-fields/upload-file.js";
import SwitchButton from "../form-fields/switch-button.js";
import UploadButton from "../form-fields/upload-button.js";
const Header = ({
  editBarHeading,
  seteditBarHeading,
  setiframeKey,
  iFrameKey,
  onClickSubmit,
  setformData,
  formData,
  values,
  setactiveComponent,
  seteditFormNavigation,
  editFormNavigation,
  imagesToBeUploaded,
  setimagesToBeUploaded,
  setshowLoader,
  userTemplateData,
  setuserTemplateData,
  setFieldValue,
  componentIndex,
  userSettings,
  setuserSettings,
  userWebPagesData,
  userWebPagesDataKeys,
  currentPage,
  allWebPages,
}) => {
  const [showSocialLinksContent, setshowSocialLinksContent] = useState(false);
  const [showMenuItemsContent, setshowMenuItemsContent] = useState(false);

  setactiveComponent("header" + componentIndex);
  const props = {
    iFrameKey: iFrameKey,
    setiframeKey: setiframeKey,
    setshowLoader: setshowLoader,
    values: values,
    formData: formData,
    userTemplateData: userTemplateData,
    setuserTemplateData: setuserTemplateData,
    editFormNavigation: editFormNavigation,
    seteditFormNavigation: seteditFormNavigation,
    imagesToBeUploaded: imagesToBeUploaded,
    setimagesToBeUploaded: setimagesToBeUploaded,
    editBarHeading: editBarHeading,
    seteditBarHeading: seteditBarHeading,
    componentIndex: componentIndex,
    userSettings: userSettings,
    setuserSettings: setuserSettings,
    userWebPagesData: userWebPagesData,
    userWebPagesDataKeys: userWebPagesDataKeys,
    currentPage: currentPage,
    allWebPages,
  };
  return (
    <div>
      {editFormNavigation[editFormNavigation.length - 1] ===
        `header${componentIndex}` &&
        userTemplateData[currentPage][componentIndex].data.name && (
          <>
            <SimpleInput
              name={`${componentIndex}.data.name.text`}
              placeHolder="Enter Name"
              title="Name"
              {...props}
            />

            <UploadButton
              //imageField={`${componentIndex}.data.name.logoName`}
              imageField={`storeSettings.general.others.logo`}
              seteditFormNavigation={seteditFormNavigation}
              editFormNavigation={editFormNavigation}
              title="Upload Logo"
              {...props}
              imageType="base64"
            />
          </>
        )}
      {editFormNavigation[editFormNavigation.length - 1] ===
        `header${componentIndex}` &&
        userTemplateData[currentPage][componentIndex].data.phone.text && (
          <SimpleInput
            name={`${componentIndex}.data.phone.text`}
            placeHolder="Enter Phone"
            title="Phone"
            {...props}
          />
        )}
      {editFormNavigation[editFormNavigation.length - 1] ===
        `header${componentIndex}` &&
        userTemplateData[currentPage][componentIndex].data.time.text && (
          <SimpleInput
            name={`${componentIndex}.data.time.text`}
            placeHolder="Enter Time"
            title="Time"
            {...props}
          />
        )}

      {editFormNavigation[editFormNavigation.length - 1] ===
        `header${componentIndex}` &&
        userTemplateData[currentPage][componentIndex].data.styles
          .backgroundColorTop && (
          <ColorPicker
            title="background Color Top"
            name={`${componentIndex}.data.styles.backgroundColorTop`}
            {...props}
          />
        )}
      {editFormNavigation[editFormNavigation.length - 1] ===
        `header${componentIndex}` &&
        formData.header.data.social_links && (
          <EditBarCards
            name="Social Links"
            editFormNavigationValue="socialLinks"
            {...props}
          />
        )}
      {editFormNavigation[editFormNavigation.length - 1] === "socialLinks" && (
        <SocialLinks {...props} />
      )}
      {editFormNavigation[editFormNavigation.length - 1] ===
        `header${componentIndex}` &&
        userTemplateData[currentPage][componentIndex].data.menus && (
          <EditBarCards
            name="Menu Items"
            editFormNavigationValue="menuItems"
            {...props}
          />
        )}
      {editFormNavigation[editFormNavigation.length - 1] === "menuItems" &&
        userTemplateData[currentPage][componentIndex].data.menus && (
          <MenuItems {...props} />
        )}
    </div>
  );
};
export default Header;
