import React, { useState } from "react";
import SimpleInput from "../form-fields/simple-input.js";
import Link from "../form-fields/links.js";
import MenuItemsComponent from "./menu-items-component.js";
import _ from "lodash";
import AddButton from "../form-fields/add-button.js";

const MenuItems = ({
  componentIndex,
  userTemplateData,
  setuserTemplateData,
  formData,
  setformData,
  values,
  setiframeKey,
  iFrameKey,
  setshowLoader,
  userWebPagesData,
  userWebPagesDataKeys,
  currentPage,
  allWebPages,
}) => {
  const [showField, setshowField] = useState(0);
  return (
    <div>
      {_.times(
        userTemplateData[currentPage][componentIndex].data.menus[0] !==
          "true" &&
          userTemplateData[currentPage][componentIndex].data.menus.length,
        (e) => (
          <>
            <MenuItemsComponent
              e={e}
              iFrameKey={iFrameKey}
              setiframeKey={setiframeKey}
              setshowLoader={setshowLoader}
              setformData={setformData}
              formData={formData}
              values={values}
              showField={showField}
              setshowField={setshowField}
              setuserTemplateData={setuserTemplateData}
              userTemplateData={userTemplateData}
              componentIndex={componentIndex}
              userWebPagesData={userWebPagesData}
              userWebPagesDataKeys={userWebPagesDataKeys}
              currentPage={currentPage}
              allWebPages={allWebPages}
            />
          </>
        )
      )}

      <hr />

      <AddButton
        pathToAdd={`${componentIndex}.data.menus`}
        pathFromAdd={`header.data.menus`}
        values={values}
        title="Add Menu Item"
        iFrameKey={iFrameKey}
        setiframeKey={setiframeKey}
        setshowLoader={setshowLoader}
        setformData={setformData}
        formData={formData}
        userTemplateData={userTemplateData}
        setuserTemplateData={setuserTemplateData}
        currentPage={currentPage}
      />
      {/* <span onClick={()=>{if(formData.sections.header.menus<7){setformData({...formData}, formData.sections.header.menus = formData.sections.header.menus + 1)}}}><i class="far fa-plus-square fa-2x"></i></span>
<span onClick={()=>{if(formData.sections.header.menus<=7 && formData.sections.header.menus>=1){setformData({...formData} ,formData.sections.header.menus = formData.sections.header.menus - 1)}}}><i class="far fa-minus-square fa-2x"></i></span> */}
    </div>
  );
};
export default MenuItems;
