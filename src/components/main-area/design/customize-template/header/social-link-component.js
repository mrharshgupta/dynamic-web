import React, { useState } from "react";
import SimpleInput from "../form-fields/simple-input.js";
import ChooseSocialIcon from "../form-fields/choose-social-icon.js";
import _ from "lodash";
import initialValues from "../edit-bar/initialvalues.js";
import SwitchButton from "../form-fields/switch-button.js";
import RemoveButton from "../form-fields/remove-button.js";
import { Button } from "react-bootstrap";
import DropdownHeading from "../form-fields/dropdown-heading.js";
import styles from "../media-manager/styles.js";

const SocialLinkComponent = ({
  componentIndex,
  showField,
  setshowField,
  userTemplateData,
  setuserTemplateData,
  e,
  setshowLoader,
  setiframeKey,
  iFrameKey,
  formData,
  setformData,
  values,
  seteditFormNavigation,
  editFormNavigation,
  imagesToBeUploaded,
  setimagesToBeUploaded,
  currentPage,
}) => {
  const styles = {
    container: { width: "100%", transition: "height 0.5s" },
  };
  return (
    <div style={styles.container}>
      <DropdownHeading
        heading={`Social Link ${e + 1}`}
        action="setAsE"
        setshowFields={setshowField}
        showFields={showField}
        e={e}
        currentPage={currentPage}
        setuserTemplateData={setuserTemplateData}
        userTemplateData={userTemplateData}
      />

      {showField === e && (
        <>
          <SimpleInput
            name={`${componentIndex}.data.social_links.${e}.link`}
            placeHolder={`Enter Link ${e + 1}`}
            values={values}
            formData={formData}
            iFrameKey={iFrameKey}
            setiframeKey={setiframeKey}
            setuserTemplateData={setuserTemplateData}
            userTemplateData={userTemplateData}
            setshowLoader={setshowLoader}
            currentPage={currentPage}
          />

          <SwitchButton
            title="Open link in New Tab"
            name={`${componentIndex}.data.social_links[${e}].targetBlank`}
            formData={formData}
            values={values}
            iFrameKey={iFrameKey}
            setiframeKey={setiframeKey}
            setshowLoader={setshowLoader}
            currentPage={currentPage}
            userTemplateData={userTemplateData}
            setuserTemplateData={setuserTemplateData}
          />
          <ChooseSocialIcon
            e={e}
            fileName={`social_link_${e}`}
            name={`${componentIndex}.data.social_links.${e}.logo`}
            values={values}
            setiframeKey={setiframeKey}
            iFrameKey={iFrameKey}
            setshowLoader={setshowLoader}
            formData={formData}
            other={
              values[componentIndex].data.social_links[e] &&
              values[componentIndex].data.social_links[e].logo
            }
            setimagesToBeUploaded={setimagesToBeUploaded}
            imagesToBeUploaded={imagesToBeUploaded}
            seteditFormNavigation={seteditFormNavigation}
            editFormNavigation={editFormNavigation}
            userTemplateData={userTemplateData}
            setuserTemplateData={setuserTemplateData}
            componentIndex={componentIndex}
            currentPage={currentPage}
          />

          <RemoveButton
            UTDpath={`${componentIndex}.data.social_links`}
            setshowFields={setshowField}
            values={values}
            showFields={showField}
            setiframeKey={setiframeKey}
            iFrameKey={iFrameKey}
            setshowLoader={setshowLoader}
            formData={formData}
            setformData={setformData}
            userTemplateData={userTemplateData}
            currentPage={currentPage}
            setuserTemplateData={setuserTemplateData}
            e={e}
          />
        </>
      )}
    </div>
  );
};
export default SocialLinkComponent;
