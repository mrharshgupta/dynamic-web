import React, { useState } from "react";
import UploadFile from "../form-fields/upload-file.js";
import SimpleInput from "../form-fields/simple-input.js";
import Link from "../form-fields/links.js";
import ColorPicker from "../form-fields/color-picker.js";
import UploadButton from "../form-fields/upload-button.js";
import styles from "./styles.js";
import DropdownHeading from "../form-fields/dropdown-heading.js";
import RemoveButton from "../form-fields/remove-button.js";
import _ from "lodash";

const TestimonialComponent = ({
  componentIndex,
  setshowFields,
  showFields,
  userTemplateData,
  setuserTemplateData,
  iFrameKey,
  setiframeKey,
  setshowLoader,
  seteditFormNavigation,
  editFormNavigation,
  e,
  setformData,
  formData,
  values,
  setactiveComponent,
  imagesToBeUploaded,
  setimagesToBeUploaded,
  currentPage,
  allWebPages,
}) => {
  const props = {
    iFrameKey: iFrameKey,
    setiframeKey: setiframeKey,
    setshowLoader: setshowLoader,
    values: values,
    formData: formData,
    componentIndex: componentIndex,
    currentPage: currentPage,
    showFields: showFields,
    setshowFields: setshowFields,
    imagesToBeUploaded: imagesToBeUploaded,
    setimagesToBeUploaded: setimagesToBeUploaded,
    setformData: setformData,
    setactiveComponent: setactiveComponent,

    seteditFormNavigation: seteditFormNavigation,
    editFormNavigation: editFormNavigation,
    userTemplateData: userTemplateData,
    setuserTemplateData: setuserTemplateData,
    allWebPages,
  };
  return (
    <div style={styles.componentContainer}>
      <DropdownHeading
        heading={`Testimonial ${e + 1}`}
        action="setAsE"
        e={e}
        showFields={showFields}
        setshowFields={setshowFields}
      />

      {showFields === e && (
        <>
          <SimpleInput
            name={`${componentIndex}.data.testimonials.${e}.name`}
            placeHolder="Enter name"
            {...props}
          />
          <SimpleInput
            name={`${componentIndex}.data.testimonials.${e}.position`}
            placeHolder="Enter position"
            {...props}
          />
          <SimpleInput
            name={`${componentIndex}.data.testimonials.${e}.review`}
            placeHolder={`Enter review ${e + 1}`}
            {...props}
          />

          <UploadButton
            imageField={`${componentIndex}.data.testimonials.${e}.backgroundImage`}
            title={`Upload Image`}
            {...props}
          />

          <ColorPicker
            title="Background Color"
            name={`${componentIndex}.data.testimonials.${e}.styles.backgroundColor`}
            {...props}
            e={e}
          />

          <RemoveButton
            UTDpath={`${componentIndex}.data.testimonials`} //FDpath={`${componentIndex}.data.slides`}
            setshowFields={setshowFields} //FDpath={`formData.slides.data.slides`}
            showFields={showFields}
            setiframeKey={setiframeKey}
            iFrameKey={iFrameKey}
            values={values}
            setshowLoader={setshowLoader}
            formData={formData}
            setformData={setformData}
            userTemplateData={userTemplateData}
            setuserTemplateData={setuserTemplateData}
            e={e}
            currentPage={currentPage}
          />
        </>
      )}
    </div>
  );
};
export default TestimonialComponent;
