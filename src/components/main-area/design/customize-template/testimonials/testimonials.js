import React, { useState } from "react";
import UploadFiles from "../form-fields/upload-file.js";
import DoubleInput from "../form-fields/double-input.js";
import SimpleInput from "../form-fields/simple-input.js";
import Link from "../form-fields/links.js";
import _ from "lodash";
import ColorPicker from "../form-fields/color-picker.js";
import TestimonialComponent from "./testimonial-component.js";
import AddButton from "../form-fields/add-button.js";

const Testimonials = ({
  componentIndex,
  userTemplateData,
  setuserTemplateData,
  seteditFormNavigation,
  editFormNavigation,
  iFrameKey,
  setiframeKey,
  setshowLoader,
  setformData,
  formData,
  values,
  setactiveComponent,
  imagesToBeUploaded,
  setimagesToBeUploaded,
  currentPage,
  allWebPages,
}) => {
  const [showFields, setshowFields] = useState(0);
  setactiveComponent("testimonials" + componentIndex);
  const props = {
    iFrameKey: iFrameKey,
    setiframeKey: setiframeKey,
    setshowLoader: setshowLoader,
    values: values,
    formData: formData,
    componentIndex: componentIndex,
    currentPage: currentPage,
    showFields: showFields,
    setshowFields: setshowFields,
    imagesToBeUploaded: imagesToBeUploaded,
    setimagesToBeUploaded: setimagesToBeUploaded,
    setformData: setformData,
    setactiveComponent: setactiveComponent,

    seteditFormNavigation: seteditFormNavigation,
    editFormNavigation: editFormNavigation,
    userTemplateData: userTemplateData,
    setuserTemplateData: setuserTemplateData,
    allWebPages,
  };
  return (
    <div>
      <SimpleInput
        name={`${componentIndex}.data.title.text`}
        placeHolder="Enter Title"
        title="Title"
        {...props}
      />
      <SimpleInput
        name={`${componentIndex}.data.description.text`}
        placeHolder="Enter Description"
        title="Description"
        {...props}
      />
      <ColorPicker
        title="Background Color"
        name={`${componentIndex}.data.styles.backgroundColor`}
        {...props}
      />

      {_.times(
        userTemplateData[currentPage][componentIndex].data.testimonials[0] !==
          "true" &&
          userTemplateData[currentPage][componentIndex].data.testimonials
            .length,
        (e) => (
          <>
            <TestimonialComponent e={e} {...props} />
          </>
        )
      )}

      <AddButton
        pathToAdd={`${componentIndex}.data.testimonials`}
        pathFromAdd={`testimonials.data.testimonials`}
        title="Add Testimonial"
        {...props}
      />
    </div>
  );
};
export default Testimonials;
