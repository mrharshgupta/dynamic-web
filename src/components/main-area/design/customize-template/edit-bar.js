import React, { useState, useEffect } from "react";
import { Formik, Form } from "formik";
import { useHistory } from "react-router-dom";
import { connect } from "react-redux";
import styles from "./styles";
import Header from "./header/header.js";
import { onClickSubmit, onClickSave, upLoadImages, copyFiles } from "./api.js";
import EditBarCards from "./edit-bar/edit-bar.js";
import MediaManager from "./media-manager/media-manager.js";
import initialValues from "./edit-bar/initialvalues.js";
import Carousel from "./carousel/carousel.js";
import ContactInfo from "./contact-info/contact-info.js";
import Services from "./services/services.js";
import Statistics from "./statistics/statistics.js";
import ImageWithText from "./image-with-text/image-with-text.js";
import Testimonials from "./testimonials/testimonials.js";
import Footer from "./footer/footer";
import ContactForm from "./contact-form/contact-form.js";
import AddTemplateComponent from "./add-template-component/add-template-component.js";
import Partners from "./partners/partners.js";
import Map from "./map/map.js";
import TextWithBackgroundImage from "./text-with-background-image/text-with-background-image";
import CircularProgress from "@material-ui/core/CircularProgress";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import _ from "lodash";

const EditForm = ({
  crossButton,
  backButton,
  customizeTemplateEditBarHeading,
  editBarLoader,
  seteditBarLoader,
  setsubmit,
  setformData,
  formData,
  userTemplateData,
  setuserTemplateData,
  iFrameKey,
  setiframeKey,
  setshowLoader,
  setactiveComponent,
  activeComponent,
  userTemplateId,
  userSettings,
  setuserSettings,
  userWebPagesData,
  userWebPagesDataKeys,
  currentPage,
  editFormNavigation,
  seteditFormNavigation,
  editBarHeading,
  seteditBarHeading,
  defaultPages,
  allWebPages,
  userTemplateDataBackup,
}) => {
  let history = useHistory();

  const [imagesToBeUploaded, setimagesToBeUploaded] = useState({
    file: [],
    fileName: [],
  });
  const [saveButtonLoader, setsaveButtonLoader] = useState(false);

  const editBarCardProps = {
    currentPage: currentPage,
    iFrameKey: iFrameKey,
    setiframeKey: setiframeKey,
    seteditFormNavigation: seteditFormNavigation,
    editFormNavigation: editFormNavigation,
    activeComponent: activeComponent,
    userTemplateData: userTemplateData,
    setuserTemplateData: setuserTemplateData,
    setformData: setformData,
    formData: formData,
    setshowLoader: setshowLoader,
    editBarHeading: editBarHeading,
    seteditBarHeading: seteditBarHeading,
    userSettings: userSettings,
    userWebPagesDataKeys: userWebPagesDataKeys,
    allWebPages,
  };
  const templateComponentProps = {
    currentPage: currentPage,
    imagesToBeUploaded: imagesToBeUploaded,
    setimagesToBeUploaded: setimagesToBeUploaded,
    setformData: setformData,
    formData: formData,
    setactiveComponent: setactiveComponent,
    seteditFormNavigation: seteditFormNavigation,
    editFormNavigation: editFormNavigation,
    onClickSubmit: onClickSubmit,
    setiframeKey: setiframeKey,
    iFrameKey: iFrameKey,
    setshowLoader: setshowLoader,
    userTemplateData: userTemplateData,
    setuserTemplateData: setuserTemplateData,
    editBarHeading: editBarHeading,
    seteditBarHeading: seteditBarHeading,
    userSettings: userSettings,
    setuserSettings: setuserSettings,
    userWebPagesData: userWebPagesData,
    userWebPagesDataKeys: userWebPagesDataKeys,
    allWebPages,
  };
  let dragDropValues = [];
  if (defaultPages.includes(currentPage)) {
    dragDropValues =
      userTemplateData && Object.values(userTemplateData[currentPage]);
    console.log(dragDropValues && dragDropValues, "ddv");
  }
  var isTemplateDataChanged = !_.isEqual(
    JSON.stringify(userTemplateDataBackup),
    JSON.stringify(userTemplateData)
  );

  return (
    <div style={styles.editForm.content}>
      {editBarLoader && (
        <div style={styles.editBarLoader}>
          <CircularProgress />
        </div>
      )}
      {
        userTemplateData && formData && (
          //(defaultPages.includes(currentPage) ? (
          <Formik
            enableReinitialize
            //initialValues={initialValues(userTemplateData, formData)}
            initialValues={userTemplateData[currentPage]}
            onSubmit={(values) => {
              setiframeKey(iFrameKey + 1);
              setshowLoader(true);
              copyFiles(() => setiframeKey(iFrameKey + 1));
              // onClickSubmit(values);
              // onClickSave(values);
              onClickSubmit({ ...userTemplateData, [currentPage]: values });
              onClickSave({ ...userTemplateData, [currentPage]: values });
            }}
          >
            {({ errors, touched, values, setFieldValue }) => (
              <>
                {/* {setuserTemplateData(values)}  */}
                <Form>
                  <div style={styles.editFormHeader}>
                    {editFormNavigation[editFormNavigation.length - 1] !==
                      "home" && (
                      <span
                        onClick={() => {
                          backButton();
                          seteditFormNavigation(
                            editFormNavigation.splice(editFormNavigation.pop())
                          );
                        }}
                      >
                        <i
                          class="fas fa-arrow-left fa-1x"
                          style={styles.backButtonITag}
                        ></i>
                      </span>
                    )}
                    <span style={styles.editTemplateHeading}>
                      {customizeTemplateEditBarHeading &&
                        customizeTemplateEditBarHeading[
                          customizeTemplateEditBarHeading.length - 1
                        ]}
                      <span
                        onClick={() => {
                          crossButton();
                          history.push(
                            `/${localStorage.getItem("storeName")}/home`
                          );
                        }}
                      >
                        <i class="fas fa-times"></i>
                      </span>
                    </span>
                  </div>

                  {defaultPages.includes(currentPage) ? (
                    <div style={styles.cardsContainer}>
                      {editFormNavigation[editFormNavigation.length - 1] ===
                        "mediaManager" && (
                        <MediaManager
                          {...templateComponentProps}
                          name="Media Manager"
                          values={values}
                          setFieldValue={setFieldValue}
                        />
                      )}

                      <DragDropContext
                        onDragEnd={(param) => {
                          const source =
                            param && param.source && param.source.index;
                          const destination =
                            param &&
                            param.destination &&
                            param.destination.index;
                          dragDropValues.splice(
                            destination,
                            0,
                            dragDropValues.splice(source, 1)[0]
                          );
                          setuserTemplateData({
                            ...userTemplateData,
                            [currentPage]: dragDropValues,
                          });
                          let a = param.draggableId.split(".");
                          setactiveComponent(a[1]);
                          setshowLoader(true);
                          setiframeKey(iFrameKey + 1);
                          //onClickSubmit(dragDropValues);
                          onClickSubmit({
                            ...userTemplateData,
                            [currentPage]: dragDropValues,
                          });
                        }}
                      >
                        <div>
                          <Droppable
                            droppableId="droppable-1"
                            direction="vertical"
                          >
                            {(provided, snapshot) => (
                              <div
                                ref={provided.innerRef}
                                {...provided.droppableProps}
                              >
                                {userTemplateData &&
                                  dragDropValues.map(
                                    (item, i) =>
                                      editFormNavigation[
                                        editFormNavigation.length - 1
                                      ] === "home" &&
                                      (item["sectionInfo"]["type"] ===
                                        "header" ||
                                      item["sectionInfo"]["type"] ===
                                        "footer" ? (
                                        <EditBarCards
                                          componentIndex={i}
                                          {...editBarCardProps}
                                          name={item["sectionInfo"]["name"]}
                                          editFormNavigationValue={
                                            item["sectionInfo"]["type"] + i
                                          }
                                          values={values}
                                        />
                                      ) : (
                                        <Draggable
                                          key={i}
                                          draggableId={
                                            i +
                                            "." +
                                            item["sectionInfo"]["type"]
                                          }
                                          index={i}
                                        >
                                          {(provided, snapshot) => (
                                            <div
                                              ref={provided.innerRef}
                                              {...provided.draggableProps}
                                            >
                                              <EditBarCards
                                                componentIndex={i}
                                                trash={true}
                                                eye={true}
                                                handle={
                                                  provided.dragHandleProps
                                                }
                                                {...editBarCardProps}
                                                name={
                                                  item["sectionInfo"]["name"]
                                                }
                                                editFormNavigationValue={
                                                  item["sectionInfo"]["type"] +
                                                  i
                                                }
                                                values={values}
                                              />
                                            </div>
                                          )}
                                        </Draggable>
                                      ))
                                  )}
                                {provided.placeholder}
                              </div>
                            )}
                          </Droppable>
                        </div>
                      </DragDropContext>

                      {_.times(dragDropValues.length, (e) => {
                        if (
                          dragDropValues[e]["sectionInfo"]["type"] === "header"
                        ) {
                          const ComponentName = Header;
                          return (
                            (editFormNavigation[
                              editFormNavigation.length - 1
                            ] ===
                              dragDropValues[e]["sectionInfo"]["type"] + e ||
                              editFormNavigation[
                                editFormNavigation.length - 1
                              ] === "socialLinks" ||
                              editFormNavigation[
                                editFormNavigation.length - 1
                              ] === "menuItems") && (
                              <ComponentName
                                componentIndex={e}
                                {...templateComponentProps}
                                values={values}
                              />
                            )
                          );
                        }
                        if (
                          dragDropValues[e]["sectionInfo"]["type"] ===
                          "carousel"
                        ) {
                          const ComponentName = Carousel;
                          return (
                            editFormNavigation[
                              editFormNavigation.length - 1
                            ] ===
                              dragDropValues[e]["sectionInfo"]["type"] + e && (
                              <ComponentName
                                componentIndex={e}
                                {...templateComponentProps}
                                values={values}
                              />
                            )
                          );
                        }
                        if (
                          dragDropValues[e]["sectionInfo"]["type"] ===
                          "contactInfo"
                        ) {
                          const ComponentName = ContactInfo;
                          return (
                            editFormNavigation[
                              editFormNavigation.length - 1
                            ] ===
                              dragDropValues[e]["sectionInfo"]["type"] + e && (
                              <ComponentName
                                componentIndex={e}
                                {...templateComponentProps}
                                values={values}
                              />
                            )
                          );
                        }
                        if (
                          dragDropValues[e]["sectionInfo"]["type"] ===
                          "services"
                        ) {
                          const ComponentName = Services;
                          return (
                            editFormNavigation[
                              editFormNavigation.length - 1
                            ] ===
                              dragDropValues[e]["sectionInfo"]["type"] + e && (
                              <ComponentName
                                componentIndex={e}
                                {...templateComponentProps}
                                values={values}
                              />
                            )
                          );
                        }
                        if (
                          dragDropValues[e]["sectionInfo"]["type"] === "stats"
                        ) {
                          const ComponentName = Statistics;
                          return (
                            editFormNavigation[
                              editFormNavigation.length - 1
                            ] ===
                              dragDropValues[e]["sectionInfo"]["type"] + e && (
                              <ComponentName
                                componentIndex={e}
                                {...templateComponentProps}
                                values={values}
                              />
                            )
                          );
                        }
                        if (
                          dragDropValues[e]["sectionInfo"]["type"] ===
                          "imageWithText"
                        ) {
                          const ComponentName = ImageWithText;
                          return (
                            editFormNavigation[
                              editFormNavigation.length - 1
                            ] ===
                              dragDropValues[e]["sectionInfo"]["type"] + e && (
                              <ComponentName
                                componentIndex={e}
                                {...templateComponentProps}
                                values={values}
                              />
                            )
                          );
                        }
                        if (
                          dragDropValues[e]["sectionInfo"]["type"] ===
                          "testimonials"
                        ) {
                          const ComponentName = Testimonials;
                          return (
                            editFormNavigation[
                              editFormNavigation.length - 1
                            ] ===
                              dragDropValues[e]["sectionInfo"]["type"] + e && (
                              <ComponentName
                                componentIndex={e}
                                {...templateComponentProps}
                                values={values}
                              />
                            )
                          );
                        }
                        if (
                          dragDropValues[e]["sectionInfo"]["type"] ===
                          "contactForm"
                        ) {
                          const ComponentName = ContactForm;
                          return (
                            editFormNavigation[
                              editFormNavigation.length - 1
                            ] ===
                              dragDropValues[e]["sectionInfo"]["type"] + e && (
                              <ComponentName
                                componentIndex={e}
                                {...templateComponentProps}
                                values={values}
                              />
                            )
                          );
                        }
                        if (
                          dragDropValues[e]["sectionInfo"]["type"] ===
                          "textWithBackgroundImage"
                        ) {
                          const ComponentName = TextWithBackgroundImage;
                          return (
                            editFormNavigation[
                              editFormNavigation.length - 1
                            ] ===
                              dragDropValues[e]["sectionInfo"]["type"] + e && (
                              <ComponentName
                                componentIndex={e}
                                {...templateComponentProps}
                                values={values}
                              />
                            )
                          );
                        }
                        if (
                          dragDropValues[e]["sectionInfo"]["type"] ===
                          "partners"
                        ) {
                          const ComponentName = Partners;
                          return (
                            editFormNavigation[
                              editFormNavigation.length - 1
                            ] ===
                              dragDropValues[e]["sectionInfo"]["type"] + e && (
                              <ComponentName
                                componentIndex={e}
                                {...templateComponentProps}
                                values={values}
                              />
                            )
                          );
                        }
                        if (
                          dragDropValues[e]["sectionInfo"]["type"] === "map"
                        ) {
                          const ComponentName = Map;
                          return (
                            editFormNavigation[
                              editFormNavigation.length - 1
                            ] ===
                              dragDropValues[e]["sectionInfo"]["type"] + e && (
                              <ComponentName
                                componentIndex={e}
                                {...templateComponentProps}
                                values={values}
                              />
                            )
                          );
                        }
                        if (
                          dragDropValues[e]["sectionInfo"]["type"] === "footer"
                        ) {
                          const ComponentName = Footer;
                          return (
                            editFormNavigation[
                              editFormNavigation.length - 1
                            ] ===
                              dragDropValues[e]["sectionInfo"]["type"] + e && (
                              <ComponentName
                                componentIndex={e}
                                {...templateComponentProps}
                                values={values}
                              />
                            )
                          );
                        }
                      })}

                      {editFormNavigation[editFormNavigation.length - 1] ===
                        "home" && (
                        <EditBarCards
                          {...editBarCardProps}
                          name="Add Component"
                          editFormNavigationValue="addComponent"
                        />
                      )}

                      {editFormNavigation[editFormNavigation.length - 1] ===
                        "addComponent" && (
                        <AddTemplateComponent
                          {...templateComponentProps}
                          values={values}
                          setFieldValue={setFieldValue}
                        />
                      )}
                    </div>
                  ) : null}

                  <button
                    type="submit"
                    // disabled={
                    //   userTemplateData[currentPage] !== initialValues
                    //     ? false
                    //     : true
                    // }
                    class="button"
                    style={styles.editBarSaveButtonActive}
                    // style={
                    //   isTemplateDataChanged
                    //     ? styles.editBarSaveButtonActive
                    //     : styles.editBarSaveButtonDisabled
                    // }
                  >
                    {saveButtonLoader ? (
                      <CircularProgress />
                    ) : (
                      <>
                        <i
                          class="fas fa-save"
                          style={styles.saveButtonITag}
                        ></i>
                        <strong>Save</strong>
                      </>
                    )}
                  </button>
                </Form>
              </>
            )}
          </Formik>
        )
        //) : null)
      }
    </div>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    backButton: () =>
      dispatch({ type: "CUSTOMIZE_TEMPLATE_EDIT_BAR_BACK_BUTTON" }),
    crossButton: () =>
      dispatch({ type: "CUSTOMIZE_TEMPLATE_EDIT_BAR_CROSS_BUTTON" }),
  };
};

const mapStateToProps = ({ customizeTemplateEditBarHeading, loginData }) => ({
  customizeTemplateEditBarHeading: customizeTemplateEditBarHeading,
  loginData,
});

export default connect(mapStateToProps, mapDispatchToProps)(EditForm);
