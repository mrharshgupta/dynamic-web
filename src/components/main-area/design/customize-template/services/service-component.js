import React, { useState } from "react";
import UploadFile from "../form-fields/upload-file.js";
import SimpleInput from "../form-fields/simple-input.js";
import ColorPicker from "../form-fields/color-picker.js";
import Link from "../form-fields/links.js";
import UploadButton from "../form-fields/upload-button.js";
import _ from "lodash";
import styles from "./styles.js";
import DropdownHeading from "../form-fields/dropdown-heading";
import RemoveButton from "../form-fields/remove-button.js";

const ServiceComponent = ({
  componentIndex,
  showFields,
  setshowFields,
  userTemplateData,
  setuserTemplateData,
  iFrameKey,
  setiframeKey,
  setshowLoader,
  seteditFormNavigation,
  editFormNavigation,
  e,
  setformData,
  formData,
  values,
  setactiveComponent,
  imagesToBeUploaded,
  setimagesToBeUploaded,
  currentPage,
  allWebPages,
}) => {
  const props = {
    setiframeKey: setiframeKey,
    iFrameKey: iFrameKey,
    setactiveComponent: setactiveComponent,
    setformData: setformData,
    values: values,
    formData: formData,
    setshowLoader: setshowLoader,
    userTemplateData: userTemplateData,
    setuserTemplateData: setuserTemplateData,
    seteditFormNavigation: seteditFormNavigation,
    editFormNavigation: editFormNavigation,
    imagesToBeUploaded: imagesToBeUploaded,
    setimagesToBeUploaded: setimagesToBeUploaded,
    showFields: showFields,
    componentIndex: componentIndex,
    setshowFields: setshowFields,
    e: e,
    currentPage: currentPage,
    allWebPages,
  };
  return (
    <div style={styles.componentContainer}>
      <DropdownHeading
        heading={`Service ${e + 1}`}
        action="setAsE"
        {...props}
      />

      {showFields === e && (
        <>
          <SimpleInput
            name={`${componentIndex}.data.services.${e}.title.text`}
            placeHolder="Enter Title"
            {...props}
          />
          <SimpleInput
            name={`${componentIndex}.data.services.${e}.description.text`}
            placeHolder="Enter description"
            {...props}
          />

          <SimpleInput
            name={`${componentIndex}.data.services.${e}.button.text`}
            placeHolder="Enter button text"
            {...props}
          />
          <Link
            title="Button Link"
            name1={`${componentIndex}.data.services.${e}.button.linkType`}
            name2={`${componentIndex}.data.services.${e}.button.link`}
            targetBlankTitle="Open Link in New Tab"
            targetBlankName={`${componentIndex}.data.services.${e}.button.targetBlank`}
            options={allWebPages}
            other={
              values[componentIndex].data.services[e] &&
              values[componentIndex].data.services[e].button.linkType
            }
            {...props}
          />

          <UploadButton
            imageField={`${componentIndex}.data.services.${e}.backgroundImage`}
            {...props}
            title="Upload Image"
          />
          <ColorPicker
            title="Button Color"
            name={`${componentIndex}.data.services[${e}].button.styles.backgroundColor`}
            {...props}
          />

          <ColorPicker
            title="Background Color"
            name={`${componentIndex}.data.services[${e}].styles.backgroundColor`}
            {...props}
          />

          <RemoveButton
            UTDpath={`${componentIndex}.data.services`}
            {...props}
          />
        </>
      )}
    </div>
  );
};
export default ServiceComponent;
