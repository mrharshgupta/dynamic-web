import React, { useState } from "react";
import UploadFiles from "../form-fields/upload-file.js";
import SimpleInput from "../form-fields/simple-input.js";
import Link from "../form-fields/links.js";
import UploadFile from "../form-fields/upload-file.js";
import ServiceComponent from "./service-component.js";
import _ from "lodash";
import AddButton from "../form-fields/add-button.js";

const Services = ({
  componentIndex,
  userTemplateData,
  setuserTemplateData,
  seteditFormNavigation,
  editFormNavigation,
  iFrameKey,
  setiframeKey,
  setshowLoader,
  setformData,
  formData,
  values,
  setactiveComponent,
  imagesToBeUploaded,
  setimagesToBeUploaded,
  currentPage,
  allWebPages,
}) => {
  setactiveComponent("services" + componentIndex);
  const [showFields, setshowFields] = useState(0);
  const props = {
    setiframeKey: setiframeKey,
    iFrameKey: iFrameKey,
    setactiveComponent: setactiveComponent,
    setformData: setformData,
    values: values,
    formData: formData,
    setshowLoader: setshowLoader,
    userTemplateData: userTemplateData,
    setuserTemplateData: setuserTemplateData,
    seteditFormNavigation: seteditFormNavigation,
    editFormNavigation: editFormNavigation,
    imagesToBeUploaded: imagesToBeUploaded,
    setimagesToBeUploaded: setimagesToBeUploaded,
    showFields: showFields,
    componentIndex: componentIndex,
    setshowFields: setshowFields,
    currentPage: currentPage,
    allWebPages,
  };
  return (
    <div>
      <SimpleInput
        name={`${componentIndex}.data.title.text`}
        placeHolder="Enter Title"
        title="Top title"
        {...props}
      />
      <SimpleInput
        name={`${componentIndex}.data.description.text`}
        placeHolder="Enter description"
        title="Top description"
        {...props}
      />

      {_.times(
        userTemplateData[currentPage][componentIndex].data.services[0] !==
          "true" &&
          userTemplateData[currentPage][componentIndex].data.services.length,
        (e) => (
          <div>
            <ServiceComponent e={e} {...props} />
          </div>
        )
      )}

      <hr />

      <AddButton
        pathToAdd={`${componentIndex}.data.services`}
        pathFromAdd={`services.data.services`}
        values={values}
        title="Add Service"
        iFrameKey={iFrameKey}
        setiframeKey={setiframeKey}
        setshowLoader={setshowLoader}
        setformData={setformData}
        formData={formData}
        userTemplateData={userTemplateData}
        setuserTemplateData={setuserTemplateData}
        currentPage={currentPage}
      />
    </div>
  );
};
export default Services;
