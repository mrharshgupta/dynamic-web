import colors from "../../../../constants/colors";

const styles = {
  noTemplateContainer: {
    width: "100%",
    height: "100%",
  },

  loader: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: "100vh",
    width: "80%",
    position: "fixed",
    right: 0,
  },
  editForm: {
    content: {
      top: 0,
      left: 0,
      position: "absolute",
      height: "100vh",
      overflowY: "hidden",
      width: "20%",
      userSelect: "none",
      backgroundColor: "#fffde6",
    },
  },
  editBarLoader: {
    display: "flex",
    height: "100vh",
    alignItems: "center",
    justifyContent: "center",
  },
  editFormHeader: {
    height: "46px",
    backgroundColor: colors.primary,
    width: "100%",
    display: "flex",
    alignItems: "center",
  },

  backButtonITag: {
    color: "white",
    marginLeft: "10px",
  },

  editTemplateHeading: {
    width: "100%",
    display: "flex",
    justifyContent: "space-between",
    color: "white",
    padding: "10px",
    fontSize: "20px",
  },

  cardsContainer: {
    overflowY: "scroll",
    height: "84vh",
  },

  iFrameContainer: {
    width: "80%",
    right: 0,
    position: "fixed",
    height: "92%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },

  iFrameFull: {
    height: "100%",
    width: "100%",
    transition: "width 0.5s",
  },
  iFramePhone: {
    height: "90%",
    width: "27%",
    transition: "width 0.5s",
    border: "4px solid black",
  },
  iFrameTablet: {
    height: "90%",
    width: "60%",
    transition: "width 0.5s",
    border: "4px solid black",
  },

  wrapData: {
    height: "80vh",
    overflowY: "scroll",
  },

  link: {
    textDecoration: "none",
    color: "white",
    fontSize: 14,
  },

  editBarSaveButtonActive: {
    position: "fixed",
    fontSize: 20,
    color: "white",
    height: "8%",
    width: "20%",
    left: 0,
    bottom: 0,
  },
  editBarSaveButtonDisabled: {
    position: "fixed",
    fontSize: 20,
    color: "#5e5e5e",
    height: "8%",
    width: "20%",
    left: 0,
    bottom: 0,
    backgroundColor: "#cfcfcf",
  },

  saveButtonITag: {
    marginRight: "20px",
  },

  gadgetsButton: {
    marginLeft: "15px",
  },

  footer: {
    width: "80%",
    position: "fixed",
    right: 0,
    height: "8%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },

  footerButtons: { marginRight: "5px" },
};

export default styles;
