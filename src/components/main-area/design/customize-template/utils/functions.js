import React from "react";

const getComponentsAccordingToPlan = (
  templateData = {},
  templateDataObjectType
) => {
  var components = {};
  const userPlan = localStorage.getItem("selectedPlan");

  if (parseInt(userPlan) === 1) {
    const allowedComponents = [
      "header",
      "footer",
      "carousel",
      "services",
      "contactInfo",
      "contactForm",
      "team",
      "photoGallery",
      "textWithBackgroundImage",
    ];
    if (templateDataObjectType === "withPages") {
      Object.keys(templateData).map((page, i) => {
        components[page] = templateData[page].filter((item, index) => {
          return allowedComponents.includes(item.sectionInfo.type) && item;
        });
      });
    }
    if (templateDataObjectType === "onlyComponents") {
      Object.keys(templateData).filter((item, i) => {
        components[item] =
          allowedComponents.includes(templateData[item].sectionInfo.type) &&
          templateData[item];
      });
    }
  }

  if (parseInt(userPlan) === 2) {
    const allowedComponents = [
      "header",
      "footer",
      "carousel",
      "services",
      "contactInfo",
      "contactForm",
      "testimonials",
      "googleMap",
      "team",
      "photoGallery",
      "textWithBackgroundImage",
      "map",
    ];
    if (templateDataObjectType === "withPages") {
      Object.keys(templateData).map((page, i) => {
        components[page] = templateData[page].filter((item, index) => {
          return allowedComponents.includes(item.sectionInfo.type) && item;
        });
      });
    }
    if (templateDataObjectType === "onlyComponents") {
      Object.keys(templateData).filter((item, i) => {
        components[item] =
          allowedComponents.includes(templateData[item].sectionInfo.type) &&
          templateData[item];
      });
    }
  }

  console.log(components, "componentscomponentscomponents");
  return components;
};

export { getComponentsAccordingToPlan };
