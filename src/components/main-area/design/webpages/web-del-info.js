import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import Popups from '../../../popups/index';
import styles from './styles';
import '../../../../css/style.css';
import '../../../../css/device.css';
import '../../../../css/editor.css';
import '../../../../css/main.css';
import '../../../../css/sanjana.css';
import '../../../../css/bootstrap.min.css';

const WebDelInfo = (props) => {

return(
<>

   <div class="wrap-right">
      <div class="page-nav">
         <div class="page-nav-left">
                       <h1 class="pg-ttl">Web Pages</h1>
         </div>
         <div class="page-nav-right">
            <a href="#" class="button"><i class="text-white fa fa-eye"></i> Preview</a>
           <button class="button">Save</button>
            <button class="button">Cancle</button>
         </div>
      </div>
      <div class="wrap-data">
        <div class="web-wrap">
         <div class="web-left">
            <div class="web-cont">
               <div class="web-title">
                  <h3>Global Settings</h3>
               </div>
               <div class="web-cont-in">
                  <div class="form-group">
                     <label for="cat1">Blog system status</label>
                     <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch1" checked />
                        <label class="custom-control-label" for="customSwitch1">Enable</label>
                     </div>
                  
            </div>
         </div>
      </div>
      </div>

          <div class="web-right">
            <div class="web-cont">
               <div class="web-title">
                  <h3>General</h3>
               </div>
               <div class="web-cont-in">
                  <form>
                      <div class="form-group">
                     <label for="cat1">Web Page Title *</label>
                     <input type="text" class="form-control" value="About us" title="Customer" />
                  </div>
                  <div class="form-group">
                     <label for="cat1">Description</label>
                     <div class="txt-edit">
                  <textarea id="txtEditor" class="texteditor"></textarea> 
               </div>
               </div>
                  </form>
                 
         </div>
            </div>
         </div>



         
        </div>
      </div>
   </div>


<div class="modal fade video-modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">How to Add a Product to Your Online Store</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="video-div">
               <iframe width="100%" height="100%" src="https://www.youtube.com/embed/yAoLSRbwxL8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
         </div>
      </div>
   </div>
</div>
   
</>
);
}

export default WebDelInfo;
