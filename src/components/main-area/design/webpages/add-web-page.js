import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import styles from "./styles";
import {
  getUserWebPages,
  saveUserWebPage,
  getUserWebPageDataForEdit,
  updateUserWebPageAfterEdit,
  deleteUserWebPage,
} from "./apis";
import "../../../../css/style.css";
import "../../../../css/device.css";
import "../../../../css/editor.css";
import "../../../../css/main.css";
import "../../../../css/sanjana.css";
import "../../../../css/bootstrap.min.css";
import { Editor } from "@tinymce/tinymce-react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import NativeSelect from "@material-ui/core/NativeSelect";
import InputBase from "@material-ui/core/InputBase";

// const BootstrapInput = withStyles((theme) => ({
//   root: {
//     "label + &": {
//       marginTop: theme.spacing(3),
//     },
//   },
//   input: {
//     minWidth: "200px",
//     borderRadius: 4,
//     position: "relative",
//     backgroundColor: theme.palette.background.paper,
//     border: "1px solid #ced4da",
//     fontSize: 16,
//     padding: "10px 26px 10px 12px",
//     transition: theme.transitions.create(["border-color", "box-shadow"]),
//     // Use the system font instead of the default Roboto font.
//     fontFamily: [
//       "-apple-system",
//       "BlinkMacSystemFont",
//       '"Segoe UI"',
//       "Roboto",
//       '"Helvetica Neue"',
//       "Arial",
//       "sans-serif",
//       '"Apple Color Emoji"',
//       '"Segoe UI Emoji"',
//       '"Segoe UI Symbol"',
//     ].join(","),
//     "&:focus": {
//       borderRadius: 4,
//       borderColor: "#80bdff",
//       boxShadow: "0 0 0 0.2rem rgba(0,123,255,.25)",
//     },
//   },
// }))(InputBase);

// const useStyles = makeStyles((theme) => ({
//   margin: {
//     margin: theme.spacing(1),
//   },
// }));

const AddWebPage = (props) => {
  //   const classes = useStyles();
  //   let getCurrentDate = (separator = "/") => {
  //     let newDate = new Date();
  //     let date = newDate.getDate();
  //     let month = newDate.getMonth() + 1;
  //     let year = newDate.getFullYear();
  //     return `${date}${separator}${
  //       month < 10 ? `0${month}` : `${month}`
  //     }${separator}${year}`;
  //   };

  const [addWebPageData, setaddWebPageData] = useState({
    name: "",
    description: "",
    content: "",
    webPageImage: "",
    favicon: "",
  });
  //const [editortry, seteditortry] = useState("");
  useEffect(() => {
    if (
      props.match.params.webpageKey !== "undefined" &&
      props.match.params.edit === "true"
    ) {
      getUserWebPageDataForEdit(
        (abc) => setaddWebPageData(abc),
        props.match.params.webpageKey
      );
    }
  }, []);
  console.log(addWebPageData, "dadadada");

  let history = useHistory();
  const [showGlobalSettings, setshowGlobalSettings] = useState(undefined);
  const [showPublishNewPost, setshowPublishNewPost] = useState(undefined);

  const imageSelect = (event, type) => {
    var file = event.target.files[0];
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      console.log(reader.result, "base64 sting of selected image");
      if (type === "webpageImage") {
        setaddWebPageData({ ...addWebPageData, webPageImage: reader.result });
      }
      if (type === "favicon") {
        setaddWebPageData({ ...addWebPageData, favicon: reader.result });
      }
    };
    reader.onerror = function (error) {
      console.log("Error: ", error);
    };
  };

  const onClickSave = () => {
    if (
      props.match.params.webpageKey !== "undefined" &&
      props.match.params.edit === "true"
    ) {
      updateUserWebPageAfterEdit(addWebPageData, props.match.params.webpageKey);
    } else {
      saveUserWebPage(addWebPageData);
    }
    history.goBack();
  };

  return (
    <>
      <div className="wrap-right">
        <div className="page-nav">
          <div className="page-nav-left">
            <h1 className="pg-ttl">Add New Web Page</h1>
          </div>
          <div className="page-nav-right">
            <button className="button" onClick={() => onClickSave()}>
              Save
            </button>
            <button className="button" onClick={() => history.goBack()}>
              Cancel
            </button>
          </div>
        </div>
        <div class="wrap-data" style={styles.wrapData}>
          <div class="product-update-div">
            <div class="product-info-wrap">
              <a
                onClick={() => {
                  showPublishNewPost === true
                    ? setshowPublishNewPost(undefined)
                    : setshowPublishNewPost(true);
                }}
                href="javascript:void(0)"
                style={styles.dropDowns}
                className="search-btn"
                aria-expanded="false"
              >
                Publish New Page
              </a>
              {showPublishNewPost === true && (
                <div id="post" class="search-form1">
                  <form action="" class="col-12">
                    <ul class="row form-div">
                      {/* <li class="col-12 form-group">
                        <label for="">Status</label>
                        <div class="custom-control custom-switch">
                          <input
                            onClick={() =>
                              addBlogData.status === "active"
                                ? setaddBlogData({
                                    ...addBlogData,
                                    status: "inactive",
                                  })
                                : setaddBlogData({
                                    ...addBlogData,
                                    status: "active",
                                  })
                            }
                            checked={
                              addBlogData.status === "active" ? true : false
                            }
                            type="checkbox"
                            class="custom-control-input"
                            id="customSwitch1"
                          />
                          <label
                            class="custom-control-label"
                            for="customSwitch1"
                          >
                            {addBlogData.status}
                          </label>
                        </div>
                      </li> */}
                      <li class="col-12 form-group">
                        <label for="">
                          Web Page name<span class="text-red">*</span>
                        </label>
                        <input
                          type="text"
                          class="form-control"
                          value={addWebPageData.name}
                          onChange={(e) => {
                            setaddWebPageData({
                              ...addWebPageData,
                              name: e.target.value,
                            });
                          }}
                        />
                      </li>
                      <li class="col-12 form-group">
                        <label for="">
                          Descripton<span class="text-red">*</span>
                        </label>
                        <input
                          type="text"
                          class="form-control"
                          value={addWebPageData.description}
                          onChange={(e) =>
                            setaddWebPageData({
                              ...addWebPageData,
                              description: e.target.value,
                            })
                          }
                        />
                      </li>
                      {/* <li class="col-6 form-group">
                        <FormControl className={classes.margin}>
                          <InputLabel id="demo-customized-select-label">
                            Blog category
                          </InputLabel>
                          <Select
                            labelId="demo-customized-select-label"
                            id="demo-customized-select"
                            placeholder="Blog category"
                            value={addBlogData.category}
                            onChange={(e) =>
                              setaddBlogData({
                                ...addBlogData,
                                category: e.target.value,
                              })
                            }
                            input={<BootstrapInput />}
                          >
                            <MenuItem value={"Sports"}>Sports</MenuItem>
                            <MenuItem value={"Fashion"}>Fashion</MenuItem>
                            <MenuItem value={"Adventure"}>Adventure</MenuItem>
                          </Select>
                        </FormControl>
                      </li> */}
                      {/* <li class="col-6 form-group">
                        <label for="">
                          Author Name<span class="text-red">*</span>
                        </label>
                        <input
                          type="text"
                          class="form-control"
                          value={addBlogData.authorName}
                          onChange={(e) =>
                            setaddBlogData({
                              ...addBlogData,
                              authorName: e.target.value,
                            })
                          }
                        />
                      </li> */}
                      <li>
                        <div class="img-option">
                          <ul class="cat-list">
                            {/* <li><a href="#" data-toggle="modal" data-target="#Browse-img">Browse</a></li> */}
                            <div
                              class="input-group mb-3"
                              style={styles.imageContainer}
                            >
                              <div class="custom-file">
                                <input
                                  type="file"
                                  class="custom-file-input"
                                  id="inputGroupFile01"
                                  onChange={(e) =>
                                    imageSelect(e, "webpageImage")
                                  }
                                  accept=".png, .jpg, .jpeg"
                                />
                                <label
                                  class="custom-file-label"
                                  for="inputGroupFile01"
                                >
                                  Choose file
                                </label>
                              </div>
                            </div>
                            {/* <li><a href="#">Clear</a></li> */}
                          </ul>
                          <div class="img-icon">
                            {/* <img src={selectedIcon} class="img-fluid" alt="img" /> */}
                            <img
                              src={addWebPageData.webPageImage}
                              alt="Web Page image"
                              style={styles.image}
                            />
                          </div>
                        </div>
                      </li>
                      <li class="col-12 form-group">
                        <label for="">
                          {" "}
                          Content
                          <Editor
                            initialValue="<p>This is the initial content of the editor</p>"
                            apiKey="51cwur6cytdvu3huo6pkx9zr6bjmkjdem4ljp1wenc4ih3ch"
                            init={{
                              height: 300,
                              menubar: false,
                              image_uploadtab: true,
                              images_upload_url:
                                "https://sterlingweb.in/dq/files/apis/blog-image-upload.php",

                              images_upload_handler: function (
                                blobInfo,
                                success,
                                failure,
                                progress
                              ) {
                                var xhr, formData;

                                xhr = new XMLHttpRequest();
                                xhr.withCredentials = false;
                                xhr.open(
                                  "POST",
                                  "https://sterlingweb.in/dq/files/apis/blog-image-upload.php"
                                );

                                xhr.upload.onprogress = function (e) {
                                  progress((e.loaded / e.total) * 100);
                                };
                                xhr.onload = function () {
                                  var json;
                                  if (xhr.status < 200 || xhr.status >= 300) {
                                    failure("HTTP Error: " + xhr.status);
                                    return;
                                  }
                                  json = JSON.parse(xhr.responseText);
                                  if (
                                    !json ||
                                    typeof json.location != "string"
                                  ) {
                                    failure(
                                      "Invalid JSON: " + xhr.responseText
                                    );
                                    return;
                                  }
                                  success(json.location);
                                };
                                xhr.onerror = function () {
                                  failure(
                                    "Image upload failed due to a XHR Transport error. Code: " +
                                      xhr.status
                                  );
                                };
                                formData = new FormData();
                                formData.append(
                                  "file",
                                  blobInfo.blob(),
                                  blobInfo.filename()
                                );
                                xhr.send(formData);
                              },
                              plugins: [
                                "advlist autolink lists link charmap print preview anchor",
                                "hr",
                                "searchreplace visualblocks code fullscreen",

                                "insertdatetime media table paste code help wordcount",
                              ],
                              toolbar:
                                "undo redo | image | imageupload | table | formatselect | bold italic backcolor | \
             alignleft aligncenter alignright alignjustify hr | \
             bullist numlist outdent indent | removeformat | help",
                            }}
                            value={addWebPageData.content}
                            onEditorChange={(e) =>
                              setaddWebPageData({
                                ...addWebPageData,
                                content: e,
                              })
                            }
                          />
                        </label>
                        {/**  <FroalaEditor tag='textarea' value={description} model={description} onModelChange={(e)=>setdescription(e)}/> **/}
                      </li>
                      {/** <li class="col-12 col-md-6 form-group">
         <label for="">meta tag description<span class="text-red">*</span></label>
         <input type="text" class="form-control" value="" />
      </li>
       <li class="col-12 col-md-6 form-group">
         <label for="">meta tag keywords<span class="text-red">*</span></label>
         <input type="text" class="form-control" value="" />
      </li>
       <li class="col-12 form-group">
         <label for="">Post tags<span class="text-red">*</span></label>
         <input type="text" class="form-control" value="" />
      </li> **/}
                    </ul>
                  </form>
                </div>
              )}
              {/* <a
                onClick={() => {
                  showGlobalSettings === true
                    ? setshowGlobalSettings(undefined)
                    : setshowGlobalSettings(true);
                }}
                href="javascript:void(0)"
                style={styles.dropDowns}
                className="search-btn"
                data-toggle="collapse"
                aria-expanded="false"
              >
                Global Settings
              </a> */}
              {/* {showGlobalSettings === true && (
                <div id="globe-setting" class="search-form1">
                  <form action="" class="col-12">
                    <ul class="row form-div">
                      <li class="col-12 col-md-6 form-group">
                        <div class="w-300">
                          <div class="img-option ">
                            <ul class="cat-list">
                              <li>
                                <a
                                  href="#"
                                  data-toggle="modal"
                                  data-target="#Browse-img"
                                >
                                  Browse
                                </a>
                              </li>
                              <li>
                                <a href="#">Clear</a>
                              </li>
                            </ul>
                            <div class="img-icon">
                              <img
                                src={require("../../../../constants/images/product1.jpg")}
                                class="img-fluid"
                                alt="img"
                              />
                            </div>
                          </div>
                        </div>
                      </li>

                      <li class="col-12 col-md-6 form-group">
                        <div class="img-option">
                          <ul class="cat-list">
                            <li><a href="#" data-toggle="modal" data-target="#Browse-img">Browse</a></li>
                            <div
                              class="input-group mb-3"
                              style={styles.imageContainer}
                            >
                              <div class="custom-file">
                                <input
                                  type="file"
                                  class="custom-file-input"
                                  id="inputGroupFile01"
                                  onChange={(e) => imageSelect(e, "favicon")}
                                  accept=".png, .jpg, .jpeg"
                                />
                                <label
                                  class="custom-file-label"
                                  for="inputGroupFile01"
                                >
                                  Choose file
                                </label>
                              </div>
                            </div>
                            <li><a href="#">Clear</a></li>
                          </ul>
                          <div class="img-icon">
                            <img src={selectedIcon} class="img-fluid" alt="img" />
                            <img
                              src={addBlogData.favicon}
                              alt="blog image"
                              style={styles.image}
                            />
                          </div>
                        </div>
                      </li>
                      <li class="col-12 col-md-6 form-group">
                        <label for="">
                          Name<span class="text-red">*</span>
                        </label>
                        <input
                          type="text"
                          class="form-control"
                          value={addBlogData.topTitle}
                          onChange={(e) =>
                            setaddBlogData({
                              ...addBlogData,
                              topTitle: e.target.value,
                            })
                          }
                        />
                      </li>
                    </ul>
                  </form>
                </div>
              )} */}
            </div>
          </div>
        </div>
      </div>

      <div
        class="modal fade video-modal"
        id="Browse-img"
        tabindex="-1"
        role="dialog"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">
                Image Manager
              </h5>
              <button
                type="button"
                class="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="wrap-data">
                <div class="drop-filewrap text-center">
                  <div class="file-cont">
                    <form action="upload.php" method="POST">
                      <input type="file" multiple />
                      <i class="fa fa-folder fa-5x" class="icon"></i>
                      <p>Drag your files here or click in this area.</p>
                    </form>
                  </div>
                  <button type="submit" class="button mx-auto">
                    Upload
                  </button>
                </div>
                <div class="category-wrap">
                  <div class="category-div">
                    <div class="category-cont">
                      <a href="product-list.php" class="cat-img">
                        <i class="fa fa-folder fa-5x" class="icon"></i>
                      </a>
                      <div class="form-check">
                        <input
                          class="form-check-input"
                          type="checkbox"
                          value=""
                          id="defaultCheck1"
                        />
                        <label class="form-check-label" for="defaultCheck1">
                          Products
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="category-div">
                    <div class="category-cont">
                      <a href="product-list.php" class="cat-img">
                        <i class="fa fa-folder fa-5x" class="icon"></i>
                      </a>
                      <div class="form-check">
                        <input
                          class="form-check-input"
                          type="checkbox"
                          value=""
                          id="defaultCheck1"
                        />
                        <label class="form-check-label" for="defaultCheck1">
                          Logo
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="category-div">
                    <div class="category-cont">
                      <a href="product-list.php" class="cat-img">
                        <i class="fa fa-folder fa-5x" class="icon"></i>
                      </a>
                      <div class="form-check">
                        <input
                          class="form-check-input"
                          type="checkbox"
                          value=""
                          id="defaultCheck1"
                        />
                        <label class="form-check-label" for="defaultCheck1">
                          Category
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="category-div">
                    <div class="category-cont">
                      <a href="product-list.php" class="cat-img">
                        <i class="fa fa-folder fa-5x" class="icon"></i>
                      </a>
                      <div class="form-check">
                        <input
                          class="form-check-input"
                          type="checkbox"
                          value=""
                          id="defaultCheck1"
                        />
                        <label class="form-check-label" for="defaultCheck1">
                          Products
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default AddWebPage;
