import * as firebase from "firebase";

export function getUserWebPages(
  dispatch,
  setuserWebPageData,
  setuserWebPageDataKeys,
  setshowLoader
) {
  const userWebPageData = firebase
    .database()
    .ref("user_webpages_data/" + localStorage.getItem("userId"));
  userWebPageData.on("value", (snapshot) => {
    setuserWebPageData(snapshot.val() && Object.values(snapshot.val()));
    setuserWebPageDataKeys(snapshot.val() && Object.keys(snapshot.val()));
    setshowLoader();
  });
}

export function saveUserWebPage(addWebPageData) {
  const pageId = addWebPageData.name.replace(/\s+/g, "-").toLowerCase();
  const Data = firebase
    .database()
    .ref(
      "user_webpages_data/" +
        localStorage.getItem("userId") +
        "/" +
        `webpage-${pageId}`
    )
    .set(addWebPageData);
}

export function getUserWebPageDataForEdit(setWebPageDataForEdit, key) {
  const userWebPageData = firebase
    .database()
    .ref("user_webpages_data/" + localStorage.getItem("userId") + "/" + key);
  userWebPageData.on("value", (snapshot) => {
    setWebPageDataForEdit(snapshot.val());
  });
}

export function updateUserWebPageAfterEdit(addWebPageData, key) {
  const userRef = firebase
    .database()
    .ref("user_webpages_data/" + localStorage.getItem("userId") + "/" + key)
    .set(addWebPageData);
}

export function deleteUserWebPage(key) {
  const userRef = firebase
    .database()
    .ref("user_webpages_data/" + localStorage.getItem("userId") + "/" + key)
    .remove();
}
