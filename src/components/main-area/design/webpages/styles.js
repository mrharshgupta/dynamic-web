const styles = {
  wrapData: {
    height: "100vh",
    overflowY: "scroll",
  },
  link: {
    textDecoration: "none",
    color: "white",
  },

  linkgrey: {
    textDecoration: "none",
    color: "grey",
  },
  image: { height: "90px", width: "180px" },
};

export default styles;
