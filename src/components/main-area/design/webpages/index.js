import React, { useState, useEffect } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { useDispatch } from "react-redux";
import { LoaderOne } from "../../../loaders/index";
import Popups from "../../../popups/index";
import styles from "./styles";
import "../../../../css/style.css";
import "../../../../css/device.css";
import "../../../../css/editor.css";
import "../../../../css/main.css";
import "../../../../css/sanjana.css";
import "../../../../css/bootstrap.min.css";
import Loader from "react-loader";
import CircularProgress from "@material-ui/core/CircularProgress";
import {
  getUserWebPages,
  saveUserWebPage,
  getUserWebPageDataForEdit,
  updateUserWebPageAfterEdit,
  deleteUserWebPage,
} from "./apis";

const WepPages = () => {
  const dispatch = useDispatch();
  const [showWebPagesPopup, setshowWebPagesPopup] = useState(false);
  const [showLoader, setshowLoader] = useState(true);
  const [userWebPagesData, setuserWebPagesData] = useState(undefined);
  const [userWebPagesDataKeys, setuserWebPagesDataKeys] = useState([]);

  useEffect(() => {
    getUserWebPages(
      dispatch,
      (abc) => setuserWebPagesData(abc),
      (abc) => setuserWebPagesDataKeys(abc),
      () => setshowLoader(false)
    );
  }, []);

  const WebPageComponent = ({ name, description, id }) => {
    console.log(userWebPagesDataKeys, "userWebPagesData");
    return (
      <tr>
        <td>
          <Link to={`webpages/add-webpage/true/${userWebPagesDataKeys[id]}`}>
            <i class="act-icon text-green fas fa-edit"></i>{" "}
          </Link>
          <span
            onClick={() => deleteUserWebPage(userWebPagesDataKeys[id])}
            style={{ marginLeft: "5px", cursor: "pointer" }}
          >
            <i class="act-icon text-red far fa-trash-alt"></i>
          </span>
        </td>
        {/* <td>
        <div class="switch">
          <label>
            Inactive 
            <input type="checkbox" checked={currentStatus === 'active'? true : false} onClick={()=>switchClick()} />
            <span class="lever"></span>
            Active
          </label>
        </div>
      </td> */}
        <td>{name}</td>
        <td>{description}</td>
      </tr>
    );
  };
  return (
    <>
      <div class="wrap-right">
        <div class="page-nav">
          <div class="page-nav-left">
            <div class="page-action">
              {/* <Link style={styles.link} to="#" class="button blog-search" onClick={()=>setshowBlogPopup(!showBlogPopup)}>blog category</Link> */}
              <Link style={styles.link} to="#" class="button">
                export
              </Link>
              <Link style={styles.link} to="#" class="button">
                print order
              </Link>
            </div>
            <h1 class="pg-ttl">Web Pages</h1>
          </div>
          <div class="page-nav-right">
            <div class="paging">
              <input
                type="text"
                name="showRec"
                id="showRec"
                value="20"
                class="show-rec"
                original-title="Show Records"
              />
              <div class="result">1 – 20 of 42</div>
              <a class="prev" href="sdc">
                <i class="fas fa-caret-left"></i>
              </a>
              <a class="next" href="sax">
                <i class="fas fa-caret-right"></i>
              </a>
            </div>
            {/**<Link style={styles.link} to="#" class="button">delete</Link>**/}

            <Link
              style={styles.link}
              to={`webpages/add-webpage/false/undefined`}
              class="button"
            >
              Create New Post
            </Link>
          </div>
        </div>
        <div class="wrap-data" style={styles.wrapData}>
          {showLoader && (
            <div
              style={{
                display: "flex",
                height: "100vh",
                justifyContent: "center",
                alignItems: "center",
                width: "100%",
              }}
            >
              <CircularProgress />
              <br />
              <span>Loading Web Pages...</span>
            </div>
          )}
          <table class="tbl-list">
            <thead>
              <tr>
                <th>
                  <a className="sort" href="xzz">
                    Actions
                  </a>
                </th>
                {/* <th>
                  <a className="sort" href="zx">
                    Status
                  </a>
                </th> */}
                <th>
                  <a className="sort" href="xzz">
                    Name
                  </a>
                </th>
                <th>
                  <a className="sort" href="zx">
                    Description
                  </a>
                </th>
              </tr>
            </thead>

            <tbody>
              {userWebPagesData &&
                userWebPagesData.map((value, index) => {
                  return (
                    <WebPageComponent
                      name={value.name}
                      description={value.description}
                      id={index}
                    />
                  );
                })}
            </tbody>
          </table>
          <center><h1>{!userWebPagesData && 'No pages found'}</h1></center>
        </div>
      </div>
      {/* <Popups showBlogPopup={showBlogPopup} /> */}
    </>
  );
};

export default WepPages;

// import React, {useState} from 'react';
// import {Link} from 'react-router-dom';
// import Popups from '../../../popups/index';
// import styles from './styles';
// import '../../../../css/style.css';
// import '../../../../css/device.css';
// import '../../../../css/editor.css';
// import '../../../../css/main.css';
// import '../../../../css/sanjana.css';
// import '../../../../css/bootstrap.min.css';

// const Webpages = (props) => {

//  const [showSearch, setshowSearch] = useState(false);

// return(
// <>

// <div class="wrap-right">
//       <div class="page-nav">
//          <div class="page-nav-left">
//             <div class="page-action">
//                <Link to="#" style={styles.link} className="button search" onClick={()=>setshowSearch(!showSearch)}>search</Link>
//                     <Link to="#" style={styles.link} className="button">export</Link>
//                     <Link to="#" style={styles.link} className="button">import</Link>
//                     <Link to="#" style={styles.link} className="button">print order</Link>
//             </div>
//             <h1 class="pg-ttl">Web Pages</h1>
//          </div>
//          <div class="page-nav-right">
//             <div class="paging">
//                <input type="text" name="showRec" id="showRec" value="20" class="show-rec" original-title="Show Records" />
//                <div class="result">1 – 20 of 42</div>
//                <a class="prev" href="#"><i class="fas fa-caret-left"></i></a>
//                <a class="next" href="#"><i class="fas fa-caret-right"></i></a>
//             </div>
//             <Link to="#" style={styles.link} className="button" data-toggle="modal" data-target="#exampleModal">Video Guide</Link>
//          </div>
//       </div>
//       <div class="wrap-data">

//          <table class="tbl-list">
//             <thead>
//                <tr>
//                   <th><i class="chk"><input type="checkbox" class="chkAll" title="Select All" /><em></em></i></th>
//                   <th width="30%"><a class="sort" href="#">Web Page Title</a></th>
//                   <th width="30%"><a class="sort" href="#">Status</a></th>
//                   <th><a class="sort" href="#">Action</a></th>
//                </tr>
//             </thead>
//             <tbody>
//                <tr>
//                   <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
//                   <td><Link style={styles.linkgrey} to="/home/design/webpages/web-info">About Us</Link></td>
//                   <td>
//                      <div class="custom-control custom-switch">
//                         <input type="checkbox" class="custom-control-input" id="customSwitch1" />
//                         <label class="custom-control-label" for="customSwitch1">Disable</label>
//                      </div>
//                   </td>
//                   <td>
//                      <ul class="action-icon">
//                         <li><a href="#"><i class="act-icon text-blue fa fa-eye fa-lg"></i></a></li>
//                         <li><a href="#"><i class="act-icon text-green fas fa-edit"></i></a></li>
//                         <li><a href="#"><i class="act-icon text-red far fa-trash-alt"></i></a></li>
//                      </ul>
//                   </td>
//                </tr>
//                <tr>
//                   <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
//                   <td><Link style={styles.linkgrey} to="/home/design/webpages/web-del-info">Delivery Information</Link></td>
//                   <td>
//                      <div class="custom-control custom-switch">
//                         <input type="checkbox" class="custom-control-input" id="customSwitch2" />
//                         <label class="custom-control-label" for="customSwitch2">Disable</label>
//                      </div>
//                   </td>
//                   <td>
//                      <ul class="action-icon">
//                         <li><a href="#"><i class="act-icon text-blue fa fa-eye fa-lg"></i></a></li>
//                         <li><a href="#"><i class="act-icon text-green fas fa-edit"></i></a></li>
//                         <li><a href="#"><i class="act-icon text-red far fa-trash-alt"></i></a></li>
//                      </ul>
//                   </td>
//                </tr>
//                <tr>
//                   <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
//                   <td><Link style={styles.linkgrey} to="/home/design/webpages/web-store">Store Privacy</Link></td>
//                   <td>
//                      <div class="custom-control custom-switch">
//                         <input type="checkbox" class="custom-control-input" id="customSwitch3" />
//                         <label class="custom-control-label" for="customSwitch3">Disable</label>
//                      </div>
//                   </td>
//                   <td>
//                      <ul class="action-icon">
//                         <li><a href="#"><i class="act-icon text-blue fa fa-eye fa-lg"></i></a></li>
//                         <li><a href="#"><i class="act-icon text-green fas fa-edit"></i></a></li>
//                         <li><a href="#"><i class="act-icon text-red far fa-trash-alt"></i></a></li>
//                      </ul>
//                   </td>
//                </tr>
//                <tr>
//                   <td><i class="chk"><input type="checkbox" value="1" /><em></em></i></td>
//                   <td><Link style={styles.linkgrey} to="/home/design/webpages/web-terms">Terms & Conditions</Link></td>
//                   <td>
//                      <div class="custom-control custom-switch">
//                         <input type="checkbox" class="custom-control-input" id="customSwitch4" />
//                         <label class="custom-control-label" for="customSwitch4">Disable</label>
//                      </div>
//                   </td>
//                   <td>
//                      <ul class="action-icon">
//                         <li><a href="#"><i class="act-icon text-blue fa fa-eye fa-lg"></i></a></li>
//                         <li><a href="#"><i class="act-icon text-green fas fa-edit"></i></a></li>
//                         <li><a href="#"><i class="act-icon text-red far fa-trash-alt"></i></a></li>
//                      </ul>
//                   </td>
//                </tr>
//             </tbody>
//          </table>
//       </div>
//    </div>

// <div class="modal fade video-modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
//    <div class="modal-dialog" role="document">
//       <div class="modal-content">
//          <div class="modal-header">
//             <h5 class="modal-title" id="exampleModalLabel">How to Add a Product to Your Online Store</h5>
//             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
//             <span aria-hidden="true">&times;</span>
//             </button>
//          </div>
//          <div class="modal-body">
//             <div class="video-div">
//                <iframe width="100%" height="100%" src="https://www.youtube.com/embed/yAoLSRbwxL8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
//             </div>
//          </div>
//       </div>
//    </div>
// </div>

// <Popups showSearch={showSearch} />
// </>
// );
// }

// export default Webpages;
