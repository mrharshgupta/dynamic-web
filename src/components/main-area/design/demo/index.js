import React, {useState, useContext} from 'react';
import {LoaderOne} from '../../../loaders/index';
import '../../../../css/style.css';
import '../../../../css/device.css';
import '../../../../css/editor.css';
import '../../../../css/main.css';
import Loader from 'react-loader';
import '../../../../css/sanjana.css';
import '../../../../css/bootstrap.min.css';
import {ServerContext} from '../../../../contexts/contexts.js';

const Demo = ({match}) => {
  const [showLoader1, setshowLoader1] = useState(true);
console.log(match.params.templateId); 
const server = useContext(ServerContext);
return(
<>
{(showLoader1) && (
  <div style={{display: 'flex' ,justifyContent: 'center', alignItems: 'center', height: '100vh', width: '100%'}}>
<Loader loaded={false}  /></div>
 )} 

<iframe 
  src={`https://${server}/dq/files/stock/${match.params.templateId}`}
  onLoad={()=>setshowLoader1(false)}
  style={{height: '100vh', width: '100%',}} />
</>
)
}
export default Demo;
