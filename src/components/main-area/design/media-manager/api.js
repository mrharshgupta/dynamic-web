import {apiUrls} from '../../../../api/helper.js';
import {consoleInDebugModeOnly2} from '../../../../helpers/debugging';
import * as firebase  from 'firebase';
import axios from 'axios';
import _ from 'lodash';

export function  getMediaManagerImages (setmediaManagerImages, setloader, setapiStatus) {
  
  fetch(apiUrls.getMediaManagerImages, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      
      userId: localStorage.getItem('userId'),
    }),
  }) 
    .then((response) => {console.log(response, 'response'); return response.json()})
    .then((responseData) => {
     consoleInDebugModeOnly2('logInSuccess-responseData', responseData);
        setmediaManagerImages(responseData);
        setloader();
        setapiStatus(true);
  })
    .catch((error) => {
      consoleInDebugModeOnly2('logInErrorWithoutApi', error.response);
      setloader();    
      setapiStatus(false);
    });
};


export function  deleteFile  (fileName, useEffectArray, setuseEffectArray, setdeleteImageLoader) {
  fetch(apiUrls.deleteFile, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      fileName: fileName,
      userId: localStorage.getItem('userId'),
    }),
  }) 
    .then((response) => {setuseEffectArray(useEffectArray+1); console.log(response, 'response'); return response.json()})
    .then((responseData) => {
     consoleInDebugModeOnly2('logInSuccess-responseData', responseData);
     setdeleteImageLoader();
  })
    .catch((error) => {
      consoleInDebugModeOnly2('logInErrorWithoutApi', error.response);
      setdeleteImageLoader(); 
    });
};



export const fileSelect = (event, useEffectArray, setuseEffectArray, selectedFile, setselectedFile, setuploadImageLoader) => {
  //setselectedFile(URL.createObjectURL(event.target.files[0]));
  const fd = new FormData();
  
   for (const key of Object.keys(event.target.files)) {
             setselectedFile(selectedFile.concat(URL.createObjectURL(event.target.files[key])));
             fd.append('image[]', event.target.files[key])
             fd.append('fileName[]', event.target.files[key].name);
         }

  //fd.append('image[]', event.target.files[0]);
  fd.append('userId', localStorage.getItem('userId'));
  //fd.append('fileName', event.target.files[0].name);

//   for (var pair of fd.entries()) {
//     console.log(pair[0]+ ', ' + pair[1]); 
// }
    axios.post(apiUrls.imageUpload, 
               fd, 
               {headers: {
                 Accept: 'application/json',
                 'Content-Type': 'application/json',
               },},
    )
    .then((response) => {console.log(response, 'response'); setuseEffectArray(useEffectArray+1); return response.json()})
    .then((response) => {
     consoleInDebugModeOnly2('fileuploadSuccess-responseData', response);
     setuploadImageLoader();
     fd.reset();
    }) 
    .catch((error) => {
      consoleInDebugModeOnly2('fileuploadErrorWithoutApi', error.response);
      setuploadImageLoader();
    });
}
