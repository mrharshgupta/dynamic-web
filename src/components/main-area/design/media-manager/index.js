import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Popups from "../../../popups/index";
import styles from "./styles";
import "../../../../css/style.css";
import "../../../../css/device.css";
import "../../../../css/editor.css";
import "../../../../css/main.css";
import "../../../../css/sanjana.css";
import "../../../../css/bootstrap.min.css";
import { MainPlaceholder, UploadImagePlaceholder } from "./placeholder.js";
import { fileSelect, deleteFile, getMediaManagerImages } from "./api.js";
import ImageComponent from "./image-component.js";
import _ from "lodash";

const MediaManager = (props) => {
  const [selectedFile, setselectedFile] = useState([]);
  const [mediaManagerImages, setmediaManagerImages] = useState(undefined);
  const [loader, setloader] = useState(true);
  const [uploadImageLoader, setuploadImageLoader] = useState(false);
  const [useEffectArray, setuseEffectArray] = useState(1);
  const [imagesToBeDeleted, setimagesToBeDeleted] = useState([]);
  const [apiStatus, setapiStatus] = useState(undefined);

  useEffect(() => {
    getMediaManagerImages(
      (abc) => setmediaManagerImages(abc),
      () => setloader(false),
      (abc) => setapiStatus(abc)
    );
  }, [useEffectArray]);
  console.log(imagesToBeDeleted && imagesToBeDeleted);
  console.log(mediaManagerImages, "media-manager-images");
  function sort_by_key(array, key) {
    return (
      array &&
      array.sort(function (b, a) {
        var x = a[key];
        var y = b[key];
        return x < y ? -1 : x > y ? 1 : 0;
      })
    );
  }
  let sortArray;
  sortArray = sort_by_key(mediaManagerImages, "date");

  return (
    <>
      <div class="wrap-right">
        <div class="page-nav">
          <div class="page-nav-left">
            <div class="page-action page-action2">
              <a
                href="javascript:void(0)"
                onClick={() => {
                  setloader(true);
                  setuseEffectArray(useEffectArray + 1);
                }}
                className="button"
                data-toggle="tooltip"
                title="Refresh"
              >
                <i class="text-white fa fa-refresh"></i>
              </a>
              {/**<a href="javascript:void(0)" className="button" data-toggle="tooltip" title="Up"><i class="text-white fa fa-level-up"></i></a>
               <a href="javascript:void(0)" class="button" data-toggle="tooltip" title="Upload"><i class="text-white fa fa-upload"></i></a>
               <a href="javascript:void(0)" class="button" data-toggle="tooltip" title="Folder"><i class="text-white fa fa-folder"></i></a>
               <a href="javascript:void(0)" class="button" data-toggle="tooltip" title="Delete"><i class="text-white fa fa-trash-o"></i></a>
            **/}
            </div>
            <h1 class="pg-ttl"> Media Manager</h1>
          </div>
          <div class="page-nav-right">
            <div class="paging">
              <input
                type="text"
                name="showRec"
                id="showRec"
                value="20"
                class="show-rec"
                original-title="Show Records"
              />
              <div class="result">1 – 20 of 42</div>
              <a class="prev" href="#">
                <i class="fas fa-caret-left"></i>
              </a>
              <a class="next" href="#">
                <i class="fas fa-caret-right"></i>
              </a>
            </div>
          </div>
        </div>
        {loader ? (
          <MainPlaceholder />
        ) : (
          <>
            <div class="wrap-data" style={styles.wrapData}>
              <div class="subtitle">
                <h2>Upload image</h2>
              </div>
              <div class="drop-filewrap text-center">
                <div class="file-cont">
                  <form onSubmit={fileSelect} enctype="multipart/form-data">
                    <input
                      type="file"
                      multiple
                      onChange={(e) => {
                        setuploadImageLoader(true);
                        fileSelect(
                          e,
                          useEffectArray,
                          setuseEffectArray,
                          selectedFile,
                          (abc) => setselectedFile(abc),
                          () => setuploadImageLoader(false)
                        );
                      }}
                      accept=".png, .jpg, .jpeg"
                    />
                    {uploadImageLoader ? (
                      <UploadImagePlaceholder />
                    ) : (
                      <span>
                        <i className="fa fa-folder fa-5x icon"></i>
                      </span>
                    )}

                    {/**(selectedFile ? <>{_.times(selectedFile.length, (e) =><img src={selectedFile[e]} style={{height: '100px', width: '100px'}} />)}</> : <span><i className="fa fa-folder fa-5x icon"></i></span>)}
                     **/}
                    <p>Drag your file here or click in this area.</p>
                  </form>
                </div>
                {/**<button type="submit" class="button mx-auto">Upload</button>**/}
              </div>
              <div class="category-wrap">
                {mediaManagerImages && mediaManagerImages.length === 0 && (
                  <h2>No uploaded image found.</h2>
                )}
                {!apiStatus && (
                  <div>
                    <h2>An Error Occurred</h2>
                  </div>
                )}
                {mediaManagerImages &&
                  mediaManagerImages.map((item, i) => {
                    return (
                      <div class="category-div">
                        <div class="category-cont">
                          <ImageComponent
                            imageName={mediaManagerImages[i].name}
                            useEffectArray={useEffectArray}
                            setuseEffectArray={setuseEffectArray}
                          />
                        </div>
                      </div>
                    );
                  })}
              </div>
            </div>
          </>
        )}
      </div>
    </>
  );
};

export default MediaManager;
