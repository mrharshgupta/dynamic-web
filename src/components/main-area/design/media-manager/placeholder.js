import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

export const MainPlaceholder = () => {
return(
<div style={{height: '100vh', width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center'}}> 
<CircularProgress />
</div>
);
}

export const UploadImagePlaceholder = () => {
return(
<div> 
<CircularProgress />
</div>
);
}

export const DeleteImagePlaceholder = () => {
return(
<div> 
<CircularProgress />
</div>
);
}