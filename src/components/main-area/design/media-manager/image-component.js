import React, { useState } from "react";
import styles from "./styles";
import { MainPlaceholder, UploadImagePlaceholder } from "./placeholder.js";
import { fileSelect, deleteFile, getMediaManagerImages } from "./api.js";
import { DeleteImagePlaceholder } from "./placeholder.js";
import { connect } from "react-redux";

const ImageComponent = ({
  serverName,
  imageName,
  useEffectArray,
  setuseEffectArray,
}) => {
  const [deleteImageLoader, setdeleteImageLoader] = useState(false);
  return (
    <span
      style={{
        height: "120px",
        margin: "10px",
        flexDirection: "column",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      {deleteImageLoader ? (
        <DeleteImagePlaceholder />
      ) : (
        <>
          <img
            src={`http://${serverName}/dq/files/media-manager/${localStorage.getItem(
              "userId"
            )}/submit/${imageName}`}
            style={{ position: "relative", height: "90px", width: "90px" }}
          />
          <span
            onClick={() => {
              setdeleteImageLoader(true);
              deleteFile(imageName, useEffectArray, setuseEffectArray, () =>
                setdeleteImageLoader(false)
              );
            }}
            style={{ marginTop: "5px", cursor: "pointer" }}
          >
            <i class="act-icon text-red far fa-trash-alt fa-2x"></i>
          </span>
        </>
      )}
    </span>
  );
};
const mapStateToProps = ({ serverName }) => ({
  serverName,
});
export default connect(mapStateToProps, null)(ImageComponent);
