import React, { useState, useEffect } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { getUserBlogs, updateUserBlogStatus, deleteUserBlog } from "./api.js";
import { useDispatch } from "react-redux";
import { LoaderOne } from "../../../loaders/index";
import Popups from "../../../popups/index";
import styles from "./styles";
import "../../../../css/style.css";
import "../../../../css/device.css";
import "../../../../css/editor.css";
import "../../../../css/main.css";
import "../../../../css/sanjana.css";
import "../../../../css/bootstrap.min.css";
import Loader from "react-loader";
import CircularProgress from "@material-ui/core/CircularProgress";

const Blog = () => {
  const dispatch = useDispatch();
  const [showBlogPopup, setshowBlogPopup] = useState(false);
  const [showLoader, setshowLoader] = useState(true);
  const [userBlogData, setuserBlogData] = useState(undefined);
  const [userBlogDataKeys, setuserBlogDataKeys] = useState([]);
  useEffect(() => {
    getUserBlogs(
      dispatch,
      (abc) => setuserBlogData(abc),
      (abc) => setuserBlogDataKeys(abc),
      () => setshowLoader(false)
    );
  }, []);
  console.log(
    userBlogDataKeys,
    "userBlogDataKeys userBlogDataKeys userBlogDataKeys"
  );
  const BlogComponent = ({ title, status, id, userBlogDataKeys, date }) => {
    const [currentStatus, setcurrentStatus] = useState(status);
    const switchClick = () => {
      setcurrentStatus(currentStatus === "active" ? "inactive" : "active");
      updateUserBlogStatus(
        currentStatus === "active" ? "inactive" : "active",
        userBlogDataKeys[id]
      );
    };
    return (
      <tr>
        <td>
          <Link to={`blog/add-post/true/${userBlogDataKeys[id]}`}>
            <i class="act-icon text-green fas fa-edit"></i>{" "}
          </Link>
          <span
            onClick={() => deleteUserBlog(userBlogDataKeys[id])}
            style={{ marginLeft: "5px", cursor: "pointer" }}
          >
            <i class="act-icon text-red far fa-trash-alt"></i>
          </span>
        </td>
        <td>
          <div class="switch">
            <label>
              Inactive
              <input
                type="checkbox"
                checked={currentStatus === "active" ? true : false}
                onClick={() => switchClick()}
              />
              <span class="lever"></span>
              Active
            </label>
          </div>
        </td>
        <td>{title}</td>
        <td>{date}</td>
      </tr>
    );
  };
  return (
    <>
      <div class="wrap-right">
        <div class="page-nav">
          <div class="page-nav-left">
            <div class="page-action">
              <Link
                style={styles.link}
                to="#"
                class="button blog-search"
                onClick={() => setshowBlogPopup(!showBlogPopup)}
              >
                blog category
              </Link>
              <Link style={styles.link} to="#" class="button">
                export
              </Link>
              <Link style={styles.link} to="#" class="button">
                print order
              </Link>
            </div>
            <h1 class="pg-ttl">Blog Post</h1>
          </div>
          <div class="page-nav-right">
            <div class="paging">
              <input
                type="text"
                name="showRec"
                id="showRec"
                value="20"
                class="show-rec"
                original-title="Show Records"
              />
              <div class="result">1 – 20 of 42</div>
              <a class="prev" href="sdc">
                <i class="fas fa-caret-left"></i>
              </a>
              <a class="next" href="sax">
                <i class="fas fa-caret-right"></i>
              </a>
            </div>
            {/**<Link style={styles.link} to="#" class="button">delete</Link>**/}

            <Link
              style={styles.link}
              to={`blog/add-post/false/undefined`}
              class="button"
            >
              Create New Post
            </Link>
          </div>
        </div>
        <div class="wrap-data" style={styles.wrapData}>
          {showLoader && (
            <div
              style={{
                display: "flex",
                height: "100vh",
                justifyContent: "center",
                alignItems: "center",
                width: "100%",
              }}
            >
              <CircularProgress />
              <br />
              <span>Loading Blogs...</span>
            </div>
          )}
          <table class="tbl-list">
            <thead>
              <tr>
                <th>
                  <a className="sort" href="xzz">
                    Actions
                  </a>
                </th>
                <th>
                  <a className="sort" href="zx">
                    Status
                  </a>
                </th>
                <th>
                  <a className="sort" href="xzz">
                    Title
                  </a>
                </th>
                <th>
                  <a className="sort" href="zx">
                    Publishing Date
                  </a>
                </th>
              </tr>
            </thead>

            <tbody>
              {userBlogData &&
                userBlogData.map((value, index) => {
                  return (
                    <BlogComponent
                      title={value.title}
                      description={value.description}
                      status={value.status}
                      id={index}
                      date={value.date}
                      userBlogDataKeys={userBlogDataKeys}
                    />
                  );
                })}
            </tbody>
          </table>
          <center><h1>{!userBlogData && 'No Blogs found'}</h1></center>
        </div>
      </div>
      <Popups showBlogPopup={showBlogPopup} />
    </>
  );
};

export default Blog;
