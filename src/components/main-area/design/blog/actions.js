export const GET_USER_BLOGS_REQUEST = 'GET_USER_BLOGS_REQUEST';
export const GET_USER_BLOGS_SUCCESS = 'GET_USER_BLOGS_SUCCESS';
export const GET_USER_BLOGS_FAILURE = 'GET_USER_BLOGS_FAILURE';

export const getUserBlogsRequest = (dispatch) => {
	dispatch({type: GET_USER_BLOGS_REQUEST, data: {loading: true}});
};

export const getUserBlogsSuccess = (dispatch, data) => {
	dispatch({
		type: GET_USER_BLOGS_SUCCESS,
		data: {data, loading: false},
	});
};

export const getUserBlogsFailure = (dispatch) => {
	dispatch({
		type: GET_USER_BLOGS_FAILURE,
		data: {loadng: false},
	});
};
