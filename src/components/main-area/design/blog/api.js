import * as firebase from "firebase";

export function getUserBlogs(
  dispatch,
  setuserBlogData,
  setuserBlogDataKeys,
  setshowLoader
) {
  const userBlogData = firebase
    .database()
    .ref("user_blog_data/" + localStorage.getItem("userId"));
  userBlogData.on("value", (snapshot) => {
    setuserBlogData(snapshot.val() && Object.values(snapshot.val()));
    setuserBlogDataKeys(snapshot.val() && Object.keys(snapshot.val()));
    setshowLoader();
  });
}

export function saveUserBlog(addBlogData) {
  const blogId = addBlogData.title.replace(/\s+/g, "-").toLowerCase();
  const Data = firebase
    .database()
    .ref(
      "user_blog_data/" +
        localStorage.getItem("userId") +
        "/" +
        `blog-${blogId}`
    )
    .set(addBlogData);
}

export function updateUserBlogStatus(status, key) {
  const userRef = firebase
    .database()
    .ref("user_blog_data/" + localStorage.getItem("userId") + "/" + key);
  userRef.update({ status: status });
}

export function getUserBlogDataForEdit(setblogDataForEdit, key) {
  const userBlogData = firebase
    .database()
    .ref("user_blog_data/" + localStorage.getItem("userId") + "/" + key);
  userBlogData.on("value", (snapshot) => {
    setblogDataForEdit(snapshot.val());
  });
}

export function updateUserBlogAfterEdit(addBlogData, key) {
  const userRef = firebase
    .database()
    .ref("user_blog_data/" + localStorage.getItem("userId") + "/" + key)
    .set(addBlogData);
}

export function deleteUserBlog(key) {
  const userRef = firebase
    .database()
    .ref("user_blog_data/" + localStorage.getItem("userId") + "/" + key)
    .remove();
}
