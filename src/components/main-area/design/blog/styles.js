const styles = {
  wrapData: {
    height: "100vh",
    overflowY: "scroll",
  },
  link: {
    textDecoration: "none",
    color: "white",
  },
  dropDowns: {
    textDecoration: "none",
    color: "black",
  },
  imageContainer: {
    marginTop: "5px",
    marginBottom: "5px",
    marginRight: "10px",
    marginLeft: "10px",
    width: "90%",
  },

  image: { height: "90px", width: "180px" },
};

export default styles;
