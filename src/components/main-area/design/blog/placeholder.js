import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

const Placeholder = () => {
return(
<div style={{display: 'flex', height: '100vh', justifyContent: 'center', alignItems: 'center', width: '100%'}}>
<CircularProgress />
</div>
);
} 
export default Placeholder;