import React from "react";
import { Link } from "react-router-dom";
import styles from "./styles";
import "../../css/style.css";
import "../../css/device.css";
import "../../css/editor.css";
import "../../css/main.css";
import "../../css/sanjana.css";
import "../../css/bootstrap.min.css";
import { useDispatch } from "react-redux";

const Footer = () => {
  const dispatch = useDispatch();
  return (
    <div className="trial-bottom">
      <div
        style={styles.crossButtonDiv}
        onClick={() =>
          dispatch({ type: "HIDE_SHOW_FOOTER_FOR_SUBSCRIPTION", data: false })
        }
      >
        <i style={styles.crossButton} class="fas fa-times fa-2x"></i>
      </div>
      <p>
        <strong>9 Days</strong> left in your trial{" "}
        <Link
          to={`/${localStorage.getItem("storeName")}/home/subscriptions`}
          style={styles.link}
          className="button"
        >
          Select A Plan
        </Link>
      </p>
    </div>
  );
};
export default Footer;
