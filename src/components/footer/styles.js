const styles = {
  link: {
    textDecoration: "none",
    color: "white",
  },
  crossButtonDiv: {
    position: "absolute",
    right: "50px",
  },
  crossButton: {
    color: "white",
    cursor: "pointer",
  },
};

export default styles;
