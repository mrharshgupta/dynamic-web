import React, { useState } from "react";
import { Link } from "react-router-dom";
import styles from "./styles.js";
import DropdownOptions from "./drop-down-options.js";
import "../../css/style.css";
import "../../css/device.css";
import "../../css/editor.css";
import "../../css/main.css";
import "../../css/sanjana.css";
import "../../css/bootstrap.min.css";
import { connect } from "react-redux";

const WrapLeft = ({ userTemplateId, appliedTemplate }) => {
  const [showDropDownOptions, setshowDropDownOptions] = useState(undefined);

  return (
    <div style={styles.container} className="wrap-left">
      <div style={{ background: "white" }}>
        <ul className="main-nav">
          <li className="active">
            <Link
              to={`/${localStorage.getItem("storeName")}/home`}
              style={styles.link}
            >
              Home
            </Link>
          </li>
          {/**
<li><a href="javascript:void(0)" style={(showDropDownOptions === 'orders') ? styles.activePlusButton : {}}  className="down-arrow" onClick={()=>{(showDropDownOptions === 'orders')? (setshowDropDownOptions(undefined)) : (setshowDropDownOptions('orders'))}}></a>
<Link to="/home/order-list" style={(showDropDownOptions === 'orders')? styles.activeLink : styles.link}>Orders</Link>
    {(showDropDownOptions === 'orders') && <DropdownOptions options={[["Orders", "/home/order-list"], ["Returns", "/home/returns"]]} />}
</li>

<li><a href="javascript:void(0)" 
       style={(showDropDownOptions === 'products') ? styles.activePlusButton : {}} 
       class="down-arrow" onClick={()=>{(showDropDownOptions === 'products')? (setshowDropDownOptions(undefined)) : (setshowDropDownOptions('products'))}}></a>
<Link to="#" style={(showDropDownOptions === 'products')? styles.activeLink : styles.link}>Products</Link>
    {(showDropDownOptions === 'products') && <DropdownOptions options={[["Products", "/home/products"], ["Categories", "/home/categories"], ["Options", "/home/options"], ["Attributes", "/home/attributes"], ["Reviews", "/home/reviews"], ["Brands", "/home/brands"], ["Downloads", "/home/downloads"]]}/>}
</li>

<li><a href="javascript:void(0)" 
       style={(showDropDownOptions === 'customers') ? styles.activePlusButton : {}} 
       class="down-arrow" 
       onClick={
         ()=>{(showDropDownOptions === 'customers')? (setshowDropDownOptions(undefined)) : (setshowDropDownOptions('customers'))}} />
<Link to="#" style={(showDropDownOptions === 'customers')? styles.activeLink : styles.link}>Customers</Link>
    {(showDropDownOptions === 'customers') && <DropdownOptions options={[["Customers", "/home/customers"], ["Customer Groups", "/home/customer-groups"]]} />}      
</li>

<li><a href="javascript:void(0)" 
       class="down-arrow" 
       style={(showDropDownOptions === 'marketing') ? styles.activePlusButton : {}} 
       onClick={()=>{(showDropDownOptions === 'marketing')? (setshowDropDownOptions(undefined)) : (setshowDropDownOptions('marketing'))}
       }/>
<Link to="#" style={(showDropDownOptions === 'marketing')? styles.activeLink : styles.link}>Marketing</Link>
    {(showDropDownOptions === 'marketing') && <DropdownOptions options={[["Campaigns", "/home/marketing/campaigns"], ["Affiliates", "/home/marketing/affiliates"], ["Coupons", "/home/marketing/coupons"], ["Gift Vouchers", "/home/marketing/gift-vouchers"], ["Integrations", "/home/marketing/integrations"]]} />}      
</li>

**/}
          <li>
            <a
              href="javascript:void(0)"
              style={
                showDropDownOptions === "design" ? styles.activePlusButton : {}
              }
              class="down-arrow"
              onClick={() => {
                showDropDownOptions === "design"
                  ? setshowDropDownOptions(undefined)
                  : setshowDropDownOptions("design");
              }}
            />
            <Link
              to={`/${localStorage.getItem("storeName")}/home/design/templates`}
              style={
                showDropDownOptions === "design"
                  ? styles.activeLink
                  : styles.link
              }
            >
              Design
            </Link>
            {showDropDownOptions === "design" && (
              <DropdownOptions
                options={[
                  [
                    "Templates",
                    `/${localStorage.getItem(
                      "storeName"
                    )}/home/design/templates`,
                  ],
                  [
                    "Customize",
                    `/${localStorage.getItem(
                      "storeName"
                    )}/customize-template/${userTemplateId}/${appliedTemplate}/true`,
                    "#",
                  ],
                  [
                    "Web Pages",
                    `/${localStorage.getItem(
                      "storeName"
                    )}/home/design/webpages`,
                    "#",
                  ],
                  [
                    "Blog",
                    `/${localStorage.getItem("storeName")}/home/design/blog`,
                    `/${localStorage.getItem(
                      "storeName"
                    )}/home/design/blog/add-post/false/null`,
                  ],
                  [
                    "Media Manager",
                    `/${localStorage.getItem(
                      "storeName"
                    )}/home/design/media-manager`,
                    "#",
                  ],
                ]}
              />
            )}
          </li>
          {/** 
<li><a href="#" className="add"></a><Link to="/home/analytics" style={styles.link}>Analytics</Link></li>
<li><a href="#" className="add"></a><Link to="#" style={styles.link}>Mobile App</Link></li>
<li><a href="#" className="add"></a><Link to="#" style={styles.link}>Apps & Services</Link></li>
**/}
          <li>
            <a href="#" className="add"></a>
            <Link
              to={`/${localStorage.getItem("storeName")}/home/settings`}
              style={styles.link}
            >
              Settings
            </Link>
          </li>
        </ul>
      </div>
    </div>
  );
};

const mapStateToProps = ({ appliedTemplate, userTemplateId }) => ({
  userTemplateId,
  appliedTemplate,
});
export default connect(mapStateToProps)(WrapLeft);
