import colors from "../../constants/colors";

const styles = {
  container: {
    height: "100vh",
    overflowY: "auto",
    backgroundColor: "white",
  },
  link: {
    textDecoration: "none",
    backgroundColor: "white",
  },

  activeLink: {
    textDecoration: "none",
    backgroundColor: "rgba(220, 220, 220, 0.4)",
    marginBottom: 4,
  },

  activePlusButton: {
    textDecoration: "none",
    backgroundColor: colors.primary,
  },

  dropDownLink: {
    textDecoration: "none",
    borderTop: "2px solid white",
    borderBottom: "2px solid white",
    backgroundColor: "rgba(220, 220, 220, 0.7)",
    width: "81.5%",
    color: "grey",
  },

  dropDownLinkWithoutAddButton: {
    textDecoration: "none",
    borderTop: "2px solid white",
    borderBottom: "2px solid white",
    backgroundColor: "rgba(220, 220, 220, 0.7)",
    width: "100%",
    color: "grey",
  },

  dropDownPlusLink: {
    borderTop: "2px solid white",
    borderBottom: "2px solid white",
    backgroundColor: "rgba(220, 220, 220, 1.0)",
    width: "18.5%",
    height: "100%",
    color: "grey",
  },

  plusButtonSubMenu: {
    backgroundColor: "rgba(220, 220, 220, 1.0)",
    borderTop: "2px solid white",
    borderBottom: "2px solid white",
    height: "100%",
  },
};
export default styles;
