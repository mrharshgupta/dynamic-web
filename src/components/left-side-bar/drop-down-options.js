import React from "react";
import { Link } from "react-router-dom";
import { Animated } from "react-animated-css";
import AnimateHeight from "react-animate-height";
import styles from "./styles.js";
import "../../css/style.css";
import "../../css/device.css";
import "../../css/editor.css";
import "../../css/main.css";
import "../../css/sanjana.css";
import "../../css/bootstrap.min.css";
import colors from "../../constants/colors";

const DropdownOptions = ({ options }) => {
  function MouseOver(event) {
    event.target.style.backgroundColor = colors.primary;
  }
  function MouseOut(event) {
    event.target.style.backgroundColor = "rgba(220, 220, 220, 1.0)";
  }
  return (
    <div style={{ width: "100%" }}>
      {options.map((value, index) => {
        return (
          <div style={{ Display: "flex", flexDirection: "row", width: "100%" }}>
            <li>
              <Link
                to={value[1]}
                style={
                  value[2]
                    ? styles.dropDownLink
                    : styles.dropDownLinkWithoutAddButton
                }
                key={index}
              >
                {value[0]}
              </Link>

              {value[2] && (
                <Link
                  to={value[2]}
                  class="add"
                  style={styles.plusButtonSubMenu}
                  onMouseOver={MouseOver}
                  onMouseOut={MouseOut}
                />
              )}
            </li>
          </div>
        );
      })}
    </div>
  );
};

export default DropdownOptions;
