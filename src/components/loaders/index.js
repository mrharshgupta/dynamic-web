import React from "react";
import Loader from "react-loader";
import LinearProgress from "@material-ui/core/LinearProgress";

export function SlidingLoader() {
  return (
    <div
      style={{
        backgroundColor: "rgba(0,0,0,0.3)",
        position: "absolute",
        height: "100%",
        justifyContent: "center",
        alignItems: "center",
        width: "100%",
        zIndex: 9999,
      }}
    >
      <LinearProgress />
    </div>
  );
}

export function LoaderOne() {
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "100vh",
        width: "100%",
      }}
    >
      <Loader loaded={false} />
    </div>
  );
}

export function LoaderTwo() {
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "90vh",
        width: "100%",
      }}
    >
      <Loader loaded={false} />
    </div>
  );
}

export function MainLoader() {
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "90vh",
        width: "100%",
      }}
    >
      <Loader loaded={false} />
    </div>
  );
}

export function MainAreaLoader() {
  return (
    <div
      style={{
        backgroundColor: "rgba(0,0,0,0.3)",
        position: "absolute",
        height: "100%",
        justifyContent: "center",
        alignItems: "center",
        width: "100%",
        zIndex: 9999,
      }}
    >
      <LinearProgress />
    </div>
    // <div
    //   style={{
    //     display: "flex",
    //     justifyContent: "center",
    //     alignItems: "center",
    //     height: "90vh",
    //     width: "100%",
    //   }}
    // >
    //   <Loader loaded={false} />
    // </div>
  );
}
