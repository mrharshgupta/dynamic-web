import React, { useState } from "react";
import { Redirect, Link } from "react-router-dom";
import styles from "./styles";

import "../../css/style.css";
import "../../css/device.css";
import "../../css/editor.css";
import "../../css/main.css";
import "../../css/sanjana.css";
import "../../css/bootstrap.min.css";

const Popups = ({ showAdminOptions, showSearch, logout, showBlogPopup }) => {
  return (
    <div>
      <div className={showAdminOptions ? "admin-nav active" : "admin-nav"}>
        <ul>
          <li>
            <Link
              to={`/${localStorage.getItem("storeName")}/home/subscriptions`}
              style={styles.link}
            >
              <i class="fas fa-coins"></i> Billing Account
            </Link>
          </li>
          <li>
            <Link to="#" style={styles.link}>
              <i class="fas fa-comment-alt"></i> Open a Support Ticket
            </Link>
          </li>
          <li>
            <Link to="#" style={styles.link}>
              <i class="fa fa-archive"></i> Knowledgebase
            </Link>
          </li>
          <li>
            <Link to="#" style={styles.link}>
              <i class="fas fa-user-cog"></i> Profile
            </Link>
          </li>
          <li>
            <Link to="#" style={styles.link}>
              <i class="fas fa-puzzle-piece"></i> My Apps
            </Link>
          </li>
          <li onClick={() => logout()}>
            <Link to="#" style={styles.link}>
              <i class="fas fa-power-off"></i> Logout
            </Link>
          </li>
        </ul>
      </div>

      <div className={showSearch ? "search-data active" : "search-data"}>
        <form>
          <ul>
            <li>
              <select name="" id="">
                <option value="" disabled selected>
                  Order statuses
                </option>
                <option value="">Option 1</option>
                <option value="">Option 1</option>
                <option value="">Option 1</option>
              </select>
            </li>
            <li>
              <input type="text" placeholder="Customer" title="Customer" />
            </li>
            <li>
              <input type="text" placeholder="Product" title="Product" />
            </li>
            <li>
              <input type="text" placeholder="Country" title="Country" />
            </li>
            <li>
              <input type="text" placeholder="Zone" title="Zone" />
            </li>
            <li>
              <input
                type="text"
                placeholder="Order status"
                title="Order status"
              />
            </li>
            <li>
              <input type="text" placeholder="Date Added" title="Date Added" />
            </li>
            <li>
              <input type="text" placeholder="Total" title="Total" />
            </li>
            <li>
              <input type="submit" class="button" value="Search" />
              <input type="reset" class="button" value="reset" />
            </li>
          </ul>
        </form>
      </div>

      {/* Blog PopUp */}
      {/* <div className={(showBlogPopup)? ("blog-data active") : ("blog-data")}>
   <form action="">
      <div class="input-group flex-nowrap">
  <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="addon-wrapping" />
  <div class="input-group-prepend">
    <span class="input-group-text" id="addon-wrapping"><i class="fas fa-search"></i></span>
  </div>
</div>
</form>
     <h2 class="blog-title">Blog Category </h2>
      <ul>
         <li>
           <a href="blog.php">Post</a>
         </li>
          <li>
           <a href="blog-cat.php">Category</a>
         </li>
          <li>
           <a href="blog-comment.php">Comments</a>
         </li>
          <li>
           <a href="blog-setting.php">Setting</a>
         </li>
        
      </ul>
   
</div> */}
    </div>
  );
};

export default Popups;
