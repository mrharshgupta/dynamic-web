import React, { useState, useContext } from "react";
import { Redirect, Link } from "react-router-dom";
import styles from "./styles";
import Popups from "../popups/index";
import "../../css/style.css";
import "../../css/device.css";
import "../../css/editor.css";
import "../../css/main.css";
import "../../css/sanjana.css";
import "../../css/bootstrap.min.css";
import { ServerContext } from "../../contexts/contexts.js";
import { useDispatch, connect } from "react-redux";

const Header = (props) => {
  const dispatch = useDispatch();
  const [showAdminOptions, setshowAdminOptions] = useState(false);
  const [token, settoken] = useState(
    localStorage.getItem("token") || undefined
  );

  const servers = useContext(ServerContext);

  if (!token) {
    return <Redirect to="/" />;
  }

  const logout = () => {
    localStorage.clear();
    settoken(undefined);
    dispatch({ type: "LOGOUT" });
  };
  const appliedTemplate = localStorage.getItem("appliedTemplate");

  return (
    <>
      {" "}
      <div className="header">
        <ul className="nav-left">
          <li>
            <i
              className="hamburger"
              onClick={() => props.sethideSideBar(!props.hideSideBar)}
            >
              <span></span>
              <span></span>
              <span></span>
            </i>
          </li>
          <li>
            <Link to="/home" className="logo">
              <img
                src={require("../../constants/images/logo.png")}
                alt="logo"
              />
            </Link>
          </li>
        </ul>

        <ul className="nav-right">
          <li>
            {localStorage.getItem("appliedTemplate") !== "undefined" ? (
              <a
                href={`http://${servers}/onitt/stores/${localStorage.getItem(
                  "storeName"
                )}`}
                target="_blank"
                className="store-front"
                style={styles.link}
              >
                <i className="fas fa-store" /> View Website
              </a>
            ) : (
              <a
                href="#"
                onClick={() =>
                  alert(
                    "No store found, please select a template to create one."
                  )
                }
                className="store-front"
                style={styles.link}
              >
                <i className="fas fa-store" />
                Store Front
              </a>
            )}
          </li>
          <li>
            <a
              href="#"
              style={styles.link}
              className="admin"
              onClick={() => setshowAdminOptions(!showAdminOptions)}
            >
              <i className="fas fa-user-circle"></i> Admin{" "}
              <i className="fas fa-chevron-down"></i>
            </a>
          </li>
        </ul>
      </div>
      {/* <Popups showAdminOptions={showAdminOptions} logout={logout} /> */}
      <div className={showAdminOptions ? "admin-nav active" : "admin-nav"}>
        <ul>
          <li>
            <Link
              onClick={() => setshowAdminOptions(false)}
              to={`/${localStorage.getItem("storeName")}/home/subscriptions`}
              style={styles.link}
            >
              <i class="fas fa-coins"></i> Billing Account
            </Link>
          </li>
          <li>
            <Link to="#" style={styles.link}>
              <i class="fas fa-comment-alt"></i> Open a Support Ticket
            </Link>
          </li>
          <li>
            <Link to="#" style={styles.link}>
              <i class="fa fa-archive"></i> Knowledgebase
            </Link>
          </li>
          <li>
            <Link to="#" style={styles.link}>
              <i class="fas fa-user-cog"></i> Profile
            </Link>
          </li>
          <li>
            <Link to="#" style={styles.link}>
              <i class="fas fa-puzzle-piece"></i> My Apps
            </Link>
          </li>
          <li onClick={() => logout()}>
            <Link to="#" style={styles.link}>
              <i class="fas fa-power-off"></i> Logout
            </Link>
          </li>
        </ul>
      </div>
    </>
  );
};

const mapStateToProps = ({ appliedTemplate }) => ({
  appliedTemplate,
});
export default connect(mapStateToProps)(Header);
