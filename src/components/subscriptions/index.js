import React from "react";
import Box from "@material-ui/core/Box";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import FormLabel from "@material-ui/core/FormLabel";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import Paper from "@material-ui/core/Paper";
import Plan from "./plan/index";
import PlansData from "./plans-data";
import { connect } from "react-redux";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    justifyContent: "center",
    alignItems: "center",
    overflowY: "scroll",

    height: "100vh",
    paddingBottom: 200,
    paddingTop: 20,
  },
}));

const Subscriptions = ({ loginData }) => {
  const classes = useStyles();
  console.log(loginData, "ususususuus");
  return (
    <div class="wrap-right">
      <Box component="div" className={classes.root}>
        <center>
          <span style={{ fontSize: 30 }}>
            Pick a plan to use when your free trial ends
          </span>
          <br />
          <span
            style={{
              fontSize: 25,
              color: "#6e6e6e",
              textAlign: "center",
              flex: 1,
            }}
          >
            There is no risk—if Onitt isn’t right for you, cancel till 30 days
            and we won’t charge you.
          </span>
          <br />
        </center>
        <Grid container spacing={2} style={{ marginTop: 20 }}>
          <Grid item xs={12}>
            <Grid container justify="center" spacing={2}>
              {PlansData.map((value) => (
                <Plan value={value} userData={loginData} />
              ))}
            </Grid>
          </Grid>
        </Grid>
      </Box>
    </div>
  );
};

const mapStateToProps = ({ loginData }) => ({
  loginData,
});
export default connect(mapStateToProps)(Subscriptions);
