import { apiUrls } from "../../../api/helper.js";
import { consoleInDebugModeOnly2 } from "../../../helpers/debugging";
import axios from "axios";

const choosePlanFunction = async (dispatch, planChoosen) => {
  dispatch({ type: "SET_SLIDING_LOADER", data: true });
  consoleInDebugModeOnly2(planChoosen, "planChoosen");
  try {
    const { data = {} } = await axios.post(apiUrls.choosePlan, {
      userId: await localStorage.getItem("userId"),
      planChoosen,
    });
    consoleInDebugModeOnly2(data, "response-resendOtp");
    window.location.reload();
    dispatch({ type: "SET_SLIDING_LOADER", data: false });
  } catch (error) {
    consoleInDebugModeOnly2(error.response, "error-resendOtp");
    dispatch({ type: "SET_SLIDING_LOADER", data: false });
  }
};

export { choosePlanFunction };
