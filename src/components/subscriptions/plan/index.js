import React from "react";
import Box from "@material-ui/core/Box";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import colors from "../../../constants/colors";
import { choosePlanFunction } from "./apis";
import { useDispatch } from "react-redux";

const useStyles = makeStyles((theme) => ({
  paper: {
    height: 420,
    width: 250,
    paddingRight: 20,
    paddingLeft: 20,
    paddingTop: 20,
  },
  price: {
    fontSize: 35,
    fontWeight: "bold",
  },
  perMonth: {
    marginTop: 20,
  },
  chooseButton: {
    backgroundColor: colors.primary,
    marginTop: 20,
    marginBottom: 20,
  },
  planName: {
    fontSize: 22,
    fontWeight: "600",
  },
}));

const Plan = ({ value, userData }) => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const [elevation, setElevation] = React.useState(1);

  const increasePaperElevaton = () => {
    setElevation(5);
  };
  const decreasePaperElevaton = () => {
    setElevation(1);
  };
  const isPlanChoosen =
    parseInt(userData?.selectedPlan) === value.id ? true : false;

  const applyPlan = (choosenPlan) => {
    choosePlanFunction(dispatch, choosenPlan);
  };
  return (
    <Grid key={value} item>
      <Paper
        className={classes.paper}
        elevation={elevation}
        onMouseEnter={increasePaperElevaton}
        onMouseLeave={decreasePaperElevaton}
      >
        <center>
          <span className={classes.price}>{value.pricing.monthlyPrice}</span>
          <br />
          <span className={classes.perMonth}>per month</span>
          <br />
          <Button
            disabled={isPlanChoosen ? true : false}
            className={classes.chooseButton}
            variant="contained"
            disableElevation
            onClick={() => applyPlan(value.id)}
          >
            {isPlanChoosen ? "Already applied" : "Choose this plan"}
          </Button>
          <br />
          <span className={classes.planName}>{value.name}</span>
          <br />

          <p style={{}}>
            {value.features.map((item, index) => (
              <>
                <span>{item.name}, </span>
                {/* <br /> */}
              </>
            ))}
          </p>
        </center>
      </Paper>
    </Grid>
  );
};

export default Plan;
