const PlansData = [
  {
    id: 1,
    name: "Plan 1",
    features: [
      {
        name: "Logo",
      },
      {
        name: "Tagline",
      },
      {
        name: "Fixed/Slider Banner",
      },
      {
        name: "About Us",
      },
      {
        name: "Services (upto 4) - Service Pic + Description",
      },
      {
        name: "Contact Details",
      },
      {
        name: "Social Media Links",
      },
      {
        name: "Team (upto 4 team members",
      },
      {
        name: "Contact Us Form",
      },
      {
        name: "Photo Gallery",
      },
      {
        name: "8 Templates",
      },
    ],
    pricing: {
      trial: "Always Free",
      monthlyPrice: "Free",
    },
  },
  {
    id: 2,
    name: "Plan 2",
    features: [
      {
        name: "Logo",
      },
      {
        name: "Tagline",
      },
      {
        name: "Fixed/Slider Banner",
      },
      {
        name: "About Us",
      },
      {
        name: "Services (upto 4) - Service Pic + Description",
      },
      {
        name: "Contact Details",
      },
      {
        name: "Social Media Links",
      },
      {
        name: "Team (upto 4 team members",
      },
      {
        name: "Contact Us Form",
      },
      {
        name: "Custom URL",
      },
      {
        name: "Google Map",
      },
      {
        name: "Testimonials",
      },
      {
        name: "Photo Gallery",
      },
      {
        name: "8 Templates",
      },
    ],
    pricing: {
      monthlyPrice: "$9.99",
      trial: "30-Day Free Trial",
    },
  },
];

export default PlansData;
