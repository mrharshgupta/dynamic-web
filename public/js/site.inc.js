// document.onreadystatechange = function() {
//     if (document.readyState !== "complete") {
//         document.querySelector("body").style.visibility = "hidden";
//         document.querySelector(".preloader").style.visibility = "visible";
//     } else {
//         document.querySelector(".preloader").style.display = "none";
//         document.querySelector("body").style.visibility = "visible";
//     }
// };




















































































































































\\$(".header .hamburger").click(function() {
    $("div.wrapper").toggleClass("active");
    return false;
});

$(document).ready(function() {
    $(".admin").click(function() {
        $(".admin-nav").toggleClass("active");
        return false;
    });

    $(document).on("click", function(event) {
        var $trigger = $("div.admin-nav");
        if ($trigger !== event.target && !$trigger.has(event.target).length) {
            $(".admin-nav").removeClass("active");
        }
    });

    $(".search").click(function() {
        $(".search-data").toggleClass("active");
        return false;
    });

    $(document).on("click", function(event) {
        var $trigger = $("div.search-data");
        if ($trigger !== event.target && !$trigger.has(event.target).length) {
            $(".search-data").removeClass("active");
        }
    });

});

$(document).ready(function() {
    $("a.down-arrow").click(function() {
        var link = $(this);
        var closest_ul = link.closest("ul");
        var parallel_active_links = closest_ul.find(".active")
        var closest_li = link.closest("li");
        var link_status = closest_li.hasClass("active");
        var count = 0;

        closest_ul.find("ul").slideUp(function() {
            if (++count == closest_ul.find("ul").length)
                parallel_active_links.removeClass("active");
        });

        if (!link_status) {
            closest_li.children("ul").slideDown();
            closest_li.addClass("active");
        }
    })
})



//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

$(".next").click(function() {
    if (animating) return false;
    animating = true;

    current_fs = $(this).parent();
    next_fs = $(this).parent().next();

    //activate next step on progressbar using the index of next_fs
    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

    //show the next fieldset
    next_fs.show();
    //hide the current fieldset with style
    current_fs.animate({ opacity: 0 }, {
        step: function(now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale current_fs down to 80%
            scale = 1 - (1 - now) * 0.2;
            //2. bring next_fs from the right(50%)
            left = (now * 50) + "%";
            //3. increase opacity of next_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({
                'transform': 'scale(' + scale + ')',
                'position': 'absolute'
            });
            next_fs.css({ 'left': left, 'opacity': opacity });
        },
        duration: 800,
        complete: function() {
            current_fs.hide();
            animating = false;
        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
});

$(".previous").click(function() {
    if (animating) return false;
    animating = true;

    current_fs = $(this).parent();
    previous_fs = $(this).parent().prev();

    //de-activate current step on progressbar
    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

    //show the previous fieldset
    previous_fs.show();
    //hide the current fieldset with style
    current_fs.animate({ opacity: 0 }, {
        step: function(now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale previous_fs from 80% to 100%
            scale = 0.8 + (1 - now) * 0.2;
            //2. take current_fs to the right(50%) - from 0%
            left = ((1 - now) * 50) + "%";
            //3. increase opacity of previous_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({ 'left': left });
            previous_fs.css({ 'transform': 'scale(' + scale + ')', 'opacity': opacity });
        },
        duration: 800,
        complete: function() {
            current_fs.hide();
            animating = false;
        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
});

$(".submit").click(function() {
    return false;
})


$(document).ready(function() {

    $(".advsearch").click(function() {
        $(".advsearch-data").toggleClass("active");
        return false;
    });

    $(".blog-search").click(function() {
        $(".blog-data").toggleClass("active");
        return false;
    });

    $(document).ready(function() {
        $("#txtEditor").Editor();
    });
    //---------------- 24 may added -------------------
      $(".pro-avail").click(function() {
        $(".pro-avail-data").toggleClass("active");
        return false;
    });
       //---------------- 24 may added -------------------
});

$(document).ready(function() {
    $('form input').change(function() {
        $('form p').text(this.files.length + " file(s) selected");
    });
});